
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

Sx = createSx(parameter) ;
Sy = createSy(parameter) ;
Sz = createSz(parameter) ;

Q1 = Sx * Sx - Sy * Sy ;
Q2 = (2 * Sz * Sz - Sx * Sx - Sy * Sy) / sqrt(3) ;
Q3 = Sx * Sy + Sy * Sx ;
Q4 = Sy * Sz + Sz * Sy ;
Q5 = Sx * Sz + Sz * Sx ;