function [expectValueBond, expectValueSite, T] = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, A)
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4) T3(a3,a5,a2,a6)

%* transpose to left transferMatrix
%* T1(a6,a4,a1,a3) -> T1(a1,a3,a6,a4)
%* T2(a1,a2,a5,a4) -> T2(a5,a4,a1,a2)
%* T3(a3,a5,a2,a6) -> T3(a2,a6,a3,a5)
T = transposeTransferMatrix1D(T) ;

leftMPS = iTEBD_computeSingleGate1D_3(parameter, T{3}, leftMPS) ;

%* transpose to right transferMatrix
%* T1(a1,a3,a6,a4) -> T1(a6,a4,a1,a3)
%* T2(a5,a4,a1,a2) -> T2(a1,a2,a5,a4)
%* T3(a2,a6,a3,a5) -> T3(a3,a5,a2,a6)
T = transposeTransferMatrix1D(T) ;

%==================================================================================================================
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
[PR12, PR34, PL12, PL34] = createFinalChain(T, rightMPS, leftMPS) ;

%* vectorR(b4,b2') vectorL(b4,b2')
[vectorR, vectorL] = computeDominantEigenVector(parameter, T{3}, PR12, PR34, PL12, PL34) ;

%===================================================================================================================
computation = parameter.computation ;

expectValueSite = struct ;
if computation.S == 1
    %* site 1: 
    operator = createSxI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Sx = Sx ;
    
    operator = createSyI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Sy = Sy ;
    
    operator = createSzI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Sz = Sz ;
    
    %* site 2:
    operator = createISx(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Sx = Sx ;
    
    operator = createISy(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Sy = Sy ;
    
    operator = createISz(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Sz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Sz = Sz ;
    
    %* site 3:
    operator = createISx(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Sx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Sx = Sx ;
    
    operator = createISy(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Sy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Sy = Sy ;
    
    operator = createISz(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Sz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Sz = Sz ;
end
%==================================================================================================================
if computation.Q == 1
    %* site 1: 
    operator = createQxxI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qxx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qxx = Qxx ;
    
    operator = createQyyI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qyy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qyy = Qyy ;
    
    operator = createQzzI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qzz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qzz = Qzz ;
    
    operator = createQxyI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qxy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qxy = Qxy ;
    
    operator = createQyzI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qyz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qyz = Qyz ;
    
    operator = createQzxI(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qzx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(1).Qzx = Qzx ;
    %---------------
    %* site 2:
    operator = createIQxx(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qxx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qxx = Qxx ;
    
    operator = createIQyy(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qyy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qyy = Qyy ;
    
    operator = createIQzz(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qzz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qzz = Qzz ;
    
    operator = createIQxy(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qxy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qxy = Qxy ;
    
    operator = createIQyz(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qyz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qyz = Qyz ;
    
    operator = createIQzx(parameter) ;
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    Qzx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueSite(2).Qzx = Qzx ;
    %-----------------
    %* site 3:
    operator = createIQxx(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qxx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qxx = Qxx ;
    
    operator = createIQyy(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qyy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qyy = Qyy ;
    
    operator = createIQzz(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qzz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qzz = Qzz ;
    
    operator = createIQxy(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qxy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qxy = Qxy ;
    
    operator = createIQyz(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qyz = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qyz = Qyz ;
    
    operator = createIQzx(parameter) ;
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    Qzx = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueSite(3).Qzx = Qzx ;
end
%*******************************************************************************************************************
expectValueBond = struct ;
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueBond(1).energy = energy ;
       
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueBond(2).energy = energy ;
    
    [QR12, TH3] = createSpecialTensor_3(parameter, operator, rightMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, QR12, PR34, PL12, PL34) ;
    clear QR12 TH3
    expectValueBond(3).energy = energy ;
    
    [QR34, QL12] = createSpecialTensor_4(parameter, operator, rightMPS, leftMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, PR12, QR34, QL12, PL34) ;
    clear QR34 QL12
    expectValueBond(4).energy = energy ;
    
    [QR34, TH3] = createSpecialTensor_5(parameter, operator, rightMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, QR34, PL12, PL34) ;
    clear QR34 TH3
    expectValueBond(5).energy = energy ;
    
    [QL12, TH3] = createSpecialTensor_6(parameter, operator, leftMPS, A) ;
    energy = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, QL12, PL34) ;
    clear QL12 TH3
    expectValueBond(6).energy = energy ;
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    
    [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, QR12, PR34, PL12, QL34) ;
    clear QR12 QL34
    expectValueBond(1).nearSpinCorrelation = nearSpinCorrelation ;
       
    [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, PL12, QL34) ;
    clear QL34 TH3
    expectValueBond(2).nearSpinCorrelation = nearSpinCorrelation ;
    
    [QR12, TH3] = createSpecialTensor_3(parameter, operator, rightMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, QR12, PR34, PL12, PL34) ;
    clear QR12 TH3
    expectValueBond(3).nearSpinCorrelation = nearSpinCorrelation ;
    
    [QR34, QL12] = createSpecialTensor_4(parameter, operator, rightMPS, leftMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, T{3}, PR12, QR34, QL12, PL34) ;
    clear QR34 QL12
    expectValueBond(4).nearSpinCorrelation = nearSpinCorrelation ;
    
    [QR34, TH3] = createSpecialTensor_5(parameter, operator, rightMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, QR34, PL12, PL34) ;
    clear QR34 TH3
    expectValueBond(5).nearSpinCorrelation = nearSpinCorrelation ;
    
    [QL12, TH3] = createSpecialTensor_6(parameter, operator, leftMPS, A) ;
    nearSpinCorrelation = findExpectationValue(T{3}, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, PR12, PR34, QL12, PL34) ;
    clear QL12 TH3
    expectValueBond(6).nearSpinCorrelation = nearSpinCorrelation ;
end