function [mag, agl] = computeQuadAngle(q)
%* compute angle between 2 adjacent quadrupole

%* A is quadrupole matrix
A = zeros(3, 3) ;
a = zeros(1, 3) ;

direction = zeros(3, 3) ;

for i = 1 : 3
    A(1, 1) = q(i, 1) ; % Qxx
    A(2, 2) = q(i, 2) ; % Qyy
    A(3, 3) = q(i, 3) ; % Qzz
    
    A(1, 2) = q(i, 4) ; % Qxy
    A(2, 1) = q(i, 4) ; % Qxy
    
    A(2, 3) = q(i, 5) ; % Qyz
    A(3, 2) = q(i, 5) ; % Qyz
    
    A(1, 3) = q(i, 6) ; % Qzx
    A(3, 1) = q(i, 6) ; % Qzx
    
    [U, D] = eig(A) ;
    
    D = diag(D) 
    [~, ind] = max(abs(D)) ;
    a(i) = D(ind) ;
    direction(:, i) = U(:, ind) ;
    
end

b = zeros(1, 3) ;

for i = 1 : 3
    if i < 3 
        j = i + 1 ;
    else
        j = 1 ;
    end
    
    n1 = norm(direction(:, i)) ;
    n2 = norm(direction(:, j)) ;
    phi = sum(direction(:, i) .* direction(:, j)) / (n1 * n2) ;
    
    b(i) = real(acos(phi)) ;
end
b = b ./ pi 
agl = mean(b) ;
mag = mean(a) ;