function A = Atimes4Lambda(A, Lambda)

%* A = A(i1, i2, i3, i4, m) * 
%* Lambda(1).tensor1(i1)*Lambda(2).tensor1(i2)*Lambda(3).tensor1(i3)*Lambda(4).tensor1(i4)

for i1 = 1 : Lambda(1).dim
    A.tensor4(i1, :, :, :, :) = A.tensor4(i1, :, :, :, :) .* Lambda(1).tensor1(i1) ;
end

for i2 = 1 : Lambda(2).dim
    A.tensor4(:, i2, :, :, :) = A.tensor4(:, i2, :, :, :) .* Lambda(2).tensor1(i2) ;
end

for i3 = 1 : Lambda(3).dim
    A.tensor4(:, :, i3, :, :) = A.tensor4(:, :, i3, :, :) .* Lambda(3).tensor1(i3) ;
end

for i4 = 1 : Lambda(4).dim
    A.tensor4(:, :, :, i4, :) = A.tensor4(:, :, :, i4, :) .* Lambda(4).tensor1(i4) ;
end