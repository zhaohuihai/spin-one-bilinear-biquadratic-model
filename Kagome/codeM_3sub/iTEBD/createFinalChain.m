function [PR12, PR34, PL12, PL34] = createFinalChain(T, rightMPS, leftMPS)
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4) T3(a3,a5,a2,a6)

%* BBR12(b4,a6,a4,b2) = sum{b1}_[rightMPS.B1(b4,a6,b1)*rightMPS.B2(b1,a4,b2)]
BBR12 = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;
 
%* BBR34(b2,a1,a2,b4) = sum{b3}_[rightMPS.B3(b2,a1,b3)*rightMPS.B4(b3,a2,b4)]
BBR34 = contractTensors(rightMPS.B{3}, 3, 3, rightMPS.B{4}, 3, 1) ;

%* BBL12(b4',a1,a3,b2') = sum{b1'}_[leftMPS.B1(b4',a1,b1') * leftMPS.B2(b1',a3,b2')]
BBL12 = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;

%* BBL34(b2',a5,a4,b4') = sum{b3'}_[leftMPS.B3(b2',a5,b3') * leftMPS.B4(b3',a4,b4')]
BBL34 = contractTensors(leftMPS.B{3}, 3, 3, leftMPS.B{4}, 3, 1) ;
%---------------------------------------------------------------------------------
%* PR12(b4,b2,a1,a3) = sum{a6,a4}_(BBR12(b4,a6,a4,b2)*T1(a6,a4,a1,a3))
PR12 = contractTensors(BBR12, 4, [2, 3], T{1}, 4, [1, 2]) ;

%* PR34(b2,b4,a5,a4) = sum{a1,a2}_(BBR34(b2,a1,a2,b4)*T2(a1,a2,a5,a4))
PR34 = contractTensors(BBR34, 4, [2, 3], T{2}, 4, [1, 2]) ;

%* PL12(b4',b2',a6,a4) = sum{a1,a3}_(BBL12(b4',a1,a3,b2')*T1(a6,a4,a1,a3))
PL12 = contractTensors(conj(BBL12), 4, [2, 3], T{1}, 4, [3, 4]) ;

%* PL34(b2',b4',a1,a2) = sum{a5,a4}_(BBL34(b2',a5,a4,b4')*T2(a1,a2,a5,a4))
PL34 = contractTensors(conj(BBL34), 4, [2, 3], T{2}, 4, [3, 4]) ;

