function P = contractAA_T(AA, T)
%* P(a,xf,c,zf) = sum{zb,xb}_(AA((a,c),(zb,xb))*T(zb,xb,xf,zf))

%* T(zb,xb,xf,zf)
Ttensor = T.tensor4 ;
%* T((zb,xb),(xf,zf))
Ttensor = reshape(Ttensor, [T.dim(1) * T.dim(2), T.dim(3) * T.dim(4)]) ;
%* P((a,c),(xf,zf)) = sum{zb,xb}_(AA((a,c),(zb,xb))*T((zb,xb),(xf,zf)))
Ptensor = AA.tensor2 * Ttensor ;

%* P(a,c,xf,zf)
Ptensor = reshape(Ptensor, [AA.dim(1), AA.dim(2), T.dim(3), T.dim(4)]) ;

%* P(a,xf,c,zf)
Ptensor = permute(Ptensor, [1, 3, 2, 4]) ;

P.dim = [AA.dim(1), T.dim(3), AA.dim(2), T.dim(4)] ;
P.tensor4 = Ptensor ;
