function [QL12, TH3] = createSpecialTensor_6(parameter, operator, leftMPS, A)
%* A3(a3,a5,a2,a6,m3) A1(a6,a4,a1,a3,m1)

%* H((m3, m1),(m3', m1')) = sum{n}_(U((m3,m3'),n)*S(n)*conj(V((m1,m1'),n)))
%* U((m3,m3'),n) = U((m3,m3'),n)*sqrt(S(n))
%* V((m1,m1'),n) = conj(V((m1,m1'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;
%----------------------------------------------------------------------------------------------------------------------------

%* TH3((a3,a3'),(a5,a5'),(a2,a2'),(a6,a6',n)) = sum{m3,m3'}_[conj(A3(a3,a5,a2,a6,m3))*A3(a3',a5',a2',a6',m3')*U((m3,m3'),n)]
%* change notation: TH3(a3,a5,a2,a6)
TH3 = createTH(parameter, A{3}, U, 4) ;

%* TH1((a6,a6',n),(a4,a4'),(a1,a1'),(a3,a3')) = sum{m1,m1'}_[conj(A1(a6,a4,a1,a3,m1))*A1(a6',a4',a1',a3',m1')*V((m1,m1'),n)]
%* change notation: TH1(a6,a4,a1,a3)
TH1 = createTH(parameter, A{1}, V, 1) ;

%* BBL12(b4',a1,a3,b2') = sum{b1'}_[leftMPS.B1(b4',a1,b1') * leftMPS.B2(b1',a3,b2')]
BBL12 = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;

%* QL12(b4',b2',a6,a4) = sum{a1,a3}_(BBL12(b4',a1,a3,b2')*TH1(a6,a4,a1,a3))
QL12 = contractTensors(conj(BBL12), 4, [2, 3], TH1, 4, [3, 4]) ;

clear TH1 ;
clear BBL12 ;

