function [MPS, coef, truncationError_TM] = applyTMP_left(parameter, transferMatrix, MPS)

Ta = transferMatrix.Ta ;
Tb = transferMatrix.Tb ;
Tc = transferMatrix.Tc ;
A1 = MPS.A1 ;
A2 = MPS.A2 ;
B1 = MPS.B1 ;
B2 = MPS.B2 ;
Lambda = MPS.Lambda ;
c = zeros(1, 6) ;
%* act Tc on L3*B2*L4*A1*L1
[B2, A1, Lambda(4), c(1)] = applyGateOperation(parameter, Tc, B2, A1, Lambda([3, 4, 1])) ;

%* act Tb on L4*A1*L1*A2*L2
[A1, A2, Lambda(1), c(2), truncationError_TM] = applyGateOperation(parameter, Tb, A1, A2, Lambda([4, 1, 2])) ;

%* act Ta on L2*B1*L3*B2*L4
[B1, B2, Lambda(3), c(3)] = applyGateOperation(parameter, Ta, B1, B2, Lambda([2, 3, 4])) ;

%************************************************************************************
%* act Tc on L1*A2*L2*B1*L3
[A2, B1, Lambda(2), c(4)] = applyGateOperation(parameter, Tc, A2, B1, Lambda([1, 2, 3])) ;

%* act Ta on L4*A1*L1*A2*L2
[A1, A2, Lambda(1), c(5)] = applyGateOperation(parameter, Ta, A1, A2, Lambda([4, 1, 2])) ;

%* act Tb on L2*B1*L3*B2*L4
[B1, B2, Lambda(3), c(6)] = applyGateOperation(parameter, Tb, B1, B2, Lambda([2, 3, 4])) ;

coef = 1 ;
for i = 1 : 6
    coef = coef * c(i) ;
end
% 
% for i = 1 : 4
%     Lambda(i).tensor1 = Lambda(i).tensor1 ./ a ;
% end

MPS.Lambda = Lambda ;
MPS.A1 = A1 ;
MPS.A2 = A2 ;
MPS.B1 = B1 ;
MPS.B2 = B2 ;