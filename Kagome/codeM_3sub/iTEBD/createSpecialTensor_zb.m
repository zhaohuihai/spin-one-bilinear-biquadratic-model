function [QBL, THc] = createSpecialTensor_zb(parameter, rightMPS, leftMPS, TNS)
%* QBL(c',zb,e',xb) THc(zf,yf,yb,zb)

%* U((mi,mi'),n) V((mj,mj'),n)
[U, V] = decomposeHamiltonian(parameter) ;

%* THc(yf,zf,yb,zb)
THc = createTH(parameter, TNS.C, U, 4) ;
%* THc(zf,yf,yb,zb)
THc.dim = THc.dim([2, 1, 3, 4]) ;
THc.tensor4 = permute(THc.tensor4, [2, 1, 3, 4]) ;

%* THa(zf,xf,zb,xb)
THa = createTH(parameter, TNS.A, V, 3) ;
%* THa(xf,zf,zb,xb)
THa.dim = THa.dim([2, 1, 3, 4]) ;
THa.tensor4 = permute(THa.tensor4, [2, 1, 3, 4]) ;
%* B1L(c',d',xf)
B1L = leftMPS.B1 ;
%* B2L(d',e',zf)
B2L = leftMPS.B2 ;
Lambda = leftMPS.Lambda(3 : 4) ;
%* BBL((c',e'),(xf,zf))
BBL = computeA1_L1_A2_L2(B1L, B2L, Lambda) ;
%* QBL(c',zb,e',xb) = sum{xf,zf}_(BBL((c',e'),(xf,zf))*THa(xf,zf,zb,xb))
QBL = contractAA_T(BBL, THa) ;
