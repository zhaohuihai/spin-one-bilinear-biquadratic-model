function T = createTransferMatrix1D(A)
%* right transfer matrix
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4) T3(a3,a5,a2,a6)

T = cell(1, 3) ;

%* T1(a3,a1,a6,a4)
T{1} = computeTensorProductA_A(A{1}) ;
%* T1(a3,a1,a6,a4) -> T1(a6,a4,a1,a3)
T{1} = permute(T{1}, [3, 4, 2, 1]) ;

%* T2(a1,a2,a4,a5)
T{2} = computeTensorProductA_A(A{2}) ;
%* T2(a1,a2,a4,a5) -> T2(a1,a2,a5,a4)
T{2} = permute(T{2}, [1, 2, 4, 3]) ;

%* T3(a2,a3,a5,a6)
T{3} = computeTensorProductA_A(A{3}) ;
%* T3(a2,a3,a5,a6) -> T3(a3,a5,a2,a6)
T{3} = permute(T{3}, [2, 3, 1, 4]) ;

