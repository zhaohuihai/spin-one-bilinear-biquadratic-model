function MPS = initializeMPS(parameter)
%* MPS: A1(a,b,zb) A2(b,c,xb) B1(c,d,xf) B2(d,e,yf) 
%* Lambda(1) Lambda(2) Lambda(3) Lambda(4)

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

A.dim = [D, D, d] ;
A.tensor3 = rand(D, D, d) ;

MPS.A1 = A ; %* A1(a,b,zb)
MPS.A2 = A ; %* A2(b,c,xb)
MPS.B1 = A ; %* B1(c,d,xf)
MPS.B2 = A ; %* B2(d,e,yf)

tensor1 = sort(rand(D, 1), 'descend') ;
for i = 1 : 4
    Lambda(i).dim = D ;
    Lambda(i).tensor1 = tensor1 ;
end
MPS.Lambda = Lambda ;