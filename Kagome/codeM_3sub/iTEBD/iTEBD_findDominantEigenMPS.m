function [rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T)
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4) T3(a3,a5,a2,a6)

% project from bottom to top
[rightMPS, T] = iTEBD_computeDominantRightEigenMPS(parameter, T) ;

%* transpose to left transferMatrix
%* T1(a6,a4,a1,a3) -> T1(a1,a3,a6,a4)
%* T2(a1,a2,a5,a4) -> T2(a5,a4,a1,a2)
%* T3(a3,a5,a2,a6) -> T3(a2,a6,a3,a5)
T = transposeTransferMatrix1D(T) ;

[leftMPS, T] = iTEBD_computeDominantLeftEigenMPS(parameter, T) ;

T = transposeTransferMatrix1D(T) ;