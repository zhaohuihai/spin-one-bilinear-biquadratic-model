function [A, B, S, coef, truncationError_TM] = applyGateOperation(parameter, T, A, B, Lambda)

%* AB((a,c),(i,j))
AB = computeL1_A_L2_B_L3(A, B, Lambda) ;

%* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
R = projectTransferMatrix(T, AB) ;

[U, S, V, coef] = svd_TMP(R) ;
[U, S, V, truncationError_TM] = truncate_TMP(parameter, U, S, V) ;

[A, B] = recover_TMP(U, S, V, Lambda) ;