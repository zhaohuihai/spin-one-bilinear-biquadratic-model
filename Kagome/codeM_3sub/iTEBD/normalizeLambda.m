function MPS = normalizeLambda(MPS)

for i = 1 : 4
    MPS.S{i} = MPS.S{i} ./ MPS.S{i}(1) ;
end