function [QAL, THc] = createSpecialTensor_yf(parameter, rightMPS, leftMPS, TNS)
%* QAL(a',xf,c',yf) THc(zf,yf,yb,zb)
 
%* U((mi,mi'),n) V((mj,mj'),n)
[U, V] = decomposeHamiltonian(parameter) ;

%* THc(yf,zf,yb,zb)
THc = createTH(parameter, TNS.C, U, 1) ;
%* THc(zf,yf,yb,zb)
THc.dim = THc.dim([2, 1, 3, 4]) ;
THc.tensor4 = permute(THc.tensor4, [2, 1, 3, 4]) ;

%* THb(xf,yf,xb,yb)
THb = createTH(parameter, TNS.B, V, 2) ;
%* THb(yb,xb,xf,yf)
THb.dim = THb.dim([4, 3, 1, 2]) ;
THb.tensor4 = permute(THb.tensor4, [4, 3, 1, 2]) ;
%* A1L(a',b',yb)
A1L = leftMPS.A1 ;
%* A2L(b',c',xb)
A2L = leftMPS.A2 ;
Lambda = leftMPS.Lambda(1 : 2) ;
%* AAL((a',c'),(yb,xb))
AAL = computeA1_L1_A2_L2(A1L, A2L, Lambda) ;
%* QAL(a',xf,c',yf) = sum{yb,xb}_(AAL((a',c'),(yb,xb))*THb(yb,xb,xf,yf))
QAL = contractAA_T(AAL, THb) ;