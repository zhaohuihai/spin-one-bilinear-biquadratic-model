function [vectorR, vectorL] = computeDominantEigenVector(parameter, T3, PR12, PR34, PL12, PL34)
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
%* T3(a3,a5,a2,a6)
%* vectorR(b4,b2') vectorL(b4,b2')


vectorR = applyPowerMethod(parameter, T3, PR12, PR34, PL12, PL34) ;

%* PR12(b4,b2,a1,a3) -> PR12(b2,b4,a3,a1)
PR12 = permute(conj(PR12), [2, 1, 4, 3]) ;
%* PR34(b2,b4,a5,a4) -> PR34(b4,b2,a4,a5)
PR34 = permute(conj(PR34), [2, 1, 4, 3]) ;

%* PL12(b4',b2',a6,a4) -> PL12(b2',b4',a4,a6)
PL12 = permute(conj(PL12), [2, 1, 4, 3]) ;
%* PL34(b2',b4',a1,a2) -> PL34(b4',b2',a2,a1)
PL34 = permute(conj(PL34), [2, 1, 4, 3]) ;

%* T3(a3,a5,a2,a6) -> T3(a5,a3,a6,a2)
T3 = permute(conj(T3), [2, 1, 4, 3]) ;

vectorL = applyPowerMethod(parameter, T3, PR34, PR12, PL34, PL12) ;