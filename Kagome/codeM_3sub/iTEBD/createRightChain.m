function [PAR, PBR] = createRightChain(Ta, Tb, MPS)

A1 = MPS.A1 ;
A2 = MPS.A2 ;
B1 = MPS.B1 ;
B2 = MPS.B2 ;
Lambda = MPS.Lambda ;

%* AA((a,c),(zb,xb))
AA = computeA1_L1_A2_L2(A1, A2, Lambda(1 : 2)) ;
%* PAR(a,xf,c,zf) = sum{zb,xb}_(AA((a,c),(zb,xb))*Ta(zb,xb,xf,zf))
PAR = contractAA_T(AA, Ta) ;

%* BB((c,e),(xf,yf))
BB = computeA1_L1_A2_L2(B1, B2, Lambda(3 : 4)) ;
%* PBR(c,yb,e,xb) = sum{xf,yf}_(BB((c,e),(xf,yf))*Tb(xf,yf,yb,xb))
PBR = contractAA_T(BB, Tb) ;
