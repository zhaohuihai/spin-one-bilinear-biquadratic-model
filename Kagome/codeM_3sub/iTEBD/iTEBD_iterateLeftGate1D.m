function [MPS, T, truncationError, iTEBDstep] = iTEBD_iterateLeftGate1D(parameter, T, MPS, iTEBDstep)

converge = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
err = zeros(1, 4) ;
while converge >= parameter.tol_iTEBD && j <= parameter.maxITEBDstep
    iTEBDstep = iTEBDstep + 1 ;
    j = j + 1 ;
    if parameter.display == 1
        disp(['dim MPS = ', num2str(D), ', iTEBD step = ', num2str(iTEBDstep)]) ;
    end
    tic
    [MPS, T, truncationError] = iTEBD_applyLeftGateOperation1D(parameter, T, MPS) ;
    cpu_time = toc ;
    if parameter.display == 1
        disp(['single step iTEBD time: ', num2str(cpu_time), ' seconds']) ;
    end
    
    if j > 1
        for i = 1 : 4
            err(i) = norm(MPS.S{i} - S0{i}) / sqrt(D - 1) ;
        end
        converge = max(err) ;
    end
    flag = iTEBDstep / 1 ;
    if flag == floor(flag) && parameter.display == 1
        disp(['iTEBD convergence error = ', num2str(converge)]) ;
        disp(['truncation error = ', num2str(truncationError)]) ;
    end
    S0 = MPS.S ;
end