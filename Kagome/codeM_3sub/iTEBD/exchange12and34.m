function MPS = exchange12and34(MPS)

%* MPS.B(1,2,3,4) -> MPS.B(3,4,1,2)
MPS.B = MPS.B([3, 4, 1, 2]) ;

%* MPS.S(1,2,3,4) -> MPS.S(3,4,1,2)
MPS.S = MPS.S([3, 4, 1, 2]) ;