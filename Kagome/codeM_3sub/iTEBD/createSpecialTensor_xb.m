function [QBR, QBL] = createSpecialTensor_xb(parameter, rightMPS, leftMPS, TNS)
%* QBR(c,yb,e,xb) QBL(c',zb,e',xb)

%* U((mi,mi'),n) V((mj,mj'),n)
[U, V] = decomposeHamiltonian(parameter) ;

%* THa(zf,xf,zb,xb)
THa = createTH(parameter, TNS.A, U, 4) ;
%* THa(xf,zf,zb,xb)
THa.dim = THa.dim([2, 1, 3, 4]) ;
THa.tensor4 = permute(THa.tensor4, [2, 1, 3, 4]) ;
%* B1L(c',d',xf)
B1L = leftMPS.B1 ;
%* B2L(d',e',zf)
B2L = leftMPS.B2 ;
Lambda = leftMPS.Lambda(3 : 4) ;
%* BBL((c',e'),(xf,zf))
BBL = computeA1_L1_A2_L2(B1L, B2L, Lambda) ;
%* QBL(c',zb,e',xb) = sum{xf,zf}_(BBL((c',e'),(xf,zf))*THa(xf,zf,zb,xb))
QBL = contractAA_T(BBL, THa) ;

%* THb(xf,yf,xb,yb)
THb = createTH(parameter, TNS.B, V, 3) ;
%* THb(xf,yf,yb,xb)
THb.dim = THb.dim([1, 2, 4, 3]) ;
THb.tensor4 = permute(THb.tensor4, [1, 2, 4, 3]) ;
%* B1R(c,d,xf)
B1R = rightMPS.B1 ;
%* B2R(d,e,yf)
B2R = rightMPS.B2 ;
Lambda = rightMPS.Lambda(3 : 4) ;
%* BBR((c,e),(xf,yf))
BBR = computeA1_L1_A2_L2(B1R, B2R, Lambda) ;
%* QBR(c,yb,e,xb) = sum{xf,yf}_(BBR((c,e),(xf,yf))*THb(xf,yf,yb,xb))
QBR = contractAA_T(BBR, THb) ;