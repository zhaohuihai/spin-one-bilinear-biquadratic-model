function [A, B] = recover_TMP(U, S, V, Lambda)


%* U((a,k),b)
Utensor = U.tensor2 ;

%* U(a,k,b)
Utensor = reshape(Utensor, U.dim) ;

%* A(a,b,k)
Atensor = permute(Utensor, [1, 3, 2]) ;
%*****************************************************************
%* A(a,b,k) = A(a,b,k) / L1(a)
for a = 1 : Lambda(1).dim
    Atensor(a, :, :) = Atensor(a, :, :) ./ Lambda(1).tensor1(a) ;
end
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% %* A(a,b,k) = A(a,b,k) * sqrt(S(b))
% for b = 1 : S.dim
%     Atensor(:, b, :) = Atensor(:, b, :) .* sqrt(S.tensor1(b)) ;
% end
%*****************************************************************
A.dim = U.dim([1, 3, 2]) ;
A.tensor3 = Atensor ;

%* V((c,l),b)
Vtensor = V.tensor2 ;

%* V(c,l,b)
Vtensor = reshape(Vtensor, V.dim) ;

%* B(b,c,l)
Btensor = permute(Vtensor, [3, 1, 2]) ;
%*****************************************************************
%* B(b,c,l) = B(b,c,l) / L3(c)
for c = 1 : Lambda(3).dim
    Btensor(:, c, :) = Btensor(:, c, :) ./ Lambda(3).tensor1(c) ;
end
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% %* B(b,c,l) = B(b,c,l) * sqrt(S(b))
% for b = 1 : S.dim
%     Btensor(b, :, :) = Btensor(b, :, :) .* sqrt(S.tensor1(b)) ;
% end
%******************************************************************
B.dim = V.dim([3, 1, 2]) ;
B.tensor3 = Btensor ;