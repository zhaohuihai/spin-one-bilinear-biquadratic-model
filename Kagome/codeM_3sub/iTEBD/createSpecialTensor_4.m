function [QR34, QL12] = createSpecialTensor_4(parameter, operator, rightMPS, leftMPS, A)
%* A1(a6,a4,a1,a3,m1) A2(a1,a2,a5,a4,m2)

%* H((m1, m2),(m1', m2')) = sum{n}_(U((m1,m1'),n)*S(n)*conj(V((m2,m2'),n)))
%* U((m1,m1'),n) = U((m1,m1'),n)*sqrt(S(n))
%* V((m2,m2'),n) = conj(V((m2,m2'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;

%* TH1((a6,a6'),(a4,a4',n),(a1,a1'),(a3,a3')) = sum{m1,m1'}_[conj(A1(a6,a4,a1,a3,m1))*A1(a6',a4',a1',a3',m1')*U((m1,m1'),n)]
%* change notation: TH1(a6,a4,a1,a3)
TH1 = createTH(parameter, A{1}, U, 2) ;

%* BBL12(b4',a1,a3,b2') = sum{b1'}_[leftMPS.B1(b4',a1,b1')*leftMPS.B2(b1',a3,b2')]
BBL12 = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;

%* QL12(b4',b2',a6,a4) = sum{a1,a3}_(BBL12(b4',a1,a3,b2')*TH1(a6,a4,a1,a3))
QL12 = contractTensors(conj(BBL12), 4, [2, 3], TH1, 4, [3, 4]) ;

clear TH1 ;
clear BBL12 ;

%* TH2((a1,a1'),(a2,a2'),(a5,a5'),(a4,a4',n)) = sum{m2,m2'}_[conj(A2(a1,a2,a5,a4,m2))*A2(a1',a2',a5',a4',m2')*V((m2,m2'),n)]
%* change notation: TH2(a1,a2,a5,a4)
TH2 = createTH(parameter, A{2}, V, 4) ;

%* BBR34(b2,a1,a2,b4) = sum{b3}_[rihgtMPS.B3(b2,a1,b3) * rightMPS.B4(b3,a2,b4)]
BBR34 = contractTensors(rightMPS.B{3}, 3, 3, rightMPS.B{4}, 3, 1) ;

%* QR34(b2,b4,a5,a4) = sum{a1,a2}_(BBR34(b2,a1,a2,b4)*TH2(a1,a2,a5,a4))
QR34 = contractTensors(BBR34, 4, [2, 3], TH2, 4, [1, 2]) ;

clear TH2 ;
clear BBR34 ;

