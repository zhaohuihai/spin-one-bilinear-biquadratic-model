function [QBR, THc] = createSpecialTensor_yb(parameter, rightMPS, leftMPS, TNS)
%* QBR(c,yb,e,xb) THc(zf,yf,yb,zb)

%* U((mi,mi'),n) V((mj,mj'),n)
[U, V] = decomposeHamiltonian(parameter) ;

%* THc(yf,zf,yb,zb)
THc = createTH(parameter, TNS.C, U, 3) ;
%* THc(zf,yf,yb,zb)
THc.dim = THc.dim([2, 1, 3, 4]) ;
THc.tensor4 = permute(THc.tensor4, [2, 1, 3, 4]) ;

%* THb(xf,yf,xb,yb)
THb = createTH(parameter, TNS.B, V, 4) ;
%* THb(xf,yf,yb,xb)
THb.dim = THb.dim([1, 2, 4, 3]) ;
THb.tensor4 = permute(THb.tensor4, [1, 2, 4, 3]) ;
%* B1R(c,d,xf)
B1R = rightMPS.B1 ;
%* B2R(d,e,yf)
B2R = rightMPS.B2 ;
Lambda = rightMPS.Lambda(3 : 4) ;
%* BBR((c,e),(xf,yf))
BBR = computeA1_L1_A2_L2(B1R, B2R, Lambda) ;
%* QBR(c,yb,e,xb) = sum{xf,yf}_(BBR((c,e),(xf,yf))*THb(xf,yf,yb,xb))
QBR = contractAA_T(BBR, THb) ;