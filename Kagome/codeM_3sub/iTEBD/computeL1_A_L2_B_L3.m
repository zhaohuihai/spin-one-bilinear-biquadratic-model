function AB = computeL1_A_L2_B_L3(A, B, Lambda)

Atensor = A.tensor3 ;
%****************************************************************
%* A(a,b,i) = A(a,b,i)*L1(a)
for a = 1 : Lambda(1).dim
    Atensor(a, :, :) = Atensor(a, :, :) * Lambda(1).tensor1(a) ;
end
%* A(a,b,i) = A(a,b,i)*L2(b)
for b = 1 : Lambda(2).dim
    Atensor(:, b, :) = Atensor(:, b, :) * Lambda(2).tensor1(b) ;
end
%****************************************************************
%* A(a,i,b)
Atensor = permute(Atensor, [1, 3, 2]) ;
d1 = A.dim(1) * A.dim(3) ;
d2 = A.dim(2) ;
%* A((a,i),b)
Atensor = reshape(Atensor, [d1, d2]) ;

Btensor = B.tensor3 ;
%****************************************************************
%* B(b,c,j) = B(b,c,j)*L3(c)
for c = 1 : Lambda(3).dim
    Btensor(:, c, :) = Btensor(:, c, :) * Lambda(3).tensor1(c) ;
end
%****************************************************************
d1 = B.dim(1) ;
d2 = B.dim(2) * B.dim(3) ;
%* B(b,(c,j))
Btensor = reshape(Btensor, [d1, d2]) ;

%* AB((a,i),(c,j)) = sum{b}_(A((a,i),b)*B(b,(c,j)))
ABtensor = Atensor * Btensor ;

%* AB(a,i,c,j)
ABtensor = reshape(ABtensor, [A.dim(1), A.dim(3), B.dim(2), B.dim(3)]) ;
%* AB(a,c,i,j)
ABtensor = permute(ABtensor, [1, 3, 2, 4]) ;
%* AB((a,c),(i,j))
ABtensor = reshape(ABtensor, [A.dim(1) * B.dim(2), A.dim(3) * B.dim(3)]) ;

AB.dim = [A.dim(1), B.dim(2), A.dim(3), B.dim(3)] ;
AB.tensor2 = ABtensor ;