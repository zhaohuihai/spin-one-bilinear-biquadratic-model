function [U, S, V, coef] = svd_TMP(R)

%* R((a, k), (c, l))
Rtensor = R.tensor2 ;

%* R((a,k), (c,l)) = sum{b}_(U((a,k),b)*S(b)*V((c,l),b))
[Utensor, Stensor, Vtensor] = svd(Rtensor) ;
Stensor = diag(Stensor) ;
coef = Stensor(1) ;
% %* normalize
Stensor = Stensor ./ coef ;

%* U((a,k),b)
U.dim = [R.dim(1), R.dim(2), R.dim(1) * R.dim(2)] ;
U.tensor2 = Utensor ;

S.dim = length(Stensor) ;
S.tensor1 = Stensor ;
%* V((c,l),b)
V.dim = [R.dim(3), R.dim(4), R.dim(3) * R.dim(4)] ;
V.tensor2 = Vtensor ;
