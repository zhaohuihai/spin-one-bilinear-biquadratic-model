function T = transposeT(T)
%* T(i,j,k,l) -> T(k,l,i,j)

T.dim = T.dim([3, 4, 1, 2]) ;
T.tensor4 = permute(T.tensor4, [3, 4, 1, 2]) ;
