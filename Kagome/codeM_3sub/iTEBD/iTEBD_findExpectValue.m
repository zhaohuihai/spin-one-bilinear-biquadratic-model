function iTEBD_findExpectValue(parameter, wave)
%* compute expectation value of local operator


%----------------------------------------------------------------------------------------------------------
A = wave.A ;
%* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a1,a3,m1)
A{1} = permute(A{1}, [3, 4, 2, 1, 5]) ;
%* A2(a1,a2,a4,a5,m2) -> A2(a1,a2,a5,a4,m2)
A{2} = permute(A{2}, [1, 2, 4, 3, 5]) ;
%* A3(a2,a3,a5,a6,m3) -> A3(a3,a5,a2,a6,m3)
A{3} = permute(A{3}, [2, 3, 1, 4, 5]) ;
%----------------------------------------------------------------------------------------------------------
%* right transfer matrix
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4) T3(a3,a5,a2,a6)
T = cell(1, 3) ;
for i = 1 : 3
    T{i} = computeTensorProductA_A(A{i}) ;
end
%--------------------------------------------------------------------------------------------------------
[rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T) ;

[expectValueBond, expectValueSite, T] = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, A) ;
clear T ;
%=====================================================================================================
if parameter.computation.OneSiteOperator == 1
    averageSite(parameter, expectValueSite) ;
end
if parameter.computation.twoSiteOperator == 1
    averageBond(parameter, expectValueBond) ;
end