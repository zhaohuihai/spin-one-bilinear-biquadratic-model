function vector = updateVector(T3, PR12, PR34, PL12, PL34, vector)
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
%* T3(a3,a5,a2,a6)
%* vector(b4,b2') 

%* vector(b4',a6,a4,b4) = sum{b2'}_[PL12(b4',b2',a6,a4) * vector(b4,b2')]
vector = contractTensors(PL12, 4, 2, vector, 2, 2) ;

%* vector(b2,a5,b4',a6) = sum{b4,a4}_[PR34(b2,b4,a5,a4) * vector(b4',a6,a4,b4)]
vector = contractTensors(PR34, 4, [2, 4], vector, 4, [4, 3]) ;

%* vector(a3,a2,b2,b4') = sum{a5,a6}_(T3(a3,a5,a2,a6) * vector(b2,a5,b4',a6))
vector = contractTensors(T3, 4, [2, 4], vector, 4, [2, 4]) ;

%* vector(b2',a1,a3,b2) = sum{b4',a2}_(PL34(b2',b4',a1,a2)*vector(a3,a2,b2,b4'))
vector = contractTensors(PL34, 4, [2, 4], vector, 4, [4, 2]) ;

%* vector(b4,b2') = sum{b2,a1,a3}_(PR12(b4,b2,a1,a3)*vector(b2',a1,a3,b2))
vector = contractTensors(PR12, 4, [2, 3, 4], vector, 4, [4, 2, 3]) ;

