function [QAR, THc] = createSpecialTensor_zf(parameter, rightMPS, leftMPS, TNS)
%* QAR(a,xf,c,zf) THc(zf,yf,yb,zb)

%* U((mi,mi'),n) V((mj,mj'),n)
[U, V] = decomposeHamiltonian(parameter) ;

%* THc(yf,zf,yb,zb)
THc = createTH(parameter, TNS.C, U, 2) ;
%* THc(zf,yf,yb,zb)
THc.dim = THc.dim([2, 1, 3, 4]) ;
THc.tensor4 = permute(THc.tensor4, [2, 1, 3, 4]) ;

%* THa(zf,xf,zb,xb)
THa = createTH(parameter, TNS.A, V, 1) ;
%* THa(zb,xb,xf,zf)
THa.dim = THa.dim([3, 4, 2, 1]) ;
THa.tensor4 = permute(THa.tensor4, [3, 4, 2, 1]) ;
%* A1R(a,b,zb)
A1R = rightMPS.A1 ;
%* A2R(b,c,xb)
A2R = rightMPS.A2 ;
Lambda = rightMPS.Lambda(1 : 2) ;
%* AAR((a,c),(zb,xb))
AAR = computeA1_L1_A2_L2(A1R, A2R, Lambda) ;
%* QAR(a,xf,c,zf) = sum{zb,xb}_(AAR((a,c),(zb,xb))*THa(zb,xb,xf,zf))
QAR = contractAA_T(AAR, THa) ;
