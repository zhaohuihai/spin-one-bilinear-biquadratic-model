function MPS = findDominantRightEigenMPS(parameter, transferMatrix)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;

%* MPS: A1 A2 B1 B2 Lambda(1) Lambda(2) Lambda(3) Lambda(4)
MPS = initializeMPS(parameter) ;

TMconvergence = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
coef0 = 1 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMPstep
    j = j + 1 ;
    Lambda0 = MPS.Lambda(1).tensor1 ;
    [MPS, coef, truncationError_TM] = applyTMP_right(parameter, transferMatrix, MPS) ;
    Lambda1 = MPS.Lambda(1).tensor1 ;
    TMconvergence = norm(Lambda1 - Lambda0) / sqrt(D - 1) ;
    coefConvergence  = abs(coef0 - coef) / coef ;
    flag = j / 10 ;
    if flag == floor(flag)
        TMconvergence
        coefConvergence
        truncationError_TM
    end
    coef0 = coef ;
end

while parameter.dim_MPS < maxDim
    j = j + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(j)]) ;
    [MPS, coef, truncationError_TM] = applyTMP_right(parameter, transferMatrix, MPS) ;
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;

TMconvergence = 1 ;
D = parameter.dim_MPS ;
coef0 = 1 ;
j = 0 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMPstep
    j = j + 1 ;
    Lambda0 = MPS.Lambda(1).tensor1 ;
    [MPS, coef, truncationError_TM] = applyTMP_right(parameter, transferMatrix, MPS) ;
    Lambda1 = MPS.Lambda(1).tensor1 ;
    if j > 1
        TMconvergence = norm(Lambda1 - Lambda0) / sqrt(D - 1) ;
        coefConvergence  = abs(coef0 - coef) / coef ;
    end
    flag = j / 10 ;
    if flag == floor(flag)
        TMconvergence
        coefConvergence
        truncationError_TM
    end
    coef0 = coef ;
end

