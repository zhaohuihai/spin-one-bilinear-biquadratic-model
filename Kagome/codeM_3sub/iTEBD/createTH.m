function TH = createTH(parameter, A, U, id)
%* A(a1,a2,a3,a4,m1)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = size(U, 2) ;

%* U((m1,m1'),n) -> U(m1,m1',n)
U = reshape(U, [M, M, N]) ;

%* AU(a1,a2,a3,a4,m1',n) = sum{m1}_[conj(A(a1,a2,a3,a4,m1)) * U(m1,m1',n)]
AU = contractTensors(conj(A), 5, 5, U, 3, 1) ;

%* TH(a1,a2,a3,a4,n,a1',a2',a3',a4') = sum{m1'}_[AU(a1,a2,a3,a4,m1',n)*A(a1',a2',a3',a4',m1')]
TH = contractTensors(AU, 6, 5, A, 5, 5) ;

if id == 1
    %* TH(a1,a1',n,a2,a2',a3,a3',a4,a4')
    TH = permute(TH, [1, 6, 5, 2, 7, 3, 8, 4, 9]) ;
    
    %* TH((a1,a1',n),(a2,a2'),(a3,a3'),(a4,a4'))
    TH = reshape(TH, [D^2 * N, D^2, D^2, D^2]) ;
elseif id == 2
    %* TH(a1,a1',a2,a2',n,a3,a3',a4,a4')
    TH = permute(TH, [1, 6, 2, 7, 5, 3, 8, 4, 9]) ;

    %* TH((a1,a1'),(a2,a2',n),(a3,a3'),(a4,a4'))
    TH = reshape(TH, [D^2, D^2 * N, D^2, D^2]) ;
elseif id == 3
    %* TH(a1,a1',a2,a2',a3,a3',n,a4,a4')
    TH = permute(TH, [1, 6, 2, 7, 3, 8, 5, 4, 9]) ;

    %* TH((a1,a1'),(a2,a2'),(a3,a3',n),(a4,a4'))
    TH = reshape(TH, [D^2, D^2, D^2 * N, D^2]) ;
elseif id == 4
    %* TH(a1,a1',a2,a2',a3,a3',a4,a4',n)
    TH = permute(TH, [1, 6, 2, 7, 3, 8, 4, 9, 5]) ;

    %* TH((a1,a1'),(a2,a2'),(a3,a3'),(a4,a4',n))
    TH = reshape(TH, [D^2, D^2, D^2, D^2 * N]) ;
end
