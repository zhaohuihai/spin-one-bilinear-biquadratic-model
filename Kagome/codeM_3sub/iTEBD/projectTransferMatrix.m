function R = projectTransferMatrix(T, AB)
%* R((a,k), (c,l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))

%* T(i,j,k,l)
Ttensor = T.tensor4 ;
%* T((i,j),(k,l)))
Ttensor = reshape(Ttensor, [T.dim(1) * T.dim(2), T.dim(3) * T.dim(4)]) ;
%* R((a,c),(k,l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
Rtensor = AB.tensor2 * Ttensor ;

%* R(a,c,k,l)
Rtensor = reshape(Rtensor, [AB.dim(1), AB.dim(2), T.dim(3), T.dim(4)]) ;

%* R(a,k,c,l)
Rtensor = permute(Rtensor, [1, 3, 2, 4]) ;

%* R((a,k), (c,l))
Rtensor = reshape(Rtensor, [AB.dim(1) * T.dim(3), AB.dim(2) * T.dim(4)]) ;

R.dim = [AB.dim(1), T.dim(3), AB.dim(2), T.dim(4)] ;
R.tensor2 = Rtensor ;