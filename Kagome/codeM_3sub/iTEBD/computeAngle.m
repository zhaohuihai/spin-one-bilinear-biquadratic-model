function [mag, agl] = computeAngle(s)

a = zeros(1, 3) ;
b = zeros(1, 3) ;

for i = 1 : 3
    if i < 3 
        j = i + 1 ;
    else
        j = 1 ;
    end
    
    n1 = norm(s(i, :)) ;
    n2 = norm(s(j, :)) ;
    a(i) = n1 ;
    phi = sum(s(i, :) .* s(j, :)) / (n1 * n2) ;
    
    b(i) = real(acos(phi)) ;
end
b = b ./ pi 
agl = mean(b) ;
mag = mean(a) ;