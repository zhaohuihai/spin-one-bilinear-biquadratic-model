function [rightMPS, leftMPS] = computeDominantEigenMPS(parameter, transferMatrix)

% %* MPS: A1 A2 B1 B2 Lambda(1) Lambda(2) Lambda(3) Lambda(4)
% MPS = initializeMPS(parameter) ;

% rand('twister', 1) ;
rightMPS = findDominantRightEigenMPS(parameter, transferMatrix) ;

%* transpose to left transferMatrix
%* Ta(zb,xb,xf,zf) -> Ta(xf,zf,zb,xb)
%* Tb(xf,yf,yb,xb) -> Tb(yb,xb,xf,yf)
%* Tc(zf,yb,yf,zb) -> Tc(yf,zb,zf,yb)
transferMatrix = transposeTransferMatrix(transferMatrix) ;

leftMPS = findDominantLeftEigenMPS(parameter, transferMatrix) ;

