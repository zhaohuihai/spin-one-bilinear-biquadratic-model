function [QR12, TH3] = createSpecialTensor_3(parameter, operator, rightMPS, A)
%* A3(a3,a5,a2,a6,m3) A1(a6,a4,a1,a3,m1)

%* H((m3, m1),(m3', m1')) = sum{n}_(U((m3,m3'),n)*S(n)*conj(V((m1,m1'),n)))
%* U((m3,m3'),n) = U((m3,m3'),n)*sqrt(S(n))
%* V((m1,m1'),n) = conj(V((m1,m1'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;
%----------------------------------------------------------------------------------------------------------------------------

%* TH3((a3,a3',n),(a5,a5'),(a2,a2'),(a6,a6')) = sum{m3,m3'}_[conj(A3(a3,a5,a2,a6,m3))*A3(a3',a5',a2',a6',m3')*U((m3,m3'),n)]
%* change notation: TH3(a3,a5,a2,a6)
TH3 = createTH(parameter, A{3}, U, 1) ;


%* TH1((a6,a6'),(a4,a4'),(a1,a1'),(a3,a3',n)) = sum{m1,m1'}_[conj(A1(a6,a4,a1,a3,m1))*A1(a6',a4',a1',a3',m1')*V((m1,m1'),n)]
%* change notation: TH1(a6,a4,a1,a3)
TH1 = createTH(parameter, A{1}, V, 4) ;

%* BBR12(b4,a6,a4,b2) = sum{b1}_[rightMPS.B1(b4,a6,b1)*rightMPS.B2(b1,a4,b2)]
BBR12 = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;

%* QR12(b4,b2,a1,a3) = sum{a6,a4}_(BBR12(b4,a6,a4,b2)*TH1(a6,a4,a1,a3))
QR12 = contractTensors(BBR12, 4, [2, 3], TH1, 4, [1, 2]) ;

clear TH1 ;
clear BBR12 ;

