function averageSite(parameter, expectValueSite)

computation = parameter.computation ;

precision = 12 ;
if computation.S == 1
    
    s = zeros(3, 3) ;
    for i = 1 : 3
        disp(['Sx', num2str(i), ': ', num2str(expectValueSite(i).Sx, precision)]) ;
        disp(['Sy', num2str(i), ': ', num2str(expectValueSite(i).Sy, precision)]) ;
        disp(['Sz', num2str(i), ': ', num2str(expectValueSite(i).Sz, precision)]) ;
        disp(' ') ;
        s(i, :) = [expectValueSite(i).Sx, expectValueSite(i).Sy, expectValueSite(i).Sz] ;
    end
    [spinMagnitude, spinAngle] = computeAngle(s) ;
    saveExpectationValue(parameter, spinMagnitude, 'spin_Magnitude') ;
    saveExpectationValue(parameter, spinAngle, 'spin_Angle_pi') ;
end

if computation.Q == 1
    
    q = zeros(3, 6) ;
    for i = 1 : 3
        disp(['Qxx', num2str(i), ': ', num2str(expectValueSite(i).Qxx, precision)]) ;
        disp(['Qyy', num2str(i), ': ', num2str(expectValueSite(i).Qyy, precision)]) ;
        disp(['Qzz', num2str(i), ': ', num2str(expectValueSite(i).Qzz, precision)]) ;
        disp(['Qxy', num2str(i), ': ', num2str(expectValueSite(i).Qxy, precision)]) ;
        disp(['Qyz', num2str(i), ': ', num2str(expectValueSite(i).Qyz, precision)]) ;
        disp(['Qzx', num2str(i), ': ', num2str(expectValueSite(i).Qzx, precision)]) ;
        disp(' ') ;
        q(i, 1) = expectValueSite(i).Qxx ;
        q(i, 2) = expectValueSite(i).Qyy ;
        q(i, 3) = expectValueSite(i).Qzz ;
        q(i, 4) = expectValueSite(i).Qxy ;
        q(i, 5) = expectValueSite(i).Qyz ;
        q(i, 6) = expectValueSite(i).Qzx ;
        
    end
    [quadMagnitude, quadAngle] = computeQuadAngle(q) ;
%     
    saveExpectationValue(parameter, quadMagnitude, 'quadrupole_Magnitude') ;
    saveExpectationValue(parameter, quadAngle, 'quadrupole_Angle_pi') ;
end