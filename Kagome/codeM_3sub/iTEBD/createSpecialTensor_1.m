function [QR12, QL34] = createSpecialTensor_1(parameter, operator, rightMPS, leftMPS, A)
%* A1(a6,a4,a1,a3,m1) A2(a1,a2,a5,a4,m2)

%* H((m1, m2),(m1', m2')) = sum{n}_(U((m1,m1'),n)*S(n)*conj(V((m2,m2'),n)))
%* U((m1,m1'),n) = U((m1,m1'),n)*sqrt(S(n))
%* V((m2,m2'),n) = conj(V((m2,m2'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;

%* TH1((a6,a6'),(a4,a4'),(a1,a1',n),(a3,a3')) = sum{m1,m1'}_[conj(A1(a6,a4,a1,a3,m1))*A1(a6',a4',a1',a3',m1')*U((m1,m1'),n)]
%* change notation: TH1(a6,a4,a1,a3)
TH1 = createTH(parameter, A{1}, U, 3) ;

%* BBR12(b4,a6,a4,b2) = sum{b1}_[rightMPS.B1(b4,a6,b1)*rightMPS.B2(b1,a4,b2)]
BBR12 = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;

%* QR12(b4,b2,a1,a3) = sum{a6,a4}_(BBR12(b4,a6,a4,b2)*TH1(a6,a4,a1,a3))
QR12 = contractTensors(BBR12, 4, [2, 3], TH1, 4, [1, 2]) ;

clear TH1 ;
clear BBR12 ;

%* TH2((a1,a1',n),(a2,a2'),(a5,a5'),(a4,a4')) = sum{m2,m2'}_[conj(A2(a1,a2,a5,a4,m2))*A2(a1',a2',a5',a4',m2')*V((m2,m2'),n)]
%* change notation: TH2(a1,a2,a5,a4)
TH2 = createTH(parameter, A{2}, V, 1) ;

%* BBL34(b2',a5,a4,b4') = sum{b3'}_[leftMPS.B3(b2',a5,b3') * leftMPS.B4(b3',a4,b4')]
BBL34 = contractTensors(leftMPS.B{3}, 3, 3, leftMPS.B{4}, 3, 1) ;

%* QL34(b2',b4',a1,a2) = sum{a5,a4}_(BBL34(b2',a5,a4,b4')*TH2(a1,a2,a5,a4))
QL34 = contractTensors(conj(BBL34), 4, [2, 3], TH2, 4, [3, 4]) ;

clear TH2 ;
clear BBL34 ;

