function MPS = applyAdditionalGate(parameter, Tc, MPS)

B2 = MPS.B2 ;
A1 = MPS.A1 ;
Lambda = MPS.Lambda ;

%* act Tc on B2*A1
[B2, A1, Lambda(4), coef] = applyGateOperation(parameter, Tc, B2, A1, Lambda([3, 4, 1])) ;

% Lambda(4).tensor1 = Lambda(4).tensor1 ./ coef ;

MPS.Lambda = Lambda ;
MPS.B2 = B2 ;
MPS.A1 = A1 ;