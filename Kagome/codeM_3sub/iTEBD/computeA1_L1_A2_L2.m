function AA = computeA1_L1_A2_L2(A1, A2, Lambda)
%* AA((a,c),(zb,xb)) = sum{b}_(A1(a,b,zb)*L1(b)*A2(b,c,xb)*L2(c))

%* A1(a,b,zb)
A1tensor = A1.tensor3 ;

%* A1(a,b,zb) = A(a,b,zb)*L1(b)
for b = 1 : Lambda(1).dim
    A1tensor(:, b, :) = A1tensor(:, b, :) * Lambda(1).tensor1(b) ;
end

%* A1(a,zb,b)
A1tensor = permute(A1tensor, [1, 3, 2]) ;
d1 = A1.dim(1) * A1.dim(3) ;
d2 = A1.dim(2) ;
%* A1((a,zb),b)
A1tensor = reshape(A1tensor, [d1, d2]) ;

%* A2(b,c,xb)
A2tensor = A2.tensor3 ;
%* A2(b,c,xb) = A2(b,c,xb)*L2(c)
for c = 1 : Lambda(2).dim
    A2tensor(:, c, :) = A2tensor(:, c, :) * Lambda(2).tensor1(c) ;
end

d1 = A2.dim(1) ;
d2 = A2.dim(2) * A2.dim(3) ;
%* A2(b,(c,xb))
A2tensor = reshape(A2tensor, [d1, d2]) ;

%* AA((a,zb),(c,xb)) = sum{b}_(A1((a,zb),b)*A2(b,(c,xb)))
AAtensor = A1tensor * A2tensor ;

%* AA(a,zb,c,xb)
AAtensor = reshape(AAtensor, [A1.dim(1), A1.dim(3), A2.dim(2), A2.dim(3)]) ;
%* AA(a,c,zb,xb)
AAtensor = permute(AAtensor, [1, 3, 2, 4]) ;
%* AA((a,c),(zb,xb))
AAtensor = reshape(AAtensor, [A1.dim(1) * A2.dim(2), A1.dim(3) * A2.dim(3)]) ;

AA.dim = [A1.dim(1), A2.dim(2), A1.dim(3), A2.dim(3)] ;
AA.tensor2 = AAtensor ;
