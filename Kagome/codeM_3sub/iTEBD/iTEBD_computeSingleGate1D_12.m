function [MPS, truncErr] = iTEBD_computeSingleGate1D_12(parameter, T, MPS)
%* T1(a6,a4,a1,a3) T2(a1,a2,a5,a4)

for k = 1 : 2
    i = 2 * k - 1 ;
    j = 2 * k ;
    %* BB(b4,b2,a6,a4) = sum{b1}_[B1(b4,a6,b1)*B2(b1,a4,b2)]
    %* BB(b2,b4,a1,a2) = sum{b3}_[B3(b2,a1,b3)*B4(b3,a2,b4)]
    BB = contractTensors(MPS.B{i}, 3, 3, MPS.B{j}, 3, 1, [1, 4, 2, 3]) ;
    
    %* A1(b4,a1,a3,b2) = sum{a6,a4}_[BB(b4,b2,a6,a4) * T1(a6,a4,a1,a3)]
    %* A2(b2,a5,a4,b4) = sum{a1,a2}_(BB(b2,b4,a1,a2) * T2(a1,a2,a5,a4))
    MPS2.A{k} = contractTensors(BB, 4, [3, 4], T{k}, 4, [1, 2], [1, 3, 4, 2]) ;
end

%* MPS.B([1,2,3,4]), MPS.S([1,2,3,4])
[MPS, truncErr] = canonicalize_quad(parameter, MPS2) ;


