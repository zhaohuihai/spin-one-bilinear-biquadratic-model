function T = transposeTransferMatrix1D(T)
%* transpose to left transferMatrix
%* T1(a6,a4,a1,a3) -> T1(a1,a3,a6,a4)
%* T2(a1,a2,a5,a4) -> T2(a5,a4,a1,a2)
%* T3(a3,a5,a2,a6) -> T3(a2,a6,a3,a5)


%* T1(a6,a4,a1,a3) -> T1(a1,a3,a6,a4)
T{1} = permute(T{1}, [3, 4, 1, 2]) ;
T{1} = conj(T{1}) ;

%* T2(a1,a2,a5,a4) -> T2(a5,a4,a1,a2)
T{2} = permute(T{2}, [3, 4, 1, 2]) ;
T{2} = conj(T{2}) ;

%* T3(a3,a5,a2,a6) -> T3(a2,a6,a3,a5)
T{3} = permute(T{3}, [3, 4, 1, 2]) ;
T{3} = conj(T{3}) ;
