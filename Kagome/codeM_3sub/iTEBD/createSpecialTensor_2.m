function [QL34, TH3] = createSpecialTensor_2(parameter, operator, leftMPS, A)
%* A2(a1,a2,a5,a4,m2) A3(a3,a5,a2,a6,m3)

%* H((m2, m3),(m2', m3')) = sum{n}_(U((m2,m2'),n)*S(n)*conj(V((m3,m3'),n)))
%* U((m2,m2'),n) = U((m2,m2'),n)*sqrt(S(n))
%* V((m3,m3'),n) = conj(V((m3,m3'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;
%----------------------------------------------------------------------------------------------------------------------------

%* TH2((a1,a1'),(a2,a2',n),(a5,a5'),(a4,a4')) = sum{m2,m2'}_[conj(A2(a1,a2,a5,a4,m2))*A2(a1',a2',a5',a4',m2')*U((m2,m2'),n)]
%* change notation: TH2(a1,a2,a5,a4)
TH2 = createTH(parameter, A{2}, U, 2) ;

%* BBL34(b2',a5,a4,b4') = sum{b3'}_[leftMPS.B3(b2',a5,b3') * leftMPS.B4(b3',a4,b4')]
BBL34 = contractTensors(leftMPS.B{3}, 3, 3, leftMPS.B{4}, 3, 1) ;

%* QL34(b2',b4',a1,a2) = sum{a5,a4}_(BBL34(b2',a5,a4,b4')*TH2(a1,a2,a5,a4))
QL34 = contractTensors(conj(BBL34), 4, [2, 3], TH2, 4, [3, 4]) ;

clear TH2 ;
clear BBL34 ;

%* TH3((a3,a3'),(a5,a5'),(a2,a2',n),(a6,a6')) = sum{m3,m3'}_[conj(A3(a3,a5,a2,a6,m3))*A3(a3',a5',a2',a6',m3')*V((m3,m3'),n)]
%* change notation: TH3(a3,a5,a2,a6)
TH3 = createTH(parameter, A{3}, V, 3) ;