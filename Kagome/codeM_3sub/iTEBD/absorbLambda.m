function TNS = absorbLambda(wave)

Lambda = wave.Lambda ;
for i = 1 : 6
    Lambda(i).tensor1 = sqrt(Lambda(i).tensor1) ;
end
%* A(zf, xf, zb, xb)
TNS.A = Atimes4Lambda(wave.A, Lambda([5, 1, 6, 2])) ;
%* B(xf, yf, xb, yb)
TNS.B = Atimes4Lambda(wave.B, Lambda([1, 3, 2, 4])) ;
%* C(yf, zf, yb, zb)
TNS.C = Atimes4Lambda(wave.C, Lambda([3, 5, 4, 6])) ;