function [U, S, V, truncationError_TM] = truncate_TMP(parameter, U, S, V)

D = parameter.dim_MPS ;

Stensor = S.tensor1(1 : D) ;

S.dim = D ;
truncationError_TM = 1 - (sum(Stensor) / sum(S.tensor1)) ;
S.tensor1 = Stensor ;


U.dim(3) = D ;
U.tensor2 = U.tensor2(:, 1 : D) ;

V.dim(3) = D ;
V.tensor2 = V.tensor2(:, 1 : D) ;
