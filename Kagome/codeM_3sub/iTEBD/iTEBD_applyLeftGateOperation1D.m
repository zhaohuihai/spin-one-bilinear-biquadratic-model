function [MPS, T, truncationError] = iTEBD_applyLeftGateOperation1D(parameter, T, MPS)
%* T1(a1,a3,a6,a4) T2(a5,a4,a1,a2) T3(a2,a6,a3,a5)

truncErr_single = zeros(1, 4) ;
%------------------------------------------------------------------------------------
%* T{3}
[MPS, truncErr_single(1)] = iTEBD_computeSingleGate1D_3(parameter, T{3}, MPS) ;

%* apply T{1} T{2} gate
[MPS, truncErr_single(2)] = iTEBD_computeSingleGate1D_12(parameter, T([1, 2]), MPS) ;

%* MPS.B(1,2,3,4) -> (3,4,1,2)
MPS = exchange12and34(MPS) ;

[MPS, truncErr_single(3)] = iTEBD_computeSingleGate1D_3(parameter, T{3}, MPS) ;

[MPS, truncErr_single(4)] = iTEBD_computeSingleGate1D_12(parameter, T([1, 2]), MPS) ;

%* MPS.B(1,2,3,4) -> (3,4,1,2)
MPS = exchange12and34(MPS) ;


MPS = normalizeLambda(MPS) ;
%-------------------------------------------
truncationError = max(truncErr_single) ;
