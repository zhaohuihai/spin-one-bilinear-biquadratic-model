function [vector, eta] = applyPowerMethod(parameter, T3, PR12, PR34, PL12, PL34)
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
%* T3(a3,a5,a2,a6)
%* vectorR(b4,b2') 

D = parameter.dim_MPS ;
%* vectorR(b4,b2') 
if parameter.complexWave == 1
    vector = rand(D, D) + 1i * rand(D, D) ;
else
    vector = rand(D, D) ;
end
vector = normalizeVector(vector) ;

convergence = 1 ;
eta = 1 ;
j = 0 ;
while convergence >= parameter.tol_power && j < parameter.maxPowerStep
    j = j + 1 ;
    vector = updateVector(T3, PR12, PR34, PL12, PL34, vector) ;
    [vector, eta1] = normalizeVector(vector) ;

    convergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        convergence
    end
    eta = eta1 ;
end
if parameter.display == 1
    disp(['power steps = ', num2str(j)]) ;
end