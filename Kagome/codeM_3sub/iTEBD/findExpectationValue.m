function expectValue = findExpectationValue(T3, PR12, PR34, PL12, PL34, vectorR, vectorL, TH3, QR12, QR34, QL12, QL34)
%* PR12(b4,b2,a1,a3) PR34(b2,b4,a5,a4) PL12(b4',b2',a6,a4) PL34(b2',b4',a1,a2)
%* T3(a3,a5,a2,a6)
%* vector(b4,b2') 

%* QvectorR(b4,b2')
QvectorR = updateVector(TH3, QR12, QR34, QL12, QL34, vectorR) ;

%* h = sum{b4,b2'}_[conj(vectorL(b4,b2')) * QvectorR(b4,b2')]
h = contractTensors(conj(vectorL), 2, [1, 2], QvectorR, 2, [1, 2]) ;

%* PvectorR(b4,b2')
PvectorR = updateVector(T3, PR12, PR34, PL12, PL34, vectorR) ;

%* g = sum{b4,b2'}_[conj(vectorL(b4,b2')) * PvectorR(b4,b2')]
g = contractTensors(conj(vectorL), 2, [1, 2], PvectorR, 2, [1, 2]) ;

expectValue = h / g ;

if abs( imag(expectValue) ) > 1e-6
    disp('imag part of expectValue may be too large!') ;
end

expectValue = real(expectValue) ;