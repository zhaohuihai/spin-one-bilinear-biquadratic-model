function MPS = iTEBD_initializeMPS(parameter)

% rng(2) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

for i = 1 : 4
    if parameter.complexWave == 1
        MPS.B{i} = rand(D, d, D) + 1j * rand(D, d, D) ;
    else
        MPS.B{i} = rand(D, d, D) ;
    end
    
    L = sort(rand(D, 1), 'descend') ;
    MPS.S{i} = L ./ L(1) ; 
end