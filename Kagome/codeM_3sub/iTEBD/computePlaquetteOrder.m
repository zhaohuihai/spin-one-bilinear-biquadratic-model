function pVBC = computePlaquetteOrder(nearSpinCorrelationBond)

P = zeros(1, 2) ;

P(1) = sum(nearSpinCorrelationBond([1, 2, 3])) / (sum(nearSpinCorrelationBond([4, 5, 6]))) - 1 ;

P(2) = sum(nearSpinCorrelationBond([4, 5, 6])) / (sum(nearSpinCorrelationBond([1, 2, 3]))) - 1 ;

pVBC = max(P) ;