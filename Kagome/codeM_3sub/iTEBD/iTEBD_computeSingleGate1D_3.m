function [MPS, truncErr] = iTEBD_computeSingleGate1D_3(parameter, T3, MPS)
%* T3(a3,a5,a2,a6)
%* B1(b4,a1,b1) B2(b1,a3,b2) B3(b2,a5,b3) B4(b3,a4,b4)


%* BB(b1,b3,a3,a5) = sum{b2}_[B2(b1,a3,b2) * B3(b2,a5,b3)]
BB = contractTensors(MPS.B{2}, 3, 3, MPS.B{3}, 3, 1, [1, 4, 2, 3]) ;

%* A2(b1,a2,a6,b3) = sum{a3,a5}_[BB(b1,b3,a3,a5) * T3(a3,a5,a2,a6)]
A2 = contractTensors(BB, 4, [3, 4], T3, 4, [1, 2], [1, 3, 4, 2]) ;

[db, da2, da6, ~] = size(A2) ;

%* A2(b1,a2,a6,b3) -> A2(b1,(a2,a6),b3)
A2 =  reshape(A2, [db, da2 * da6, db]) ;

MPS3.A{1} = MPS.B{1} ;
MPS3.A{2} = A2 ;
MPS3.A{3} = MPS.B{4} ;

%* MPS.A([1,2,3]), MPS.S([1,2,3])
MPS3 = canonicalize_triple(parameter, MPS3) ;

MPS.B{1} = MPS3.A{1} ;
MPS.B{4} = MPS3.A{3} ;

MPS.S{1} = MPS3.S{1} ;
MPS.S{3} = MPS3.S{2} ;
MPS.S{4} = MPS3.S{3} ;

[MPS.B([2, 3]), MPS.S{2}, truncErr] = singleTOdoubleMPS(parameter, MPS3.A{2}, MPS3.S{1}, MPS3.S{2}, da2, da6, db, db) ;

%-----------------------------------------------------------------------------------------
% verifyRightCano(MPS.B{1}, MPS.S{1}, MPS.S{4})
% verifyLeftCano(MPS.B{1}, MPS.S{4}, MPS.S{1}) 
% 
% verifyRightCano(MPS.B{2}, MPS.S{2}, MPS.S{1})
% verifyLeftCano(MPS.B{2}, MPS.S{1}, MPS.S{2}) 
% 
% verifyRightCano(MPS.B{3}, MPS.S{3}, MPS.S{2})
% verifyLeftCano(MPS.B{3}, MPS.S{2}, MPS.S{3}) 
% 
% verifyRightCano(MPS.B{4}, MPS.S{4}, MPS.S{3})
% verifyLeftCano(MPS.B{4}, MPS.S{3}, MPS.S{4}) 