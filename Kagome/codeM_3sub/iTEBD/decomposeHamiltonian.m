function [U, V] = decomposeHamiltonian(parameter)

M = parameter.siteDimension ;
MM = M * M ;
%* H(mi',mj',mi,mj)
H = reshape(parameter.H, [M, M, M, M]) ;

%* H(mi,mi',mj,mj')
H = permute(H, [3, 1, 4, 2]) ;

%* H((mi,mi'),(mj,mj'))
H = reshape(H, [MM, MM]) ;

[U, S, V] = svd(H) ;
S = diag(S) ;
S = S(S>1e-15) ;
Sdim = length(S) ;
S = diag(S) ;
U = U(:, 1 : Sdim) ;
%* U((mi,mi'),n)
U = U * sqrt(S) ;

V = V(:, 1 : Sdim) ;
%* V((mj,mj'),n)
V = V * sqrt(S) ;

