function [QR34, TH3] = createSpecialTensor_5(parameter, operator, rightMPS, A)
%* A2(a1,a2,a5,a4,m2) A3(a3,a5,a2,a6,m3)

%* H((m2, m3),(m2', m3')) = sum{n}_(U((m2,m2'),n)*S(n)*conj(V((m3,m3'),n)))
%* U((m2,m2'),n) = U((m2,m2'),n)*sqrt(S(n))
%* V((m3,m3'),n) = conj(V((m3,m3'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;
%----------------------------------------------------------------------------------------------------------------------------

%* TH2((a1,a1'),(a2,a2'),(a5,a5',n),(a4,a4')) = sum{m2,m2'}_[conj(A2(a1,a2,a5,a4,m2))*A2(a1',a2',a5',a4',m2')*U((m2,m2'),n)]
%* change notation: TH2(a1,a2,a5,a4)
TH2 = createTH(parameter, A{2}, U, 3) ;

%* BBR34(b2,a1,a2,b4) = sum{b3}_[rightMPS.B3(b2,a1,b3)*rightMPS.B4(b3,a2,b4)]
BBR34 = contractTensors(rightMPS.B{3}, 3, 3, rightMPS.B{4}, 3, 1) ;

%* QR34(b2,b4,a5,a4) = sum{a1,a2}_(BBR34(b2,a1,a2,b4)*TH2(a1,a2,a5,a4))
QR34 = contractTensors(BBR34, 4, [2, 3], TH2, 4, [1, 2]) ;

clear TH2 ;
clear BBR34 ;

%* TH3((a3,a3'),(a5,a5',n),(a2,a2'),(a6,a6')) = sum{m3,m3'}_[conj(A3(a3,a5,a2,a6,m3))*A3(a3',a5',a2',a6',m3')*V((m3,m3'),n)]
%* change notation: TH3(a3,a5,a2,a6)
TH3 = createTH(parameter, A{3}, V, 2) ;




