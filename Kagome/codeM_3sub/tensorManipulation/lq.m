function L = lq(A)
% A(a,b) =  L(a,l) * Q(l,b)

%* A(a,b) -> A(b,a)
A = A.' ;

d2 = size(A, 2) ;

%* A(b,a) = sum{l}_[Q(b,l)*L(l,a)]
L = qr(A, 0) ;
L = triu(L) ;
L = L(1: d2, :) ;


%* L(l,a) -> L(a,l)
L = L.' ;