function T = setSmalltoZero(T)

n = numel(T) ;

for i = 1 : n
    
    if abs(T(i)) < 1e-14
        T(i) = 0 ;
    end
    
end

