function parameter = defineProjectionParameter(parameter)

%*************************************************************
%* projection method: 1. simple update; 
parameter.project.method = 1 ;
parameter.project.alpha = 0.5 ;

%**********************polarization************************
parameter.project.polarization = 1 ;
parameter.project.polarField = 1.0 ;

parameter.project.field = 0 ;

parameter.project.tauFinal_polar = 1e-2 ;
parameter.project.tol_polarization = 1e-4 ;

%-------------------------------------------------------------
parameter.project.VBSpolarRatio = 1 ; % 1: not polarized

parameter.project.VBSratio = 1 ;
%--------------------------------------------------------------
%* polarization type
parameter.project.Sz1 = 1 ;
%**************************************************************
parameter.project.tauInitial = 1e-0 ;
parameter.project.tauFinal = 1 / 1e2 ; %*
parameter.project.tauChangeFactor = 1 ;

parameter.project.eachStep = 1 ;

parameter.project.tol_projection = 1e-8 ;
parameter.project.maxProjectionStep = 2e3 ;
%*************************************************************