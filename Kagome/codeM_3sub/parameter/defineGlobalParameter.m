function parameter = defineGlobalParameter

%* yes: 1, no: 0
parameter.loadPreviousWave = 0 ; %*
%* yes: 1, no: 0
parameter.projection = 1 ; %*
%*****************************************************************
parameter.numCPUthreads = 48 ;

%*****************************************************************
parameter.display = 1 ;
%*****************************************************************
parameter.computation.expectationValue = 1 ;
%-------------------------------------------------------------
parameter.computation.OneSiteOperator = 1 ;

%* magnetization of every sublattice
%* compute: S1x S1y S1z; S2x S2y S2z; S3x S3y S3z;
parameter.computation.S = 1 ;

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
parameter.computation.Q = 1 ;
%-------------------------------------------------------------
parameter.computation.twoSiteOperator = 1 ;

% energy computation parameters
parameter.computation.energy = 1 ;
parameter.computation.energyDerivative = 0 ;

%* nearest neighbor spin correlation
parameter.computation.nearSpinCorrelation = 0 ;

