function parameter = defineModelParameter(parameter)
%* Bilinear biquadratic model parameters
parameter.siteDimension = 3 ;

parameter.theta = (0.3) * pi ;

parameter.thetaInitial = (0.26) *pi ;
parameter.thetaFinal =   (0.49) *pi ;
parameter.thetaIncre =   (0.01) *pi ;

if parameter.theta <= (-pi) || parameter.theta > (pi)
    error('the value of theta is out of parameter range') ;
end