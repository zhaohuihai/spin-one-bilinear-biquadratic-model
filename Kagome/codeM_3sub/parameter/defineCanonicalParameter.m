function parameter = defineCanonicalParameter(parameter)

parameter.cano.maxIt = 100 ;

parameter.cano.tol = 1e-10 ;

parameter.verifyCano = 0 ;