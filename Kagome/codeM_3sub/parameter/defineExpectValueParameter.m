function parameter = defineExpectValueParameter(parameter)

%* dimension of MPS
parameter.dim_MPS = 10 ;
parameter.dim_MPS_initial = 10 ;
parameter.dim_MPS_incre = 10 ;
%------------------------------------------------
parameter.tol_iTEBD = 1e-8 ;
parameter.tol_power = 1e-8 ;

parameter.maxITEBDstep = 1e2 ;
parameter.maxPowerStep = 1e2 ;
