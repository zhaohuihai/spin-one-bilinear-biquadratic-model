function ISx = createISx(parameter)
%* ISx(m1,m2,n1,n2)
M = parameter.siteDimension ;

ISx = zeros(M, M, M, M) ;

%* Sx(m1,n1)
Sx = createSx(parameter) ;

%* ISx(m1,m2,n1,n2) = delta(m1,n1) * <m2|Sx|n2>

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            ISx(m1, m2, m1, n2) = Sx(m2,n2) ;
        end
    end
end

ISx = reshape(ISx, [M^2, M^2]) ;
