function SyI = createSyI(parameter)
%* SyI(m1,m2,n1,n2)
M = parameter.siteDimension ;

SyI = zeros(M, M, M, M) ;

%* Sy(m1,n1)
Sy = createSy(parameter) ;

%* SyI(m1,m2,n1,n2) = <m1|Sy|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            SyI(m1, m2, n1, m2) = Sy(m1,n1) ;
        end
    end
end

SyI = reshape(SyI, [M^2, M^2]) ;
