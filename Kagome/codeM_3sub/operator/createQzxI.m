function QzxI = createQzxI(parameter)
%* QzxI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QzxI = zeros(M, M, M, M) ;

%* Sz(m1,n1) Sx(m1,n1)
Sz = createSz(parameter) ;
Sx = createSx(parameter) ;

Qzx = (Sz * Sx + Sx * Sz) / 2  ;

%* QzxI(m1,m2,n1,n2) = <m1|Qzx|n1>*delta(m2,n2)
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QzxI(m1, m2, n1, m2) = Qzx(m1,n1) ;
        end
    end
end

QzxI = reshape(QzxI, [M^2, M^2]) ;
