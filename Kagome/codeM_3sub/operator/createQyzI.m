function QyzI = createQyzI(parameter)
%* QyzI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QyzI = zeros(M, M, M, M) ;

%* Sy(m1,n1) Sz(m1,n1)
Sy = createSy(parameter) ;
Sz = createSz(parameter) ;

Qyz = (Sy * Sz + Sz * Sy) / 2  ;

%* QyzI(m1,m2,n1,n2) = <m1|Qyz|n1>*delta(m2,n2)
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QyzI(m1, m2, n1, m2) = Qyz(m1,n1) ;
        end
    end
end

QyzI = reshape(QyzI, [M^2, M^2]) ;
