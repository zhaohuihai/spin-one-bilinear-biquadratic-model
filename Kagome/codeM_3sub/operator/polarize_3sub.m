function hamiltonian = polarize_3sub(parameter, hamiltonian)

M = parameter.siteDimension ;

if parameter.project.Sz1 == 1
    disp('polarize one sublattice to Z direction') ;
    %* Sz(m,n)
%     hz = createSz(parameter) ;
    hz = createQzz(parameter) ;
    
    hx = zeros(M, M) ;
    hy = zeros(M, M) ;
else
    hx = zeros(M, M) ;
    hy = hx ;
    hz = hx ;
end
%* h12(m1,m2,n1,n2) h23 h31

%* h12 = Hbb - field * (Sz1 + Sx2) / 2
hamiltonian{1} = addField(parameter, hamiltonian{1}, hz, hx) ;

%* h23 = Hbb - field * (Sx1 + Sy2) / 2
hamiltonian{2} = addField(parameter, hamiltonian{2}, hx, hy) ;

%* h31 = Hbb - field * (Sy1 + Sz2) / 2
hamiltonian{3} = addField(parameter, hamiltonian{3}, hy, hz) ;

%* h12 = Hbb - field * (Sz1 + Sx2) / 2
hamiltonian{4} = addField(parameter, hamiltonian{4}, hz, hx) ;

%* h23 = Hbb - field * (Sx1 + Sy2) / 2
hamiltonian{5} = addField(parameter, hamiltonian{5}, hx, hy) ;

%* h31 = Hbb - field * (Sy1 + Sz2) / 2
hamiltonian{6} = addField(parameter, hamiltonian{6}, hy, hz) ;