function IQyz = createIQyz(parameter)
%* IQyz(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQyz = zeros(M, M, M, M) ;

%* Sy(m2,n2) Sz(m2,n2)
Sy = createSy(parameter) ;
Sz = createSz(parameter) ;

Qyz = (Sy * Sz + Sz * Sy) / 2  ;

%* IQyz(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qyz|n2>
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQyz(m1, m2, m1, n2) = Qyz(m2,n2) ;
        end
    end
end

IQyz = reshape(IQyz, [M^2, M^2]) ;
