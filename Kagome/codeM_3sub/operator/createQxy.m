function Qxy = createQxy(parameter)

Sx = createSx(parameter) ;
Sy = createSy(parameter) ;

Qxy = (Sx * Sy + Sy * Sx) / 2 ;