function QxxI = createQxxI(parameter)
%* QxxI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QxxI = zeros(M, M, M, M) ;

%* Sx(m1,n1)
Sx = createSx(parameter) ;

Qxx = Sx * Sx - eye(M) * (2 / 3) ;

%* QxxI(m1,m2,n1,n2) = <m1|Qxx|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QxxI(m1, m2, n1, m2) = Qxx(m1,n1) ;
        end
    end
end

QxxI = reshape(QxxI, [M^2, M^2]) ;
