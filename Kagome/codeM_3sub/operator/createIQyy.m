function IQyy = createIQyy(parameter)
%* IQyy(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQyy = zeros(M, M, M, M) ;

%* Sy(m2,n2)
Sy = createSy(parameter) ;

Qyy = Sy * Sy - eye(M) * (2 / 3) ;

%* IQyy(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qyy|n2>
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQyy(m1, m2, m1, n2) = Qyy(m2,n2) ;
        end
    end
end

IQyy = reshape(IQyy, [M^2, M^2]) ;
