function SzI = createSzI(parameter)
%* SzI(m1,m2,n1,n2)
M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

SzI = zeros(M, M, M, M) ;

%* SzI(m1,m2,n1,n2) = <m1|Sz|n1>*delta(m2,n2)


%* assign values to diagonal entries
for q2 = 1 : M
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        SzI(q1, q2, q1, q2) = m1 ;
    end
end

SzI = reshape(SzI, [M^2, M^2]) ;
