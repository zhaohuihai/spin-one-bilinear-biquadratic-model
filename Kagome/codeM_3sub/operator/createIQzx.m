function IQzx = createIQzx(parameter)
%* IQzx(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQzx = zeros(M, M, M, M) ;

%* Sz(m2,n2) Sx(m2,n2)
Sz = createSz(parameter) ;
Sx = createSx(parameter) ;

Qzx = (Sz * Sx + Sx * Sz) / 2  ;

%* IQzx(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qzx|n2>
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQzx(m1, m2, m1, n2) = Qzx(m2,n2) ;
        end
    end
end

IQzx = reshape(IQzx, [M^2, M^2]) ;
