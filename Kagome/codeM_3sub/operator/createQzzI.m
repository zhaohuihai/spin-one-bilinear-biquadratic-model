function QzzI = createQzzI(parameter)
%* QzzI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QzzI = zeros(M, M, M, M) ;

%* Sz(m1,n1)
Sz = createSz(parameter) ;

Qzz = Sz * Sz - eye(M) * (2 / 3 ) ;

%* QzzI(m1,m2,n1,n2) = <m1|Qzz|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QzzI(m1, m2, n1, m2) = Qzz(m1, n1) ;
        end
    end
end

QzzI = reshape(QzzI, [M^2, M^2]) ;
