function IQxx = createIQxx(parameter)
%* IQxx(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQxx = zeros(M, M, M, M) ;

%* Sx(m2,n2)
Sx = createSx(parameter) ;

Qxx = Sx * Sx - eye(M) * (2 / 3) ;

%* IQxx(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qxx|n2>

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQxx(m1, m2, m1, n2) = Qxx(m2,n2) ;
        end
    end
end

IQxx = reshape(IQxx, [M^2, M^2]) ;
