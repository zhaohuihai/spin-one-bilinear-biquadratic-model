function IQxy = createIQxy(parameter)
%* IQxy(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQxy = zeros(M, M, M, M) ;

%* Sx(m2,n2) Sy(m2,n2)
Sx = createSx(parameter) ;
Sy = createSy(parameter) ;

Qxy = (Sx * Sy + Sy * Sx) / 2  ;

%* IQxy(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qxy|n2>
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQxy(m1, m2, m1, n2) = Qxy(m2,n2) ;
        end
    end
end

IQxy = reshape(IQxy, [M^2, M^2]) ;
