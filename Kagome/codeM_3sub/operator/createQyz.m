function Qyz = createQyz(parameter)

Sy = createSy(parameter) ;
Sz = createSz(parameter) ;

Qyz = (Sy * Sz + Sz * Sy) / 2 ;