function hamiltonian = createHamiltonian_polarized(parameter)
%* create polarized hamiltonian
%* hamiltonian is 1 X 6 cell array: h12(1), h23(2), h31(3), h12(4), h23(5), h31(6)

M = parameter.siteDimension ;

%* Hbb((m1, m2), (n1, n2))
Hbb = createHamiltonian_BilinBiqua(parameter) ;
%* Hbb((m1, m2), (n1, n2)) -> Hbb(m1,m2,n1,n2)
Hbb = reshape(Hbb, [M, M, M, M]) ;


for i = 1 : 6
    hamiltonian{i} = Hbb ;
end


hamiltonian = polarize_3sub(parameter, hamiltonian) ;

for i = 4 : 6
    hamiltonian{i} = hamiltonian{i} * parameter.project.VBSratio ;
end

disp(['field = ', num2str(parameter.project.field)]) ;
disp(['VBS ratio = ', num2str(parameter.project.VBSratio)]) ;