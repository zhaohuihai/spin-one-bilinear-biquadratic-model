function Qxx = createQxx(parameter)

M = parameter.siteDimension ;

Sx = createSx(parameter) ;

Qxx = Sx * Sx - eye(M) * (2 / 3) ;