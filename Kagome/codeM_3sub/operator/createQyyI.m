function QyyI = createQyyI(parameter)
%* QyyI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QyyI = zeros(M, M, M, M) ;

%* Sy(m1,n1)
Sy = createSy(parameter) ;

Qyy = Sy * Sy - eye(M) * (2 / 3) ;

%* QyyI(m1,m2,n1,n2) = <m1|Qyy|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QyyI(m1, m2, n1, m2) = Qyy(m1,n1) ;
        end
    end
end

QyyI = reshape(QyyI, [M^2, M^2]) ;
