function ISy = createISy(parameter)
%* ISy(m1,m2,n1,n2)
M = parameter.siteDimension ;

ISy = zeros(M, M, M, M) ;

%* Sy(m1,n1)
Sy = createSy(parameter) ;

%* ISy(m1,m2,n1,n2) = delta(m1,n1) * <m2|Sy|n2>

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            ISy(m1, m2, m1, n2) = Sy(m2,n2) ;
        end
    end
end

ISy = reshape(ISy, [M^2, M^2]) ;
