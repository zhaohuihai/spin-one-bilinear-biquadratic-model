function IQzz = createIQzz(parameter)
%* IQzz(m1,m2,n1,n2)
M = parameter.siteDimension ;

IQzz = zeros(M, M, M, M) ;

%* Sz(m2,n2)
Sz = createSz(parameter) ;

Qzz = Sz * Sz - eye(M) * (2 / 3) ;

%* IQzz(m1,m2,n1,n2) = delta(m1,n1) * <m2|Qzz|n2>
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            IQzz(m1, m2, m1, n2) = Qzz(m2,n2) ;
        end
    end
end

IQzz = reshape(IQzz, [M^2, M^2]) ;
