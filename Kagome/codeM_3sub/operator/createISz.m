function ISz = createISz(parameter)
%* ISz(m1,m2,n1,n2)
M = parameter.siteDimension ;

ISz = zeros(M, M, M, M) ;

%* Sz(m1,n1)
Sz = createSz(parameter) ;

%* ISz(m1,m2,n1,n2) = delta(m1,n1) * <m2|Sz|n2>

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n2 = 1 : M
            ISz(m1, m2, m1, n2) = Sz(m2,n2) ;
        end
    end
end

ISz = reshape(ISz, [M^2, M^2]) ;
