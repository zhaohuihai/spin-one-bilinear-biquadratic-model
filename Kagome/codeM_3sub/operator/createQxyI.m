function QxyI = createQxyI(parameter)
%* QxyI(m1,m2,n1,n2)
M = parameter.siteDimension ;

QxyI = zeros(M, M, M, M) ;

%* Sx(m1,n1) Sy(m1,n1)
Sx = createSx(parameter) ;
Sy = createSy(parameter) ;

Qxy = (Sx * Sy + Sy * Sx) / 2  ;

%* QxyI(m1,m2,n1,n2) = <m1|Qxy|n1>*delta(m2,n2)
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            QxyI(m1, m2, n1, m2) = Qxy(m1,n1) ;
        end
    end
end

QxyI = reshape(QxyI, [M^2, M^2]) ;
