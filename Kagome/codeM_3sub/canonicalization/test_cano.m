function test_cano(parameter)

rng(1) ;

dim = 100 ;
d = 100 ;
% dim = dim + 2 ;
parameter.dim_MPS = dim ;
parameter.cano.maxIt = 100 ;

parameter.cano.tol = 1e-3 ;

parameter.verifyCano = 1 ;
%====================================================
% %* single sublattice
% D = dim / 2 ;
% 
% A1 = rand(D, D) ;
% A2 = rand(D, D) ;
% 
% 
% A = zeros(dim, 2, dim) ;
% alpha = 1 ;
% A(1 : D, 1, 1 : D) = A1 ;
% A((D + 1) : dim, 1, (D + 1) : dim) = A1 * alpha ;
% 
% A(1 : D, 2, 1 : D) = A2 ;
% A((D + 1) : dim, 2, (D + 1) : dim) = A2 * alpha ;
% %====================================================
% % A = rand(dim, 2, dim) ;
% %====================================================
% [A, S] = canonicalize_single(parameter, A) ;
%====================================================
%* double sublattice
A = rand(dim, d, dim) ;
B = rand(dim, d, dim) ;

MPS.A = A ;
MPS.B = B ;
%* entanglement spectrum: Sa, Sb
[MPS, truncErr, canoErr] = canonicalize_double(parameter, MPS) ;
truncErr
canoErr
