function [MPS, truncErr] = canonicalize_quad(parameter, MPS2)
%* MPS2.A{1}(a1,i,j,a3) MPS2.A{2}(a3,k,l,a5)
%* MPS.B{1}(a1,i,a2), MPS.B{2}(a2,j,a3), MPS.B{3}(a3,k,a4), MPS.B{4}(a4,l,a5)
%* MPS.S{1}{a2}, MPS.S{2}{a3}, MPS.S{3}{a4}, MPS.S{4}{a5}, 

[da1, di, dj, da3] = size(MPS2.A{1}) ;
[~, dk, dl, ~] = size(MPS2.A{2}) ;

%* MPS_double.A(a1,(i,j),a3)
MPS_double.A = reshape(MPS2.A{1}, [da1, di * dj, da3]) ;
%* MPS_double.B(a3,(k,l),a5)
MPS_double.B = reshape(MPS2.A{2}, [da3, dk * dl, da1]) ;

MPS_double = canonicalize_double(parameter, MPS_double) ;

MPS.S{2} = MPS_double.SA ;
MPS.S{4} = MPS_double.SB ;

[MPS.B([1, 2]), MPS.S{1}, truncErrA] = singleTOdoubleMPS(parameter, MPS_double.A, MPS_double.SB, MPS_double.SA, di, dj, da1, da3) ;

[MPS.B([3, 4]), MPS.S{3}, truncErrB] = singleTOdoubleMPS(parameter, MPS_double.B, MPS_double.SA, MPS_double.SB, dk, dl, da3, da1) ;
truncErr = max(truncErrA, truncErrB) ;

%-----------------------------------------------------------------------------------------
% verifyRightCano(MPS.B{1}, MPS.S{1}, MPS.S{4})
% verifyLeftCano(MPS.B{1}, MPS.S{4}, MPS.S{1}) 
% 
% verifyRightCano(MPS.B{2}, MPS.S{2}, MPS.S{1})
% verifyLeftCano(MPS.B{2}, MPS.S{1}, MPS.S{2}) 
% 
% verifyRightCano(MPS.B{3}, MPS.S{3}, MPS.S{2})
% verifyLeftCano(MPS.B{3}, MPS.S{2}, MPS.S{3}) 
% 
% verifyRightCano(MPS.B{4}, MPS.S{4}, MPS.S{3})
% verifyLeftCano(MPS.B{4}, MPS.S{3}, MPS.S{4}) 