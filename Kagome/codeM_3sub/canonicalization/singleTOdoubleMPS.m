function [B, S1, truncErr] = singleTOdoubleMPS(parameter, A, SB, SA, di, dj ,da1, da3)

%* A(a1,(i,j),a3) = sqrt(SB(a1)) * A(a1,(i,j),a3) * sqrt(SA(a3))
%* A(a1,(i,j),a3) = sqrt(SB(a1)) * A(a1,(i,j),a3)
for a1 = 1 : da1
    A(a1, :, :) = sqrt(SB(a1)) * A(a1, :, :) ;
end
%* A(a1,(i,j),a3) = A(a1,(i,j),a3) * sqrt(SA(a3))
for a3 = 1 : da3
    A(:, :, a3) = A(:, :, a3) * sqrt(SA(a3)) ;
end
%----------------------------------------------------------------------
%* A(a1,(i,j),a3) -> A((a1,i),(j,a3))
A = reshape(A, [da1 * di, dj * da3]) ;
%* A((a1,i),(j,a3)) = sum{a2}_[U((a1,i),a2)*S1(a2)*conj(V((j,a3),a2))]
[U, S1, V] = svd(A, 'econ') ;
S1 = diag(S1) ;
S1 = S1 ./ S1(1) ;

da2 = parameter.dim_MPS ;
[U, S1, V, truncErr] = truncate(U, S1, V, da2) ;

%* U((a1,i),a2) -> U(a1,i,a2)
U = reshape(U, [da1, di, da2]) ;
%* U(a1,i,a2) = U(a1,i,a2) / sqrt(SB(a1))
for a1 = 1 : da1
    U(a1, :, :) = U(a1, :, :) ./ sqrt(SB(a1)) ;
end

%* U(a1,i,a2) = U(a1,i,a2) * sqrt(S1(a2))
for a2 = 1 : da2
    U(:, :, a2) = U(:, :, a2) .* sqrt(S1(a2)) ;
end
B = cell(1 , 2) ;
B{1} = U ;

%* V(a2,(j,a3))
V = V' ;
%* V(a2,(j,a3)) -> V(a2,j,a3)
V = reshape(V, [da2, dj, da3]) ;
%* V(a2,j,a3) = V(a2,j,a3) / sqrt(SA(a3))
for a3 = 1 : da3
    V(:, :, a3) = V(:, :, a3) ./ sqrt(SA(a3)) ;
end

%* V(a2,j,a3) = V(a2,j,a3) * sqrt(S1(a2))
for a2 = 1 : da2
    V(a2, :, :) = V(a2, :, :) .* sqrt(S1(a2)) ;
end
B{2} = V ;