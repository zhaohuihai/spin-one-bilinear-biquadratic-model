function [MPS, truncErr, canoErr] = canonicalize_triple(parameter, MPS)
%* MPS.A([1,2,3]), MPS.S([1,2,3])
%* A1(c,i,a) A2(a,j,b) A3(b,k,c)
%* S1(a) S2(b) S3(c)

A = MPS.A ;

R = cell(1, 3) ;
L = cell(1, 3) ;
S0 = cell(1, 3) ;
for i = 1 : 3
    %* R1(a1,a) R2(b1,b) R3(c1,c)
    R{i} = computeRightResidual(A{i}) ;
    
    if i == 3
        j = 1 ;
    else
        j = i + 1 ;
    end
    %* L1(a,a2) L2(b,b2) L3(c,c2)
    L{i} = computeLeftResidual(A{j}) ;
    
    [~, S0{i}, ~] = decomposeRL(R{i}, L{i}) ;
end

converge = 1 ;
tol = parameter.cano.tol ;
maxIt = parameter.cano.maxIt ;

U = cell(1, 3) ;
V = cell(1, 3) ;
S = cell(1, 3) ;
err = zeros(1, 3) ;
step = 1 ;
while converge > tol && step <= maxIt
    step = step + 1 ;
    %-------
    for i = 1 : 3        
        if i == 1
            j = 3 ;
        else
            j = i - 1 ;
        end
        %* R1(a1,a) R2(b1,b) R3(c1,c)
        R{i} = computeRightResidual(A{i}, R{j}) ;
        
        j = 4 - i ;
        if i == 1
            k = 1 ;
        else
            k = 5 - i ;
        end
        %* L1(a,a2) L2(b,b2) L3(c,c2)
        L{j} = computeLeftResidual(A{k}, L{k}) ;
    end
    
    for i = 1 : 3
        [U{i}, S{i}, V{i}] = decomposeRL(R{i}, L{i}) ;
        err(i) = norm(S{i} - S0{i}) / norm(S{i}) ;
    end
    %----------------------------------    
    converge = max(err) ;    
    S0 = S ;
end
%--------------------------------------------------
% truncation
dim_MPS = parameter.dim_MPS ;
err = zeros(1, 3) ;
for i = 1 : 3
    [U{i}, S{i}, V{i}, err(i)] = truncate(U{i}, S{i}, V{i}, dim_MPS) ;
end
truncErr = max(err) ;
%--------------------------------------------------
PR = cell(1, 3) ;
PL = cell(1, 3) ;
for i = 1 : 3
    if i == 3
        j = 1 ;
    else
        j = i + 1 ;
    end
    %* Create Projectors between A1 and A2
    %* Create Projectors between A2 and A3
    %* Create Projectors between A3 and A1
    sqrtS = sqrt(S{i}) ;
    L{i} = L{i}.' ;
    %* PR1(a1,a) = sum{a2}_[L1(a2,a1)*V1(a2,a)/sqrtS(a)]
    PR{i} = createProjector(L{i}, V{i}, sqrtS) ;
    %* PL2(a1,a) = sum{a2}_[R1(a2,a1)*conj(U1(a2,a))/sqrtS(a)]
    U{i} = conj(U{i}) ;
    PL{j} = createProjector(R{i}, U{i}, sqrtS) ;
end


%* transform the MPS into its canonical form
for i = 1 : 3
    %* A1(c1,i,a) = sum{a1}_[A1(c1,i,a1) * PR1(a1,a)]
    A{i} = contractTensors(A{i}, 3, 3, PR{i}, 2, 1) ;
    %* A1(c,i,a) = sum{c1}_[PL1(c1,c) * A1(c1,i,a)]
    A{i} = contractTensors(PL{i}, 2, 1, A{i}, 3, 1) ;
end

MPS.A = A ;
MPS.S = S ;
%---------------------------------------------------------------------------
canoErr = 1 ;

if parameter.verifyCano == 1
    
    canoErr_right = zeros(1, 3) ;
    canoErr_Left = zeros(1, 3) ;
    
    for i = 1 : 3
        if i == 1
            k = 3 ;
        else 
            k = i - 1 ;
        end 
        canoErr_right(i) = verifyRightCano(A{i}, S{i}, S{k}) ;
        
        canoErr_Left(i) = verifyLeftCano(A{i}, S{k}, S{i}) ;
    end
       
    canoErr = max([canoErr_right, canoErr_Left]) 
    
end

