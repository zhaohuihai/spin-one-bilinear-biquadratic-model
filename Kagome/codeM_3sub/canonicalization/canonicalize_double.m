function [MPS, truncErr, canoErr] = canonicalize_double(parameter, MPS)
%* A(a,i,b) B(b,j,c)

A = MPS.A ;
B = MPS.B ;

%* dc = da
% [da, di, db] = size(A) ;

%* RA(b1,b)
RA = computeRightResidual(A) ;
%* LB(b,b2)
LB = computeLeftResidual(B) ;

[~, SA0, ~] = decomposeRL(RA, LB) ;

%* RB(c1,c)
RB = computeRightResidual(B) ;
%* LA(c,c2)
LA = computeLeftResidual(A) ;

[~, SB0, ~] = decomposeRL(RB, LA) ;

converge = 1 ;
tol = parameter.cano.tol ;
maxIt = parameter.cano.maxIt ;

step = 1 ;
while converge > tol && step <= maxIt 
    step = step + 1 ;
    %-------
    %* RA(b1,b)
    RA = computeRightResidual(A, RB) ;
    
    %* LB(b,b2)
    LB = computeLeftResidual(B, LA) ;

    [UA, SA, VA] = decomposeRL(RA, LB) ;

    %-------
    %* RB(c1,c)
    RB = computeRightResidual(B, RA) ;
    %* LA(c,c2)
    LA = computeLeftResidual(A, LB) ;
    
    [UB, SB, VB] = decomposeRL(RB, LA) ;
    %--------
    errA = norm(SA - SA0) / norm(SA) ;
    errB = norm(SB - SB0) / norm(SB) ;

    converge = max(errA, errB) ;
    
    SA0 = SA ;
    SB0 = SB ;
end
%--------------------------------------------------
% truncation
dim_MPS = parameter.dim_MPS ;
[UA, SA, VA, truncErrA] = truncate(UA, SA, VA, dim_MPS) ;
[UB, SB, VB, truncErrB] = truncate(UB, SB, VB, dim_MPS) ;
truncErr = max(truncErrA, truncErrB) ;
%--------------------------------------------------
%* Create Projectors between A and B
sqrtSA = sqrt(SA) ;
R2 = LB.' ;
%* PRA(b1,b) = sum{b2}_[R2(b2,b1)*VA(b2,b)/sqrtSA(b)]
PRA = createProjector(R2, VA, sqrtSA) ;
%* PLB(b1,b) = sum{b2}_[RA(b2,b1)*conj(UA(b2,b)) /sqrtSA(b)]
UA = conj(UA) ;
PLB = createProjector(RA, UA, sqrtSA) ;

%* Create Projectors between B and A
sqrtSB = sqrt(SB) ;
R2 = LA.' ;
%* PRB(c1,c) = sum{c2}_[R2(c2,c1)*VB(c2,c) / sqrtSB(c)]
PRB = createProjector(R2, VB, sqrtSB) ;
%* PLA(a1,a) = sum{a2}_[RB(a2,a1)*conj(UB(a2,a)) / sqrtSB(a)]
UB = conj(UB) ;
PLA = createProjector(RB, UB, sqrtSB) ;


%* transform the MPS into its canonical form
%* A(a1,i,b) = sum{b1}_[A(a1,i,b1) * PRA(b1,b)]
A = contractTensors(A, 3, 3, PRA, 2, 1) ;
%* A(a,i,b) = sum{a1}_[PLA(a1,a) * A(a1,i,b)]
A = contractTensors(PLA, 2, 1, A, 3, 1) ;

%* B(b1,j,c) = sum{c1}_[B(b1,j,c1) * PRB(c1,c)]
B = contractTensors(B, 3, 3, PRB, 2, 1) ;
%* B(b,j,c) = sum{b1}_[PLB(b1,b) * B(b1,j,c)]
B = contractTensors(PLB, 2, 1, B, 3, 1) ;

MPS.A = A ;
MPS.B = B ;
MPS.SA = SA ;
MPS.SB = SB ;
%---------------------------------------------------------------------------
canoErr = 1 ;

if parameter.verifyCano == 1
    
    canoErrA_right = verifyRightCano(A, SA, SB) ;

    canoErrA_left = verifyLeftCano(A, SB, SA) ;

    canoErrB_right = verifyRightCano(B, SB, SA) ;

    canoErrB_Left = verifyLeftCano(B, SA, SB) ;
    
    canoErr = max([canoErrA_right, canoErrA_left, canoErrB_right, canoErrB_Left]) ;

end

