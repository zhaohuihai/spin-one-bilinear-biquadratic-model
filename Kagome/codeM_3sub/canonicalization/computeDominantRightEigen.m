function [V, eta] = computeDominantRightEigen(T, Chi)
%* T((b,b'),(c,c'))
%* V(b,b')

% [V, eta] = eigs(T, 1) ;
%=============================================================
V = rand(Chi, Chi) ;
V = (V + V') / 2 ;
V0 = V ./ trace(V) ;

maxStep = 1e2 ;

powerConverge = 1 ;
step = 0 ;
tol = 1e-10 ;
V0 = reshape(V0, [Chi^2, 1]) ;
while powerConverge > tol && step <= maxStep
    step = step + 1 ;
    V = T * V0 ;
    eta = norm(V) ;
    V = V ./ eta ;
    powerConverge = norm(V - V0) / sqrt(Chi^2 - 1) ;
    
    V = reshape(V, [Chi, Chi]) ;
    V = (V + V') / 2 ;
    V = reshape(V, [Chi^2, 1]) ;
    
    V0 = V ;
end
V = reshape(V, [Chi, Chi]) ;
V = (V + V') / 2 ;
if powerConverge > tol
    disp(['dominant eigen vector error = ', num2str(powerConverge)])
end
%===============================================================


