function [wave, truncationError, coef] = applyFirstTrotter(parameter, hamiltonian, wave)

c = zeros(1, 6) ;
truncErr_single = zeros(1, 6) ;

for i = 1 : 6
    PO(i) = createProjectionOperator(parameter, hamiltonian{i}) ;
end
%------------------------------------------------------------------------------------------
n = 1 ;
for j = 1 : 2
    for i = 1 : 3
        [wave, truncErr_single(n), c(n)] = simpleUpdate(parameter, PO(n), wave) ;
        
        %* A(1,2,3) -> A(2,3,1)
        %* Lambda(1,2,3,4,5,6) -> Lambda(2,3,1,5,6,4)
        wave = rotateWave231(wave) ;
        
        n = n + 1 ;
    end
    
    %* Lambda(1,2,3,4,5,6) -> Lambda(4,5,6,1,2,3)
    %* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a3,a1,m1)
    wave = rotateWaveTensor(wave) ;
end
%-------------------------------------------------------------------------------------
% for i = 1 : 3
%     for j = 1 : 2
%         n = i + (j - 1) * 3 ;
%         [wave, truncErr_single(n), c(n)] = simpleUpdate(parameter, PO(n), wave) ;
%         
%         %* Lambda(1,2,3,4,5,6) -> Lambda(4,5,6,1,2,3)
%         %* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a3,a1,m1)
%         wave = rotateWaveTensor(wave) ;
%     end
%     
%     %* A(1,2,3) -> A(2,3,1)
%     %* Lambda(1,2,3,4,5,6) -> Lambda(2,3,1,5,6,4)
%     wave = rotateWave231(wave) ;
% end
%---------------------------------------------------------------------------------------


truncationError = max(truncErr_single) ;

coef = mean(c) ;