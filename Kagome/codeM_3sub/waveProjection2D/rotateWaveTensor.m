function wave = rotateWaveTensor(wave)
%* Lambda(1,2,3,4,5,6) -> Lambda(4,5,6,1,2,3)
%* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a3,a1,m1)


%* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a3,a1,m1)
for i = 1 : 3
    wave.A{i} = permute(wave.A{i}, [3, 4, 1, 2, 5]) ;
end

%* 
wave.Lambda = wave.Lambda([4, 5, 6, 1, 2, 3]) ;