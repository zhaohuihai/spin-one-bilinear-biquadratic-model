function [wave, projectionSingularValue] = projectForward(parameter, wave)

parameter.projectionOperator = createProjectionOperator(parameter) ;

%* AB(zf,zb,xb,yf,xb,yb,mi,mj) = 
%* sum{xf}_(L5(zf)*L6(zb)*L2(xb)*A(zf,xf,zb,xb,mi)*L1(xf)*B(xf,yf,xb,yb,mj)*L3(yf)*L2(xb)*L4(yb))
AB = computeLzfLzbLxb_A_Lxf_B_LyfLxbLyb(parameter, wave) ;

%* T(zf,zb,xb,yf,xb,yb,mi,mj) = sum{ni,nj}_(AB(zf,zb,xb,yf,xb,yb,ni,nj)*projectionOperator(ni,nj,mi,mj))
T = computeProjection(parameter, AB) ;

%* 
USV = svd_projection(parameter, T) ;
[USV, projectionSingularValue] = truncate(parameter, USV) ;

wave = recover(parameter, wave, USV) ;
