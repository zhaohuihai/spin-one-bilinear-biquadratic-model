function M1 = computeA1_sqrtL3_sqrtL6_sqrtL4( A1, L3, L6, L4, varargin )
%* M1((a6,a4,a3,m1),a1) = A1(a3,a1,a6,a4,m1)*sqrt(L3(a3))*sqrt(L6(a6))*sqrt(L4(a4))

[d3, d1, d6, d4, dm] = size(A1) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A1(a3,a1,a6,a4,m1)*sqrt(L3(a3))
for a3 = 1 : d3 
    A1(a3, :, :, :, :) = A1(a3, :, :, :, :) * L3(a3).^alpha ;
end

%* A1(a3,a1,a6,a4,m1)*sqrt(L6(a6))
for a6 = 1 : d6
    A1(:, :, a6, :, :) = A1(:, :, a6, :, :) * L6(a6).^alpha ;
end

%* A1(a3,a1,a6,a4,m1)*sqrt(L4(a4))
for a4 = 1 : d4
    A1(:, :, :, a4, :) = A1(:, :, :, a4, :) * L4(a4).^alpha ;
end

%* A1(a3,a1,a6,a4,m1) -> A1(a6,a4,a3,m1,a1)
A1 = permute(A1, [3, 4, 1, 5, 2]) ;

%* A1(a6,a4,a3,m1,a1) -> M1((a6,a4,a3,m1),a1)
M1 = reshape(A1, [d6 * d4 * d3 * dm, d1]) ;