function wave = projectBackward(parameter, wave)

wave.A = exchangeForwardBackward_A(parameter, wave.A) ;
wave.B = exchangeForwardBackward_A(parameter, wave.B) ;
wave.Lambda = exchangeForwardBackward_Lambda(wave.Lambda) ;

wave = projectForward(parameter, wave) ;

wave.A = exchangeForwardBackward_A(parameter, wave.A) ;
wave.B = exchangeForwardBackward_A(parameter, wave.B) ;
wave.Lambda = exchangeForwardBackward_Lambda(wave.Lambda) ;