function [T1, T2, c1, c2] = computeProjection(parameter, projectionOperator, A1, A2)
%* A1(a3,a1,a6,a4,m1) A2(a1,a2,a4,a5,m2)
%* T1(a3,(a1,l),a6,a4,m1) T2((a1,l),a2,a4,a5,m2)

%* Pa(m1,n1,l)
Pa = projectionOperator.Pa ;

%* T1(a3,a1,l,a6,a4,m1) = sum{n1}_[A1(a3,a1,a6,a4,n1)*Pa(m1,n1,l)]
T1 = contractTensors(A1, 5, 5, Pa, 3, 2, [1, 2, 6, 3, 4, 5]) ;

%* Pb(m2,n2,l)
Pb = projectionOperator.Pb ;

%* T2(a1,l,a2,a4,a5,m2) = sum{n2}_[A2(a1,a2,a4,a5,n2)*Pb(m2,n2,l)]
T2 = contractTensors(A2, 5, 5, Pb, 3, 2, [1, 6, 2, 3, 4, 5]) ;

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* T1(a3,a1,l,a6,a4,m1) -> T1(a3, (a1, l) , a6, a4, m1)
T1 = reshape(T1, [D, D * M^2, D, D, M]) ;

%* T2(a1,l,a2,a4,a5,m2) -> T2((a1,l),a2,a4,a5,m2)
T2 = reshape(T2, [D * M^2, D, D, D, M]) ;

c1 = max(max(max(max(max(abs(T1)))))) ;
c2 = max(max(max(max(max(abs(T2)))))) ;

T1 = T1 ./ c1 ;
T2 = T2 ./ c2 ;