function wave = recover(parameter, wave, USV)

%* A(zf,xf,zb,xb,mi) = U(zf,xf,zb,xb,mi)/(L5(zf)*L6(zb)*L2(xb))
%* B(xf,yf,xb,yb,mj) = V(xf,yf,xb,yb,mj)/(L3(yf)*L2(xb)*L4(yb))

M = parameter.siteDimension ;
wave.Lambda(1) = USV.S ;
Lambda = wave.Lambda ;


%* A((zf,zb,xb,mi),xf)
Atensor = USV.U.tensor2 ;
dim = [Lambda(5).dim, Lambda(6).dim, Lambda(2).dim, M, Lambda(1).dim] ;
%* A(zf,zb,xb,mi,xf)
Atensor = reshape(Atensor, dim) ;
%* A(zf,xf,zb,xb,mi)
Atensor = permute(Atensor, [1, 5, 2, 3, 4]) ;
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) / L5(zf)
for i = 1 : Lambda(5).dim
    Atensor(i, :, :, :, :) = Atensor(i, :, :, :, :) ./ Lambda(5).tensor1(i) ;
end
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) / L6(zb)
for i = 1 : Lambda(6).dim
    Atensor(:, :, i, :, :) = Atensor(:, :, i, :, :) ./ Lambda(6).tensor1(i) ;
end
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) / L2(xb)
for i = 1 : Lambda(2).dim
    Atensor(:, :, :, i, :) = Atensor(:, :, :, i, :) ./ Lambda(2).tensor1(i) ;
end
Adim = [Lambda(5).dim, Lambda(1).dim, Lambda(6).dim, Lambda(2).dim] ;
A.dim = Adim ;
A.tensor4 = Atensor ;

%* B((yf,xb,yb,mj),xf)
Btensor = USV.V.tensor2 ;
dim = [Lambda(3).dim, Lambda(2).dim, Lambda(4).dim, M, Lambda(1).dim] ;
%* B(yf,xb,yb,mj,xf)
Btensor = reshape(Btensor, dim) ;
%* B(xf,yf,xb,yb,mj)
Btensor = permute(Btensor, [5, 1, 2, 3, 4]) ;
%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) / L3(yf)
for i = 1 : Lambda(3).dim
    Btensor(:, i, :, :, :) = Btensor(:, i, :, :, :) ./ Lambda(3).tensor1(i) ;
end
%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) / L2(xb)
for i = 1 : Lambda(2).dim
    Btensor(:, :, i, :, :) = Btensor(:, :, i, :, :) ./ Lambda(2).tensor1(i) ;
end
%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) / L4(yb)
for i = 1 : Lambda(4).dim
    Btensor(:, :, :, i, :) = Btensor(:, :, :, i, :) ./ Lambda(4).tensor1(i) ;
end
Bdim = [Lambda(1).dim, Lambda(3).dim, Lambda(2).dim, Lambda(4).dim] ;
B.dim = Bdim ;
B.tensor4 = Btensor ;

wave.A = A ;
wave.B = B ;