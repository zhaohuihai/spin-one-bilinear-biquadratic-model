function [USV, projectionSingularValue] = truncate(parameter, USV)

D = parameter.bondDimension ;

Stensor = USV.S.tensor1(1 : D) ;
projectionSingularValue = Stensor ;
USV.S.dim = D ;
truncationError_projection = 1 - (sum(Stensor) / sum(USV.S.tensor1)) ;
USV.S.tensor1 = Stensor ;


USV.U.dim(2) = D ;
USV.U.tensor2 = USV.U.tensor2(:, 1 : D) ;

USV.V.dim(2) = D ;
USV.V.tensor2 = USV.V.tensor2(:, 1 : D) ;
