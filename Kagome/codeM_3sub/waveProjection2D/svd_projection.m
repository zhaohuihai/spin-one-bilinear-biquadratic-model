function USV = svd_projection(parameter, T)

M = parameter.siteDimension ;

%* T(zf,zb,xb,yf,xb,yb,mi,mj)
tensor = T.tensor6 ;

%* T(zf,zb,xb,mi,yf,xb,yb,mj)
tensor = permute(tensor, [1, 2, 3, 7, 4, 5 ,6, 8]) ;

Tdim = T.dim ;
d1 = T.dim(1) * T.dim(2) * T.dim(3) * M ;
d2 = T.dim(4) * T.dim(5) * T.dim(6) * M ;

%* T((zf,zb,xb,mi),(yf,xb,yb,mj))
tensor = reshape(tensor, [d1, d2]) ;

[Utensor, Stensor, Vtensor] = svd(tensor) ;
Stensor = diag(Stensor) ;
%* normalize
Stensor = Stensor ./ Stensor(1) ;

%* U((zf,zb,xb,mi),xf)
U.dim = [d1, d1] ;
U.tensor2 = Utensor ;

S.dim = length(Stensor) ;
S.tensor1 = Stensor ;
%* V((yf,xb,yb,mj),xf)
V.dim = [d2, d2] ;
V.tensor2 = Vtensor ;

USV.U = U ;
USV.S = S ;
USV.V = V ;