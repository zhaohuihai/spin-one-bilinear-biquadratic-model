function wave = applySecondTrotter(parameter, wave)

projectionSingularValue0 = wave.projectionSingularValue1 ;
tauSaved = parameter.tau ;

parameter.tau = tauSaved / 2 ;
for i = 1 : 2
    [wave, projectionSingularValue] = projectForward(parameter, wave) ;
    wave = projectBackward(parameter, wave) ;
    
    if i == 1
        projectionSingularValue1 = projectionSingularValue ;
    end
    %* A->B, B->C, C->A
    wave = rotateWave(wave) ;
end
%* wave: C, A, B
wave = projectForward(parameter, wave) ;
parameter.tau = tauSaved ;
wave = projectBackward(parameter, wave) ;
parameter.tau = tauSaved / 2 ;
wave = projectForward(parameter, wave) ;

for i = 1 : 2
    %* A->C, B->A, C->B 
    wave = reverseRotateWave(wave) ;
    
    wave = projectBackward(parameter, wave) ;
    wave = projectForward(parameter, wave) ;
end


wave.projectionSingularValue0 = projectionSingularValue0 ;
wave.projectionSingularValue1 = projectionSingularValue1 ;