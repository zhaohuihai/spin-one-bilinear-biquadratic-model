function [wave, truncationError, coef] = simpleUpdate(parameter, projectionOperator, wave)
%* Tensor to update: wave.A{1}, wave.A{2}, wave.Lambda{1}

%* T1(a3,a1,a6,a4,m1) T2(a1,a2,a4,a5,m2)
[T1, T2] = computeProjection(parameter, projectionOperator, wave.A{1}, wave.A{2}) ;
% ===============truncation============

alpha = parameter.project.alpha ;

%* step A: absorb lambdas
%* M1((a6,a4,a3,m1),a1) = T1(a3,a1,a6,a4,m1)*sqrt(L3(a3))*sqrt(L6(a6))*sqrt(L4(a4))
M1 = computeA1_sqrtL3_sqrtL6_sqrtL4(T1, wave.Lambda{3}, wave.Lambda{6}, wave.Lambda{4}, alpha) ;
%* M2((a2,a4,a5,m2),a1) = T2(a1,a2,a4,a5,m2)*sqrt(L2(a2))*sqrt(L4(a4))*sqrt(L5(a5))
M2 = computeA2_sqrtL2_sqrtL4_sqrtL5(T2, wave.Lambda{2}, wave.Lambda{4}, wave.Lambda{5}, alpha) ;

%* step B: QR factorization
%* M1((a6,a4,a3,m1),a1) = sum{a1'}_[Q1((a6,a4,a3,m1),a1')*R1(a1',a1)]
[~, R1] = qr(M1, 0) ;
R1 = R1 ./ norm(R1) ;

%* M2((a2,a4,a5,m2),a1) = sum{a1"}_[Q2((a2,a4,a5,m2),a1")*R2(a1",a1)]
[~, R2] = qr(M2, 0) ;
R2 = R2 ./ norm(R2) ;

%* step C: SVD and truncation
%* M(a1',a1") = sum{a1}_[R1(a1',a1)*R2(a1",a1)]
M = R1 * (R2.') ;

%* M(a1',a1") = sum{a1}_[U(a1',a1)*S(a1)*conj(V(a1",a1))]
[U0, S0, V0, coef] = applySVD(M) ;

Dcut = parameter.bondDimension ;
[U, S, V, truncationError] = truncate(U0, S0, V0, Dcut) ;

sqrtS = sqrt(S) ;
%* step D: Create Projector
%* change index notation: R1(a1',a1) -> R1(a1",a1'); R2(a1",a1) -> R2(a1",a1')
%* PR(a1',a1) = sum{a1"}_[R2(a1",a1')*V(a1",a1)/sqrtS(a1)]
PR = createProjector(R2, V, sqrtS) ;

%* PL(a1',a1) = sum{a1"}_[R1(a1",a1')*conj(U(a1",a1))/sqrtS(a1)]
U = conj(U) ;
PL = createProjector(R1, U, sqrtS) ;

%* step E: Form new system sites: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* A1(a3,a1,a6,a4,m1) = sum{a1'}_[PR(a1',a1)*T1(a3,a1',a6,a4,m1)]
wave.A{1} = contractTensors(PR, 2, 1, T1, 5, 2, [2, 1, 3, 4, 5]) ;

% wave.A{1} = wave.A{1} ./ max(max(max(max(abs(wave.A{1}))))) ;

%* A2(a1,a2,a4,a5,m2) = sum{a1'}_[PL(a1',a1)*T2(a1',a2,a4,a5,m2)]
wave.A{2} = contractTensors(PL, 2, 1, T2, 5, 1) ;

% wave.A{2} = wave.A{2} ./ max(max(max(max(abs(wave.A{2}))))) ;

wave.Lambda{1} = S ./ coef ;
% wave.Lambda{1} = S ;

wave.PR{1} = PR ;
wave.PL{1} = PL ;
