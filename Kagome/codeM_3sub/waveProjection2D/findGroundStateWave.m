function wave = findGroundStateWave(parameter, wave)


if parameter.loadPreviousWave == 1
    if parameter.project.polarization == 1
        warning('Old wave is loaded, unnecessary to polarize')
        disp('The projection process will NOT polarize the wave function')
        parameter.project.polarization = 0 ;
    end
end

% use simple update to polarize
if parameter.project.polarization == 1
    wave = polarizeWave(parameter, wave) ;
end
%-------------------------------------------------------------------------------------------------------------
if parameter.project.method == 1
    disp('simple update') ;
    wave = applyWaveProjection(parameter, wave) ;
end




