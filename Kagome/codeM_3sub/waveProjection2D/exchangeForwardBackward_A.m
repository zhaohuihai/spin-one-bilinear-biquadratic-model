function A = exchangeForwardBackward_A(parameter, A0)

M = parameter.siteDimension ;

A0dim = A0.dim ;
Atensor = zeros([A0dim(3), A0dim(4), A0dim(1), A0dim(2), M]) ;
for i = 1 : M
    %* Am(zf,xf,zb,xb)
    Am = A0.tensor4(:, :, :, :, i) ;
    %* Am((zf,xf),(zb,xb))
    Am = reshape(Am, [A0dim(1) * A0dim(2), A0dim(3) * A0dim(4)]) ;
    %* Am((zb,xb),(zf,xf))
    Am = Am' ;
    %* Am(zb,xb,zf,xf)
    Am = reshape(Am, [A0dim(3), A0dim(4), A0dim(1), A0dim(2)]) ;
    Atensor(:, :, :, :, i) = Am ;
end

A.dim = [A0dim(3), A0dim(4), A0dim(1), A0dim(2)] ;
A.tensor4 = Atensor ;