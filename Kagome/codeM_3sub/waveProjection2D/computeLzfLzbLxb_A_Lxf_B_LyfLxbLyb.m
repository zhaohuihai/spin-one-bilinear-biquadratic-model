function AB = computeLzfLzbLxb_A_Lxf_B_LyfLxbLyb(parameter, wave)

%* AB(zf,zb,xb,yf,xb,yb,mi,mj) = 
%* sum{xf}_(L5(zf)*L6(zb)*L2(xb)*A(zf,xf,zb,xb,mi)*L1(xf)*B(xf,yf,xb,yb,mj)*L3(yf)*L2(xb)*L4(yb))

M = parameter.siteDimension ;

A = wave.A ;
B = wave.B ;
Atensor = A.tensor4 ;
Btensor = B.tensor4 ;
Lambda = wave.Lambda ;

%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) * L5(zf)
for i = 1 : Lambda(5).dim
    Atensor(i, :, :, :, :) = Atensor(i, :, :, :, :) * Lambda(5).tensor1(i) ;
end
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) * L6(zb)
for i = 1 : Lambda(6).dim
    Atensor(:, :, i, :, :) = Atensor(:, :, i, :, :) * Lambda(6).tensor1(i) ;
end
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) * L2(xb)
for i = 1 : Lambda(2).dim
    Atensor(:, :, :, i, :) = Atensor(:, :, :, i, :) * Lambda(2).tensor1(i) ;
end
%* A(zf,xf,zb,xb,mi) = A(zf,xf,zb,xb,mi) * L1(xf)
for i = 1 : Lambda(1).dim
    Atensor(:, i, :, :, :) = Atensor(:, i, :, :, :) * Lambda(1).tensor1(i) ;
end
%* A(zf,zb,xb,mi,xf)
Atensor = permute(Atensor, [1, 3, 4, 5, 2]) ;
d1 = A.dim(1) * A.dim(3) * A.dim(4) * M ;
d2 = A.dim(2) ;
%* A((zf,zb,xb,mi),xf)
Atensor = reshape(Atensor, [d1, d2]) ;

%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) * L3(yf)
for i = 1 : Lambda(3).dim
    Btensor(:, i, :, :, :) = Btensor(:, i, :, :, :) * Lambda(3).tensor1(i) ;
end
%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) * L2(xb)
for i = 1 : Lambda(2).dim
    Btensor(:, :, i, :, :) = Btensor(:, :, i, :, :) * Lambda(2).tensor1(i) ;
end
%* B(xf,yf,xb,yb,mj) = B(xf,yf,xb,yb,mj) * L4(yb)
for i = 1 : Lambda(4).dim
    Btensor(:, :, :, i, :) = Btensor(:, :, :, i, :) * Lambda(4).tensor1(i) ;
end
d1 = B.dim(1) ;
d2 = B.dim(2) * B.dim(3) * B.dim(4) * M ;
%* B(xf,(yf,xb,yb,mj))
Btensor = reshape(Btensor, [d1, d2]) ;
%* AB((zf,zb,xb,mi),(yf,xb,yb,mj))
ABtensor = Atensor * Btensor ;
dim = [A.dim(1), A.dim(3), A.dim(4), M, B.dim(2), B.dim(3), B.dim(4), M] ;
%* AB(zf,zb,xb,mi,yf,xb,yb,mj)
ABtensor = reshape(ABtensor, dim) ;
%* AB(zf,zb,xb,yf,xb,yb,mi,mj)
ABtensor = permute(ABtensor, [1, 2, 3, 5, 6, 7, 4, 8]) ;

AB.dim = [A.dim(1), A.dim(3), A.dim(4), B.dim(2), B.dim(3), B.dim(4)] ;
AB.tensor6 = ABtensor ;
