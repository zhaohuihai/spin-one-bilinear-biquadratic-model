function wave = initializeWave(parameter)
%* 3 sublattice wave function

randSeed = parameter.randSeed ;
if randSeed >= 0 
    rng(randSeed) ;
else
    rng('shuffle') ;
end

M = parameter.siteDimension ;
D = parameter.bondDimension ;

if parameter.sameInitialTensor == 1
    
    if parameter.complexWave == 1
        A = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
    else
        A = rand(D, D, D, D, M) ;
    end
    L = sort(rand(D, 1), 'descend') ;
    L = L ./ L(1) ;
    %* A1(a3,a1,a6,a4,m1) A2(a1,a2,a4,a5,m2) A3(a2,a3,a5,a6,m3)
    for i = 1 : 3
        wave.A{i} = A ;
    end
    for i = 1 : 6
        wave.Lambda{i} = L ;
    end
else %* == 0
    for i = 1 : 3
        if parameter.complexWave == 1
            wave.A{i} = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
        else
            wave.A{i} = rand(D, D, D, D, M) ;
        end
    end
    for i = 1 : 6
        L = sort(rand(D, 1), 'descend') ;
        wave.Lambda{i} = L ./ L(1) ;
    end
end

%------------------------------------------------------------------------------
for i = 1 : 3
    A = wave.A{i} ;
    B = A ;
    for j = 1 : 3
        A = permute(A, [2, 3, 4, 1, 5]) ;
        B = B + A ;
    end
    B = B ./ 4 ;
    wave.A{i} = B ;
end

