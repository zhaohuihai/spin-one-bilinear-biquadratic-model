function Lambda = exchangeForwardBackward_Lambda(Lambda0)

Lambda(1) = Lambda0(2) ;
Lambda(3) = Lambda0(4) ;
Lambda(5) = Lambda0(6) ;

Lambda(2) = Lambda0(1) ;
Lambda(4) = Lambda0(3) ;
Lambda(6) = Lambda0(5) ;
