function wave = reverseRotateWave(wave0)

%* A->C, B->A, C->B 

wave.A = wave0.C ;
wave.B = wave0.A ;
wave.C = wave0.B ;

wave.Lambda(1 : 2) = wave0.Lambda(5 : 6) ;
wave.Lambda(3 : 4) = wave0.Lambda(1 : 2) ;
wave.Lambda(5 : 6) = wave0.Lambda(3 : 4) ;