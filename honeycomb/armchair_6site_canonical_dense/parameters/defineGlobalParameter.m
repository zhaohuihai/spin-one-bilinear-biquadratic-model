function parameter = defineGlobalParameter

%* yes: 1, no: 0
parameter.loadPreviousWave = 0 ; %*
%* yes: 1, no: 0
parameter.projection = 1 ; 
%********************************************************************
parameter.numCPUthreads = 4 ;

%* yes: 1, no: 0
parameter.useGPU = 0 ;
%* GPU_ID = 0, 1, 2, 3
parameter.GPU_ID = 0 ;
%********************************************************************
parameter.computation.expectationValue = 1 ;

%* spontaneous magnetization
parameter.computation.Sx = 0 ;
parameter.computation.Sy = 0 ;
parameter.computation.Sz = 1 ;

%* staggered spontaneous magnetization
parameter.computation.stagSx = 0 ;
parameter.computation.stagSy = 0 ;
parameter.computation.stagSz = 1 ;

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
parameter.computation.Qxx = 0 ;
parameter.computation.Qyy = 0 ;
parameter.computation.Qxx_yy = 0 ;
parameter.computation.Qzz = 1 ;
parameter.computation.Qxy = 0 ;
parameter.computation.Qyz = 0 ;
parameter.computation.Qzx = 0 ;

%* staggered quadrupole
parameter.computation.stagQxx = 0 ;
parameter.computation.stagQyy = 0 ;
parameter.computation.stagQxx_yy = 0 ;
parameter.computation.stagQzz = 1 ;
parameter.computation.stagQxy = 0 ;
parameter.computation.stagQyz = 0 ;
parameter.computation.stagQzx = 0 ;

% energy computation
parameter.computation.energy = 1 ;
parameter.computation.energyDerivative = 0 ;

%* nearest neighbor spin correlation
parameter.computation.nearSpinCorrelation = 1 ;
%**********************************************************
%* correlation: armchair direction
parameter.computation.correlation = 0 ;
parameter.computation.SzCorrelation = 1 ;
parameter.computation.QzzCorrelation = 1 ;
parameter.computation.maxCorrelationLength = 20 ;
