function parameter = setParameter

parameter = defineGlobalParameter ;

parameter = defineWaveParameter(parameter) ;
parameter = defineProjectionParameter(parameter) ;
parameter = defineTMparameter(parameter) ;
parameter = defineModelParameter(parameter) ;

%-------------------------------------------------------------------------------------------
disp(['set maximum number of computational threads = ', num2str(parameter.numCPUthreads)]) ;
maxNumCompThreads(parameter.numCPUthreads) ;

if parameter.useGPU == 1
    disp(['Use GPU: ',  num2str(parameter.GPU_ID)])
    addpath /usr/local/jacket/engine
    gselect(parameter.GPU_ID)
else
    disp('Use CPU only')
end





