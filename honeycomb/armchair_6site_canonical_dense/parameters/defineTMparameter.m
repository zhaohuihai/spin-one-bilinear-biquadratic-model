function parameter = defineTMparameter(parameter)

%* dimension of MPS
parameter.dim_MPS = 20 ;
parameter.dim_MPS_initial = 10 ;
parameter.dim_MPS_incre = 10 ;

parameter.convergenceCriterion_TMprojection = 1e-10 ;
parameter.convergenceCriterion_power = 1e-10 ;

parameter.maxTMstep = 1e3 ;
parameter.maxPowerStep = 1e3 ;
