function parameter = defineProjectionParameter(parameter)

%****************************************************************
%* projection sequence:
%* 1. 1,2,3,4,5,6,7,8,9
%* 2. 1,4,7,2,5,8,3,6,9
parameter.projectionSequence = 1 ;
%****************************************************************
%* projection method: 1. simple update; 2. cluster update
parameter.projectionMethod = 1 ;
%****************************************************************
parameter.polarization = 1 ;
parameter.polarField = 1 ;
parameter.field = 1e-8 ;

parameter.fieldInitial = 0.002 ;
parameter.fieldFinal = 1 ;
parameter.fieldIncre = 0.01 ;
%* FM
parameter.dipolarField_x = 0 ;
parameter.dipolarField_z = 0 ;
%* AFM
parameter.stagDipolarField_x = 0 ;
parameter.stagDipolarField_z = 1 ;
%* FQ
parameter.quadrupolarField_x = 0 ;
parameter.quadrupolarField_z = 0 ;
%* AFQ
parameter.stagQuadrupolarField_x = 0 ;
parameter.stagQuadrupolarField_z = 0 ;
parameter.stagQuadrupolarField_xx_yy = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************
parameter.VBCinitial = 0 ;
parameter.VBCpolarRatio = 1 ; % 1: not polarized
parameter.VBCanisotropyRatio = 1 ;

parameter.plaquetteVBC = 0 ;
parameter.columnarVBC = 0 ;
parameter.staggeredVBC = 0 ;
parameter.convergenceCriterion_VBC = 1e-4 ;
%*****************************************************************
parameter.symmetryWave = 0 ;
%*****************************************************************
parameter.TrotterOrder = 1 ;

parameter.tauInitial = 1e-1 ;
parameter.tauFinal = 1e-2 ; %*
parameter.tauChangeFactor = 1 ;

parameter.convergenceCriterion_projection = 1e-8 ;
parameter.maxProjectionStep = 1e4 ;