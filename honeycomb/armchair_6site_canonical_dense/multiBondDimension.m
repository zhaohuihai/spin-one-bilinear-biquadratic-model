
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

parameter.bondDimension = parameter.bondDimensionInitial ;

Chi = parameter.dim_MPS ;

prefix = ['Chi', num2str(Chi), '_D'] ;

i = 1 ;
while parameter.bondDimension <= parameter.bondDimensionFinal
    
    if parameter.loadPreviousWave == 0
        wave = initializeWave(parameter) ;
        disp('create new wave')
    else
        [wave, parameter] = loadWave(parameter) ;
    end
    
    if parameter.projection == 1
        if parameter.TrotterOrder == 1
            disp('apply first order Trotter decomposition')
        elseif parameter.TrotterOrder == 2
            disp('apply second order Trotter decomposition')
        end
        wave = findGroundStateWave(parameter, wave) ;
    end
    
    
    fileName = [prefix, num2str(parameter.bondDimension), '.txt'] ;
    delete(fileName) ;
    diary(fileName) ;
    diary on
    
    computeExpectationValue_dense(parameter, wave)
%     entangleEntropy = computeEntangleEntropy(parameter, wave)
    diary off ;
    
    parameter.bondDimension = parameter.bondDimension + parameter.bondDimensionIncre ;
    i = i + 1 ;
    
    parameter.loadPreviousWave = 1 ;
end
