function H = createEnergyDerivative(parameter)
%* first derivative of energy

theta = parameter.theta ;

H_bl = createHamiltonian_Heisenberg(parameter) ;

H = - sin(theta) * H_bl + cos(theta) * (H_bl * H_bl) ;

H = setSmalltoZero(H) ;

