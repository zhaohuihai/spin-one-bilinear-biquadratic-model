function H = createSzSz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

H = zeros(M, M, M, M) ;
MM = M * M ;

%* H(m1, m2, n1, n2) = <m1, m2| H | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        H(q1, q2, q1, q2) = H(q1, q2, q1, q2) + m1 * m2 ;
    end
end

H = reshape(H, [MM, MM]) ; %* H((m1, m2), (n1, n2))