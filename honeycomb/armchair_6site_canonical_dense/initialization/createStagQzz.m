function stagQzz = createStagQzz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

stagQzz = zeros(M, M, M, M) ;
MM = M * M ;
%* stagQzz = Sz1^2 - Sz2^2
%* stagQzz(m1, m2, n1, n2) = <m1, m2| stagQzz | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        stagQzz(q1, q2, q1, q2) = stagQzz(q1, q2, q1, q2) + (m1^2 - m2^2) / 2 ;
    end
end

stagQzz = reshape(stagQzz, [MM, MM]) ;