function Qxx_yy = createQxx_yy(parameter)

Qxx = createQxx(parameter) ;
Qyy = createQyy(parameter) ;

Qxx_yy = Qxx - Qyy ;