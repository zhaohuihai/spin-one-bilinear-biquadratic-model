function [correlation, correlationM] = findCorrelationFunction(parameter, t, rightMPS, leftMPS, A, VR, VL, PR, PL, eta)

%* normFactor = sum{c',l,c}_[VL(c',l,c)*VR(c,l,c')]
normFactor = contractTensors(VL, 3, [1, 2, 3], VR, 3, [3, 2, 1]) ;

%* QR(j,a,i,c)
[QR, ~, ~, ~, ~] = createSpecialTensor(parameter, parameter.AsiteOperator, t, rightMPS, leftMPS, A) ;
[Asite, QR, PL] = contractFinal(VL, 3, QR, 4, PL, 4, VR, 3) ;
clear QR
Asite = Asite / (eta * normFactor) ;

%* QR(j,a,i,c)
[QR, ~, ~, ~, ~] = createSpecialTensor(parameter, parameter.BsiteOperator, t, rightMPS, leftMPS, A) ;
[Bsite, QR, PL] = contractFinal(VL, 3, QR, 4, PL, 4, VR, 3) ;
clear QR
Bsite = Bsite / (eta * normFactor) ;

oddDistance = Asite * Bsite ;
evenDistance = Asite * Asite ;
%*==============================================================================
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)
[QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, parameter.correlationOperator, t, rightMPS, leftMPS, A) ;

L = parameter.computation.maxCorrelationLength ;
correlation = zeros(L, 1) ;

coef = 1 ;

for i = 1 : L
    %* distance <= 3: VL(a',j,a); distance >= 4: VL(a',j,a,n)
    if i == 4
        [VL, c, QRa, PL] = updateVector(VL, 3, QRa, 5, PL, 4) ;
        coef = coef * c ;
    elseif mod(i, 4) == 0
        [VL, c, PR, PL] = updateVector(VL, 4, PR, 4, PL, 4) ;
        coef = coef * c ;
    end
    [correlation(i), QR, QRa, QRb, QLa, QLb, PR, PL] = findCorrelation(i, QR, QRa, QRb, QLa, QLb, VR, VL, eta, PR, PL, coef, normFactor) ;
end
%===================================================================================================
correlationM = zeros(L, 1) ;
%* odd distance
correlationM(1 : 2 : L) = correlation(1 : 2 : L) - oddDistance ;
%* even distance
correlationM(2 : 2 : L) = correlation(2 : 2 : L) - evenDistance ;
