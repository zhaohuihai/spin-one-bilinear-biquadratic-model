function pVBC = computePlaquetteOrder(nearSpinCorrelationBond)

% P = sum(nearSpinCorrelationBond([1, 2, 5, 6, 7, 9])) / (sum(nearSpinCorrelationBond([3, 4, 8])) * 2) - 1 ;

P = zeros(1, 3) ;

P(1) = sum(nearSpinCorrelationBond([1, 3, 4, 5, 8, 9])) / (sum(nearSpinCorrelationBond([2, 6, 7])) * 2) - 1 ;

P(2) = sum(nearSpinCorrelationBond([1, 2, 5, 6, 7, 9])) / (sum(nearSpinCorrelationBond([3, 4, 8])) * 2) - 1 ;

P(3) = sum(nearSpinCorrelationBond([2, 3, 4, 6, 7, 8])) / (sum(nearSpinCorrelationBond([1, 5, 9])) * 2) - 1 ;

pVBC = max(P) ;