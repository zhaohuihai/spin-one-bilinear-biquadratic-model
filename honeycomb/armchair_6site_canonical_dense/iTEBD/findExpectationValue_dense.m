function expectValue = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q)

eta = (etaRight + etaLeft) / 2 ;

h = vectorLeft' * Q * vectorRight ;
g = eta .* vectorLeft' * vectorRight ;

expectValue = h / g ;