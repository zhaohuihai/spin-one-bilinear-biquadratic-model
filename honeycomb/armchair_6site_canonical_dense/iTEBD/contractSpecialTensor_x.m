function expectValueBond = contractSpecialTensor_x(parameter, t, rightMPS, leftMPS, A, B)

%* P((c,c'),(e,e'))
P = createTransferMatrix0D_x(parameter, t, rightMPS, leftMPS) ;

D = parameter.dim_MPS ;
vector = rand(D^2, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

[vectorRight, etaRight] = applyPowerMethod_dense(parameter, P, vector) ;
P = P' ;
[vectorLeft, etaLeft] = applyPowerMethod_dense(parameter, P, vectorRight) ;
%*==================================================================================
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Sx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Sx = Sx ;
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Sy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Sy = Sy ;
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Sz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['Sz x bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagSx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagSx = stagSx ;
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagSy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagSy = stagSy ;
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagSz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['stagSz x bond = ', num2str(stagSz)]) ;
    expectValueBond.stagSz = stagSz ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qxx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qxx = Qxx ;
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qyy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qyy = Qyy ;
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qzz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['Qzz x bond = ', num2str(Qzz)]) ;
    expectValueBond.Qzz = Qzz ;
end
if computation.Qxx_yy == 1
    operator = createQxx_yy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qxx_yy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['Qxx_yy x bond = ', num2str(Qxx_yy)]) ;
    expectValueBond.Qxx_yy = Qxx_yy ;
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qxy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qxy = Qxy ;
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qyz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qyz = Qyz ;
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    Qzx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qzx = Qzx ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQxx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxx = stagQxx ;
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQyy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQyy = stagQyy ;
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQzz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['stagQzz x bond = ', num2str(stagQzz)]) ;
    expectValueBond.stagQzz = stagQzz ;
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQxx_yy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQxy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxy = stagQxy ;
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQyz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQyz = stagQyz ;
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    stagQzx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQzx = stagQzx ;
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    energy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['energy x bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
end
%* energy derivative
if computation.energyDerivative == 1
    operator = createEnergyDerivative(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    energyDerivative = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['energy derivative x bond = ', num2str(energyDerivative)]) ;
    expectValueBond.energyDerivative = energyDerivative ;
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS) ;
    nearSpinCorrelation = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['nearest neighbor spin correlation x bond = ', num2str(nearSpinCorrelation)]) ;
    expectValueBond.nearSpinCorrelation = nearSpinCorrelation ;
end