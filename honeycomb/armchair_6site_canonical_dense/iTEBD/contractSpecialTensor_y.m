function expectValueBond = contractSpecialTensor_y(parameter, t, rightMPS, leftMPS, A, B)

% %* ABR((a,c),(i,j))
% ABR = computeA_B(parameter, rightMPS) ;
% 
% %* ABL((a',c'),(k,l))
% ABL = computeA_B(parameter, leftMPS) ;

%* PR((a,k),(c,l)) = sum{i,j,x,b}_(A(a,b,i)*t3(x,i,k)*B(b,c,j)*t4(x,l,j))
%* change notation: PR((a,j),(c,i))
PR = createPR(parameter, t{3}, t{4}, rightMPS) ;

%* PL((j,c'),(i,a')) = sum{k,l,x,b'}_(A(a',b',k)*t1(x,i,k)*B(b',c',l)*t2(x,l,j))
%* change notation: PL((l,c'),(i,a'))
PL = createPL(parameter, t{1}, t{2}, leftMPS) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,l,c'): column vector
vector = rand(D * d * D, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

%* vectorRight(a,j,a'): column vector
[vectorRight, etaRight] = applyPowerMethod_y_dense(parameter, PR, PL, vector) ;

%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))
[PR, PL] = transposeP_dense(parameter, PR, PL) ;

vectorLeft = reshape(vectorRight, [D, d, D]) ;
vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
vectorLeft = reshape(vectorLeft, [D * d * D, 1]) ;

%* vectorLeft(c',l,c): column vector
[vectorLeft, etaLeft] = applyPowerMethod_y_dense(parameter, PL, PR, vectorLeft) ;
%*==========================================================================================================
%* QR((a,k),(c,ln))    change notation: QR((a,j),(c,in))
%* QL((j,c'),(in,a'))  change notation: QL((l,c'),(in,a'))
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Sx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Sx = Sx ;
    clear QR QL
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Sy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Sy = Sy ;
    clear QR QL
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Sz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['Sz y bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
    clear QR QL
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagSx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagSx = stagSx ;
    clear QR QL
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagSy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagSy = stagSy ;
    clear QR QL
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagSz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['stagSz y bond = ', num2str(stagSz)]) ;
    expectValueBond.stagSz = stagSz ;
    clear QR QL
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qxx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qxx = Qxx ;
    clear QR QL
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qyy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qyy = Qyy ;
    clear QR QL
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qzz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['Qzz y bond = ', num2str(Qzz)]) ;
    expectValueBond.Qzz = Qzz ;
    clear QR QL
end
if computation.Qxx_yy == 1
    operator = createQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qxx_yy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['Qxx_yy y bond = ', num2str(Qxx_yy)]) ;
    expectValueBond.Qxx_yy = Qxx_yy ;
    clear QR QL
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qxy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qxy = Qxy ;
    clear QR QL
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qyz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qyz = Qyz ;
    clear QR QL
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [Qzx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qzx = Qzx ;
    clear QR QL
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQxx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxx = stagQxx ;
    clear QR QL
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQyy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQyy = stagQyy ;
    clear QR QL
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQzz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['stagQzz y bond = ', num2str(stagQzz)]) ;
    expectValueBond.stagQzz = stagQzz ;
    clear QR QL
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQxx_yy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
    clear QR QL
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQxy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxy = stagQxy ;
    clear QR QL
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQyz, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQyz = stagQyz ;
    clear QR QL
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [stagQzx, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQzx = stagQzx ;
    clear QR QL
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [energy, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['energy y bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
    clear QR QL
end
%* energy derivative
if computation.energyDerivative == 1
    operator = createEnergyDerivative(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [energyDerivative, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['energy derivative y bond = ', num2str(energyDerivative)]) ;
    expectValueBond.energyDerivative = energyDerivative ;
    clear QR QL
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, t{3}, t{2}, rightMPS, leftMPS, A, B) ;
    [nearSpinCorrelation, QR, QL] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['nearest neighbor spin correlation y bond = ', num2str(nearSpinCorrelation)]) ;
    expectValueBond.nearSpinCorrelation = nearSpinCorrelation ;
    clear QR QL
end