function [rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, t)

% project from bottom to top
rightMPS = findDominantRightEigenMPS_dense(parameter, t) ;

%* transpose to left transferMatrix
%* t1,t3,t5: (x,i,k) -> (x,k,i)
%* t2,t4,t6: (x,l,j) -> (x,j,l)
%* 5 -> 1; 4 -> 2; 3 -> 3; 6 -> 6; 1 -> 5; 2 -> 4
t = transposeTransferMatrix1D(t) ;

leftMPS = findDominantRightEigenMPS_dense(parameter, t) ;