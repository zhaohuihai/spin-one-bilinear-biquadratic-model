function expectValueBond = dealWithSpecialTensor_y_dense(parameter, t, rightMPS, leftMPS, A)

rightMPS = computeSingleGate1D(parameter, t([5, 4]), rightMPS) ;

rightMPS_3 = computeSingleGate1D(parameter, t([3, 6]), rightMPS) ;
rightMPS_2 = computeSingleGate1D(parameter, t([1, 2]), rightMPS_3) ;
rightMPS_1 = computeSingleGate1D(parameter, t([5, 4]), rightMPS_2) ;

t_left = transposeTransferMatrix1D(t) ;

leftMPS_1 = leftMPS ;
leftMPS_2 = computeSingleGate1D(parameter, t_left([5, 4]), leftMPS_1) ;
leftMPS_3 = computeSingleGate1D(parameter, t_left([3, 6]), leftMPS_2) ;

%* 4
expectValueBond(1) = contractSpecialTensor_y(parameter, t([1, 2, 3, 6]), rightMPS_1, leftMPS_1, A{1}, A{6}) ;
%* 5
expectValueBond(2) = contractSpecialTensor_y(parameter, t([3, 6, 5, 4]), rightMPS_2, leftMPS_2, A{3}, A{4}) ;
%* 6
expectValueBond(3) = contractSpecialTensor_y(parameter, t([5, 4, 1, 2]), rightMPS_3, leftMPS_3, A{5}, A{2}) ;