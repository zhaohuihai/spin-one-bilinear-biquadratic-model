function averageBond(parameter, expectValueBond)

bondNo = length(expectValueBond) ;
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    SxBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        SxBond(i) = expectValueBond(i).Sx ;
    end
    disp ('Sx of every bond = ') ;
    disp(SxBond) ;
    Sx = mean(SxBond) ;    
    saveExpectationValue(parameter, Sx, 'Sx') ;
end
if computation.Sy == 1
    Sy = 0 ;
    for i = 1 : bondNo
        Sy = Sy + expectValueBond(i).Sy / bondNo ;
    end
    saveExpectationValue(parameter, Sy, 'Sy') ;
end
if computation.Sz == 1
    SzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        SzBond(i) = expectValueBond(i).Sz ;
    end
    disp ('Sz of every bond = ') ;
    disp(SzBond) ;
    Sz = mean(SzBond) ;    
    saveExpectationValue(parameter, Sz, 'Sz') ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    stagSxBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagSxBond(i) = expectValueBond(i).stagSx ;
    end
    disp ('stagSx of every bond = ') ;
    disp(stagSxBond) ;
    stagSx = mean(stagSxBond) ;
    saveExpectationValue(parameter, stagSx, 'stagSx') ;
end
if computation.stagSy == 1
    stagSy = 0 ;
    for i = 1 : bondNo
        stagSy = stagSy + expectValueBond(i).stagSy / bondNo ;
    end
    saveExpectationValue(parameter, stagSy, 'stagSy') ;
end
if computation.stagSz == 1
    stagSzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagSzBond(i) = expectValueBond(i).stagSz ;
    end
    disp ('stagSz of every bond = ') ;
    disp(stagSzBond) ;
    stagSz = mean(stagSzBond) ;
    saveExpectationValue(parameter, stagSz, 'stagSz') ;
end
%=======================================================
if computation.Sx == 1 && computation.stagSx == 1
    SxSite = findExpectValSite(SxBond, stagSxBond) ;
    disp('Sx of every site') ;
    disp(SxSite) ;
end
if computation.Sz == 1 && computation.stagSz == 1
    SzSite = findExpectValSite(SzBond, stagSzBond) ;
    disp('Sz of every site') ;
    disp(SzSite) ;
end
%=======================================================
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    Qxx = 0 ;
    for i = 1 : bondNo
        Qxx = Qxx + expectValueBond(i).Qxx / bondNo ;
    end
    saveExpectationValue(parameter, Qxx, 'Qxx') ;
end
if computation.Qyy == 1
    Qyy = 0 ;
    for i = 1 : bondNo
        Qyy = Qyy + expectValueBond(i).Qyy / bondNo ;
    end
    saveExpectationValue(parameter, Qyy, 'Qyy') ;
end
if computation.Qzz == 1
    QzzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        QzzBond(i) = expectValueBond(i).Qzz ;
    end
    disp ('Qzz of every bond = ') ;
    disp(QzzBond) ;
    Qzz = mean(QzzBond) ;
    saveExpectationValue(parameter, Qzz, 'Qzz') ;
end
if computation.Qxx_yy == 1
    Qxx_yyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        Qxx_yyBond(i) = expectValueBond(i).Qxx_yy ;
    end
    disp ('Qxx_yy of every bond = ') ;
    disp(Qxx_yyBond) ;
    Qxx_yy = mean(Qxx_yyBond) ;
    saveExpectationValue(parameter, Qxx_yy, 'Qxx_yy') ;
end
if computation.Qxy == 1
    Qxy = 0 ;
    for i = 1 : bondNo
        Qxy = Qxy + expectValueBond(i).Qxy / bondNo ;
    end
    saveExpectationValue(parameter, Qxy, 'Qxy') ;
end
if computation.Qyz == 1
    Qyz = 0 ;
    for i = 1 : bondNo
        Qyz = Qyz + expectValueBond(i).Qyz / bondNo ;
    end
    saveExpectationValue(parameter, Qyz, 'Qyz') ;
end
if computation.Qzx == 1
    Qzx = 0 ;
    for i = 1 : bondNo
        Qzx = Qzx + expectValueBond(i).Qzx / bondNo ;
    end
    saveExpectationValue(parameter, Qzx, 'Qzx') ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    stagQxx = 0 ;
    for i = 1 : bondNo
        stagQxx = stagQxx + expectValueBond(i).stagQxx / bondNo ;
    end
    saveExpectationValue(parameter, stagQxx, 'stagQxx') ;
end
if computation.stagQyy == 1
    stagQyy = 0 ;
    for i = 1 : bondNo
        stagQyy = stagQyy + expectValueBond(i).stagQyy / bondNo ;
    end
    saveExpectationValue(parameter, stagQyy, 'stagQyy') ;
end
if computation.stagQzz == 1
    stagQzzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagQzzBond(i) = expectValueBond(i).stagQzz ;
    end
    disp ('stagQzz of every bond = ') ;
    disp(stagQzzBond) ;
    stagQzz = mean(stagQzzBond) ;
    saveExpectationValue(parameter, stagQzz, 'stagQzz') ;
end
if computation.stagQxx_yy == 1
    stagQxx_yyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagQxx_yyBond(i) = expectValueBond(i).stagQxx_yy ;
    end
    disp ('stagQxx_yy of every bond = ') ;
    disp(stagQxx_yyBond) ;
    stagQxx_yy = mean(stagQxx_yyBond) ;
    saveExpectationValue(parameter, stagQxx_yy, 'stagQxx_yy') ;
end
if computation.stagQxy == 1
    stagQxy = 0 ;
    for i = 1 : bondNo
        stagQxy = stagQxy + expectValueBond(i).stagQxy / bondNo ;
    end
    saveExpectationValue(parameter, stagQxy, 'stagQxy') ;
end
if computation.stagQyz == 1
    stagQyz = 0 ;
    for i = 1 : bondNo
        stagQyz = stagQyz + expectValueBond(i).stagQyz / bondNo ;
    end
    saveExpectationValue(parameter, stagQyz, 'stagQyz') ;
end
if computation.stagQzx == 1
    stagQzx = 0 ;
    for i = 1 : bondNo
        stagQzx = stagQzx + expectValueBond(i).stagQzx / bondNo ;
    end
    saveExpectationValue(parameter, stagQzx, 'stagQzx') ;
end
%=======================================================
if computation.Qzz == 1 && computation.stagQzz == 1
    QzzSite = findExpectValSite(QzzBond, stagQzzBond) ;
    disp('Qzz of every site') ;
    disp(QzzSite) ;
end
if computation.Qxx_yy == 1 && computation.stagQxx_yy == 1
    Qxx_yySite = findExpectValSite(Qxx_yyBond, stagQxx_yyBond) ;
    disp('Qxx_yy of every site') ;
    disp(Qxx_yySite) ;
end
%=======================================================
% energy computation
if computation.energy == 1
    energyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyBond(i) = expectValueBond(i).energy ;
    end
    disp ('energy of every bond = ') ;
    disp(energyBond) ;
    energy = mean(energyBond) ;
    energy = energy * 1.5 ;
    saveExpectationValue(parameter, energy, 'energy') ;
end
%* energy derivative
if computation.energyDerivative == 1
    energyDerivativeBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyDerivativeBond(i) = expectValueBond(i).energyDerivative ;
    end
    disp ('energy derivative of every bond = ') ;
    disp(energyDerivativeBond) ;
    energyDerivative = mean(energyDerivativeBond) ;
    energyDerivative = energyDerivative * 1.5 ;
    saveExpectationValue(parameter, energyDerivative, 'energyDerivative') ;
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    nearSpinCorrelationBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        nearSpinCorrelationBond(i) = expectValueBond(i).nearSpinCorrelation ;
    end
    disp ('nearest neighbor spin correlation of every bond = ') ;
    disp(nearSpinCorrelationBond) ;
    P = computePlaquetteOrder(nearSpinCorrelationBond) ;
    disp(['plaquette order = ', num2str(P, 12)])
end