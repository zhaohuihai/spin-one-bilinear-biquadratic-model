function PL = createPL_z(parameter, Ta, Tb, MPS)
%* PL((a',i),(c',j)) = sum{k,l,x,b'}_(A(a',b',k)*Ta(x,i,k)*B(b',c',l)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* At(a',i,b',x) = sum{k}_[A(a',b',k)*Ta(x,i,k)]
At = contractTensors(MPS.A, 3, 3, Ta, 3, 3, [1, 4, 2, 3], parameter) ;

%* Bt(b',x,c',l) = sum{l}_[B(b',c',l)*Tb(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, Tb, 3, 2, [1, 3, 2, 4], parameter) ;

%* PL(a',i,c',l) = sum{b',x}_[At(a',i,b',x)*Bt(b',x,c',l)]
PL = contractTensors(At, 4, [3, 4], Bt, 4, [1, 2], parameter) ;
clear At Bt
%* PL((a',i),(c',j))
PL = reshape(PL, [D * d, D * d]) ;
