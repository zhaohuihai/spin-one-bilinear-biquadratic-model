function vector = updateVector_y_dense(parameter, vector, PR, PL)
%* vector1(a,j,a') = sum{c,c',l,i}_(PR((a,j),(c,i))*vector(c,l,c')*PL((l,c'),(i,a')))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* vector(c,(l,c'))
vector = reshape(vector, [D, d * D]) ;

%* vector(c,(i,a')) = sum{l,c'}_(vector(c,(l,c'))*PL((l,c'),(i,a')))
vector = vector * PL ;

%* vector((c,i),a')
vector = reshape(vector, [D * d, D]) ;

%* vector((a,j),a') = sum{c,i}_(PR((a,j),(c,i))*vector((c,i),a'))
vector = PR * vector ;

%* vector(a,j,a') 
vector = reshape(vector, [D * d * D, 1]) ;