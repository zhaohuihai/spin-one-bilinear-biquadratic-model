function [QR, QL] = createSpecialTensor_y_dense(parameter, operator, Ta, Tb, rightMPS, leftMPS, A, B)

% %* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
% %* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
% %* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
% [U, V] = decomposeHamiltonian_dense(parameter, operator) ;
% 
% %* Wa(x,in,k)
% Wa = createW_y_dense(parameter, A, U) ;
% %* Wb(x,ln,j)
% Wb = createW_y_dense(parameter, B, V) ;
% 
% %* THR((i,j),(k,ln))
% THR = contractTa_Wb_y_dense(parameter, Ta, Wb) ;
% %* QR((a,k),(c,ln))
% [QR, THR] = contractABR_TH_dense(parameter, ABR, THR) ;
% clear THR
% 
% %* THL((k,l),(in,j))
% THL = contractTb_Wa_y_dense(parameter, Tb, Wa) ;
% %* QL((j,c'),(in,a'))
% [QL, THL] = contractABL_TH_dense(parameter, ABL, THL) ;
% clear THL



%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, operator, A, B) ;
clear A B

%* Ar(a,k,b,x) = sum{i}_[rightMPS.A(a,b,i)*Ta(x,i,k)]
Ar = contractTensors(rightMPS.A, 3, 3, Ta, 3, 2, [1, 4, 2, 3], parameter) ;
clear Ta
%* Br(b,x,c,l,n) = sum{j}_[rightMPS.B(b,c,j)*TCb(x,l,j,n)]
Br = contractTensors(rightMPS.B, 3, 3, TCb, 4, 3, [1, 3, 2, 4, 5], parameter) ;
clear rightMPS TCb

%* QR(a,k,c,l,n) = sum{b,x}_[Ar(a,k,b,x)*Br(b,x,c,l,n)]
QR = contractTensors(Ar, 4, [3, 4], Br, 5, [1, 2], parameter) ;
clear Ar Br

dim = size(QR) ;
d1 = prod(dim([1, 2])) ;
d2 = prod(dim([3, 4, 5])) ;

%* QR((a,k), (c,ln))
QR = reshape(QR, [d1, d2]) ;

%* Al(x,b',i,n,a') = sum{k}_[leftMPS.A(a',b',k)*TCa(x,i,k,n)]
Al = contractTensors(leftMPS.A, 3, 3, TCa, 4, 3, [3, 2, 4, 5, 1], parameter) ;
clear TCa
%* Bl(j,c',x,b') = sum{l}_[leftMPS.B(b',c',l)*Tb(x,l,j)]
Bl = contractTensors(leftMPS.B, 3, 3, Tb, 3, 2, [4, 2, 3, 1], parameter) ;
clear Tb
%* QL(j,c',i,n,a') = sum{x,b'}_[Bl(j,c',x,b')*Al(x,b',i,n,a')]
QL = contractTensors(Bl, 4, [3, 4], Al, 5, [1, 2], parameter) ;
clear Al Bl

dim = size(QL) ;
d1 = prod(dim([1, 2])) ;
d2 = prod(dim([3, 4, 5])) ;

%* QL((j,c'),(in,a'))
QL = reshape(QL, [d1, d2]) ;


