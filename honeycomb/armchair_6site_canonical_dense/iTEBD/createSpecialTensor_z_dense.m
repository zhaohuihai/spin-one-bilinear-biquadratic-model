function [QR, QL] = createSpecialTensor_z_dense(parameter, operator, Ta, Tb, rightMPS, leftMPS, A, B)

% %* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
% %* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
% %* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
% [U, V] = decomposeHamiltonian_dense(parameter, operator) ;
% 
% %* Wa(x,i,kn)
% Wa = createW_z_dense(parameter, A, U) ;
% %* Wb(x,l,jn)
% Wb = createW_z_dense(parameter, B, V) ;
% 
% %* THR((i,j),(kn,l))
% THR = contractTb_Wa_z_dense(parameter, Tb, Wa) ;
% %* QR((l,c),(kn,a)) = sum{i,j}_(ABR((a,c),(i,j))*THR((i,j),(kn,l)))
% [QR, THR] = contractABL_TH_dense(parameter, ABR, THR) ;
% clear THR
% 
% %* THL((k,l),(i,jn))
% THL = contractTa_Wb_z_dense(parameter, Ta, Wb) ;
% %* QL((a',i),(c',jn)) = sum{k,l}_(ABL((a',c'),(k,l))*THL((k,l),(i,jn)))
% [QL, THL] = contractABR_TH_dense(parameter, ABL, THL) ;
% clear THL

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, operator, A, B) ;
clear A B

%* Ar(b,x,k,n,a) = sum{i}_[rightMPS.A(a,b,i)*TCa(x,i,k,n)]
Ar = contractTensors(rightMPS.A, 3, 3, TCa, 4, 2, [2, 3, 4, 5, 1], parameter) ;
clear TCa
%* Br(l,c,b,x) = sum{j}_[rightMPS.B(b,c,j)*Tb(x,l,j)]
Br = contractTensors(rightMPS.B, 3, 3, Tb, 3, 3, [4, 2, 1, 3], parameter) ;
clear rightMPS Tb
%* QR(l,c,k,n,a) = sum{b,x}_[Br(l,c,b,x)*Ar(b,x,k,n,a)]
QR = contractTensors(Br, 4, [3, 4], Ar, 5, [1, 2], parameter) ;
clear Ar Br

dim = size(QR) ;
d1 = prod(dim([1, 2])) ;
d2 = prod(dim([3, 4, 5])) ;

%* QR((l,c),(kn,a))
QR = reshape(QR, [d1, d2]) ;
%---------------------------------------------------------------------
%* Al(a',i,b',x) = sum{k}_[leftMPS.A(a',b',k)*Ta(x,i,k)]
Al = contractTensors(leftMPS.A, 3, 3, Ta, 3, 3, [1, 4, 2, 3], parameter) ;
clear Ta
%* Bl(b',x,c',j,n) = sum{l}_[leftMPS.B(b',c',l)*TCb(x,l,j,n)]
Bl = contractTensors(leftMPS.B, 3, 3, TCb, 4, 2, [1, 3, 2, 4, 5], parameter) ;
clear leftMPS TCb
%* QL(a',i,c',j,n) = sum{x,b'}_[Al(a',i,b',x)*Bl(b',x,c',j,n)]
QL = contractTensors(Al, 4, [3, 4], Bl, 5, [1, 2], parameter) ;
clear Al Bl

dim = size(QL) ;
d1 = prod(dim([1, 2])) ;
d2 = prod(dim([3, 4, 5])) ;

%* QL((a',i),(c',jn))
QL = reshape(QL, [d1, d2]) ;