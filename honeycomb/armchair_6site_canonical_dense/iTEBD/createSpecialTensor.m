function [QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, operator, t, rightMPS, leftMPS, A)
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)

%* ABR(a,c,i,j) = sum{b}_[A(a,b,i)*B(b,c,j)]
ABR = contractTensors(rightMPS.A, 3, 2, rightMPS.B, 3, 1, [1, 3, 2, 4]) ;
%* ABL(a',c',k,l) = sum{b'}_[A(a',b',k)*B(b',c',l)]
ABL = contractTensors(leftMPS.A, 3, 2, leftMPS.B, 3, 1, [1, 3, 2, 4]) ;

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa1, TCb1] = createTCa_TCb(parameter, operator, A{1}, A{2}) ;
[TCa2, TCb2] = createTCa_TCb(parameter, operator, A{3}, A{4}) ;
%==============================================================
%* W(i,k,l,j) = sum{x,n}_[TCa(x,i,k,n)*TCb(x,l,j,n)]
W = contractTensors(TCa1, 4, [1, 4], TCb1, 4, [1, 4]) ;

%* QR(a,c,k,l) = sum{i,j}_[ABR(a,c,i,j)*W(i,k,l,j)]
QR = contractTensors(ABR, 4, [3, 4], W, 4, [1, 4]) ;
%* change notation: QR(a,c,j,i)
clear W
%* QR(j,a,i,c)
QR = permute(QR, [3, 1, 4, 2]) ;
%==============================================================
%* Wa(i,k,n,l,j) = sum{x}_[TCa1(x,i,k,n)*t2(x,l,j)]
WaR = contractTensors(TCa1, 4, 1, t{2}, 3, 1) ;

%* QRa(k,n,l,a,c) = sum{i,j}_[Wa(i,k,n,l,j)*ABR(a,c,i,j)]
QRa = contractTensors(WaR, 5, [1, 5], ABR, 4, [3, 4]) ;
%* change notation: QRa(j,n,i,a,c)
%* QRa(j,a,i,c,n)
QRa = permute(QRa, [1, 4, 3, 5, 2]) ;

%* WaL(i,k,n,l,j) = sum{x}_[TCa2(x,i,k,n)*t4(x,l,j)]
WaL = contractTensors(TCa2, 4, 1, t{4}, 3, 1) ;
%* QLa(i,n,j,a',c') = sum{k,l}_[Wa(i,k,n,l,j)*ABL(a',c',k,l)]
QLa = contractTensors(WaL, 5, [2, 4], ABL, 4, [3, 4]) ;
clear WaR WaL
%* change notation: QLa(i,n,l,a',c')
%* QLa(c',l,a',i,n)
QLa = permute(QLa, [5, 3, 4, 1, 2]) ;

%* Wb(i,k,l,j,n) = sum{x}_[t1(x,i,k)*TCb1(x,l,j,n)]
WbR = contractTensors(t{1}, 3, 1, TCb1, 4, 1) ;

%* QRb(k,l,n,a,c) = sum{i,j}_[Wb(i,k,l,j,n)*ABR(a,c,i,j)]
QRb = contractTensors(WbR, 5, [1, 4], ABR, 4, [3, 4]) ;
%* change notation: QRb(j,i,n,a,c)
%* QRb(j,a,i,c,n)
QRb = permute(QRb, [1, 4, 2, 5, 3]) ;

%* Wb(i,k,l,j,n) = sum{x}_[t3(x,i,k)*TCb2(x,l,j,n)]
WbL = contractTensors(t{3}, 3, 1, TCb2, 4, 1) ;
%* QLb(i,j,n,a',c') = sum{k,l}_[Wb(i,k,l,j,n)*ABL(a',c',k,l)]
QLb = contractTensors(WbL, 5, [2, 3], ABL, 4, [3, 4]) ;
clear WbR WbL
%* change notation: QLb(i,l,n,a',c')
%* QLb(c',l,a',i,n)
QLb = permute(QLb, [5, 2, 4, 1, 3]) ;



