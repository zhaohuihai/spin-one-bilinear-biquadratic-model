function R = projectTransferMatrix_dense(parameter, t, MPS)
%* R((a, k), (c, l)) = sum{i,j,x}_[A(a,b,i)*t{1}(x,i,k)*B(b,c,j)*t{2}(x,l,j)]

d = parameter.bondDimension^2 ;
D = size(MPS.A, 1) ;

%* At(a,k,b,x) = sum{i}_[A(a,b,i)*t{1}(x,i,k)]
At = contractTensors(MPS.A, 3, 3, t{1}, 3, 2, [1, 4, 2, 3], parameter) ;

%* Bt(b,x,c,l) = sum{j}_[B(b,c,j)*t{2}(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, t{2}, 3, 3, [1, 3, 2, 4], parameter) ;

%* R(a,k,c,l) = sum{b,x}_[At(a,k,b,x)*Bt(b,x,c,l)]
R = contractTensors(At, 4, [3, 4], Bt, 4, [1, 2], parameter) ;
clear At Bt
%* R((a, k), (c, l))
R = reshape(R, [D * d, D * d]) ;
