function [SzCorrelation, SzCorrelationM, QzzCorrelation, QzzCorrelationM] = computeCorrelation_x(parameter, wave)
%* correlation is maxCorrelationLength X 1 column vector

%* A(x, y, z, m)
A = absorbLambda_dense(parameter, wave) ;

t = cell(1, 6) ;
%* t1,t3,t5: (x,i,k); t2,t4,t6: (x,l,j)
for i = 1 : 6
    %* t((x,x'),(y,y'),(z,z')) = sum{m}_[A(x,y,z,m)*A(x',y',z',m)]
    t{i} = computeTensorProductA_A_dense(parameter, A{i}) ;
end

[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, t) ;
%------------------------------------------------------------------------------
rightMPS = computeSingleGate1D(parameter, t([5, 4]), rightMPS) ;

rightMPS_3 = computeSingleGate1D(parameter, t([3, 6]), rightMPS) ;
rightMPS_2 = computeSingleGate1D(parameter, t([1, 2]), rightMPS_3) ;
rightMPS_1 = computeSingleGate1D(parameter, t([5, 4]), rightMPS_2) ;

t_left = transposeTransferMatrix1D(t) ;

leftMPS_1 = leftMPS ;
leftMPS_2 = computeSingleGate1D(parameter, t_left([5, 4]), leftMPS_1) ;
leftMPS_3 = computeSingleGate1D(parameter, t_left([3, 6]), leftMPS_2) ;

SzCorrelation = [] ;
SzCorrelationM = [] ;
QzzCorrelation = [] ;
QzzCorrelationM = [] ;

[SzC, SzCM, QzzC, QzzCM] = computeCorrelation_x1(parameter, t([3, 6, 1, 2]), rightMPS_1, leftMPS_1, A([3, 6, 1, 2])) ;
SzCorrelation = [SzCorrelation, SzC] ;
SzCorrelationM = [SzCorrelationM, SzCM] ;
QzzCorrelation = [QzzCorrelation, QzzC] ;
QzzCorrelationM = [QzzCorrelationM, QzzCM] ;

[SzC, SzCM, QzzC, QzzCM] = computeCorrelation_x1(parameter, t([5, 4, 3, 6]), rightMPS_2, leftMPS_2, A([5, 4, 3, 6])) ;
SzCorrelation = [SzCorrelation, SzC] ;
SzCorrelationM = [SzCorrelationM, SzCM] ;
QzzCorrelation = [QzzCorrelation, QzzC] ;
QzzCorrelationM = [QzzCorrelationM, QzzCM] ;

[SzC, SzCM, QzzC, QzzCM] = computeCorrelation_x1(parameter, t([1, 2, 5, 4]), rightMPS_3, leftMPS_3, A([1, 2, 5, 4])) ;
SzCorrelation = [SzCorrelation, SzC] ;
SzCorrelationM = [SzCorrelationM, SzCM] ;
QzzCorrelation = [QzzCorrelation, QzzC] ;
QzzCorrelationM = [QzzCorrelationM, QzzCM] ;