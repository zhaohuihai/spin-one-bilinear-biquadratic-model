function t = transposeTransferMatrix1D(t0)
%* transpose to left transferMatrix
%* t1,t3,t5: (x,i,k) -> (x,k,i)
%* t2,t4,t6: (x,l,j) -> (x,j,l)
%* 5 -> 1; 4 -> 2; 3 -> 3; 6 -> 6; 1 -> 5; 2 -> 4

t = cell(1, 6) ;

t{1} = permute(t0{5}, [1, 3, 2]) ;
t{2} = permute(t0{4}, [1, 3, 2]) ;
t{3} = permute(t0{3}, [1, 3, 2]) ;
t{6} = permute(t0{6}, [1, 3, 2]) ;
t{5} = permute(t0{1}, [1, 3, 2]) ;
t{4} = permute(t0{2}, [1, 3, 2]) ;
