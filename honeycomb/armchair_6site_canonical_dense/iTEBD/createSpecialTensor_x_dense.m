function Q = createSpecialTensor_x_dense(parameter, operator, A, B, rightMPS, leftMPS)

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, operator, A, B) ;
clear A B

%* At(a,k,b,x,n) = sum{i}_[Ar(a,b,i)*TCa(x,i,k,n)]
At = contractTensors(rightMPS.A, 3, 3, TCa, 4, 2, [1, 4, 2, 3, 5], parameter) ;
clear TCa
%* Bt(b,x,n,c,l) = sum{j}_[Br(b,c,j)*TCb(x,l,j,n)]
Bt = contractTensors(rightMPS.B, 3, 3, TCb, 4, 3, [1, 3, 5, 2, 4], parameter) ;
clear TCb
clear rightMPS

%* R(a,k,c,l) = sum{b,x,n}_[At(a,k,b,x,n)*Bt(b,x,n,c,l)]
R = contractTensors(At, 5, [3, 4, 5], Bt, 5, [1, 2, 3], parameter) ;
clear At Bt

%* L(a',k,c',l) = sum{b'}_[Al(a',b',k)*Bl(b',c',l)]
L = contractTensors(leftMPS.A, 3, 2, leftMPS.B, 3, 1, parameter) ;
clear leftMPS

%* Q(a,a',c,c') = sum{k,l}_[R(a,k,c,l)*L(a',k,c',l)]
Q = contractTensors(R, 4, [2, 4], L, 4, [2, 4], [1, 3, 2, 4], parameter) ;
clear R L

D = parameter.dim_MPS ;

Q = reshape(Q, [D^2, D^2]) ;