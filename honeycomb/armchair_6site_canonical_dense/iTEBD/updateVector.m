function [VL, coef, PR, PL] = updateVector(VL, numindVL, PR, numindPR, PL, numindPL)
%* 6 situations:
%* 1: VL(c',l,c,n) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a)*PR(j,a,i,c,n)]   4 3 5
%* 2: VL(c',l,c,n) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a,n)*PR(j,a,i,c)]   4 4 4
%* 3: VL(c',l,c) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a)*PR(j,a,i,c)]       4 3 4
%* 4: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i,n)*VL(a',j,a)*PR(j,a,i,c,n)] 5 3 5
%* 5: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i)*VL(a',j,a,n)*PR(j,a,i,c,n)] 4 4 5
%* 6: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i,n)*VL(a',j,a,n)*PR(j,a,i,c)] 5 4 4

%* 1: VL(c',l,c,n) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a)*PR(j,a,i,c,n)]   4 3 5
if numindPL == 4 && numindVL == 3 && numindPR == 5
    %* VR(a',i,c,n) = sum{j,a}_[VL(a',j,a)*PR(j,a,i,c,n)]
    VR = contractTensors(VL, 3, [2, 3], PR, 5, [1, 2]) ;
    %* VL(c',l,c,n) = sum{a',i}_[PL(c',l,a',i)*VR(a',i,c,n)]
    VL = contractTensors(PL, 4, [3, 4], VR, 4, [1, 2]) ;
    
%* 2: VL(c',l,c,n) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a,n)*PR(j,a,i,c)]   4 4 4    
elseif numindPL == 4 && numindVL == 4 && numindPR == 4
    %* VR(i,c,a',n) = sum{j,a}_[PR(j,a,i,c)*VL(a',j,a,n)]
    VR = contractTensors(PR, 4, [1, 2], VL, 4, [2, 3]) ;
    %* VL(c',l,c,n) = sum{a',i}_[PL(c',l,a',i)*VR(i,c,a',n)]
    VL = contractTensors(PL, 4, [3, 4], VR, 4, [3, 1]) ;

%* 3: VL(c',l,c) = sum{a',j,a,i}_[PL(c',l,a',i)*VL(a',j,a)*PR(j,a,i,c)]       4 3 4    
elseif numindPL == 4 && numindVL == 3 && numindPR == 4
    %* VR(a',i,c) = sum{j,a}_[VL(a',j,a)*PR(j,a,i,c)]
    VR = contractTensors(VL, 3, [2, 3], PR, 4, [1, 2]) ;
    %* VL(c',l,c) = sum{a',i}_[PL(c',l,a',i)*VR(a',i,c)]
    VL = contractTensors(PL, 4, [3, 4], VR, 3, [1, 2]) ;
    
%* 4: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i,n)*VL(a',j,a)*PR(j,a,i,c,n)] 5 3 5
elseif numindPL == 5 && numindVL == 3 && numindPR == 5
    %* VR(a',i,c,n) = sum{j,a}_[VL(a',j,a)*PR(j,a,i,c,n)]
    VR = contractTensors(VL, 3, [2, 3], PR, 5, [1, 2]) ;
    %* VL(c',l,c) = sum{a',i,n}_[PL(c',l,a',i,n)*VR(a',i,c,n)]
    VL = contractTensors(PL, 5, [3, 4, 5], VR, 4, [1, 2, 4]) ;
    
%* 5: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i)*VL(a',j,a,n)*PR(j,a,i,c,n)] 4 4 5
elseif numindPL == 4 && numindVL == 4 && numindPR == 5
    %* VR(a',i,c) = sum{j,a,n}_[VL(a',j,a,n)*PR(j,a,i,c,n)]
    VR = contractTensors(VL, 4, [2, 3, 4], PR, 5, [1, 2, 5]) ;
    %* VL(c',l,c) = sum{a',i}_[PL(c',l,a',i)*VR(a',i,c)]
    VL = contractTensors(PL, 4, [3, 4], VR, 3, [1, 2]) ; 
    
%* 6: VL(c',l,c) = sum{a',j,a,i,n}_[PL(c',l,a',i,n)*VL(a',j,a,n)*PR(j,a,i,c)] 5 4 4    
elseif numindPL == 5 && numindVL == 4 && numindPR == 4
    %* VR(a',n,i,c) = sum{j,a}_[VL(a',j,a,n)*PR(j,a,i,c)]
    VR = contractTensors(VL, 4, [2, 3], PR, 4, [1, 2]) ;
    %* VL(c',l,c) = sum{a',i,n}_[PL(c',l,a',i,n)*VR(a',n,i,c)]
    VL = contractTensors(PL, 5, [3, 4, 5], VR, 4, [1, 3, 2]) ;
end

[VL, coef] = normalizeVector(VL) ;