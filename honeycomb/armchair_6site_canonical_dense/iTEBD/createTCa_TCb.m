function [TCa, TCb] = createTCa_TCb(parameter, operator, A, B)
%* TCa(x,i,k,n), TCb(x,l,j,n)

%* operator((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian_dense(parameter, operator) ;

%* TCa((x,x'),(y,y'),(z,z'),n) = sum{mi,mi'}_[A(x,y,z,mi)*A(x',y',z',mi')*U((mi,mi'),n)]
TCa = contractAAU(parameter, A, U) ;
%* change notation: TCa(x,i,k,n)

%* TCb((x,x'),(y,y'),(z,z'),n) = sum{mj,mj'}_[B(x,y,z,mj)*B(x',y',z',mj')*V((mj,mj'),n)]
TCb = contractAAU(parameter, B, V) ;
%* change notation: TCb(x,l,j,n)