function [V, coef] = normalizeVector(V)

sizeV = size(V) ;

dim = numel(V) ;

V = reshape(V, [1, dim]) ;

coef = norm(V) ;
V = V ./ coef ;

V = reshape(V, sizeV) ;