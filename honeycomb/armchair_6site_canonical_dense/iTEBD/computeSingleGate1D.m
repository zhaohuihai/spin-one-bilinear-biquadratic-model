function [MPS, coef, truncationError_TMP] = computeSingleGate1D(parameter, t, MPS)

%* R((a, k), (c, l)) = sum{i,j,x}_(A(a,b,i)*t{1}(x,i,k)*B(b,c,j)*t{2}(x,l,j))
R = projectTransferMatrix_dense(parameter, t, MPS) ;

[MPS, Lambda, coef] = canonicalize(parameter, R) ;
clear R
[MPS, truncationError_TMP] = truncate_canonical(parameter, MPS, Lambda) ;

MPS = exchangeAandB_TMP_dense(MPS) ;

