function valSite = findExpectValSite(valBond_uni, valBond_stag)

valSite = zeros(1, 6) ;

valSite(1) = valBond_uni(1) + valBond_stag(1) + valBond_uni(4) + valBond_stag(4) + valBond_uni(7) + valBond_stag(7) ;
valSite(1) = valSite(1) / 3 ;

valSite(2) = valBond_uni(1) - valBond_stag(1) + valBond_uni(6) - valBond_stag(6) + valBond_uni(8) - valBond_stag(8) ;
valSite(2) = valSite(2) / 3 ;

valSite(3) = valBond_uni(2) + valBond_stag(2) + valBond_uni(5) + valBond_stag(5) + valBond_uni(8) + valBond_stag(8) ;
valSite(3) = valSite(3) / 3 ;

valSite(4) = valBond_uni(3) - valBond_stag(3) + valBond_uni(5) - valBond_stag(5) + valBond_uni(7) - valBond_stag(7) ;
valSite(4) = valSite(4) / 3 ;

valSite(5) = valBond_uni(3) + valBond_stag(3) + valBond_uni(6) + valBond_stag(6) + valBond_uni(9) + valBond_stag(9) ;
valSite(5) = valSite(5) / 3 ;

valSite(6) = valBond_uni(2) - valBond_stag(2) + valBond_uni(4) - valBond_stag(4) + valBond_uni(9) - valBond_stag(9) ;
valSite(6) = valSite(6) / 3 ;


