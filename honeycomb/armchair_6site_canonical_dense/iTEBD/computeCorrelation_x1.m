function [SzCorrelation, SzCorrelationM, QzzCorrelation, QzzCorrelationM] = computeCorrelation_x1(parameter, t, rightMPS, leftMPS, A)


%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)
[VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, t, rightMPS, leftMPS) ;


if parameter.computation.SzCorrelation == 1
    parameter.AsiteOperator = createSzI(parameter) ;
    parameter.BsiteOperator = createISz(parameter) ;
    parameter.correlationOperator = createSzSz(parameter) ;
    [SzCorrelation, SzCorrelationM] = findCorrelationFunction(parameter, t, rightMPS, leftMPS, A, VR, VL, PR, PL, eta) ;
else
    SzCorrelation = [] ;
    SzCorrelationM = [] ;
end
if parameter.computation.QzzCorrelation == 1
    parameter.AsiteOperator = createQzzI(parameter) ;
    parameter.BsiteOperator = createIQzz(parameter) ;
    parameter.correlationOperator = createQzzQzz(parameter) ;
    [QzzCorrelation, QzzCorrelationM] = findCorrelationFunction(parameter, t, rightMPS, leftMPS, A, VR, VL, PR, PL, eta) ;
else
    QzzCorrelation = [] ;
    QzzCorrelationM = [] ;
end