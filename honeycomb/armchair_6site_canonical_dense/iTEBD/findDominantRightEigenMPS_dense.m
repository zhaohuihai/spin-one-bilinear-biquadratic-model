function MPS = findDominantRightEigenMPS_dense(parameter, t)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;

%* MPS: A B
MPS = initializeMPS_dense(parameter) ;

TMPstep = 0 ;
[MPS, coef, truncationError, TMPstep] = iterateRightGate1D(parameter, t, MPS, TMPstep) ;

while parameter.dim_MPS < maxDim
    TMPstep = TMPstep + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS] = applyRightGateOperation1D(parameter, t, MPS) ;
    toc
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;

[MPS, coef, truncationError, TMPstep] = iterateRightGate1D(parameter, t, MPS, TMPstep) ;

disp(['total TMP steps = ', num2str(TMPstep)]) ;