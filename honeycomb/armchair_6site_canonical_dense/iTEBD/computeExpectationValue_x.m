function expectValueBond = computeExpectationValue_x(parameter, wave)

%* (x, y, z, m)
A = absorbLambda_dense(parameter, wave) ;

t = cell(1, 6) ;
%* t1,t3,t5: (x,i,k); t2,t4,t6: (x,l,j)
for i = 1 : 6
    %* t((x,x'),(y,y'),(z,z')) = sum{m}_[A(x,y,z,m)*A(x',y',z',m)]
    t{i} = computeTensorProductA_A_dense(parameter, A{i}) ;
end

[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, t) ;

%*=========================================================================================
expectValueBond = dealWithSpecialTensor_x_dense(parameter, t, rightMPS, leftMPS, A) ;