function PL = contractABL_T_dense(parameter, ABL, T)
%* PL((j,c'),(i,a')) = sum{k,l}_(ABL((a',c'),(k,l))*T((k,l),(i,j)))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* PL((a',c'),(i,j)) = sum{k,l}_(ABL((a',c'),(k,l))*T((k,l),(i,j)))
PL = ABL * T ;

%* PL(a',c',i,j)
PL = reshape(PL, [D, D, d, d]) ;

%* PL(j,c',i,a')
PL = permute(PL, [4, 2, 3, 1]) ;

%* PL((j,c'),(i,a'))
PL = reshape(PL, [d * D, d * D]) ;