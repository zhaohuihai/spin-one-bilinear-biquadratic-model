function [MPS, coef, truncationError] = applyRightGateOperation1D(parameter, t, MPS)
% t([5, 4]),t([3, 6])),t([1, 2])

c = zeros(1, 6) ;
truncErr_single = zeros(1, 6) ;

n = 1 ;
for j = 1 : 2
    
    [MPS, c(n), truncErr_single(n)] = computeSingleGate1D(parameter, t([5, 4]), MPS) ;
    n = n + 1 ;
    [MPS, c(n), truncErr_single(n)] = computeSingleGate1D(parameter, t([3, 6]), MPS) ;
    n = n + 1 ;
    [MPS, c(n), truncErr_single(n)] = computeSingleGate1D(parameter, t([1, 2]), MPS) ;
    n = n + 1 ;
    
end

truncationError = max(truncErr_single) ;
coef = prod(c) ;
