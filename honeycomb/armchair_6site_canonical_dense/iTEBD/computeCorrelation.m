function [SzCorrelation, QzzCorrelation] = computeCorrelation(parameter, wave)
%*********************************************************************
%* Sz correlation: <Sz(i)*Sz(i+l)> - <Sz(i)><Sz(i+l)>
%* Qzz correlation: <Qzz(i)*Qzz(i+l)> - <Qzz(i)><Qzz(i+l)>
%* compute ONE bond directions: x
%* SzCorrelation and QzzCorrelation are maxCorrelationLength X 9 matrices
%********************************************************************* 
SzCorrelation = [] ;
QzzCorrelation = [] ;

L = parameter.computation.maxCorrelationLength ;
l = 1 : L ;
l = l' ;

%* x is maxCorrelationLength X 1 column vector
[SzCorrelation_x, SzCorrelation_xM, QzzCorrelation_x, QzzCorrelation_xM] = computeCorrelation_x(parameter, wave) ;

% wave = rotate(wave) ;
% [SzCorrelation_y, SzCorrelation_yM, QzzCorrelation_y, QzzCorrelation_yM] = computeCorrelation_x(parameter, wave) ;

% wave = rotate(wave) ;
% [SzCorrelation_z, SzCorrelation_zM, QzzCorrelation_z, QzzCorrelation_zM] = computeCorrelation_x(parameter, wave) ;
if parameter.computation.SzCorrelation == 1
    SzCorrelation = [l, abs(SzCorrelation_x), abs(SzCorrelation_xM)] ;
%     SzCorrelation = averageCorrelation(l, SzCorrelation_x, SzCorrelation_xM, SzCorrelation_y, SzCorrelation_yM, SzCorrelation_z, SzCorrelation_zM) ;
    saveExpectationValue(parameter, SzCorrelation, 'SzCorrelation')
end  

if parameter.computation.QzzCorrelation == 1
    QzzCorrelation = [l, abs(QzzCorrelation_x), abs(QzzCorrelation_xM)] ;
%     QzzCorrelation = averageCorrelation(l, QzzCorrelation_x, QzzCorrelation_xM, QzzCorrelation_y, QzzCorrelation_yM, QzzCorrelation_z, QzzCorrelation_zM) ;
    saveExpectationValue(parameter, QzzCorrelation, 'QzzCorrelation')
end







