function saveExpectationValue(parameter, expectValue, valueName)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

dimSave = parameter.dim_MPS ;
variableName = [valueName, 'All'] ;
dirName = ['D = ', num2str(D)] ;
pathName = ['result/', dirName] ;
fileName = [pathName, '/', valueName, 'Site', num2str(M), '_dimSave', num2str(dimSave), '.mat'] ;

id = exist(fileName, 'file') ;

if id == 2 % file exists
    load (fileName, variableName, 'thetaAll')
    index = find(thetaAll == theta) ;
    if isempty(index)
        thetaAll = [thetaAll, theta] ;
        eval([variableName, '=', '[', variableName, ', expectValue];']) ;
        [thetaAll, Order] = sort(thetaAll) ;
        eval([variableName, '=', variableName, '(Order);']) ;
        
    else
        eval([variableName, '{index}', '=', 'expectValue;']) ;
    end
else
    eval([variableName, '= cell(1) ;']) ;
    eval([variableName, '{1} =', 'expectValue;']) ;
    thetaAll = theta ;
end
save(fileName, variableName, 'thetaAll') ;
disp(['theta = ', num2str(theta)]) ;
disp([dirName, ', dimSave = ', num2str(dimSave)]) ;
disp([valueName, ': ']) ;
disp(expectValue) ;
