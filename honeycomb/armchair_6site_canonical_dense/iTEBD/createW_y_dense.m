function Wa = createW_y_dense(parameter, A, U)
%* Wa(x,in,k)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = M^2 ;
%* A((x,y,z),m)
A = reshape(A, [D^3, M]) ;
%* AA((x,y,z,x',y',z'),(m,m'))
AA = kron(A, A) ;

%* Wa((x,y,z,x',y',z'), n)
Wa = AA * U ;

%* Wa(x,y,z,x',y',z', n)
Wa = reshape(Wa, [D, D, D, D, D, D, N]) ;

%* Wa(x,x',y,y',n,z,z')
Wa = permute(Wa, [1, 4, 2, 5, 7, 3, 6]) ;

%* Wa((x,x'),(y,y',n),(z,z'))
Wa = reshape(Wa, [D^2, D^2 * N, D^2]) ;