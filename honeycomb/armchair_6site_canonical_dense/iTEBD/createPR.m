function PR = createPR(parameter, Ta, Tb, MPS)
%* PR((a,k),(c,l)) = sum{i,j,x,b}_(A(a,b,i)*Ta(x,i,k)*B(b,c,j)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* At(a,k,b,x) = sum{i}_[A(a,b,i)*Ta(x,i,k)]
At = contractTensors(MPS.A, 3, 3, Ta, 3, 2, [1, 4, 2, 3], parameter) ;

%* Bt(b,x,c,l) = sum{j}_[B(b,c,j)*Tb(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, Tb, 3, 3, [1, 3, 2, 4], parameter) ;

%* PR(a,k,c,l) = sum{b,x}_[At(a,k,b,x)*Bt(b,x,c,l)]
PR = contractTensors(At, 4, [3, 4], Bt, 4, [1, 2], parameter) ;
clear At Bt
%* PR((a, k), (c, l))
PR = reshape(PR, [D * d, D * d]) ;
