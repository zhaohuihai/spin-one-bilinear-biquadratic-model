function THL = contractTb_Wa_y_dense(parameter, Tb, Wa)
%* THL((k,l),(in,j)) = sum{x}_(Wa(x,in,k)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
N = parameter.siteDimension^2 ;

%* Tb(x,(l,j))
Tb = reshape(Tb, [d, d^2]) ;

%* Wa(x,(in,k))
Wa = reshape(Wa, [d, d*N*d]) ;

%* THL((l,j),(in,k))
THL = Tb' * Wa ;

%* THL(l,j,in,k)
THL = reshape(THL, [d, d, d*N, d]) ;

%* THL(k,l,in,j)
THL = permute(THL, [4, 1, 3, 2]) ;

%* THL((k,l),(in,j))
THL = reshape(THL, [d^2, d^2*N]) ;