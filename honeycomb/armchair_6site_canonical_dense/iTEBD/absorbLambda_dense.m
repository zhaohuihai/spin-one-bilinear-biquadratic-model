function A = absorbLambda_dense(parameter, wave)

Lambda = cell(1, 9) ;
for i = 1 : 9
    Lambda{i} = sqrt(wave.Lambda{i}) ;
end

A = cell(1, 6) ;
A{1} = Atimes3Lambda_dense(parameter, wave.A{1}, Lambda([1, 4, 7])) ;
A{2} = Atimes3Lambda_dense(parameter, wave.A{2}, Lambda([1, 6, 8])) ;
A{3} = Atimes3Lambda_dense(parameter, wave.A{3}, Lambda([2, 5, 8])) ;
A{4} = Atimes3Lambda_dense(parameter, wave.A{4}, Lambda([3, 5, 7])) ;
A{5} = Atimes3Lambda_dense(parameter, wave.A{5}, Lambda([3, 6, 9])) ;
A{6} = Atimes3Lambda_dense(parameter, wave.A{6}, Lambda([2, 4, 9])) ;

