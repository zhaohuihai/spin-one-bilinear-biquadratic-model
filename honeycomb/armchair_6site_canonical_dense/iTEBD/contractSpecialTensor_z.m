function expectValueBond = contractSpecialTensor_z(parameter, t, rightMPS, leftMPS, A, B)

% %* ABR((a,c),(i,j))
% ABR = computeA_B(parameter, rightMPS) ;
% 
% %* ABL((a',c'),(k,l))
% ABL = computeA_B(parameter, leftMPS) ;

%* PR((l,c),(k,a)) = sum{i,j,x,b}_(A(a,b,i)*t3(x,i,k)*B(b,c,j)*t4(x,l,j))
%* change notation: PR((l,c),(i,a))
PR = createPR_z(parameter, t{3}, t{4}, rightMPS) ;

%* PL((a',i),(c',j)) = sum{k,l,x,b'}_(A(a',b',k)*t1(x,i,k)*B(b',c',l)*t2(x,l,j))
%* change notation: PL((a',j),(c',i))
PL = createPL_z(parameter, t{1}, t{2}, leftMPS) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,l,c')
vector = rand(D * d * D, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

%* vectorRight(a',j,a) = sum{c,c',l,i}_(PL((a',j),(c',i))*vectorRight(c',(l,c))*PR((l,c),(i,a)))
[vectorRight, etaRight] = applyPowerMethod_y_dense(parameter, PL, PR, vector) ;

%* PR((l,c),(i,a)) -> PR((c,l),(a,i))
%* PL((a',j),(c',i)) -> PL((j,a'),(i,c'))
[PL, PR] = transposeP_dense(parameter, PL, PR) ;

vectorLeft = reshape(vectorRight, [D, d, D]) ;
vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
vectorLeft = reshape(vectorLeft, [D * d * D, 1]) ;

%* vectorLeft(c,l,c') = sum{a,a',j,i}_(PR((c,l),(a,i))*vectorLeft(a,(j,a'))*PL((j,a'),(i,c')))
[vectorLeft, etaLeft] = applyPowerMethod_y_dense(parameter, PR, PL, vectorLeft) ;
%*==============================================================================================================
%* QR((l,c),(kn,a))    change notation: QR((l,c),(in,a))
%* QL((a',i),(c',jn))  change notation: QL((a',j),(c',in))
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Sx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Sx = Sx ;
    clear QL QR
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Sy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Sy = Sy ;
    clear QL QR
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Sz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['Sz z bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
    clear QL QR
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagSx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagSx = stagSx ;
    clear QL QR
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagSy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagSy = stagSy ;
    clear QL QR
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagSz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['stagSz z bond = ', num2str(stagSz)]) ;
    expectValueBond.stagSz = stagSz ;
    clear QL QR
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qxx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qxx = Qxx ;
    clear QL QR
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qyy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qyy = Qyy ;
    clear QL QR
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qzz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['Qzz z bond = ', num2str(Qzz)]) ;
    expectValueBond.Qzz = Qzz ;
    clear QL QR
end
if computation.Qxx_yy == 1
    operator = createQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qxx_yy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['Qxx_yy z bond = ', num2str(Qxx_yy)]) ;
    expectValueBond.Qxx_yy = Qxx_yy ;
    clear QL QR
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qxy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qxy = Qxy ;
    clear QL QR
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qyz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qyz = Qyz ;
    clear QL QR
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [Qzx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qzx = Qzx ;
    clear QL QR
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQxx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxx = stagQxx ;
    clear QL QR
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQyy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQyy = stagQyy ;
    clear QL QR
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQzz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['stagQzz z bond = ', num2str(stagQzz)]) ;
    expectValueBond.stagQzz = stagQzz ;
    clear QL QR
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQxx_yy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
    clear QL QR
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQxy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxy = stagQxy ;
    clear QL QR
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQyz, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQyz = stagQyz ;
    clear QL QR
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [stagQzx, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQzx = stagQzx ;
    clear QL QR
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [energy, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['energy z bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
    clear QL QR
end
%* energy derivative
if computation.energyDerivative == 1
    operator = createEnergyDerivative(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [energyDerivative, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['energy derivative z bond = ', num2str(energyDerivative)]) ;
    expectValueBond.energyDerivative = energyDerivative ;
    clear QL QR
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, t{1}, t{4}, rightMPS, leftMPS, A, B) ;
    [nearSpinCorrelation, QL, QR] = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['nearest neighbor spin correlation z bond = ', num2str(nearSpinCorrelation)]) ;
    expectValueBond.nearSpinCorrelation = nearSpinCorrelation ;
    clear QL QR
end