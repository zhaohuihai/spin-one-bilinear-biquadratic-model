function P = createTransferMatrix0D_x(parameter, t, rightMPS, leftMPS)
%* P((a,a'),(c,c')) = sum{i,j,k,l,x,b,b'}_
%* [Ar(a,b,i)*t{1}(x,i,k)*Br(b,c,j)*t{2}(x,l,j)*Al(a',b',k)*Bl(b',c',l)]

D = size(rightMPS.A, 1) ;
%* At(a,k,b,x) = sum{i}_[A(a,b,i)*t{1}(x,i,k)]
At = contractTensors(rightMPS.A, 3, 3, t{1}, 3, 2, [1, 4, 2, 3], parameter) ;

%* Bt(b,x,c,l) = sum{j}_[B(b,c,j)*t{2}(x,l,j)]
Bt = contractTensors(rightMPS.B, 3, 3, t{2}, 3, 3, [1, 3, 2, 4], parameter) ;
clear rightMPS t
%* R(a,k,c,l) = sum{b,x}_[At(a,k,b,x)*Bt(b,x,c,l)]
R = contractTensors(At, 4, [3, 4], Bt, 4, [1, 2], parameter) ;
clear At Bt
%* L(a',k,c',l) = sum{b'}_[Al(a',b',k)*Bl(b',c',l)]
L = contractTensors(leftMPS.A, 3, 2, leftMPS.B, 3, 1, parameter) ;
clear leftMPS
%* P(a,a',c,c') = sum{k,l}_[R(a,k,c,l)*L(a',k,c',l)]
P = contractTensors(R, 4, [2, 4], L, 4, [2, 4], [1, 3, 2, 4], parameter) ;
clear R L


P = reshape(P, [D^2, D^2]) ;