function PR = createPR_z(parameter, Ta, Tb, MPS)
%* PR((l,c),(k,a)) = sum{i,j,x,b}_(A(a,b,i)*Ta(x,i,k)*B(b,c,j)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* At(b,x,k,a) = sum{i}_[A(a,b,i)*Ta(x,i,k)]
At = contractTensors(MPS.A, 3, 3, Ta, 3, 2, [2, 3, 4, 1], parameter) ;

%* Bt(l,c,b,x) = sum{j}_[B(b,c,j)*Tb(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, Tb, 3, 3, [4, 2, 1, 3], parameter) ;

%* PR(l,c,k,a) = sum{b,x}_[Bt(l,c,b,x) * At(b,x,k,a)]
PR = contractTensors(Bt, 4, [3, 4], At, 4, [1, 2], parameter) ;
clear At Bt
%* PR((l,c),(k,a))
PR = reshape(PR, [d * D, d * D]) ;

