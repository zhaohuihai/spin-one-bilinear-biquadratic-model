function A = Atimes3Lambda_dense(parameter, A, Lambda)

D = parameter.bondDimension ;
%* x
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lambda{1}(x) ;
end
%* y
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Lambda{2}(y) ;
end
%* z
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lambda{3}(z) ;
end