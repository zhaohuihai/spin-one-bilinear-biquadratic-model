function PL = createPL(parameter, Ta, Tb, MPS)
%* PL((j,c'),(i,a')) = sum{k,l,b',x}_(A(a',b',k)*Ta(x,i,k)*B(b',c',l)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* At(b',x,i,a') = sum{k}_[A(a',b',k)*Ta(x,i,k)]
At = contractTensors(MPS.A, 3, 3, Ta, 3, 3, [2, 3, 4, 1], parameter) ;

%* Bt(j,c',b',x) = sum{l}_[B(b',c',l)*Tb(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, Tb, 3, 2, [4, 2, 1, 3], parameter) ;

%* PL(j,c',i,a') = sum{b',x}_[Bt(j,c',b',x) * At(b',x,i,a')]
PL = contractTensors(Bt, 4, [3, 4], At, 4, [1, 2], parameter) ;
clear At Bt
%* PL((j,c'),(i,a'))
PL = reshape(PL, [D * d, D * d]) ;

