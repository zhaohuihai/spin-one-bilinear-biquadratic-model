function [wave, truncationError, coef] = projectVBC(parameter, wave)
%* x. Lambda{1}: 1-2; Lambda{2}: 3-6; Lambda{3}: 5-4
%* y. Lambda{4}: 1-6; Lambda{5}: 3-4; Lambda{6}: 5-2
%* z. Lambda{7}: 1-4; Lambda{8}: 3-2; Lambda{9}: 5-6
%  L(7)\            /L(6)
%       \          /
%       A\__L(1)__/B
%        /        \
%       /          \
%  L(4)/            \L(8)
%

c = zeros(1, 9) ;
truncErr_single = zeros(1, 9) ;
%====================================================================================
% PO_strong = createProjectionOperator(parameter) ;
% parameter.H = parameter.H * parameter.VBCanisotropyRatio ;
% PO_weak = createProjectionOperator(parameter) ;
% %  x
% % 1-2
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(1), c(1)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
% % 3-6 weak
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(2), c(2)] = projectBy1operator(parameter, PO_weak, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
% % 5-4
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(3), c(3)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
%
% wave = rotateWaveSite(wave) ;
% %  y
% wave = rotateWaveTensor(wave) ;
% % 1-6
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(4), c(4)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
%
% % 3-4
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(5), c(5)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
% % 5-2 weak
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(6), c(6)] = projectBy1operator(parameter, PO_weak, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
%
% wave = rotateWaveSite(wave) ;
% %  z
% wave = rotateWaveTensor(wave) ;
% % 1-4 weak
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(7), c(7)] = projectBy1operator(parameter, PO_weak, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
%
% % 3-2
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(8), c(8)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
%
% % 5-6
% wave = rotateWaveSite(wave) ;
% [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(9), c(9)] = projectBy1operator(parameter, PO_strong, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%
% wave = rotateWaveSite(wave) ;
% wave = rotateWaveTensor(wave) ;
%====================================================================================
% n = 1 ;
% for i = 1 : 3
%     for j = 1 : 3
%
%         [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(n), c(n)] = projectBy1operator(parameter, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%         % (1,2,3,4,5,6) -> (3,6,5,2,1,4)
%         wave = rotateWaveSite(wave) ;
%         n = n + 1 ;
%     end
%     % (x,y,z) -> (y,z,x)
%     wave = rotateWaveTensor(wave) ;
% end
%=====================================================================================
PO_strong = createProjectionOperator(parameter) ;
parameter.H = parameter.H * parameter.VBCanisotropyRatio ;
PO_weak = createProjectionOperator(parameter) ;

n = 1 ;
if parameter.projectionSequence == 1
    if parameter.plaquetteVBC == 1
        PO([1, 3, 4, 5, 8, 9]) = PO_strong ;
        PO([2, 6, 7]) = PO_weak ;
    elseif parameter.columnarVBC == 1
        PO([1, 3, 4, 5, 8, 9]) = PO_weak ;
        PO([2, 6, 7]) = PO_strong ;
    elseif parameter.staggeredVBC == 1
        PO([1, 2, 3]) = PO_strong ;
        PO([4, 5, 6, 7, 8, 9]) = PO_weak ;
    else
        PO(1:9) = PO_strong ;
    end
    %---------------------------------------------------------
    for i = 1 : 3
        for j = 1 : 3
            if parameter.projectionMethod == 1
                [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(n), c(n)] = projectBy1operator(parameter, PO(n), wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
            elseif parameter.projectionMethod == 2
                [wave, truncErr_single(n), c(n)] = clusterUpdate(parameter, PO(n), wave) ;
            end
            % (1,2,3,4,5,6) -> (3,6,5,2,1,4)
            wave = rotateWaveSite(wave) ;
            n = n + 1 ;
        end
        % (x,y,z) -> (y,z,x)
        wave = rotateWaveTensor(wave) ;
    end
elseif parameter.projectionSequence == 2
    %=====================================================================================
    if parameter.plaquetteVBC == 1
        PO([1, 2, 5, 6, 7, 9]) = PO_strong ;
        PO([3, 4, 8]) = PO_weak ;
    elseif parameter.columnarVBC == 1
        PO([1, 2, 5, 6, 7, 9]) = PO_weak ;
        PO([3, 4, 8]) = PO_strong ;
    elseif parameter.staggeredVBC == 1
        PO([1, 4, 7]) = PO_strong ;
        PO([2, 3, 5, 6, 8, 9]) = PO_weak ;
    else
        PO(1:9) = PO_strong ;
    end
    %---------------------------------------------------------
    for i = 1 : 3
        for j = 1 : 3
            if parameter.projectionMethod == 1
                [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(n), c(n)] = projectBy1operator(parameter, PO(n), wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
            elseif parameter.projectionMethod == 2
                [wave, truncErr_single(n), c(n)] = clusterUpdate(parameter, PO(n), wave) ;
            end
            % (x,y,z) -> (y,z,x)
            wave = rotateWaveTensor(wave) ;
            n = n + 1 ;
        end
        % (1,2,3,4,5,6) -> (3,6,5,2,1,4)
        wave = rotateWaveSite(wave) ;
    end
    %===================================================================================
    
else
    error('unknown projection sequence') ;
    
end
%===================================================================================
truncationError = max(truncErr_single) ;
coef = prod(c) ;