function [A, B] = computeLyLz_A_Lx_B_LyLz(parameter, A12, Lambda)
%*************************************
%  L(4)\z1        y2/L(3)
%       \          /
%       A\__L(1)__/B
%        /   x    \
%       /          \
%  L(2)/y1        z2\L(5)
%*************************************
D = parameter.bondDimension ;

A = A12{1} ;
B = A12{2} ;

%* A(x,y,z,m) = A(x,y,z,m)*Lambda{1}(x)
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lambda{1}(x) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Lambda{2}(y) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{4}(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lambda{4}(z) ;
end

%* B(x,y,z,m) = B(x,y,z,m)*Lambda{3}(y)
for y = 1 : D
    B(:, y, :, :) = B(:, y, :, :) * Lambda{3}(y) ;
end
%* B(x,y,z,m) = B(x,y,z,m)*Lambda{5}(z)
for z = 1 : D
    B(:, :, z, :) = B(:, :, z, :) * Lambda{5}(z) ;
end 

