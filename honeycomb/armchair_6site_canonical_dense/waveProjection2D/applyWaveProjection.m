function wave = applyWaveProjection(parameter, wave)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

disp(['site dimension = ', num2str(M)]) ;
disp(['wave function bond dimension = ', num2str(D)]) ;
disp(['theta = ', num2str(theta)]) ;

if parameter.loadPreviousWave == 0
    parameter.tau = parameter.tauInitial ;
    wave.polarization = parameter.polarization ;
else
    parameter.tau = wave.tauFinal ;
end

tauChangeFactor = parameter.tauChangeFactor ;

if isfield(wave, 'totalProjectionTimes')
    j = wave.totalProjectionTimes ;
else
    j = 0 ;
end
%**********************************************************************************
parameter.H = createHamiltonian_BilinBiqua(parameter) ;
coef0 = 0 ;

n = 1 ; % tau change times
while parameter.tau >= parameter.tauFinal
    disp(['tau = ', num2str(parameter.tau)])
    
    projectionConvergence = 1 ;  
    step = 0 ;
    while projectionConvergence > parameter.convergenceCriterion_projection && step <= parameter.maxProjectionStep

        j = j + 1 ;
        step = step + 1 ;
        
        Lambda0 = wave.Lambda{1} ;
        [wave, truncationError, coef] = projectVBC(parameter, wave) ;
        Lambda = wave.Lambda{1} ;
        
        if (j / 10) == floor(j / 10)
            
            projectionConvergence = norm(Lambda - Lambda0) / norm(Lambda) ;
%             projectionConvergence = abs(coef0 - coef) / abs(coef) ;
            
            disp(['projection step = ', num2str(step), ', tau = ', num2str(parameter.tau)])
            disp(['coef = ', num2str(coef)]) ;
            disp(['VBS ratio = ', num2str(parameter.VBCanisotropyRatio)]) ;
            disp(['truncation error = ', num2str(truncationError)])
            disp(['projection convergence error = ', num2str(projectionConvergence)]) ;
            entangleEntropy = computeEntangleEntropy(parameter, wave) ;
            coef0 = coef ;
        end
    end
    
    wave.totalProjectionTimes = j ;
    
    saveWave(parameter, wave) ;
    
    tau = 1 / ((1 / parameter.tau) + tauChangeFactor) ;
    tauChangeFactor = parameter.tauChangeFactor * n^2 ;
    if parameter.tau > parameter.tauFinal && tau < parameter.tauFinal
        parameter.tau = parameter.tauFinal ;
    else
        parameter.tau = tau ;
    end
    n = n + 1 ;
end
totalProjectionTimes = j
saveWave(parameter, wave) ;
