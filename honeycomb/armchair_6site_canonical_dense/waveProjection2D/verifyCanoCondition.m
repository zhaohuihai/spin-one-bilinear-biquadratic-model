function c1 = verifyCanoCondition(M1, R2, V, S, coef)

D = size(V, 2) ;

A = M1 * R2' ;

A = A * V ;

%* A(x1'',x1) = A(x1'',x1)/S(x1)
for x1 = 1 : D
    A(:, x1) = A(:, x1) ./ S(x1) ;
end

% a = A' * A ./ (coef^2) ;
a = A' * A ;
c1 = norm(a - eye(D)) ;