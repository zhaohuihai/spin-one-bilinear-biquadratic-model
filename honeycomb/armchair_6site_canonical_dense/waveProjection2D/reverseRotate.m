function wave = reverseRotate(wave)

%* (x, y, z) --> (z, x, y)

%* A(x,y,z,m) -> A(z,x,y,m)
wave.A = permute(wave.A, [3, 1, 2, 4]) ;
%* B(x,y,z,m) -> B(z,x,y,m)
wave.B = permute(wave.B, [3, 1, 2, 4]) ;

wave.Lambda = wave.Lambda([3, 1, 2]) ;
