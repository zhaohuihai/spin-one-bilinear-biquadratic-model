function wave = rotateWaveTensor(wave)
% (x,y,z) -> (y,z,x)

% (1, 2, 3, 4, 5, 6) -> (1, 6, 3, 2, 5, 4)
wave.A = wave.A([1, 6, 3, 2, 5, 4]) ;
% (x,y,z) -> (y,z,x)
%* A(x,y,z,m) -> A(y,z,x,m)
for i = 1 : 6
    wave.A{i} = permute(wave.A{i}, [2, 3, 1, 4]) ;
end


%* 
wave.Lambda = wave.Lambda([4, 5, 6, 7, 8, 9, 1, 2, 3]) ;