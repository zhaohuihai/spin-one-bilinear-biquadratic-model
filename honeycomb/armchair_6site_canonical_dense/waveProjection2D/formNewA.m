function A = formNewA(parameter, T1, PR)
%* A(x1,y4,z7,m1) = sum{x1'}_[PR(x1',x1)*T1(x1',y4,z7,m1)]
A = contractTensors(PR, 2, 1, T1, 4, 1, parameter) ;