function A = computeA_sqrtLx(parameter, A, L1)
%* A(x,y,z,m) = A(x,y,z,m)*sqrt(L1(x))

D = parameter.bondDimension ;

%* A(x,y,z,m) = A(x,y,z,m)*sqrt(Lambda{1}(x))
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * sqrt(L1(x)) ;
end
