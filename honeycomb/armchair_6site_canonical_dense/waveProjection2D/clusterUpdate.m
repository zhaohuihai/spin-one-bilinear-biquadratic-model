function [wave, truncationError, coef] = clusterUpdate(parameter, projectionOperator, wave)
%* Tensor to update: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* x. Lambda{1}: 1-2; Lambda{2}: 3-6; Lambda{3}: 5-4
%* y. Lambda{4}: 1-6; Lambda{5}: 3-4; Lambda{6}: 5-2
%* z. Lambda{7}: 1-4; Lambda{8}: 3-2; Lambda{9}: 5-6


%* A1(x1,y4,z7,m1) = A1(x1,y4,z7,m1)*sqrt(L1(x1))
A1 = computeA_sqrtLx(parameter, wave.A{1}, wave.Lambda{1}) ;

%* A2(x1,y6,z8,m2) = A2(x1,y6,z8,m2)*sqrt(L1(x1))
A2 = computeA_sqrtLx(parameter, wave.A{2}, wave.Lambda{1}) ;

%* T1(x1,y4,z7,m1) T2(x1,y6,z8,m2)
[T1, T2] = computeProjection(parameter, projectionOperator, A1, A2) ;

%=====================canonicalization===========================================
%* step A: absorb bond vector to environment sites
%* M4((x3,y5,m4),z7) = A4(x3,y5,z7,m4)*L3(x3)*L5(y5)*L7(z7)
M4 = computeM_z(parameter, wave.A{4}, wave.Lambda{3}, wave.Lambda{5}, wave.Lambda{7}) ;
%* M3((x2,y5,m3),z8) = A3(x2,y5,z8,m3)*L2(x2)*L5(y5)*L8(z8)
M3 = computeM_z(parameter, wave.A{3}, wave.Lambda{2}, wave.Lambda{5}, wave.Lambda{8}) ;

%* M6((x2,z9,m6),y4) = A6(x2,y4,z9,m6)*L2(x2)*L4(y4)*L9(z9)
M6 = computeM_y(parameter, wave.A{6}, wave.Lambda{2}, wave.Lambda{4}, wave.Lambda{9}) ;
%* M5((x3,z9,m5),y6) = A5(x3,y6,z9,m5)*L3(x3)*L6(y6)*L9(z9)
M5 = computeM_y(parameter, wave.A{5}, wave.Lambda{3}, wave.Lambda{6}, wave.Lambda{9}) ;

%* step B: QR factorization
%* M4((x3,y5,m4),z7) = sum{z7'}_[Q4((x3,y5,m4),z7')*R4(z7',z7)]
[Q4, R4] = qr(M4, 0) ;
R4 = R4 ./ norm(R4) ;
%* M3((x2,y5,m3),z8) = sum{z8'}_[Q3((x2,y5,m3),z8')*R3(z8',z8)]
[Q3, R3] = qr(M3, 0) ;
R3 = R3 ./ norm(R3) ;

%* M6((x2,z9,m6),y4) = sum{y4'}_[Q6((x2,z9,m6),y4')*R6(y4',y4)]
[Q6, R6] = qr(M6, 0) ;
R6 = R6 ./ norm(R6) ;
%* M5((x3,z9,m5),y6) = sum{y6'}_[Q5((x3,z9,m5),y6')*R5(y6',y6)]
[Q5, R5] = qr(M5, 0) ;
R5 = R5 ./ norm(R5) ;

%* step C: move toward system sites
%* change notation: R4(z7',z7) => R4(z7,z7'), R3(z8',z8) => R3(z8,z8'), 
%*                  R6(y4',y4) => R6(y4,y4'), R5(y6',y6) => R5(y6,y6')
%* M1((y4,z7,m1),x1) = sum{y4',z7'}_[T1(x1,y4',z7',m1)*R6(y4,y4')*R4(z7,z7')]
M1 = computeM_x(parameter, T1, R6, R4) ;

%* M2((y6,z8,m2),x1) = sum{y6',z8'}_[T2(x1,y6',z8',m2)*R5(y6,y6')*R3(z8,z8')]
M2 = computeM_x(parameter, T2, R5, R3) ;

%* step D: QR factorization

%* M1((y4,z7,m1),x1) = sum{x1'}_[Q1((y4,z7,m1),x1')*R1(x1',x1)]
[Q1, R1] = qr(M1, 0) ;

%* M2((y6,z8,m2),x1) = sum{x1''}_[Q2((y6,z8,m2),x1'')*R2(x1'',x1)]
[Q2, R2] = qr(M2, 0) ;

%* step E: SVD and truncation
%* M(x1',x1'') = sum{x1}_[R1(x1',x1)*R2(x1'',x1)]
M = R1 * R2' ;

%* M(x1',x1'') = sum{x1}_[U(x1',x1)*S(x1)*V(x1'',x1)]
[U0, S0, V0, coef] = applySVD(M) ;

[U, S, V, truncationError] = truncate(parameter, U0, S0, V0) ;

%* step F: Create Projector
%* PR(x1',x1) = sum{x1''}_[R2(x1'',x1')*V(x1'',x1)/S(x1)]
PR = createProjector(R2, V, S) ;

%* PL(x1',x1) = sum{x1''}_[R1(x1'',x1')*U(x1'',x1)/S(x1)]
PL = createProjector(R1, U, S) ;

%* step G: Form new system sites: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* A1(x1,y4,z7,m1) = sum{x1'}_[PR(x1',x1)*T1(x1',y4,z7,m1)]
wave.A{1} = formNewA(parameter, T1, PR) ;

% wave.A{1} = wave.A{1} ./ max(max(max(max(abs(wave.A{1}))))) ;

%* A2(x1,y6,z8,m2) = sum{x1'}_[PL(x1',x1)*T2(x1',y6,z8,m2)]
wave.A{2} = formNewA(parameter, T2, PL) ;

% wave.A{2} = wave.A{2} ./ max(max(max(max(abs(wave.A{2}))))) ;

wave.Lambda{1} = S ./ coef ;

%* step H: Verify the canonical condition

% c1 = verifyCanoCondition(M1, R2, V, S, coef) 
% c2 = verifyCanoCondition(M2, R1, U, S, coef) 
