function A = computeA_sqrtLx_Ly_Lz(parameter, A, Lambda)
%* A(x,y,z,m) = A(x,y,z,m)*sqrt(L1(x))*L2(y)*L3(z)

D = parameter.bondDimension ;

%* A(x,y,z,m) = A(x,y,z,m)*sqrt(Lambda{1}(x))
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * sqrt(Lambda{1}(x)) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Lambda{2}(y) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Lambda{3}(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lambda{3}(z) ;
end