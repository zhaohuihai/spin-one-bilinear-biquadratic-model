function T = computeM_x(parameter, T, Ry, Rz)
%* T((y,z,m),x) = sum{y',z'}_[T(x,y',z',m)*Ry(y,y')*Rz(z,z')]


%* T(x,z',m,y) = sum{y'}_[T(x,y',z',m)*Ry(y,y')]
T = contractTensors(T, 4, 2, Ry, 2, 2, parameter) ;

%* T(x,m,y,z) = sum{z'}_[T(x,z',m,y)*Rz(z,z')]
T = contractTensors(T, 4, 2, Rz, 2, 2, parameter) ;

%* T(x,m,y,z) => T(y,z,m,x)
T = permute(T, [3, 4, 2, 1]) ;

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* T(y,z,m,x) => T((y,z,m),x)
T = reshape(T, [D*D*M, D*M^2]) ;