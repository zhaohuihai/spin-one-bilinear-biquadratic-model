function wave = findGroundStateWave(parameter, wave)

% if parameter.VBCinitial == 1 && parameter.polarization == 1
%     error('polarize initial VBC wave function is not allowed.') ;
% end

if parameter.loadPreviousWave == 1
    if parameter.VBCinitial == 1
        warning('Old wave is loaded, unnecessary to initialize VBC wave') ;
        disp('The projection process will NOT initialize a VBC wave') ;
        parameter.VBCinitial = 0 ;
    end
    if parameter.polarization == 1
        warning('Old wave is loaded, unnecessary to polarize')
        disp('The projection process will NOT polarize the wave function')
        parameter.polarization = 0 ;
    end
end

if parameter.projectionSequence == 1
    disp('projection sequence: 1, 2, 3, 4, 5, 6, 7, 8, 9') ;
elseif parameter.projectionSequence == 2
    disp('projection sequence: 1, 4, 7, 2, 5, 8, 3, 6, 9') ;
end

if parameter.projectionMethod == 1
    disp('simple update') ;
elseif parameter.projectionMethod == 2 
    disp('cluster update') ;
end

if parameter.VBCinitial == 1 || parameter.polarization == 1
    wave = initializeVBCwave(parameter, wave) ;
end

wave = applyWaveProjection(parameter, wave) ;


