function A = computeM_z(parameter, A, Lx, Ly, Lz)
%* A((x,y,m),z) = A(x,y,z,m)*Lx(x)*Ly(y)*Lz(z)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* A(x,y,z,m) = A(x,y,z,m)*Lx(x)
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lx(x) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Ly(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Ly(y) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Lz(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lz(z) ;
end

%* A(x,y,z,m) => A(x,y,m,z)
A = permute(A, [1, 2, 4, 3]) ;

%* A(x,y,m,z) => A((x,y,m),z)
A = reshape(A, [D*D*M, D]) ;