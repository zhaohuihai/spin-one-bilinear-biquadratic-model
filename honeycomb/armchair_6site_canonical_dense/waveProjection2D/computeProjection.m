function [Ta, Tb] = computeProjection(parameter, projectionOperator, A, B)
%* Ta((x,l),y1,z1,n1) Tb((x,l),y2,z2,n2)


%* Pa(m1,n1,l)
Pa = projectionOperator.Pa ;

%* Ta(x,l,y1,z1,n1) = sum{m1}_[A(x,y1,z1,m1)*Pa(m1,n1,l)]
Ta = contractTensors(A, 4, 4, Pa, 3, 1, [1, 5, 2, 3, 4], parameter) ;

%* Pb(m2,n2,l)
Pb = projectionOperator.Pb ;

%* Tb(x,l,y2,z2,n2) = sum{m2}_[B(x,y2,z2,m2)*Pb(m2,n2,l)]
Tb = contractTensors(B, 4, 4, Pb, 3, 1, [1, 5, 2, 3, 4], parameter) ;

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* Ta(x,l,y1,z1,n1) -> Ta((x,l),y1,z1,n1)
Ta = reshape(Ta, [D * M^2, D, D, M]) ;

%* Tb(x,l,y2,z2,n2) -> Tb((x,l),y2,z2,n2)
Tb = reshape(Tb, [D * M^2, D, D, M]) ;