function A = computeM_y(parameter, A, Lx, Ly, Lz)
%* A((x,z,m),y) = A(x,y,z,m)*Lx(x)*Ly(y)*Lz(z)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* A(x,y,z,m) = A(x,y,z,m)*Lx(x)
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lx(x) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Ly(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Ly(y) ;
end

%* A(x,y,z,m) = A(x,y,z,m)*Lz(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lz(z) ;
end

%* A(x,y,z,m) => A(x,z,m,y)
A = permute(A, [1, 3, 4, 2]) ;

%* A(x,z,m,y) => A((x,z,m),y)
A = reshape(A, [D*D*M, D]) ;