function wave = symmetrizeWave(wave)

% 1, 5, 9
wave.Lambda{5} = wave.Lambda{1} ;
wave.Lambda{9} = wave.Lambda{1} ;

% 4, 8, 3
wave.Lambda{8} = wave.Lambda{4} ;
wave.Lambda{3} = wave.Lambda{4} ;

% 7, 2, 6
wave.Lambda{2} = wave.Lambda{7} ;
wave.Lambda{6} = wave.Lambda{7} ;
% A1(1,4,7)
wave.A{3} = permute(wave.A{1}, [3, 1, 2, 4]) ;
wave.A{5} = permute(wave.A{1}, [2, 3, 1, 4]) ;

wave.A{4} = permute(wave.A{2}, [3, 1, 2, 4]) ;
wave.A{6} = permute(wave.A{2}, [2, 3, 1, 4]) ;