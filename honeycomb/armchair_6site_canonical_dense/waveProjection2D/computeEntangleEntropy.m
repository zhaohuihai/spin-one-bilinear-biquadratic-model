function entangleEntropy = computeEntangleEntropy(parameter, wave)

Lambda =  wave.Lambda ;
entangleEntropy = zeros(1, length(Lambda)) ;
for i  = 1 : length(Lambda)
    L = Lambda{i}.^2 ;
    L = L ./ sum(L) ;
    entangleEntropy(i) = - sum(L .* log(L)) ;
end

P = zeros(1, 3) ;

strong = sum(entangleEntropy([1, 3, 4, 5, 8, 9])) ;
weak = sum(entangleEntropy([2, 6, 7])) ;
P(1) = strong / (2 * weak) - 1 ;

strong = sum(entangleEntropy([1, 2, 5, 6, 7, 9])) ;
weak = sum(entangleEntropy([3, 4, 8])) ;
P(2) = strong / (2 * weak) - 1 ;

strong = sum(entangleEntropy([2, 3, 4, 6, 7, 8])) ;
weak = sum(entangleEntropy([1, 5, 9])) ;
P(3) = strong / (2 * weak) - 1 ;

pVBC = max(P) ;

saveExpectationValue(parameter, pVBC, 'pVBC_entangleEntropy')