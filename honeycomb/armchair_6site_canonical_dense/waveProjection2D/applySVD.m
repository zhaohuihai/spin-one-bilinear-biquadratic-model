function [U, S, V, coef] = applySVD(M)
%* M(x1',x1'') = sum{x1}_[U(x1',x1)*S(x1)*V(x1'',x1)]

[U, S, V] = svd(M) ;
S = diag(S) ;

coef = S(1) ;

% S = S ./ coef ;