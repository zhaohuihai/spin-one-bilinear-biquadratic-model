function PR = createProjector(R2, V, S) 
%* PR(x1',x1) = sum{x1''}_[R2(x1'',x1')*V(x1'',x1)/S(x1)]

D = size(V, 2) ;

%* V(x1'',x1) = V(x1'',x1)/S(x1)
for x1 = 1 : D
    V(:, x1) = V(:, x1) ./ S(x1) ;
end

%* PR(x1',x1) = sum{x1''}_[R2(x1'',x1')*V(x1'',x1)]
PR = R2' * V ;
