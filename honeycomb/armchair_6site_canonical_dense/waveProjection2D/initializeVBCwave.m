function wave = initializeVBCwave(parameter, wave)

if parameter.polarization == 1
    parameter.field = parameter.polarField ;
    disp('Polarize the wave function') ;
end
%************************************************************************************
M = parameter.siteDimension ;
D = parameter.bondDimension ;

disp(['site dimension = ', num2str(M)]) ;
disp(['wave function bond dimension = ', num2str(D)])
disp(['theta = ', num2str(parameter.theta)]) ;

if parameter.loadPreviousWave == 0
    parameter.tau = parameter.tauInitial ;
    wave.polarization = parameter.polarization ;
else
    parameter.tau = wave.tauFinal ;
end

tauChangeFactor = parameter.tauChangeFactor ;

if isfield(wave, 'totalProjectionTimes')
    j = wave.totalProjectionTimes ;
else
    j = 0 ;
end
% **********************************************************************************
if parameter.VBCinitial == 1
    if parameter.plaquetteVBC == 1
        disp('Initialize the wave function to a plaquette valence bond crystal') ;
    elseif parameter.columnarVBC == 1
        disp('Initialize the wave function to a columnar valence bond crystal') ;
    elseif parameter.staggeredVBC == 1
        disp('Initialize the wave function to a staggered valence bond crystal') ;
    end
    parameter.VBCanisotropyRatio = parameter.VBCpolarRatio ;
    disp(['VBC anisotropy ratio = ', num2str(parameter.VBCanisotropyRatio)]) ;
    parameter.theta = 0 ;
end
parameter.H = createHamiltonian_BilinBiqua(parameter) ;
%=================================================================================
coef0 = 0 ;
while parameter.tau >= parameter.tauFinal
    disp(['tau = ', num2str(parameter.tau)])
    
    projectionConvergence = 1 ;
    step = 0 ;
    while projectionConvergence > parameter.convergenceCriterion_VBC && step <= parameter.maxProjectionStep
        
        j = j + 1 ;
        step = step + 1 ;
        [wave, truncationError, coef] = projectVBC(parameter, wave) ;
        
        projectionConvergence = abs(coef0 - coef) / abs(coef) ;
        if (j / 20) == floor(j / 20)
            disp(['projection step = ', num2str(step), ', tau = ', num2str(parameter.tau)])
            disp(['truncation error = ', num2str(truncationError)])
            disp(['projection convergence error = ', num2str(projectionConvergence)]) ;
        end
        coef0 = coef ;
    end
    
    wave.totalProjectionTimes = j ;
    
%     saveWave(parameter, wave) ;
    
    tau = 1 / ((1 / parameter.tau) + tauChangeFactor) ;
    tauChangeFactor = tauChangeFactor + parameter.tauChangeFactor ;
    if parameter.tau > parameter.tauFinal && tau < parameter.tauFinal
        parameter.tau = parameter.tauFinal ;
    else
        parameter.tau = tau ;
    end
end
totalProjectionTimes = j

saveWave(parameter, wave) ;