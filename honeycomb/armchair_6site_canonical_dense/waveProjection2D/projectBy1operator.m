function [A, S, truncationError, coef] = projectBy1operator(parameter, projectionOperator, A, Lambda)
%*************************************
%  L(4)\z1        y2/L(3)
%       \          /
%       A\__L(1)__/B
%        /   x    \
%       /          \
%  L(2)/y1        z2\L(5)
%*************************************
%* A(x,y1,z1,m1) B(x,y2,z2,m2)
[A, B] = computeLyLz_A_Lx_B_LyLz(parameter, A, Lambda) ;

%* Ta(x,l,y1,z1,m1) Tb(x,l,y2,z2,m2)
[Ta, Tb] = computeProjection(parameter, projectionOperator, A, B) ;
%* U((y1,z1,m1),x) V((y2,z2,m2),x) S(x)
[U, S, V, coef] = svd_projection(parameter, Ta, Tb) ;

[U, S, V, truncationError] = truncate(parameter, U, S, V) ;

A = recover(parameter, Lambda, U, V) ;
