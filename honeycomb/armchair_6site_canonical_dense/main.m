
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

if parameter.loadPreviousWave == 0
    wave = initializeWave(parameter) ;
    disp('create new wave')
else
    [wave, parameter] = loadWave(parameter) ;
end

if parameter.projection == 1
    if parameter.TrotterOrder == 1
        disp('apply first order Trotter decomposition')
    elseif parameter.TrotterOrder == 2
        disp('apply second order Trotter decomposition')
    end
    wave = findGroundStateWave(parameter, wave) ;
end

if parameter.computation.expectationValue == 1
    computeExpectationValue_dense(parameter, wave)
end

% entangleEntropy = computeEntangleEntropy(parameter, wave)

%* Sz correlation: <Sz(i)*Sz(i+l)> - <Sz(i)><Sz(i+l)>
%* Qzz correlation: <Qzz(i)*Qzz(i+l)> - <Qzz(i)><Qzz(i+l)>
if parameter.computation.correlation == 1
    [SzCorrelation, QzzCorrelation] = computeCorrelation(parameter, wave) ;
end
