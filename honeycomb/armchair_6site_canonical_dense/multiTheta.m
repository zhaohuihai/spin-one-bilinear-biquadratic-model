
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

parameter.theta = parameter.thetaInitial ;

D = parameter.bondDimension ;
Chi = parameter.dim_MPS ;

prefix = ['D', num2str(D), '_Chi', num2str(Chi), '_'] ;

i = 1 ;
while parameter.theta <= parameter.thetaFinal
    
    if parameter.loadPreviousWave == 0
        wave = initializeWave(parameter) ;
        disp('create new wave')
    else
        [wave, parameter] = loadWave(parameter) ;
    end
    
    if parameter.projection == 1
        if parameter.TrotterOrder == 1
            disp('apply first order Trotter decomposition')
        elseif parameter.TrotterOrder == 2
            disp('apply second order Trotter decomposition')
        end
        wave = findGroundStateWave(parameter, wave) ;
    end
    
    
    fileName = [prefix, num2str(i), '.txt'] ;
    delete(fileName) ;
    diary(fileName) ;
    diary on
    
    computeExpectationValue_dense(parameter, wave)
%     entangleEntropy = computeEntangleEntropy(parameter, wave)
    diary off ;
    
    parameter.theta = parameter.theta + parameter.thetaIncre ;
    i = i + 1 ;
end
