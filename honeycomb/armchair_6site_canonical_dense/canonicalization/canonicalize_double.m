function MPS = canonicalize_double(MPS)

%* A(a,i,b)
A = MPS.A(1).tensor3 ;
Adim = MPS.A(1).dim ;

%* B(b,j,c)
B = MPS.A(2).tensor3 ;
Bdim = MPS.A(2).dim ;

%* A((a,i),b)
A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
%* A(b,(j,c))
B = reshape(B, [Bdim(1), Bdim(2) * Bdim(3)]) ;

%* AB((a,i),(j,c))
AB = A * B ;
%* AB(a,(i,j),c)
ABdim = [Adim(1), Adim(2) * Bdim(2), Bdim(3)] ;
AB = reshape(AB, ABdim) ;

MPS_single.A.tensor3 = AB ;
MPS_single.A.dim = ABdim ;

[MPS_single, LB] = canonicalize_single(MPS_single) ;
LBdim = length(LB) ;

%* AB(a,(i,j),c)
AB = MPS_single.A.tensor3 ;
ABdim = MPS_single.A.dim ;

%* AB(a,(i,j),c) = LB(a)*AB(a,(i,j),c)
for a = 1 : ABdim(1)
    AB(a, :, :) = LB(a) * AB(a, :, :) ;
end
%--------------------------------------------
%* AB((a,i),(j,c))
AB = reshape(AB, [Adim(1) * Adim(2), Bdim(2) * Bdim(3)]) ;
%* U((a,i),b) LA(b,b) V((j,c),b)
[U, LA, V] = svd(AB, 'econ') ;
%* LA(b)
LA = diag(LA) ;
LAdim = Adim(3) ;
U = U(:, 1 : LAdim) ;
V = V(:, 1 : LAdim) ;
LA = LA(1 : LAdim) ;

%* U(a,i,b)
U = reshape(U, [Adim(1 : 2), LAdim]) ;
A = zeros(Adim) ;
%* A(a,i,b) = U(a,i,b) / LB(a) ;
for a = 1 : Adim(1)
    A(a, :, :) = U(a, :, :) ./ LB(a) ;
end
%* A(a,i,b) = A(a,i,b) * LA(b)
for b = 1 : LAdim
    A(:, :, b) = A(:, :, b) .* LA(b) ;
end
%* V(b,(j,c))
V = V' ;
%* V(b,j,c)
B = reshape(V, [LAdim, Bdim(2 : 3)]) ;

MPS.A(1).tensor3 = A ;
MPS.A(1).dim = Adim ;
MPS.A(2).tensor3 = B ;
MPS.A(2).dim = Bdim ;
%==========================================================
%* verification
A1 = A ;
id = eye(Adim(1)) ;
%* A1(a,(i,b))
A1 = reshape(A1, [Adim(1), Adim(2)*Adim(3)]) ;
aa = A1 * A1' ;
norm(aa - id * aa(1,1))

A2 = zeros(Adim) ;
%* A(a,i,b) = LB(a) * A(a,i,b)
for a = 1 : LBdim
    A2(a, :, :) = LB(a) .* A(a, :, :)  ;
end
%* A2((a,i),b)
A2 = reshape(A2, [Adim(1) * Adim(2), Adim(3)]) ;
aa = A2' * A2 ;
norm(aa - diag(LA.^2))

id = eye(Bdim(1)) ;
B1 = B ;
%* B1(b,(j,c))
B1 = reshape(B1, [Bdim(1), Bdim(2) * Bdim(3)]) ;
bb = B1 * B1' ;
norm(bb - id * bb(1,1))

B2 = zeros(Bdim) ;
%* B2(b,j,c) = LA(b) * B(b,j,c)
for b = 1 : LAdim
    B2(b, :, :) = LA(b) .* B(b, :, :)  ;
end
%* B2((b,j),c)
B2 = reshape(B2, [Bdim(1) * Bdim(2), Bdim(3)]) ;
bb = B2' * B2 ;
norm(bb - diag(LB.^2))