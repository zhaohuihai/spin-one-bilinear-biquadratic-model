function T = computeKronA_A(A, Adim)
%* A(a,b,i)
A = permute(A, [1, 3, 2]) ;
%* A((a,b),i)
A = reshape(A, [Adim(1) * Adim(3), Adim(2)]) ;

%* T((a,b),(a',b')) = sum{i}_(A((a,b),i)*conj(A((a,b),i)))
T = A * A' ;
%* T(a,b,a',b')
T = reshape(T, [Adim([1, 3]), Adim([1, 3])]) ;
%* T(a,a',b,b')
T = permute(T, [1, 3, 2, 4]) ;
%* T((a,a'),(b,b'))
T = reshape(T, [Adim(1)^2, Adim(3)^2]) ;