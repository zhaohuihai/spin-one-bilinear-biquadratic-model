
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

parameter.field = parameter.fieldInitial ;
i = 1 ;
while parameter.field <= parameter.fieldFinal
    
    if parameter.loadPreviousWave == 0
        wave = initializeWave(parameter) ;
        disp('create new wave')
    else
        [wave, parameter] = loadWave(parameter) ;
    end
    
    if parameter.projection == 1
        if parameter.TrotterOrder == 1
            disp('apply first order Trotter decomposition')
        elseif parameter.TrotterOrder == 2
            disp('apply second order Trotter decomposition')
        end
        wave = findGroundStateWave(parameter, wave) ;
    end
    
    
    fileName = [num2str(i), '.txt'] ;
    delete(fileName) ;
    diary(fileName) ;
    diary on
    
    computeExpectationValue_dense(parameter, wave)
    
    diary off ;
    
    parameter.field = parameter.field + parameter.fieldIncre ;
    i = i + 1 ;
end
