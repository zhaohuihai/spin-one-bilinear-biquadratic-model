function [U, S, V, coef] = svd_projection(parameter, Ta, Tb)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* Ta((x,l),(y1,z1,m1))
Ta = reshape(Ta, [D*M^2, D^2*M]) ;
%------------------------------------
Ta = gdouble(Ta) ;
%------------------------------------
%* Ua((x,l),s1) Sa(s1) Va((y1,z1,m1),s1)
[Ua, Sa, Va] = svd(Ta, 'econ') ;
clear Ta
%* Tb((x,l),(y2,z2,m2))
Tb = reshape(Tb, [D*M^2, D^2*M]) ;
%----------------------------------
Tb = gdouble(Tb) ;
%----------------------------------
%* Ub((x,l),s2) Sb(s2) Vb((y2,z2,m2),s2)
[Ub, Sb, Vb] = svd(Tb, 'econ') ;
clear Tb
%* Ua((x,l),s1) = Ua((x,l),s1)*Sa(s1)
Ua = Ua * Sa ;
%* Ub((x,l),s2) = Ub((x,l),s2)*Sb(s2)
Ub = Ub * Sb ;
%* N(s1,s2) = sum{x,l}_[Ua((x,l),s1)*Ub((x,l),s2)]
N = Ua' * Ub ;

%* Un(s1,x) S(s1,x) Vn(s2,x)
[Un, S, Vn] = svd(N, 'econ') ;
S = diag(S) ;
coef = S(1) ;
S = S ./ coef ;

%* U((y1,z1,m1),x) = sum{s1}_[Va((y1,z1,m1),s1) * Un(s1,x)]
U = Va * Un ;

%* V((y2,z2,m2),x) = sum{s2}_[Vb((y2,z2,m2),s2) * Vn(s2,x)]
V = Vb * Vn ;
%-----------------------------
U = double(U) ;
S = double(S) ;
V = double(V) ;
coef = double(coef) ;
%-----------------------------
