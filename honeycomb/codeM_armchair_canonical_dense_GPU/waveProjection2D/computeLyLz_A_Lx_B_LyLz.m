function [A, B] = computeLyLz_A_Lx_B_LyLz(parameter, wave)
%* A(x,y1,z1,m1) B(x,y2,z2,m2)

D = parameter.bondDimension ;

A = wave.A ;
B = wave.B ;
Lambda = wave.Lambda ;
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{1}(x)
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lambda{1}(x) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Lambda{2}(y) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{3}(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lambda{3}(z) ;
end

%* B(x,y,z,m) = B(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    B(:, y, :, :) = B(:, y, :, :) * Lambda{2}(y) ;
end
%* B(x,y,z,m) = B(x,y,z,m)*Lambda{3}(z)
for z = 1 : D
    B(:, :, z, :) = B(:, :, z, :) * Lambda{3}(z) ;
end 


