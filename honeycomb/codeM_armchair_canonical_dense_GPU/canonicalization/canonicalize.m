function [wave1D, Lambda, coef] = canonicalize(parameter, AB)
%* AB((a,i),(c,j))

di = parameter.bondDimension^2 ;
dj = di ;
D = round(size(AB, 1) / di) ;

%* AB(a,i,c,j)
AB = reshape(AB, [D, di, D, dj]) ;
%* AB(a,i,j,c)
AB = permute(AB, [1, 2, 4, 3]) ;
%* AB(a,(i,j),c)
AB = reshape(AB, [D, di * dj, D]) ;

MPS_single.A.tensor3 = AB ;
MPS_single.A.dim = [D, di * dj, D] ;

[MPS_single, LB, coef] = canonicalize_single(MPS_single) ;
% LBdim = length(LB) ;

%* AB(a,(i,j),c)
AB = MPS_single.A.tensor3 ;
ABdim = MPS_single.A.dim ;
clear MPS_single
%* AB(a,(i,j),c) = LB(a)*AB(a,(i,j),c)
for a = 1 : ABdim(1)
    AB(a, :, :) = LB(a) * AB(a, :, :) ;
end
%--------------------------------------------
AB = gdouble(AB) ;
%--------------------------------------------
%* AB((a,i),(j,c))
AB = reshape(AB, [D * di, dj * D]) ;
%* U((a,i),b) LA(b,b) V((j,c),b)
[U, LA, V] = svd(AB, 'econ') ;
clear AB ;
%--------------------------------------------
U = double(U) ;
LA = double(LA) ;
V = double(V) ;
%--------------------------------------------
%* LA(b)
LA = diag(LA) ;
LAcoef = LA(1) ;
coef = LAcoef * coef ;
LAdim = length(LA) ;

%* U(a,i,b)
U = reshape(U, [D, di, LAdim]) ;
A = zeros([D, di, LAdim]) ;
%* A(a,i,b) = U(a,i,b) / LB(a)
for a = 1 : D
    A(a, :, :) = U(a, :, :) ./ LB(a) ;
end
clear U
%* A(a,i,b) = A(a,i,b) * LA(b)
for b = 1 : LAdim
    A(:, :, b) = A(:, :, b) .* LA(b) ;
end
%* A(a,b,i)
A = permute(A, [1, 3, 2]) ;

%* V(b,(j,c))
V = V' ;
%* B(b,j,c)
B = reshape(V, [LAdim, dj, D]) ;
clear V
%* B(b,c,j)
B = permute(B, [1, 3, 2]) ;

Lambda = LA ;
wave1D.A = A ;
clear A ;
wave1D.B = B ;
clear B ;
%===================================================================
% %* verification
% A1 = A ;
% id = eye(D) ;
% %* A1(a,(b,i))
% A1 = reshape(A1, [D, LAdim*di]) ;
% aa = A1 * A1' ;
% norm(aa - id * aa(1,1))
% 
% A2 = zeros([D, LAdim, di]) ;
% %* A2(a,b,i) = LB(a) * A(a,b,i)
% for a = 1 : D
%     A2(a, :, :) = LB(a) .* A(a, :, :)  ;
% end
% %* A2(a,i,b)
% A2 = permute(A2, [1, 3, 2]) ;
% %* A2((a,i),b)
% A2 = reshape(A2, [D * di, LAdim]) ;
% aa = A2' * A2 ;
% norm(aa - diag(LA.^2))
% 
% id = eye(LAdim) ;
% B1 = B ;
% %* B1(b,(c,j))
% B1 = reshape(B1, [LAdim, D * dj]) ;
% bb = B1 * B1' ;
% norm(bb - id * bb(1,1))
% 
% B2 = zeros([LAdim, D, dj]) ;
% %* B2(b,c,j) = LA(b) * B(b,c,j)
% for b = 1 : LAdim
%     B2(b, :, :) = LA(b) .* B(b, :, :)  ;
% end
% %* B2(b,j,c)
% B2 = permute(B2, [1, 3, 2]) ;
% %* B2((b,j),c)
% B2 = reshape(B2, [LAdim * dj, D]) ;
% bb = B2' * B2 ;
% norm(bb - diag(LB.^2))