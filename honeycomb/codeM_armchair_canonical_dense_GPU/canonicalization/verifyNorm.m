function verifyNorm(A, D, M)
%* A(a,i,b)
A0 = A ;
x = zeros(D, D) ;
for i = 1 : D
    for j = 1 : D
        %* V(a,a')
        V = squeeze(A0(i, :, :))' * squeeze(A0(j, :, :)) ;
        % V = reshape(V, [D^2, 1]) ;
        % V = rand(D, D) ;
        % T = computeKronA_A(A, [D, M, D]) ;
        % T = T' ;
        % [Vright, etaR] = computeDominantRightEigen(T) ;
        coef = 1 ;
        converge = 1 ;
        step = 0 ;
        while converge > 1e-15
            step = step + 1 ;
            
            %* A(a,(i,b))
            A = reshape(A, [D, D * M]) ;
            %* VA(a,(i,b')) = V(a,a')*A(a',(i,b'))
            VA = V * A ;
            %* VA((a,i),b')
            VA = reshape(VA, [D * M, D]) ;
            
            %* A((a,i),b)
            A = reshape(A, [D * M, D]) ;
            
            V1 = (A') * VA ;
            
            c = max(abs(diag(V1))) ;
            V1 = V1 ./ c ;
            coef = coef * c ;
            converge = norm(V - V1) ;
            V = V1 ;
            %***************************************************
            %     V1 = T * V ;
            %     c = norm(V1, inf) ;
            %     V1 = V1 ./ c ;
            %     coef = coef * c ;
            %     converge = norm(V1 - V) ;
            %     V = V1 ;
        end
        x(i, j) = trace(V) * coef ;
    end
end
x