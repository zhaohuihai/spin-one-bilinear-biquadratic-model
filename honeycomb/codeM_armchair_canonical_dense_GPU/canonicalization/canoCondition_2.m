function [A, L] = canoCondition_2(A, Adim)

%* A1(b,i,a)
A1 = permute(A, [3, 2, 1]) ;

%* Vleft((b,b'))
[Vleft, etaL] = computeDominantRightEigen(A1, Adim([3,2,1])) ;

%* Vleft = Wl*Dl*Wl'
% [Wl, Dl] = decomposeV(Vleft) ;
[ Wl, Dl, SymError ] = OrderEigOfPoSy ( Vleft ) ; 
[ EigdecomError, Dl ] = CheckEigError ( Wl, Dl, Vleft ) ;

L = sqrt(diag(Dl)) ;
%* A((a,i),b)
A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
A = A * Wl ;
%* A(a,(i,b))
A = reshape(A, [Adim(1), Adim(2) * Adim(3)]) ;
A = Wl'* A ;
%*************************************************
% %* x(a',(i,b))
% x = Dl * A ;
% %* x((a',i),b)
% x = reshape(x, [Adim(1) * Adim(2), Adim(3)]) ;
% %* A((a',i),b')
% A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
% norm(x'*A - Dl)
%*************************************************
%* A(a,i,b)
A = reshape(A, Adim) ;