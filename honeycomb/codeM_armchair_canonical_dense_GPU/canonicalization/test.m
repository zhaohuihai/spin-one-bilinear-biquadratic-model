clc
clear
format long
rs = RandStream('mt19937ar', 'Seed', 5489) ;
d = 2 ;
D = 30 ;
dim = [D, d, D] ;

for i = 1 : 2
    MPS.A(i).tensor3 = rand(dim) ;
    MPS.A(i).dim = dim ;
    
end
MPS = canonicalize_double(MPS) ;
%=================================================
