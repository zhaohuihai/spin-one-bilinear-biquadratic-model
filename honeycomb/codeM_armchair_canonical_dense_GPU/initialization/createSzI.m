function SzI = createSzI(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

SzI = zeros(M, M, M, M) ;
MM = M * M ;

%* SzI(m1, m2, n1, n2) = <m1, m2| SzI | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
%     m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        SzI(q1, q2, q1, q2) = SzI(q1, q2, q1, q2) + m1 ;
    end
end

SzI = reshape(SzI, [MM, MM]) ;