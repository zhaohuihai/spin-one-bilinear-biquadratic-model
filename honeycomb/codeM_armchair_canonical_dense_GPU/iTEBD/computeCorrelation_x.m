function [SzCorrelation, SzCorrelationM, QzzCorrelation, QzzCorrelationM] = computeCorrelation_x(parameter, wave)
%* correlation is maxCorrelationLength X 1 column vector

%* A(x, y, z, m1) B(x, y, z, m2)
[A, B] = absorbLambda_dense(parameter, wave) ;

%* Ta(x,i,k) 
Ta = computeTensorProductA_A_dense(parameter, A) ;
%* Tb(x,l,j)
Tb = computeTensorProductA_A_dense(parameter, B) ;

[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, Ta, Tb) ;

%*=============================================================================
%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)
[VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, Ta, Tb, rightMPS, leftMPS) ;
%*=============================================================================
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)
% [QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, Ta, Tb, rightMPS, leftMPS, A, B) ;
%===================================================================================================
if parameter.computation.SzCorrelation == 1
    parameter.AsiteOperator = createSzI(parameter) ;
    parameter.BsiteOperator = createISz(parameter) ;
    parameter.correlationOperator = createSzSz(parameter) ;
    [SzCorrelation, SzCorrelationM] = findCorrelationFunction(parameter, Ta, Tb, rightMPS, leftMPS, A, B, VR, VL, PR, PL, eta) ;
else
    SzCorrelation = [] ;
    SzCorrelationM = [] ;
end
if parameter.computation.QzzCorrelation == 1
    parameter.AsiteOperator = createQzzI(parameter) ;
    parameter.BsiteOperator = createIQzz(parameter) ;
    parameter.correlationOperator = createQzzQzz(parameter) ;
    [QzzCorrelation, QzzCorrelationM] = findCorrelationFunction(parameter, Ta, Tb, rightMPS, leftMPS, A, B, VR, VL, PR, PL, eta) ;
else
    QzzCorrelation = [] ;
    QzzCorrelationM = [] ;
end