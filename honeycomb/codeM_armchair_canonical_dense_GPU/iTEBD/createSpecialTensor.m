function [QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, Ta, Tb, rightMPS, leftMPS, A, B)
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)

%* ABR(a,c,i,j) = sum{b}_[A(a,b,i)*B(b,c,j)]
ABR = contractTensors(rightMPS.A, 3, 2, rightMPS.B, 3, 1, [1, 3, 2, 4]) ;
%* ABL(a',c',k,l) = sum{b'}_[A(a',b',k)*B(b',c',l)]
ABL = contractTensors(leftMPS.A, 3, 2, leftMPS.B, 3, 1, [1, 3, 2, 4]) ;

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, A, B) ;
%==============================================================
%* W(i,k,l,j) = sum{x,n}_[TCa(x,i,k,n)*TCb(x,l,j,n)]
W = contractTensors(TCa, 4, [1, 4], TCb, 4, [1, 4]) ;

%* QR(a,c,k,l) = sum{i,j}_[ABR(a,c,i,j)*W(i,k,l,j)]
QR = contractTensors(ABR, 4, [3, 4], W, 4, [1, 4]) ;
%* change notation: QR(a,c,j,i)
clear W
%* QR(j,a,i,c)
QR = permute(QR, [3, 1, 4, 2]) ;
%==============================================================
%* Wa(i,k,n,l,j) = sum{x}_[TCa(x,i,k,n)*Tb(x,l,j)]
Wa = contractTensors(TCa, 4, 1, Tb, 3, 1) ;

%* QRa(k,n,l,a,c) = sum{i,j}_[Wa(i,k,n,l,j)*ABR(a,c,i,j)]
QRa = contractTensors(Wa, 5, [1, 5], ABR, 4, [3, 4]) ;
%* change notation: QRa(j,n,i,a,c)
%* QRa(j,a,i,c,n)
QRa = permute(QRa, [1, 4, 3, 5, 2]) ;

%* QLa(i,n,j,a',c') = sum{k,l}_[Wa(i,k,n,l,j)*ABL(a',c',k,l)]
QLa = contractTensors(Wa, 5, [2, 4], ABL, 4, [3, 4]) ;
clear Wa
%* change notation: QLa(i,n,l,a',c')
%* QLa(c',l,a',i,n)
QLa = permute(QLa, [5, 3, 4, 1, 2]) ;

%* Wb(i,k,l,j,n) = sum{x}_[Ta(x,i,k)*TCb(x,l,j,n)]
Wb = contractTensors(Ta, 3, 1, TCb, 4, 1) ;

%* QRb(k,l,n,a,c) = sum{i,j}_[Wb(i,k,l,j,n)*ABR(a,c,i,j)]
QRb = contractTensors(Wb, 5, [1, 4], ABR, 4, [3, 4]) ;
%* change notation: QRb(j,i,n,a,c)
%* QRb(j,a,i,c,n)
QRb = permute(QRb, [1, 4, 2, 5, 3]) ;

%* QLb(i,j,n,a',c') = sum{k,l}_[Wb(i,k,l,j,n)*ABL(a',c',k,l)]
QLb = contractTensors(Wb, 5, [2, 3], ABL, 4, [3, 4]) ;
clear Wb
%* change notation: QLb(i,l,n,a',c')
%* QLb(c',l,a',i,n)
QLb = permute(QLb, [5, 2, 4, 1, 3]) ;



