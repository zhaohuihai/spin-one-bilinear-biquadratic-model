function [A, B] = absorbLambda_dense(parameter, wave)

Lambda = cell(1, 3) ;
for i = 1 : 3
    Lambda{i} = sqrt(wave.Lambda{i}) ;
end

A = Atimes3Lambda_dense(parameter, wave.A, Lambda) ;
B = Atimes3Lambda_dense(parameter, wave.B, Lambda) ;

