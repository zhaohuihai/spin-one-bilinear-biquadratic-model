function AB = computeA_L1_B_dense(parameter, MPS)
%* AB((a,c),(i,j)) = sum{b}_(A(a,b,i)*Lambda1(b)*B(b,c,j))

d = parameter.bondDimension^2 ;
% D = parameter.dim_MPS ;

A = MPS.A ;
B = MPS.B ;
Lambda = MPS.Lambda ;

D1 = length(Lambda{1}) ;
D2 = length(Lambda{2}) ;
%* A(a,b,i) = A(a,b,i)*Lambda1(b)
for b = 1 : D1
    A(:, b, :) = A(:, b, :) * Lambda{1}(b) ;
end
%* A(a,i,b)
A = permute(A, [1, 3, 2]) ;
%* A((a,i),b)
A = reshape(A, [D2 * d, D1]) ;

%* B(b,(c,j))
B = reshape(B, [D1, D2 * d]) ;

%* AB((a,i),(c,j))
AB = A * B ;

%* AB(a,i,c,j)
AB = reshape(AB, [D2, d, D2, d]) ;
%* AB(a,c,i,j)
AB = permute(AB, [1, 3, 2, 4]) ;
%* AB((a,c),(i,j))
AB = reshape(AB, [D2^2, d^2]) ;
