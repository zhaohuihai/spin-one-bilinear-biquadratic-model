function [MPS, coef, truncationError_TMP] = applyTMP_dense(parameter, T, MPS)

%* AB((a,c),(i,j))
AB = computeA_B(parameter, MPS) ;
%* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
R = projectTransferMatrix_dense(parameter, T, AB) ;

[MPS, Lambda, coef] = canonicalize(parameter, R) ;

[MPS, truncationError_TMP] = truncate_canonical(parameter, MPS, Lambda) ;

MPS = exchangeAandB_TMP_dense(MPS) ;

