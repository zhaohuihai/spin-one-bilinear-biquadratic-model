function [MPS, coef, truncationError] = applyRightGateOperation1D(parameter, Ta, Tb, MPS)
% Ta(x,i,k) Tb(x,l,j)

c = zeros(1, 2) ;
truncErr_single = zeros(1, 2) ;

[MPS, c(1), truncErr_single(1)] = computeSingleGate1D(parameter, Ta, Tb, MPS) ;
[MPS, c(2), truncErr_single(2)] = computeSingleGate1D(parameter, Ta, Tb, MPS) ;

truncationError = max(truncErr_single) ;
coef = prod(c) ;
