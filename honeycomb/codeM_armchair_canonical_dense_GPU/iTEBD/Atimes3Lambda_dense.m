function A = Atimes3Lambda_dense(parameter, A, Lambda)

D = parameter.bondDimension ;
%* x
for i = 1 : D
    A(i, :, :, :) = A(i, :, :, :) * Lambda{1}(i) ;
end
%* y
for i = 1 : D
    A(:, i, :, :) = A(:, i, :, :) * Lambda{2}(i) ;
end
%* z
for i = 1 : D
    A(:, :, i, :) = A(:, :, i, :) * Lambda{3}(i) ;
end