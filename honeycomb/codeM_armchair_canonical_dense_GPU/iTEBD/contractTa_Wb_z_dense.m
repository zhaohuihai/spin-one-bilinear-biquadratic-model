function THL = contractTa_Wb_z_dense(parameter, Ta, Wb)
%* THL((k,l),(i,jn)) = sum{x}_(Ta(x,i,k)*Wb(x,l,jn))

d = parameter.bondDimension^2 ;
N = parameter.siteDimension^2 ;

%* Ta(x,(i,k))
Ta = reshape(Ta, [d, d^2]) ;

%* Wb(x,(l,jn))
Wb = reshape(Wb, [d, d*N*d]) ;

%* THL((i,k),(l,jn))
THL = Ta' * Wb ;

%* THL(i,k,l,jn)
THL = reshape(THL, [d, d, d, d*N]) ;

%* THL(k,l,i,jn)
THL = permute(THL, [2, 3, 1, 4]) ;

%* THL((k,l),(i,jn))
THL = reshape(THL, [d^2, d^2*N]) ;