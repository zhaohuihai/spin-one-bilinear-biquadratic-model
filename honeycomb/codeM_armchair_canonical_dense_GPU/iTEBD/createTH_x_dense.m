function TH = createTH_x_dense(parameter, H, A, B)
%* TH((yi,yi',zj,zj'),(zi,zi',yj,yj')) = 
%* sum{x,x',mi,mj,mi',mj'}_(A(x,yi,zi,mi)*B(x,yj,zj,mj)*H(mi,mj,mi',mj')
%* A(x',yi',zi',mi')*B(x',yj',zj',mj'))

D = parameter.bondDimension ;
M = parameter.siteDimension ;


%* A(x,(yi,zi,mi))
A = reshape(A, D, D^2 * M) ;
%* B(x,(yj,zj,mj))
B = reshape(B, D, D^2 * M) ;
%* AB((yi,zi,mi),(yj,zj,mj)) = sum{x}_(A(x,(yi,zi,mi))*B(x,(yj,zj,mj)))
AB = A' * B ;
%* AB((yi,zi),mi,(yj,zj),mj)
AB = reshape(AB, [D^2, M, D^2, M]) ;

%* AB(mi,mj,(yi,zi),(yj,zj))
AB = permute(AB, [2, 4, 1, 3]) ;
%* AB((mi,mj),(yi,zi,yj,zj))
AB = reshape(AB, [M^2, D^4]) ;
%* TH((yi,zi,yj,zj),(yi',zi',yj',zj'))
TH = AB' * H * AB ;
clear AB
%* TH(yi,zi,yj,zj,yi',zi',yj',zj')
TH = reshape(TH, [D, D, D, D, D, D, D, D]) ;
%* TH(yi,yi',zj,zj',zi,zi',yj,yj') 
TH = permute(TH, [1, 5, 4, 8, 2, 6, 3, 7]) ;
%* TH((yi,yi',zj,zj'),(zi,zi',yj,yj'))
TH = reshape(TH, D^4, D^4) ;