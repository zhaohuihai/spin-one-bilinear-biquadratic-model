function computeExpectationValue_dense(parameter, wave)
%* compute expectation value of local operator

expectValueBond(1) = computeExpectationValue_x(parameter, wave) ;
wave = rotate(wave) ;
expectValueBond(2) = computeExpectationValue_x(parameter, wave) ;
wave = rotate(wave) ;
expectValueBond(3) = computeExpectationValue_x(parameter, wave) ;

average3bond(parameter, expectValueBond) ;