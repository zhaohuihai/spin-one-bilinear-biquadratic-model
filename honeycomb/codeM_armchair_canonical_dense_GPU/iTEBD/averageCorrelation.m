function correlation = averageCorrelation(l, x, xM, y, yM, z, zM)

a = abs(mean([x, y,z], 2)) ;
aM = abs(mean([xM, yM, zM], 2)) ;
correlation = [l,x,y,z,a, xM, yM, zM, aM] ;