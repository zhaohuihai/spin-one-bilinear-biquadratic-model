function [vector, eta] = applyPowerMethod_y_dense(parameter, PR, PL, vector)
%* vector(a,j,a')
%* PR((a,j),(c,i))
%* PL((l,c'),(i,a'))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

powerConvergence = 1 ;
eta = 1 ;
j = 0 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector1 = updateVector_y_dense(parameter, vector, PR, PL) ;
    eta1 = norm(vector1) ;
    vector1 = vector1 ./ eta1 ;
    powerConvergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;