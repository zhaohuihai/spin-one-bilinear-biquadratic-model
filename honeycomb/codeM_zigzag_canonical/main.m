clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

if parameter.loadPreviousWave == 0
    wave = initializeWave(parameter) ;
    disp('create new wave')
else
    [wave, parameter] = loadWave(parameter) ;
end

if parameter.projection == 1
    wave = findGroundStateWave(parameter, wave) ;
end

parameter.polarization = 0 ;
% parameter.theta = 0 ;
if parameter.computation.expectationValue == 1
    computeExpectationValue(parameter, wave)
end

%* Sz correlation: <Sz(i)*Sz(i+l)> - <Sz(i)><Sz(i+l)>
%* Qzz correlation: <Qzz(i)*Qzz(i+l)> - <Qzz(i)><Qzz(i+l)>
if parameter.computation.correlation == 1
    [SzCorrelation, QzzCorrelation] = computeCorrelation(parameter, wave) ;
end