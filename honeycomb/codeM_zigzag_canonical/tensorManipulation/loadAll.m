function T = loadAll(T)

for i = 1 : T.subNo
    if ischar(T.tensor{i})
        fileName = T.tensor{i} ;
        load(fileName, 'tensor') ;
        T.tensor{i} = tensor ;
    end
end