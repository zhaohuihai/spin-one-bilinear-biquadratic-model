function vector = eliminateEmptyTensor(vector)

subNo = vector.subNo ;
iSub = 1 ;
while iSub <= subNo
    tensor = vector.tensor{iSub} ;
    dim = vector.dim(:, iSub) ;
    a = norm(reshape(tensor, [prod(dim), 1])) ;
    if a < 1e-12
        subNo = subNo - 1 ;
        vector.quantNo(:, iSub) = [] ;
        vector.dim(:, iSub) = [] ;
        vector.tensor(iSub) = [] ;
    else
        iSub = iSub + 1 ;
    end
end
vector.subNo = subNo ;
