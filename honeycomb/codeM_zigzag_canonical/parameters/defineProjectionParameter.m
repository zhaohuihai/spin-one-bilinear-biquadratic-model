function parameter = defineProjectionParameter(parameter)

parameter.polarization = 1 ;

parameter.stagDipolarField_z = 1 ;

parameter.quadrupolarField_z = 0 ;

parameter.stagQuadrupolarField_z = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************
parameter.TrotterOrder = 2 ;

parameter.tauInitial = 0.5 ;
parameter.tauFinal = 1e-3 ; %*
parameter.tauChangeFactor = 0.1 ;
% disp(['initial tau = ', num2str(parameter.tauInitial)])

parameter.convergenceCriterion_projection = 1e-12 ;
parameter.maxProjectionStep = 2e4 ;
%* if the retaining dimension is smaller than 'svdRatio' of the total dimention, use svds
parameter.svdRatio_projection = 0.0 ;

