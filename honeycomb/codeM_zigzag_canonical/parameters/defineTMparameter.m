function parameter = defineTMparameter(parameter)

%* dimension of MPS
parameter.dim_MPS = 40 ;
parameter.dim_MPS_initial = 10 ;
parameter.dim_MPS_incre = 2 ;
parameter.virtualSpin_MPS = 100 ;

%* If the entries of tensor is more than 'minSizeSaveInHD', then save it in hard disk.
parameter.minSizeSaveInHD = 1e6 ;

parameter.convergenceCriterion_canonical = 1e-14 ;
parameter.maxCanoStep = 1e4 ;
parameter.convergenceCriterion_TMprojection = 1e-14 ;
parameter.maxTMstep = 1e4 ;
parameter.convergenceCriterion_power = 1e-14 ;
parameter.maxPowerStep = 1e4 ;


parameter.svdRatio_TMP = 0 ;