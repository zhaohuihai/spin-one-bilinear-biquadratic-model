function [LAR, L] = canoCondition_2(parameter, A)
%* A(a,i,j,c)

TAdir = 'tensor/TA/' ;
%* TA(c,c',a,a') = sum{i,j}_[A(a,i,j,c)*A(a',i,j,c')]
%* conservation law: c - c' = a - a'
TA = contractSparseTensors(A, 4, [2, 3], A, 4, [2, 3], parameter, TAdir, [2, 4, 1, 3]) ;

TA = partitionTensor(TA, 4, [1, 2], '+-') ;

%* find block of 0 subspace 
TA = findBlock(TA, 0) ;
%* Vl(c,c')
[Vl, etaL] = computeDominantRightEigen(parameter, TA) ;

%* Vl = Wl * Dl * Wl'
[Wl, Dl] = decomposeV(Vl) ;

L = computeSqrt(Dl) ;

ARdir = 'tensor/AR/' ;
%* AR(a,i,j,c') = sum{c}_[A(a,i,j,c)*Wl(c,c')]
AR = contractSparseTensors(A, 4, 4, Wl, 2, 1, parameter, ARdir) ;
LARdir = 'tensor/LAR/' ;
%* LAR(a',i,j,c') = sum{a}_[Wl(a,a')*AR(a,i,j,c')]
LAR = contractSparseTensors(Wl, 2, 1, AR, 4, 1, parameter, LARdir) ;