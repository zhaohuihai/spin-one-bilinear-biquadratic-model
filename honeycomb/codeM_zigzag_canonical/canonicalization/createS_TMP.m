function S = createS_TMP(Sblock, S, blockQN, i)

S.quantNo(i) = blockQN ;
S.dim(i) = length(Sblock) ;
S.tensor{i} = Sblock ;