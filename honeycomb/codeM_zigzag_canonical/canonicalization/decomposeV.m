function [W, D] = decomposeV(V)

W = V ;
W.tensor = cell(0) ;

D.indNo = 1 ;
D.subNo = V.subNo ;
D.dim = V.dim(1, :) ;
D.quantNo = V.quantNo(1, :) ;
D.tensor = cell(0) ;

a = 0 ;
for i = 1 : V.subNo
    Vtensor = V.tensor{i} ;
    [Wtensor, Dtensor] = eig(Vtensor) ; %* V = W * D * W'
    %* V might be positive-definite or negative-definite
    Dtensor = abs(diag(Dtensor)) ;
    a = a + sum(Dtensor) ;
    W.tensor{i} = Wtensor ;
    D.tensor{i} = Dtensor ;
end

%* make D unit trace
for i = 1 : D.subNo
    D.tensor{i} = D.tensor{i} ./ a ;
end