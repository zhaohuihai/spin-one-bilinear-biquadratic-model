function V = symmetrizeVector(V)

for i = 1 : V.subNo
    V.tensor{i} = (V.tensor{i} + V.tensor{i}') / 2 ;
end