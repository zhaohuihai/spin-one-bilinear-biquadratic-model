function [V, eta] = computeDominantRightEigen(parameter, T)
%* T(a,a',c,c')

%* power method
Tmap = rmfield(T, 'tensor') ;
%* initialize a normalized symmetric vector: V(c, c')
V = initializeVector(Tmap, [3, 4]) ;
V = symmetrizeVector(V) ;
V = normalizeVector(V) ;

convergence = 1 ;
eta = 0 ;
j = 0 ;
while convergence >= parameter.convergenceCriterion_canonical && j <= parameter.maxCanoStep
    j = j + 1 ;
    %* V(a,a') = sum{c,c'}_(T(a,a',c,c')*V(c,c'))
    V = contractSparseTensors(T, 4, [3, 4], V, 2, [1, 2]) ;
    [V, eta1] = normalizeVector(V) ;
%     V = eliminateEmptyTensor(V) ;
    convergence = abs(eta - eta1) / abs(eta1) ;
    eta = eta1 ;
    V = symmetrizeVector(V) ;
end

if convergence > parameter.convergenceCriterion_canonical
    disp(['power method in canonicalization is not converged after ', num2str(parameter.maxCanoStep), ' steps projection.'])
    disp(['convergence error is ', num2str(convergence)])
end