function [LAR, coef] = canoCondition_1(parameter, A)
%* A(a,i,j,c)

TAdir = 'tensor/TA/' ;
%* TA(a,a',c,c') = sum{i,j}_[A(a,i,j,c)*A(a',i,j,c')]
%* conservation law: a - a' = c - c'
TA = contractSparseTensors(A, 4, [2, 3], A, 4, [2, 3], parameter, TAdir, [1, 3, 2, 4]) ;

TA = partitionTensor(TA, 4, [1, 2], '+-') ;
%**********************************************************
% Udir = 'tensor/U/' ;
% Vdir = 'tensor/V/' ;
% %* U(a,i,b), V(j,c,b), LA(b)
% [U, LA, V] = applySVD(parameter, Udir, Vdir, TA) ;
%***********************************************************
%* find block of 0 subspace 
TA = findBlock(TA, 0) ;
%* Vr(a,a')
[Vr, etaR] = computeDominantRightEigen(parameter, TA) ;

%*****************************************************************
% TA = contractSparseTensors(A, 4, [2, 3], A, 4, [2, 3], parameter, TAdir, [1, 3, 2, 4]) ;
% Vr1 = contractSparseTensors(TA, 4, [3, 4], Vr, 2, [1, 2]) ;
% [Vr1, eta1] = normalizeVector(Vr1) ;
%*****************************************************************

coef = sqrt(etaR) ;
%* Vr = Wr * Dr * Wr'
[Wr, Dr] = decomposeV(Vr) ;

sqDr = computeSqrt(Dr) ;
%* Aright(c,c') = Wr(c,c')*sqDr(c')
Aright = tensorTimesVector(Wr, 2, 2, sqDr, 1, 1, '*') ;
%*******************************************************************
% xx = contractSparseTensors(Aright, 2, 2, Aright, 2, 2) ;
%*******************************************************************
%* Aright(c,c') = Aright(c,c') / coef
Aright = tensorTimesScalar(Aright, coef, '/') ;
%* Aleft(a,a') = Wr(a,a') / sqDr(a')
Aleft = tensorTimesVector(Wr, 2, 2, sqDr, 1, 1, '/') ;

ARdir = 'tensor/AR/' ;
%* AR(a,i,j,c') = sum{c}_[A(a,i,j,c)*Aright(c,c')]
AR = contractSparseTensors(A, 4, 4, Aright, 2, 1, parameter, ARdir) ;
%************************************************************************
% rr = contractSparseTensors(AR, 4, [2, 3, 4], AR, 4, [2, 3, 4]) ;
%************************************************************************

LARdir = 'tensor/LAR/' ;
%* LAR(a',i,j,c') = sum{a}_[Aleft(a,a')*AR(a,i,j,c')]
LAR = contractSparseTensors(Aleft, 2, 1, AR, 4, 1, parameter, LARdir) ;