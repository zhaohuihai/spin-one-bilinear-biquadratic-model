function Qzz = createQzz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Qzz = zeros(M, M, M, M) ;
MM = M * M ;
%* Qzz = Sz1^2 + Sz2^2
%* Qzz(m1, -m2, n1, -n2) = <m1, -m2| Qzz | n1, -n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        Qzz(q1, q2, q1, q2) = Qzz(q1, q2, q1, q2) + (m1^2 + m2^2) / 2 ;
    end
end

Qzz = reshape(Qzz, [MM, MM]) ;
Qzz = Qzz - eye(MM) * (2 / 3 ) ;