function vector1 = eliminateEmptyVector(vector1)

subNo = vector1.subNo ;
iSub = 1 ;
while iSub <= subNo
    a = norm(vector1.tensor1{iSub}) ;
    if a < 1e-12
        subNo = subNo - 1 ;
        vector1.quantNo(:, iSub) = [] ;
        vector1.dim(:, iSub) = [] ;
        vector1.tensor1(iSub) = [] ;
    else
        iSub = iSub + 1 ;
    end
end
vector1.subNo = subNo ;
