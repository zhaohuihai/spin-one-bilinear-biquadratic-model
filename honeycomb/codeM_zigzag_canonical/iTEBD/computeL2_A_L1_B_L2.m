function AB = computeL2_A_L1_B_L2(parameter, dirName, MPS)

%* A(a,i,b)
A = MPS.A ;
%* B(b,j,c)
B = MPS.B ;
Lambda = MPS.Lambda ;

%* A(a,i,b) = A(a,i,b)*Lambda(2)(a)
A = AtimesLambda_TMP(A, Lambda(2), 1) ;
%* A(a,i,b) = A(a,i,b)*Lambda(1)(b)
A = AtimesLambda_TMP(A, Lambda(1), 3) ;

%* B(b,j,c) = B(b,j,c)*Lambda(2)(c)
B = AtimesLambda_TMP(B, Lambda(2), 3) ;

%* AB((a,c),(i,j)) = sum{b}_(A(a,i,b)*B(b,j,c))
AB = contractA_B_TMP(parameter, dirName, A, B) ;