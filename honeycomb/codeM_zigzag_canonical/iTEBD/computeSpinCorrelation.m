function spinCorrelation = computeSpinCorrelation(parameter, wave)
%*********************************************************************
%* spin-spin correlation: <S(i)*S(i+l)> and <S(i)*S(i+l)>-<S(i)><S(i+l)>
%* compute 3 bond directions: x, y, z
%* spinCorrelation is a maxCorrelationLength X 9 matrix
%********************************************************************* 

parameter.H = createSzSz(parameter) ;
% parameter.H = createHamiltonian_Heisenberg(parameter) ;

L = parameter.computation.maxCorrelationLength ;
l = 1 : L ;
l = l' ;

%* x, y, z are maxCorrelationLength X 1 column vectors
[x, xM] = computeSpinCorrelation_x(parameter, wave) 

wave = rotate(parameter, wave) ;
[y, yM] = computeSpinCorrelation_x(parameter, wave) 

wave = rotate(parameter, wave) ;
[z, zM] = computeSpinCorrelation_x(parameter, wave) 

a = abs(mean([x,y,z], 2))
aM = abs(mean([xM, yM, zM], 2))
spinCorrelation = [l,x,y,z,a, xM, yM, zM, aM] ;

saveExpectationValue(parameter, spinCorrelation, 'spinCorrelation')

[status, message, messageid] = rmdir('tensor', 's') ;



