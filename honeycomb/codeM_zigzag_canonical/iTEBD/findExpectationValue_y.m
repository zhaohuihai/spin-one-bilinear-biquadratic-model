function [expectValue, QR, QL] = findExpectationValue_y(VR, VL, eta, QR, QL)
%* QR(j,a,i,c,n), QL(c',l,a',i,n)
%* VR(c,l,c'), VL(a',j,a)

%* g = sum{c',l,c}_[VL(c',l,c)*VR(c,l,c')]
g = contract2vector(VL, [1, 2, 3], VR, [3, 2, 1]) ;

h = contractFinal(VL, 3, QR, 5, QL, 5, VR, 3) ;

g = eta .* g ;

expectValue = h / g ;