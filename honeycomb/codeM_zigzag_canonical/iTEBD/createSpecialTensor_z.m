function [QR, QL] = createSpecialTensor_z(parameter, ABR, ABL, AA, BB, Ta, Tb)
%* Ta(x,l,k) Tb(x,i,j)
%* AA(x,l,k;mi,mi') BB(x,i,j;mj,mj')
%* QR(c,l,a,k,n)    change notation: QR(c,l,a,i,n)
%* QL(i,a',j,c',n)  change notation: QL(j,a',i,c',n)

%* TCa(x,l,k,n), TCb(x,i,j,n)
[TCa, TCb] = createTCa_TCb(parameter, AA, BB) ;
%==============================================================
WaDir = 'tensor/Wa/' ;
%* Wa(l,k,n,i,j) = sum{x}_[TCa(x,l,k,n)*Tb(x,i,j)]
Wa = contractSparseTensors(TCa, 4, 1, Tb, 3, 1, parameter, WaDir) ;

WbDir = 'tensor/Wb/' ;
%* Wb(l,k,i,j,n) = sum{x}_[Ta(x,l,k)*TCb(x,i,j,n)]
Wb = contractSparseTensors(Ta, 3, 1, TCb, 4, 1, parameter, WbDir) ;
%======================================================================
QRdir = 'tensor/QR/' ;
%* QR(c,l,a,k,n) = sum{i,j}_[Wa(l,k,n,i,j)*ABR(a,c,i,j)]
QR = contractSparseTensors(Wa, 5, [4, 5], ABR, 4, [3, 4], parameter, QRdir, [5, 1, 4, 2, 3]) ;
%* change notation: QR(c,l,a,i,n)

QLdir = 'tensor/QL/' ;
%* QL(i,a',j,c',n) = sum{k,l}_[Wb(l,k,i,j,n)*ABL(a',c',k,l)]
QL = contractSparseTensors(Wb, 5, [2, 1], ABL, 4, [3, 4], parameter, QLdir, [1, 4, 2, 5, 3]) ;
%* change notation: QL(j,a',i,c',n)


