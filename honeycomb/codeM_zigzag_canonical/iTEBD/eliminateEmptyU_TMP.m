function U = eliminateEmptyU_TMP(U)

UsubNo = U.subNo ;
iUsub = 1 ;
while iUsub <= UsubNo
    if isempty(U.tensor2{iUsub})
        U.quantNo(:, iUsub) = [] ; %* (a,k,b)
        U.dim(:, iUsub) = [] ;
        U.tensor2(iUsub) = [] ; %* ((a,k),b)
        UsubNo = UsubNo - 1 ;
    else
        iUsub = iUsub + 1 ;
    end
end
U.subNo = UsubNo ;