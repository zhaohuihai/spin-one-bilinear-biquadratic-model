function testDominantEigen(parameter, rightMPS)

ABRdir = 'tensor/ABR/' ;
%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2(parameter, ABRdir, rightMPS) ;

Pdir = 'tensor/P/' ;
%* P((a,a'),(c,c')) = sum{k,l}_[ABR((a,c), (k,l))*ABR((a',c'),(k,l))]
[P, ~, ~] = contractRT_ABL(parameter, Pdir, ABR, ABR) ;

Pmap = rmfield(P, 'tensor2') ;
%* initialize a normalized vector(c1, c2)
vector = initializeVector_x(Pmap) ;
vector = normalize_x(vector) ;

[vectorRight, etaRight, P] = findDominantRightEigenVector_x(parameter, P, vector) ;