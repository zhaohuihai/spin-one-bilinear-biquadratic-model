function T = saveTensor2(parameter, T, tensor, index)

dim = T.dim(:, index) ;
d = 1 ;
for i = 1 : length(dim)
    d = d * dim(i) ;
end

if d <= parameter.minSizeSaveInHD
    T.tensor2{index} = tensor ;
else
    fileName = [T.dirName, 't', num2str(index), '.mat'] ;
    save(fileName, 'tensor', '-v7.3') ;
    T.tensor2{index} = fileName ;
end