function T = reformTensor3(T0)

T = rmfield(T0, 'tensor3') ;
T.indNo = 3 ; 

for i = 1 : T0.subNo
    T.tensor{i} = T0.tensor3{i} ;
end