function P = reform2to4(parameter, P0)
%* PR((j,a),(i,c)) -> PR(j,a,i,c)

P = rmfield(P0, 'tensor2') ;
P.indNo = 4 ;

for i = 1 : P0.subNo
    pDim = P0.dim(:, i) ;
    %* P((j,a),(i,c))
    %* pTensor = P.tensor2{i} ;
    pTensor = loadTensor2(parameter, P0, i) ;
    %* P(j,a,i,c)
    pTensor = reshape(pTensor, pDim') ;
    
    % P.tensor{i} = pTensor ;
    P = saveTensor(P, pTensor, i, parameter) ;
end