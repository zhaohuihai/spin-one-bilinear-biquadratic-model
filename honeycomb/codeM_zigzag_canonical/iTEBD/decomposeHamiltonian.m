function [U, V] = decomposeHamiltonian(parameter, H)

%* H(mi,mi',mj,mj') = sum{n}_(U(mi,mi',n)*S(n)*V(mj,mj',n))
[U, S, V] = applySVD(parameter, H) ;

sqrtS = computeSqrt(S) ;
%* U(mi,mi',n) = U(mi,mi',n)*sqrtS(n)
U = tensorTimesVector(U, 3, 3, sqrtS, 1, 1) ;

%* V(mj,mj',n) = V(mj,mj',n)*sqrtS(n)
V = tensorTimesVector(V, 3, 3, sqrtS, 1, 1) ;

