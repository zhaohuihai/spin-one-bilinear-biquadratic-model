function MPS = exchangeAandB_canonical(MPS0)

MPS.A = MPS0.B ;
MPS.B = MPS0.A ;
