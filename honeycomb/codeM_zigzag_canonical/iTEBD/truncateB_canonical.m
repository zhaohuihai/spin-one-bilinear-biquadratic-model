function B = truncateB_canonical(B, QN, cut)

cutNo = length(cut) ;

Bindex = find(B.quantNo(1, :) == QN) ;
Bno = length(Bindex) ;
i = 1 ;
while i <= Bno
    b = Bindex(i) ;
    Bdim = B.dim(1, b) - cutNo ;
    if Bdim ~= 0
        B.dim(1, b) = Bdim ;
        Btensor = loadTensor(B, b) ;
        Btensor(cut, :, :) = [] ;
        B.tensor{b} = Btensor ;
        i = i + 1 ;
    else %* eliminate sub tensor
        B.quantNo(:, b) = [] ;
        B.dim(:, b) = [] ;
        B.tensor(b) = [] ;
        B.subNo = B.subNo - 1 ;
        Bno = Bno - 1 ;
    end
    
end
