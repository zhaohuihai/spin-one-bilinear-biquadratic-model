function [expectValue_y, T, ABR, ABL] = dealWithSpecialTensor_y(parameter, T, ABR, ABL, AA, BB, Ta, Tb)
%* T(i,j,k,l)

%* VR(c,l,c'), VL(a',j,a)
[VR, VL, eta] = findDominantEigenVector(parameter, ABR, ABL, T) ;

%* QR(j,a,i,c,n), QL(c',l,a',i,n)
[QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValue_y, QR, QL] = findExpectationValue_y(VR, VL, eta, QR, QL) ;
clear QR QL