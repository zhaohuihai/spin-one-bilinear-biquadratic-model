function [vector, eta] = normalize_x(vector)


eta = 0 ;
for i = 1 : vector.subNo
    t = vector.tensor1{i} ;
    eta = eta + sum(t.^2) ;
end
eta = sqrt(eta) ;

for i = 1 : vector.subNo
    vector.tensor1{i} = vector.tensor1{i} ./ eta ;
end