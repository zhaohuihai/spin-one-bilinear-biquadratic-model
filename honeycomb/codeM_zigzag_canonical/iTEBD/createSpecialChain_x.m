function [P, Q, TH, T] = createSpecialChain_x(parameter, TH, T, rightMPS, leftMPS)

ABRdir = 'tensor/ABR/' ;
%* ABR(a,c,i,j) = sum{b}_[A(a,i,b)*B(b,j,c)]
%* conservation law: a - c = i + j
ABR = contractSparseTensors(rightMPS.A, 3, 3, rightMPS.B, 3, 1, parameter, ABRdir, [1, 4, 2, 3]) ;
ABLdir = 'tensor/ABL/' ;
%* ABL(a',c',k,l) = sum{b'}_[A(a',k,b')*B(b',l,c')]
%* conservation law: a' - c' = k + l
ABL = contractSparseTensors(leftMPS.A, 3, 3, leftMPS.B, 3, 1, parameter, ABLdir, [1, 4, 2, 3]) ;

Pdir = 'tensor/P/' ;
%* P(c,c',e,e') = ABR(c,e,i,j)*T(i,j,k,l)*ABL(c',e',k,l)
[P, T, ABR, ABL] = computeABR_T_ABL(parameter, Pdir, ABR, ABL, T) ;

Qdir = 'tensor/Q/' ;
%* Q(a,a',c,c') = ABR(a,c,i,j)*TH(i,j,k,l)*ABL(a',c',k,l)
[Q, TH, ABR, ABL] = computeABR_T_ABL(parameter, Qdir, ABR, ABL, TH) ;
clear ABR ABL


