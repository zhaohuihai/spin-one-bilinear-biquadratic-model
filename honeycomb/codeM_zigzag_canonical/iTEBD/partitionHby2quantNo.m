function H = partitionHby2quantNo(H0)
%* H((mi,mi'),(mj,mj'))
%* conservation law: mi - mi' = mj - mj'

quantNo = H0.quantNo ;
dim = H0.dim ;
tensor2 = H0.tensor2 ;
hQN = quantNo(1, :) - quantNo(2, :) ;
iH = 0 ;
t = H0.subNo ;
while t > 0
    max_hQN = max(hQN) ;
    index = find(hQN == max_hQN) ;
    indexNo = length(index) ;
    
    iH = iH + 1 ;
    H(iH).twoQuantNo = max_hQN ;
    
    H(iH).subNo = indexNo ;
    H(iH).quantNo = quantNo(:, index) ;
    H(iH).dim = dim(:, index) ;
    H(iH).tensor2 = tensor2(index) ;
    
    hQN(index) = hQN(index) - 1e9 ;
    t = t - indexNo ;
end
