function [vector1, PL] = contractVector_PL(parameter, vector, PL)
%* vector(c,i,a') = sum{l,c'}_(vector(c,l,c')*PL((l,c'),(i,a')))

vector1.subNo = 0 ;
vector1.quantNo = zeros(3, 0) ;
vector1.dim = zeros(3, 0) ;
vector1.tensor1 = cell(0) ;
n = 0 ;
for i = 1 : vector.subNo
    vQN = vector.quantNo(:, i) ;
    vDim = vector.dim(:, i) ;
    %* Vtensor(c,l,c')
    Vtensor = vector.tensor1{i} ;
    %* Vtensor(c,(l,c'))
    Vtensor = reshape(Vtensor, [vDim(1), vDim(2) * vDim(3)]) ;
    
    equal = cell(1, 2) ;
    equal{1} = (vQN(2) == PL.quantNo(1, :)) ;
    equal{2} = (vQN(3) == PL.quantNo(2, :)) ;
    j = find(equal{1} & equal{2}) ;
    if ~isempty(j)
        for k = 1 : length(j)
            pQN = PL.quantNo(:, j(k)) ;
            pDim = PL.dim(:, j(k)) ;
            %* pTensor((l,c'),(i,a'))
%             pTensor = PL.tensor2{j(k)} ;
            pTensor = loadTensor2(parameter, PL, j(k)) ;
            %* tensor(c,(i,a'))
            tensor = Vtensor * pTensor ;
            
            v1QN = [vQN(1); pQN(3); pQN(4)] ;
            v1Dim = [vDim(1); pDim(3); pDim(4)] ;
            v1d = vDim(1) * pDim(3) * pDim(4) ;
            %* tensor(c,i,a')
            tensor = reshape(tensor, [v1d, 1]) ;
            equalv1 = cell(1, 3) ;
            for m = 1 : 3
                equalv1{m} = (v1QN(m) == vector1.quantNo(m, :)) ;
            end
            index = find(equalv1{1} & equalv1{2} & equalv1{3}) ;
            
            if isempty(index)
                n = n + 1 ;
                vector1.quantNo(:, n) = v1QN ;
                vector1.dim(:, n) = v1Dim ;
                vector1.tensor1{n} = tensor ;
            else
                vector1.tensor1{index} = vector1.tensor1{index} + tensor ;
            end
        end
    end
end
vector1.subNo = n ;