function [MPS] = initializeMPS(parameter, T)
%* T(i,j,k,l) field: subNo, quantNo, dim
mapping = initializeMPSmapping(parameter) ;
RandStream.setDefaultStream(RandStream('mt19937ar', 'Seed', 5489))
%* A(a,i,b): S[a]-S[b]=S[i]
MPS.A = initializeA(mapping, T.quantNo(1, :), T.dim(1, :)) ;
%* B(b,j,c): S[b]-S[c]=S[j]
MPS.B = initializeA(mapping, T.quantNo(2, :), T.dim(2, :)) ;
