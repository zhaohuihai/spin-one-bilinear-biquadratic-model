function TH = createSpecialTensor_x(parameter, AA, BB)
%* TH(i,j,k,l) = sum{x,mi,mj,mi',mj'}_[AA(x,l,k;mi,mi')*H(mi,mj,mi',mj')*BB(x,i,j;mj,mj')]

M = parameter.siteDimension ;

HBB = computeH_BB(parameter, BB) ;
for m1 = 1 : M
    for m2 = 1 : M
        HBB(m1, m2) = reduceTa(HBB(m1, m2)) ;
    end
end
THdir = 'tensor/TH/' ;
TH = computeAA_HBB(parameter, THdir, M, AA, HBB) ;
