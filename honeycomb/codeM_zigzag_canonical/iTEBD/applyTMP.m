function [MPS, T, allSingularValue, coef, truncationError] = applyTMP(parameter, T, MPS)

%******************************************************
%* for test only D = 4, Dcut = 30
% load rightMPS.mat
% MPS.A = convertMPS(rightMPS.A, rightMPS.Lambda(1), 3) ;
% MPS.B = convertMPS(rightMPS.B, rightMPS.Lambda(2), 3) ;
%******************************************************

ABdir = 'tensor/AB/' ;
%* AB(a,c,i,j) = sum{b}_[A(a,i,b)*B(b,j,c)]
%* conservation law: a - c = i + j
AB = contractSparseTensors(MPS.A, 3, 3, MPS.B, 3, 1, parameter, ABdir, [1, 4, 2, 3]) ;

Rdir = 'tensor/R/' ;
%* R(a, k, l, c) = sum{i,j}_(AB(a,c,i,j)*T(i,j,k,l))
%* conservation law: a - k = l + c
R = contractSparseTensors(AB, 4, [3, 4], T, 4, [1, 2], parameter, Rdir, [1, 3, 4, 2]) ;
clear AB

[MPS, Lambda, coef] = canonicalize(parameter, R) ;

[MPS, allSingularValue, truncationError] = truncate_canonical(parameter, MPS, Lambda) ;

MPS = exchangeAandB_canonical(MPS) ;