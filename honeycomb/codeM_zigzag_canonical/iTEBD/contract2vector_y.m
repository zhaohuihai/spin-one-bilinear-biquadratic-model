function h = contract2vector_y(vL, vR)
%* h = sum{a,j,a'}_(vL(a,j,a')*vR(a,j,a'))
h = 0 ;
for i = 1 : vL.subNo
    lQN = vL.quantNo(:, i) ;
    lTensor = vL.tensor1{i} ;
    equal = cell(1, 3) ;
    equal{1} = (lQN(1) == vR.quantNo(1, :)) ;
    equal{2} = (lQN(2) == vR.quantNo(2, :)) ;
    equal{3} = (lQN(3) == vR.quantNo(3, :)) ;
    j = find(equal{1} & equal{2} & equal{3}) ;
    if ~isempty(j)
        h = h + lTensor' * vR.tensor1{j} ;
    end
end