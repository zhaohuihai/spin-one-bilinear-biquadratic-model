function AA = blockAAUby2bond_z(AAU, totMapping)

subNo = AAU.subNo ;

twoQuantNo(1, :) = AAU.quantNo(1, :) ;
twoQuantNo(2, :) = AAU.quantNo(2, :) ;
twoQuantNo(3, :) = AAU.quantNo(3, :) - AAU.quantNo(4, :) ;

n = 0 ;
while subNo > 0
    QN = twoQuantNo(:, 1) ;
    for i = 1 : 3
        equal{i} = (QN(i) == twoQuantNo(i, :)) ;
        mapIndex = find(QN(i) == totMapping(i).twoQuantNo) ;
        mapping(i).map = totMapping(i).map{mapIndex} ;
        mapping(i).totDim = totMapping(i).totDim(mapIndex) ;
    end
    
    index = find(equal{1} & equal{2} & equal{3}) ;
    indexNo = length(index) ;
    n = n + 1 ;
    [AAblock, AAblockDim] = combineAAUsub_z(AAU, index, mapping) ;
    
    AAU.quantNo(:, index) = [] ;
    AAU.dim(:, index) = [] ;
    AAU.tensor3(index) = [] ;
    twoQuantNo(:, index) = [] ;
    
    tensor3{n} = AAblock ;
    quantNo(:, n) = QN ;
    dim(:, n) = AAblockDim ;
    
    subNo = subNo - indexNo ;
end
AA.subNo = n ;
AA.quantNo = quantNo ;
AA.dim = dim ;
AA.tensor3 = tensor3 ;