function Ta = reduceTa(Ta)

subNo = Ta.subNo ;
n = 0 ;
if Ta.subNo == 0
    return
end
while subNo > 0
    QN = Ta.quantNo(:, 1) ; 
    equalX = (QN(1) == Ta.quantNo(1, :)) ;
    equalY = (QN(2) == Ta.quantNo(2, :)) ;
    equalZ = (QN(3) == Ta.quantNo(3, :)) ;
    index = find(equalX & equalY & equalZ) ;
    indexNo = length(index) ;
    n = n + 1 ;
    TaTensor = 0 ;
    for i = 1 : indexNo
        TaTensor = TaTensor + Ta.tensor{index(i)} ;
    end
    subNo = subNo - indexNo ;
    tensor{n} = TaTensor ;
    quantNo(:, n) = QN ;
    dim(:, n) = Ta.dim(:, 1) ;
    Ta.quantNo(:, index) = [] ;
    Ta.dim(:, index) = [] ;
    Ta.tensor(index) = [] ;
end
Ta.subNo = n ;
Ta.quantNo = quantNo ;
Ta.dim = dim ;
Ta.tensor = tensor ;
