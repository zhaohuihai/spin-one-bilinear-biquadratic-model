function [QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb)
%* Ta(x,l,k) Tb(x,i,j)
%* AA(x,l,k;mi,mi') BB(x,i,j;mj,mj')
%* QR(j,a,i,c,n), QL(c',l,a',i,n)

%* TCa(x,l,k,n), TCb(x,i,j,n)
[TCa, TCb] = createTCa_TCb(parameter, AA, BB) ;
%==============================================================
WaDir = 'tensor/Wa/' ;
%* Wa(l,k,n,i,j) = sum{x}_[TCa(x,l,k,n)*Tb(x,i,j)]
Wa = contractSparseTensors(TCa, 4, 1, Tb, 3, 1, parameter, WaDir) ;

WbDir = 'tensor/Wb/' ;
%* Wb(l,k,i,j,n) = sum{x}_[Ta(x,l,k)*TCb(x,i,j,n)]
Wb = contractSparseTensors(Ta, 3, 1, TCb, 4, 1, parameter, WbDir) ;
%======================================================================
QRdir = 'tensor/QR/' ;
%* QR(k,a,l,c,n) = sum{i,j}_[Wa(l,k,n,i,j)*ABR(a,c,i,j)]
QR = contractSparseTensors(Wa, 5, [4, 5], ABR, 4, [3, 4], parameter, QRdir, [2, 4, 1, 5, 3]) ;
%* change notation: QR(j,a,i,c,n)

QLdir = 'tensor/QL/' ;
%* QL(c',j,a',i,n) = sum{k,l}_[Wb(l,k,i,j,n)*ABL(a',c',k,l)]
QL = contractSparseTensors(Wb, 5, [2, 1], ABL, 4, [3, 4], parameter, QLdir, [5, 2, 4, 1, 3]) ;
%* change notation: QL(c',l,a',i,n)


