function computeStagMag_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

parameter.H = createStagSz(parameter) ;
disp('compute stagger magnetization')
stagMagBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) 

stagMag = mean(stagMagBond) ;

saveExpectationValue(parameter, stagMag, 'staggerMagnetization_z') ;