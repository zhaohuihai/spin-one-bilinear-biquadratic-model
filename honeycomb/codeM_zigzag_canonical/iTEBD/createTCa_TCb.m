function [TCa, TCb] = createTCa_TCb(parameter, AA, BB)
%* TCa(x,l,k,n), TCb(x,i,j,n)
%* AA(x,l,k;mi,mi') BB(x,i,j;mj,mj')

% parameter.H = createHamiltonian_Heisenberg(parameter) ;
% parameter.H = createSzSz(parameter) ;
%* H(mi,mi',mj,mj')
%* conservation law: mi - mi' = mj - mj'
H = convertToSparseH(parameter) ;
H = partitionTensor(H, 4, [1, 2], '+-') ;


%* H(mi,mi',mj,mj') = sum{n}_(U(mi,mi',n)*S(n)*V(mj,mj',n))
%* U(mi,mi',n) = U(mi,mi',n)*sqrt(S(n))
%* V(mj,mj',n) = V(mj,mj',n)*sqrt(S(n))
[U, V] = decomposeHamiltonian(parameter, H) ;

%* TCa(x,l,k,n) = sum{mi,mi'}_[AA(x,l,k;mi,mi')*U(mi,mi',n)]
TCa = contractAA_U(parameter, AA, U) ;

%* TCb(x,i,j,n) = sum{mj,mj'}_[BB(x,i,j;mj,mj')*V(mj,mj',n)]
TCb = contractAA_U(parameter, BB, V) ;