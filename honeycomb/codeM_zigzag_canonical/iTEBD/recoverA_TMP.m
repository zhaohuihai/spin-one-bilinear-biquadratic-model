function A = recoverA_TMP(U, invL)
%* A(a,k,b) = U((a,k),b)*invL(a)

A = rmfield(U, 'tensor2') ;
for i = 1 : U.subNo
    
    Adim = A.dim(:, i) ;
    A.tensor3{i} = reshape(U.tensor2{i}, Adim') ;
end
%* A(a,k,b) = A(a,k,b)*invL(a)
A = AtimesLambda_TMP(A, invL, 1) ;