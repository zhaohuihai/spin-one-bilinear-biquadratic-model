function [correlation, correlationM] = findCorrelationFunction(parameter, ABR, ABL, AA, BB, Ta, Tb, VR, VL, PR, PL, eta)

parameter.H = parameter.AsiteOperator ;
%* QR(j,a,i,c,n), QL(c',l,a',i,n)
[QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb) ;
Asite = findExpectationValue_y(VR, VL, eta, QR, QL) ;

parameter.H = parameter.BsiteOperator ;
%* QR(j,a,i,c,n), QL(c',l,a',i,n)
[QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb) ; 
Bsite = findExpectationValue_y(VR, VL, eta, QR, QL) ;
oddDistance = Asite * Bsite ;
evenDistance = Asite * Asite ;
%*==============================================================================
parameter.H = parameter.correlationOperator ;
%* QR(j,a,i,c,n), QL(c',l,a',i,n)
[QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

L = parameter.computation.maxCorrelationLength ;
correlation = zeros(L, 1) ;

%* normFactor = sum{c',l,c}_[VL(c',l,c)*VR(c,l,c')]
normFactor = contract2vector(VL, [1, 2, 3], VR, [3, 2, 1]) ;

coef = 1 ;

for i = 1 : L
    %* distance <= 1: VL(a',j,a); distance >= 2: VL(a',j,a,n)
    if i == 2
        [VL, c] = updateVector(VL, 3, QR, 5, PL, 4) ;
        coef = coef * c ;
    elseif mod(i, 2) == 0
        [VL, c] = updateVector(VL, 4, PR, 4, PL, 4) ;
        coef = coef * c ;
    end
    [correlation(i)] = findCorrelation(i, QR, QL, VR, VL, eta, PR, PL, coef, normFactor) ;
end
%===================================================================================================
correlationM = zeros(L, 1) ;
%* odd distance
correlationM(1 : 2 : L) = correlation(1 : 2 : L) - oddDistance ;
%* even distance
correlationM(2 : 2 : L) = correlation(2 : 2 : L) - evenDistance ;
