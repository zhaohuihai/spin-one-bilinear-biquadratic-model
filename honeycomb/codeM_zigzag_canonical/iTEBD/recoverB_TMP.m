function B = recoverB_TMP(V, invL)
%* B(b,l,c) = V((l,c),b)*invL(c)

B.subNo = V.subNo ;
B.quantNo = V.quantNo([3, 1, 2], :) ;
B.dim = V.dim([3, 1, 2], :) ;
for i = 1 : V.subNo
    Bdim = B.dim(:, i) ;
    %* ((l,c),b)
    tensor = V.tensor2{i} ;
    %* (b,(l,c))
    tensor = tensor' ;
    %* (b,l,c)
    B.tensor3{i} = reshape(tensor, Bdim') ;
end
%* B(b,l,c) = B(b,l,c)*invL(c)
B = AtimesLambda_TMP(B, invL, 3) ;