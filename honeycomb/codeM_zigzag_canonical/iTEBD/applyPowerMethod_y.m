function vector = applyPowerMethod_y(PR, PL, vector)
%* vector(a,j,a') = sum{c,c',l,i}_[PR(a,j,c,i)*vector(c,l,c')*PL(l,c',i,a')]


%* vector(c,i,a') = sum{l,c'}_[vector(c,l,c')*PL(l,c',i,a')]
vector = contractSparseTensors(vector, 3, [2, 3], PL, 4, [1, 2]) ;

%* vector(a,j,a') = sum{c,i}_(PR(a,j,c,i)*vector(c,i,a'))
vector = contractSparseTensors(PR, 4, [3, 4], vector, 3, [1, 2]) ;
