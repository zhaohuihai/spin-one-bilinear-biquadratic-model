function [expectValue_x, T] = dealWithSpecialTensor_x(parameter, T, rightMPS, leftMPS, AA, BB)

%* do one step projection and exchange A, B
[rightMPS, T] = applyTMP(parameter, T, rightMPS) ;

%* TH(i,j,k,l) = sum{x,mi,mj,mi',mj'}_[AA(x,l,k;mi,mi')*H(mi,mj,mi',mj')*BB(x,i,j;mj,mj')]
TH = createSpecialTensor_x(parameter, AA, BB) ;
%* expectValue = trace(QPPP...)
[P, Q, TH, T] = createSpecialChain_x(parameter, TH, T, rightMPS, leftMPS) ;
clear TH

Pmap = rmfield(P, 'tensor') ;
%* initialize a normalized vector(c1, c2)
vector = initializeVector_x(Pmap) ;
vector = normalizeVector(vector) ;

[vectorRight, etaRight, P] = findDominantRightEigenVector_x(parameter, P, vector) ;

%* transpose to left transferMatrix
%* T(a,a',c,c') --> T(c,c',a,a')
P = permuteSparseTensor(P, [3, 4, 1, 2], parameter) ;

[vectorLeft, etaLeft, P] = findDominantRightEigenVector_x(parameter, P, vectorRight) ;
clear P
[expectValue_x, Q] = findExpectationValue_x(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
clear Q