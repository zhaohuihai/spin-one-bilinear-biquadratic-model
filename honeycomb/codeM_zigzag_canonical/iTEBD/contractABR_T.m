function [RT, ABR, T] = contractABR_T(parameter, dirName, ABR, T)
%* RT((a,c), (k,l)) = ABR((a,c),(i,j))*T((i,j),(k,l))

[status,message,messageid] = mkdir(dirName) ;
RT.dirName = dirName ;
RT.subNo = 0 ;
RT.quantNo = zeros(4, 0) ;
RT.dim = zeros(4, 0) ;
RT.tensor2 = cell(0) ;
n = 0 ;
for i = 1 : ABR.subNo
    ABRqn = ABR.quantNo(:, i) ;
    ABRdim = ABR.dim(:, i) ;
    %* ABRtensor((a,c),(i,j))
%     ABRtensor = ABR.tensor2{i} ;
    ABRtensor = loadTensor2(parameter, ABR, i) ;
    equali = (ABRqn(3) == T.quantNo(1, :)) ;
    equalj = (ABRqn(4) == T.quantNo(2, :)) ;
    j = find(equali & equalj) ;
    if ~isempty(j)        
        for k = 1 : length(j)
            Tqn = T.quantNo(:, j(k)) ;
            Tdim = T.dim(:, j(k)) ;
            %* Ttensor((i,j),(k,l))
%             Ttensor = T.tensor2{j(k)} ;
            Ttensor = loadTensor2(parameter, T, j(k)) ;
            
            %* tensor((a,c),(k,l))
            tensor = ABRtensor * Ttensor ;
            
            rQN = [ABRqn(1); ABRqn(2); Tqn(3); Tqn(4)] ;
            rDim = [ABRdim(1); ABRdim(2); Tdim(3); Tdim(4)] ;
            equalRT = cell(1, 3) ;
            for m = 1 : 3
                equalRT{m} = (rQN(m) == RT.quantNo(m, :)) ;
            end
            index = find(equalRT{1} & equalRT{2} & equalRT{3}) ;
            
            if isempty(index)
                n = n + 1 ;
                RT.quantNo(:, n) = rQN ;
                RT.dim(:, n) = rDim ;
%                 RT.tensor2{n} = tensor ;
                RT = saveTensor2(parameter, RT, tensor, n) ;
            else
                RTtensor = loadTensor2(parameter, RT, index) ;
                tensor = RTtensor + tensor ;
%                 RT.tensor2{index} = RT.tensor2{index} + tensor ;
                RT = saveTensor2(parameter, RT, tensor, index) ;
            end
        end
    end
end
RT.subNo = n ;

