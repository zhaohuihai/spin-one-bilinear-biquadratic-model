function [vector, eta, P] = findDominantRightEigenVector_x(parameter, P, vector)
%* vector(a1,a2)
%* P(a1,a2,c1,c2)

powerConvergence = 1 ;
eta = 1 ;
j = 0 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    %* vector1(a1,a2) = sum{c1,c2}_(P(a1,a2,c1,c2)*vector(c1,c2))
    vector1 = contractSparseTensors(P, 4, [3, 4], vector, 2, [1, 2]) ;
    [vector1, eta1] = normalizeVector(vector1) ;
    
    
    powerConvergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;