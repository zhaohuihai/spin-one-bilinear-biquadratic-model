function Wa = createW_z_dense(parameter, A, U)
%* Wa(x,i,kn)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = M^2 ;
%* A((x,y,z),m)
A = reshape(A, [D^3, M]) ;
%* AA((x,y,z,x',y',z'),(m,m'))
AA = kron(A, A) ;

%* Wa((x,y,z,x',y',z'), n)
Wa = AA * U ;

%* Wa(x,y,z,x',y',z', n)
Wa = reshape(Wa, [D, D, D, D, D, D, N]) ;

%* Wa(x,x',y,y',z,z',n)
Wa = permute(Wa, [1, 4, 2, 5, 3, 6, 7]) ;

%* Wa((x,x'),(y,y'),(z,z',n))
Wa = reshape(Wa, [D^2, D^2, D^2 * N]) ;