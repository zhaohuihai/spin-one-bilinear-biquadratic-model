function T = createTransferMatrix_dense(parameter, Ta, Tb)
%* T((i,j),(k,l)) = sum{x}_(Ta(x,l,k)*Tb(x,i,j))

d = parameter.bondDimension^2 ;

%* T(i,j,k,l) = sum{x}_(Ta(x,l,k)*Tb(x,i,j))
T = contractTensors(Ta, 3, 1, Tb, 3, 1, [3, 4, 2, 1]) ;

%* T((i,j),(k,l))
T = reshape(T, [d^2, d^2]) ;

