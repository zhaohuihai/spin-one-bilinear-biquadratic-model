function computeExpectationValue_dense(parameter, wave)
%* compute expectation value of local operator

%* A(x, y, z) B(x, y, z)
[A, B] = absorbLambda_dense(parameter, wave) ;

%* Ta(x,i,k) 
Ta = computeTensorProductA_A_dense(parameter, A) ;
%* Tb(x,l,j)
Tb = computeTensorProductA_A_dense(parameter, B) ;

%* T((i,j),(k,l)) right transfer matrix
T = createTransferMatrix_dense(parameter, Ta, Tb) ;

[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, T) ;

%*=========================================================================================
expectValueBond(1) = dealWithSpecialTensor_x_dense(parameter, T, rightMPS, leftMPS, A, B) ;

%* ABR((a,c),(i,j))
ABR = computeA_B(parameter, rightMPS) ;
%* ABL((a',c'),(k,l))
ABL = computeA_B(parameter, leftMPS) ;

expectValueBond(2) = dealWithSpecialTensor_y_dense(parameter, T, ABR, ABL, A, B, Ta, Tb) ;

expectValueBond(3) = dealWithSpecialTensor_z_dense(parameter, T, ABR, ABL, A, B, Ta, Tb) ;

average3bond(parameter, expectValueBond) ;