function [wave1D, Lambda, coef, truncationError] = computeSingleGate1D(parameter, T, wave1D)

Tdim = size(T) ;
%* T((i,j), (k,l))
T = reshape(T, [Tdim(1)*Tdim(2), Tdim(3)*Tdim(4)]) ;

%* AB((a,c),(i,j))
AB = computeA_B_1D(parameter, wave1D) ;
%* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
R = computeProjection1D(parameter, T, Tdim, AB) ;

[wave1D, Lambda, coef] = canonicalize(Tdim(3), Tdim(4), R) ;

[wave1D, truncationError] = truncateWave1D_canonical(parameter, wave1D, Lambda) ;

% wave1D = recoverWave1D(parameter, Tdim, wave1D, U, S, V) ;
