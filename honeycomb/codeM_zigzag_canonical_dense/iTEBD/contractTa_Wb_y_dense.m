function THR = contractTa_Wb_y_dense(parameter, Ta, Wb)
%* THR((i,j),(k,ln)) = sum{x}_(Ta(x,i,k)*Wb(x,ln,j))

d = parameter.bondDimension^2 ;
N = parameter.siteDimension^2 ;

%* Ta(x,(i,k))
Ta = reshape(Ta, [d, d^2]) ;

%* Wb(x,(ln,j))
Wb = reshape(Wb, [d, d*N*d]) ;

%* THR((i,k),(ln,j))
THR = Ta' * Wb ;

%* THR(i,k,ln,j)
THR = reshape(THR, [d, d, d*N, d]) ;

%* THR(i,j,k,ln)
THR = permute(THR, [1, 4, 2, 3]) ;

%* THR((i,j),(k,ln))
THR = reshape(THR, [d^2, d^2*N]) ;