function [correlation] = findSpinCorrelation(distance, QR, QL, VR, VL, eta, PR, PL, coef, normFactor)
%* QR(j,a,i,c), QL(c',l,a',i,n)
%* PR(j,a,i,c), PL(c',l,a',i)
%* VR(c,l,c') 
%* distance <= 1: VL(a',j,a); distance >= 2: VL(a',j,a,n)

%* k = 0,1
k = mod(distance, 2) ;

if distance == 1
    correlation = contractFinal(VL, 3, QR, 5, QL, 5, VR, 3) ;
elseif k == 0
    correlation = contractFinal(VL, 4, QR, 5, PL, 4, VR, 3) ;
elseif k == 1
    correlation = contractFinal(VL, 4, PR, 4, QL, 5, VR, 3) ;
end
    
n = floor(distance / 4) + 1 ;

correlation = correlation * coef / (eta^n * normFactor) ;
