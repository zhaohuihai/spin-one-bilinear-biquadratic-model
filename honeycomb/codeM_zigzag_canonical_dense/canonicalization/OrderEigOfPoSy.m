function [ U1, C1, SymError ] = OrderEigOfPoSy ( Vec ) 
%========for test=========
% Vec = rand ( 4 ) ; 
% Vec = - Vec * Vec' ; 
%======================
SymError = max ( max ( abs ( Vec - Vec' ) ) ) ;

Vec = ( Vec + Vec' ) / 2 ;
[ U, C ] = eig ( Vec ) ;
d = length ( C ) ;

%======for test=======
% C0 = diag ( C ) ;
% if ( C0 ( 1 ) + C0 ( d ) ) > 0
%     Sign = 1 ;
% else
%     Sign = -1 ;
% end
%==================

C = abs ( diag ( C ) ) ;
C1 = sort ( C, 'descend' ) ; 
U1 = U ;

for i = 1 : d
    index = find ( C == C1 ( i ) ) ;
    place = index ( 1 ) ;
    U1 ( :, i ) = U ( :, place ) ;
    C ( place ) = - 1 ;
end

C1 = diag ( C1 ) ;

%======for test=======
% EigError = max ( max ( abs ( U1 * C1 / U1 - Sign * Vec ) ) ) ;
% if EigError > 10^(-10)
%     save Vec
%     pause
% end
%=================

