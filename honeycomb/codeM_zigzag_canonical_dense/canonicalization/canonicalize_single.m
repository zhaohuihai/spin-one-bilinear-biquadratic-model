function [MPS, L, coef] = canonicalize_single(MPS)

%* A(a,i,b)
A = MPS.A.tensor3 ;
Adim = MPS.A.dim ;
c = zeros(1, 2) ;
%==============================================================
[A, c(1)] = canoCondition_1(A, Adim) ;
%==============================================================
[A, c(2)] = canoCondition_1(A, Adim) ;
coef = c(1) * c(2) ;
[A, L] = canoCondition_2(A, Adim) ;
Ldim = length(L) ;
MPS.A.tensor3 = A ;
%*--------------------------------------------------------------

%*************************************************
% %* verification
% A1 = A ;
% id = eye(Adim(1)) ;
% %* A1(a,(i,b))
% A1 = reshape(A1, [Adim(1), Adim(2)*Adim(3)]) ;
% aa = A1 * A1' ;
% norm(A1 * A1' - id * aa(1,1))
% 
% A2 = zeros(Adim) ;
% for a = 1 : Ldim
%     A2(a, :, :) = L(a) * A(a, :, :) ;
% end
% %* A2((a,i),b)
% A2 = reshape(A2, [Adim(1) * Adim(2), Adim(3)]) ;
% norm(A2' * A2 - diag(L.^2))