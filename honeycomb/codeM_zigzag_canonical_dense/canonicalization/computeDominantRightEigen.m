function [V, eta] = computeDominantRightEigen(A0, Adim)
%* estimate iteration times
iterTimes = 100 ;
%* time complexity of two methods
t1 = Adim(1) * Adim(2) * Adim(3)^2 + Adim(1)^2*Adim(2)*Adim(3) ;
t1 = t1 * iterTimes ;
t2 = Adim(1)^2 * Adim(3)^2 * (Adim(2) + iterTimes) ;

dim = Adim(1) ;
%=============================================================
% V = rand(dim, dim) ;
% V = (V + V') / 2 ;
% V0 = V ./ trace(V) ;
% 
% maxStep = 1e4 ;
% 
% powerConverge = 1 ;
% step = 0 ;
% if t1 <= t2
% %     disp('matrix form')
%     while powerConverge > 1e-14 && step <= maxStep
%         step = step + 1 ;
%         %* A((a,i),b)
%         A = reshape(A0, [Adim(1) * Adim(2), Adim(3)]) ;
%         %* AV((a,i),b') = sum{b}_(A((a,i),b)*V0(b,b'))
%         AV = A * V0 ;
%         %* AV(a,(i,b'))
%         AV = reshape(AV, [Adim(1), Adim(2) * Adim(3)]) ;
%         %* A(a',(i,b'))
%         A = reshape(A, [Adim(1), Adim(2) * Adim(3)]) ;
%         %* V(a,a') = sum{i,b'}_(AV(a,(i,b'))*A(a',(i,b')))
%         V = AV * A' ;
%         
%         eta = trace(V) ;
%         V = V ./ eta ;
%         V = (V + V') / 2 ;
%         
%         
%         powerConverge = norm(V - V0) / sqrt(dim^2 - 1) ;
%         V0 = V ;
%     end
% else
%     T = computeKronA_A(A0, Adim) ;
% %     disp('vector form')
%     V0 = reshape(V0, [dim^2, 1]) ;
%     while powerConverge > 1e-14 && step <= maxStep
%         step = step + 1 ;
%         V = T * V0 ;
%         eta = norm(V) ;
%         V = V ./ eta ;
%         powerConverge = norm(V - V0) / sqrt(dim^2 - 1) ;
%         
%         V = reshape(V, [dim, dim]) ;
%         V = (V + V') / 2 ;
%         V = reshape(V, [dim^2, 1]) ;
%         
%         V0 = V ;
%         eta0 = eta ;
%     end
%     V = reshape(V, [dim, dim]) ;
%     V = (V + V') / 2 ;
% end
% if powerConverge > 1e-14
%     disp(['dominant eigen vector error = ', num2str(powerConverge)])
% end
%===============================================================
T = computeKronA_A(A0, Adim) ;
[V, eta] = eigs(T, 1) ;
V = reshape(V, [dim, dim]) ;
V = (V + V') / 2 ;
