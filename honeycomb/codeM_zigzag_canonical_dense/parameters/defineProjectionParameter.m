function parameter = defineProjectionParameter(parameter)

parameter.polarization = 1 ;

parameter.dipolarField_x = 0 ;
parameter.dipolarField_z = 0 ;

parameter.stagDipolarField_x = 0 ;
parameter.stagDipolarField_z = 1 ;

parameter.quadrupolarField_x = 0 ;
parameter.quadrupolarField_z = 0 ;

parameter.stagQuadrupolarField_x = 0 ;
parameter.stagQuadrupolarField_z = 0 ;
parameter.stagQuadrupolarField_xx_yy = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************
parameter.TrotterOrder = 2 ;

parameter.tauInitial = 0.1 ;
parameter.tauFinal = 1e-2 ; %*
parameter.tauChangeFactor = 0.1 ;

% parameter.field = 0.5 ;

parameter.convergenceCriterion_projection = 1e-12 ;
parameter.maxProjectionStep = 2e4 ;