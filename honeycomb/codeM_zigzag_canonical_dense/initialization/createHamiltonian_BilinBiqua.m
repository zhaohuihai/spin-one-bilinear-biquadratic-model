function H = createHamiltonian_BilinBiqua(parameter)
%* Bilinear biquadratic model Hamiltonian

theta = parameter.theta ;

H_bl = createHamiltonian_Heisenberg(parameter) ;

H = cos(theta) * H_bl + sin(theta) * (H_bl * H_bl) ;

H = setSmalltoZero(H) ;

if parameter.polarization == 1
    if parameter.dipolarField_x ~= 0
        disp('dipolarize X')
        H_polar = createSx(parameter) ;
        H = H - H_polar * parameter.dipolarField_x ;
        
    elseif parameter.dipolarField_z ~= 0
        disp('dipolarize Z')
        H_polar = createSz(parameter) ;
        H = H - H_polar * parameter.dipolarField_z ;
        
    elseif parameter.stagDipolarField_x ~= 0
        disp('stagger dipolarize X')
        H_polar = createStagSx(parameter) ;
        H = H - H_polar * parameter.stagDipolarField_x ;
        
    elseif parameter.stagDipolarField_z ~= 0
        disp(['stagger dipolarize Z, field = ', num2str(parameter.stagDipolarField_z)])
        H_polar = createStagSz(parameter) ;
        H = H - H_polar * parameter.stagDipolarField_z ;
        
    elseif parameter.quadrupolarField_x ~= 0
        disp('quadrupolarize X')
        H_polar = createQxx(parameter) ;
        H = H - H_polar * parameter.quadrupolarField_x ;
        
    elseif parameter.quadrupolarField_z ~= 0
        disp('quadrupolarize Z')
        H_polar = createQzz(parameter) ;
        H = H - H_polar * parameter.quadrupolarField_z ;
        
    elseif parameter.stagQuadrupolarField_x ~= 0
        disp('stagger quadrupolarize X')
        H_polar = createStagQxx(parameter) ;
        H = H - H_polar * parameter.stagQuadrupolarField_x ;
        
    elseif parameter.stagQuadrupolarField_z ~= 0
        disp(['stagger quadrupolarize Z, field = ', num2str(parameter.stagQuadrupolarField_z)])
        H_polar = createStagQzz(parameter) ;
        H = H - H_polar * parameter.stagQuadrupolarField_z ;
    elseif parameter.stagQuadrupolarField_xx_yy ~= 0
        disp(['stagger quadrupolarize XX-YY, field = ', num2str(parameter.stagQuadrupolarField_xx_yy)])
        H_polar = createStagQxx_yy(parameter) ;
        H = H - H_polar * parameter.stagQuadrupolarField_xx_yy ;
    end
end