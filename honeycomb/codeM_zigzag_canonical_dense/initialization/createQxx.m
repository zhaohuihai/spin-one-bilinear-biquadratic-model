function Qxx = createQxx(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Qxx = zeros(M, M, M, M) ;
MM = M * M ;
%* Qxx(m1, m2, n1, n2) = <m1, m2| Qxx | n1, n2 >
%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    x2 = (S + m2) * (S - m2 + 1) / 8 ;
    y2 = (S - m2) * (S + m2 + 1) / 8 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        x1 = (S + m1) * (S - m1 + 1) / 8 ;
        y1 = (S - m1) * (S + m1 + 1) / 8 ;
        Qxx(q1, q2, q1, q2) = Qxx(q1, q2, q1, q2) + x1 + y1 + x2 + y2 ;
    end
end

%* assign values to off-diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    %* x2 = (S2+)^2 / 8
    x2 = sqrt((S - m2) * (S + m2 + 1) * (S - m2 - 1) * (S + m2 + 2)) / 8 ;
    %* y2 = (S2-)^2 / 8
    y2 = sqrt((S + m2) * (S - m2 + 1) * (S + m2 - 1) * (S - m2 + 2)) / 8 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        %* x1 = (S1+)^2 / 8
        x1 = sqrt((S - m1) * (S + m1 + 1) * (S - m1 - 1) * (S + m1 + 2)) / 8 ;
        %* y1 = (S1-)^2 / 8
        y1 = sqrt((S + m1) * (S - m1 + 1) * (S + m1 - 1) * (S - m1 + 2)) / 8 ;
        
        if x1 ~= 0
            Qxx(q1 - 2, q2, q1, q2) = Qxx(q1 - 2, q2, q1, q2) + x1 ;
        end
        if y1 ~= 0
            Qxx(q1 + 2, q2, q1, q2) = Qxx(q1 + 2, q2, q1, q2) + y1 ;
        end
        if x2 ~= 0
            Qxx(q1, q2 - 2, q1, q2) = Qxx(q1, q2 - 2, q1, q2) + x2 ;
        end
        if y2 ~= 0
            Qxx(q1, q2 + 2, q1, q2) = Qxx(q1, q2 + 2, q1, q2) + y2 ;
        end
    end
end

Qxx = reshape(Qxx, [MM, MM]) ;
Qxx = Qxx - eye(MM) * (2 / 3 ) ;