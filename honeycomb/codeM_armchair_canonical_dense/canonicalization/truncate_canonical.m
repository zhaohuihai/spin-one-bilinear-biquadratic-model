function [wave1D, truncationError] = truncate_canonical(parameter, wave1D, Lambda)

D = parameter.dim_MPS ;

%* A(a,b,i)
wave1D.A = wave1D.A(:, 1 : D, :) ;
%* B(b,c,j)
wave1D.B = wave1D.B(1 : D, :, :) ;


truncationError = 1 - sum(Lambda(1 : D).^2) / sum(Lambda.^2) ;

