function parameter = defineWaveParameter(parameter)

parameter.bondDimension = 3 ;
disp(['bond dimension = ', num2str(parameter.bondDimension)]) ;

% *************************************************************
% if initial A and B are same
parameter.sameInitialTensor = 0 ;