function parameter = setParameter

parameter = defineGlobalParameter ;

parameter = defineWaveParameter(parameter) ;
parameter = defineProjectionParameter(parameter) ;
parameter = defineTMparameter(parameter) ;
parameter = defineModelParameter(parameter) ;






