function expectValueBond = dealWithSpecialTensor_x_dense(parameter, Ta, Tb, rightMPS, leftMPS, A, B)

%* do one step projection and exchange A, B
rightMPS = computeSingleGate1D(parameter, Ta, Tb, rightMPS) ;

expectValueBond = contractSpecialTensor_x(parameter, Ta, Tb, rightMPS, leftMPS, A, B) ;