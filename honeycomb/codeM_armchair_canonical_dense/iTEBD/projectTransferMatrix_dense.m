function R = projectTransferMatrix_dense(parameter, Ta, Tb, MPS)
%* R((a, k), (c, l)) = sum{i,j,x,b}_[A(a,b,i)*t{1}(x,i,k)*B(b,c,j)*t{2}(x,l,j)]

d = parameter.bondDimension^2 ;
D = size(MPS.A, 1) ;

%* At(a,k,b,x) = sum{i}_[A(a,b,i)*t{1}(x,i,k)]
At = contractTensors(MPS.A, 3, 3, Ta, 3, 2, [1, 4, 2, 3]) ;

%* Bt(b,x,c,l) = sum{j}_[B(b,c,j)*t{2}(x,l,j)]
Bt = contractTensors(MPS.B, 3, 3, Tb, 3, 3, [1, 3, 2, 4]) ;

%* R(a,k,c,l) = sum{b,x}_[At(a,k,b,x)*Bt(b,x,c,l)]
R = contractTensors(At, 4, [3, 4], Bt, 4, [1, 2]) ;
clear At Bt
%* R((a, k), (c, l))
R = reshape(R, [D * d, D * d]) ;
