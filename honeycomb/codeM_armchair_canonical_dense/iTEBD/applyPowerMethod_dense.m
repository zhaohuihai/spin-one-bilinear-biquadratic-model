function [vector, eta] = applyPowerMethod_dense(parameter, P, vector)

D = parameter.dim_MPS ;

powerConvergence = 1 ;
j = 0 ;
eta = 1 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector1 = P * vector ;
    eta1 = norm(vector1) ;
    vector1 = vector1 ./ eta1 ;
    powerConvergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;