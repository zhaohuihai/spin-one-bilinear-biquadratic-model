function [PR, PL] = transposeP_dense(parameter, PR, PL)
%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* PR(a,j,c,i)
PR = reshape(PR, [D, d, D, d]) ;

%* PR(j,a,i,c)
PR = permute(PR, [2, 1, 4, 3]) ;

%* PR((j,a),(i,c))
PR = reshape(PR, [d * D, d * D]) ;

%* PL(l,c',i,a')
PL = reshape(PL, [d, D, d, D]) ;

%* PL(c',l,a',i)
PL = permute(PL, [2, 1, 4, 3]) ;

%* PL((c',l),(a',i))
PL = reshape(PL, [D * d, D * d]) ;