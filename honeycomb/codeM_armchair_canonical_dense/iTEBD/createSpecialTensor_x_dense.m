function [Q, ABR, ABL] = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL)

TH = createTH_x_dense(parameter, operator, A, B) ;

%* Q((a,a'),(c,c')) = ABR((a,c),(i,j))*TH((i,j),(k,l))*ABL((a',c'),(k,l))
[Q, ABR, ABL, TH] = computeABR_T_ABL_dense(parameter, ABR, ABL, TH) ;
clear TH  