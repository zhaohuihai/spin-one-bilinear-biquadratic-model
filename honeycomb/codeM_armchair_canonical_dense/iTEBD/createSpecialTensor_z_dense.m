function [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb)

%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian_dense(parameter, operator) ;

%* Wa(x,i,kn)
Wa = createW_z_dense(parameter, A, U) ;
%* Wb(x,l,jn)
Wb = createW_z_dense(parameter, B, V) ;

%* THR((i,j),(kn,l))
THR = contractTb_Wa_z_dense(parameter, Tb, Wa) ;
%* QR((l,c),(kn,a)) = sum{i,j}_(ABR((a,c),(i,j))*THR((i,j),(kn,l)))
[QR, THR] = contractABL_TH_dense(parameter, ABR, THR) ;
clear THR

%* THL((k,l),(i,jn))
THL = contractTa_Wb_z_dense(parameter, Ta, Wb) ;
%* QL((a',i),(c',jn)) = sum{k,l}_(ABL((a',c'),(k,l))*THL((k,l),(i,jn)))
[QL, THL] = contractABR_TH_dense(parameter, ABL, THL) ;
clear THL
