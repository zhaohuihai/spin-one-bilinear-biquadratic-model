function [U, V] = decomposeHamiltonian_dense(parameter, operator)

M = parameter.siteDimension ;
MM = M * M ;
%* H(m1,m2,n1,n2)
H = reshape(operator, [M, M, M, M]) ;

%* H(m1,n1,m2,n2)
H = permute(H, [1, 3, 2, 4]) ;

%* H((m1,n1),(m2,n2))
H = reshape(H, [MM, MM]) ;

[U, S, V] = svd(H) ;
%************************************
%* truncation
S = diag(S) ;
cut = find(S < 1e-12) ;
S(cut) = [] ;
U(:, cut) = [] ;
V(:, cut) = [] ;
S = diag(S) ;
%************************************
%* U((mi,mi'),n)
U = U * sqrt(S) ;

%* V((mj,mj'),n)
V = V * sqrt(S) ;

