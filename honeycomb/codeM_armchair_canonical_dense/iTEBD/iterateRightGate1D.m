function [MPS, coef, truncationError, TMPstep] = iterateRightGate1D(parameter, Ta, Tb, MPS, TMPstep)

TMconvergence = 1 ;
D = parameter.dim_MPS ;
coef0 = 1 ;
j = 0 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(D), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, coef, truncationError] = applyRightGateOperation1D(parameter, Ta, Tb, MPS) ;
    toc
    TMconvergence = abs(coef0 - coef) / abs(coef) ;
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMconvergence)])
        disp(['truncation error = ', num2str(truncationError)])
    end
    coef0 = coef ;
end
