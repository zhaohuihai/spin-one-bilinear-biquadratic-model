function [MPS, coef, truncationError_TMP] = computeSingleGate1D(parameter, Ta, Tb, MPS)

%* R((a, k), (c, l)) = sum{i,j,x}_(A(a,b,i)*Ta(x,i,k)*B(b,c,j)*Tb(x,l,j))
R = projectTransferMatrix_dense(parameter, Ta, Tb, MPS) ;

[MPS, Lambda, coef] = canonicalize(parameter, R) ;
clear R
[MPS, truncationError_TMP] = truncate_canonical(parameter, MPS, Lambda) ;

MPS = exchangeAandB_TMP_dense(MPS) ;

