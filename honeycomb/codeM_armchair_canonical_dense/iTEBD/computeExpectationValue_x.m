function expectValueBond = computeExpectationValue_x(parameter, wave)

%* A(x, y, z, m1) B(x, y, z, m2)
[A, B] = absorbLambda_dense(parameter, wave) ;

%* Ta(x,i,k) 
Ta = computeTensorProductA_A_dense(parameter, A) ;
%* Tb(x,l,j)
Tb = computeTensorProductA_A_dense(parameter, B) ;


[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, Ta, Tb) ;

%*=========================================================================================
expectValueBond = dealWithSpecialTensor_x_dense(parameter, Ta, Tb, rightMPS, leftMPS, A, B) ;