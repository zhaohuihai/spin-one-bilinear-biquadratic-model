function [rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, Ta, Tb)


% project from bottom to top
rightMPS = findDominantRightEigenMPS_dense(parameter, Ta, Tb) ;

%* transpose to left transferMatrix
%* Ta: (x,i,k) -> (x,k,i)
%* Tb: (x,l,j) -> (x,j,l)
Ta = permute(Ta, [1, 3, 2]) ;
Tb = permute(Tb, [1, 3, 2]) ;

leftMPS = findDominantRightEigenMPS_dense(parameter, Ta, Tb) ;