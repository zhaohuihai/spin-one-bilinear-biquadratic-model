function [VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, Ta, Tb, rightMPS, leftMPS)

%* PR((a,k),(c,l)) = sum{i,j,x,b}_(A(a,b,i)*Ta(x,i,k)*B(b,c,j)*Tb(x,l,j))
%* change notation: PR((a,j),(c,i))
PR = createPR(parameter, Ta, Tb, rightMPS) ;

%* PL((j,c'),(i,a')) = sum{k,l,x,b'}_(A(a',b',k)*Ta(x,i,k)*B(b',c',l)*Tb(x,l,j))
%* change notation: PL((l,c'),(i,a'))
PL = createPL(parameter, Ta, Tb, leftMPS) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* V(c,l,c')
V = rand(D * d * D, 1) ;
eta = norm(V) ;
V = V ./ eta ;

%* VR(a,j,a')=sum{c,l,c'}_[PR((a,j),(c,i))*VR(c,l,c')*PL((l,c'),(i,a'))]
[VR, etaR] = applyPowerMethod_y_dense(parameter, PR, PL, V) ;
%* change notation: VR(c,l,c')

%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))
[PR, PL] = transposeP_dense(parameter, PR, PL) ;

VL = reshape(VR, [D, d, D]) ;
VL = permute(VL, [3, 2, 1]) ;
VL = reshape(VL, [D * d * D, 1]) ;

%* VL(c',l,c)=sum{a',j,a}_[PL((c',l),(a',i))*VL(a',j,a)*PR((j,a),(i,c))]
[VL, etaL] = applyPowerMethod_y_dense(parameter, PL, PR, VL) ;
%* change notation: VL(a',j,a)

eta = (etaR + etaL) / 2 ;

%===========================================================================
%* PR(j,a,i,c)
PR = reshape(PR, [d, D, d, D]) ;
%* PL(c',l,a',i)
PL = reshape(PL, [D, d, D, d]) ;

%* VR(c,l,c')
VR = reshape(VR, [D, d, D]) ;
%* VL(a',j,a)
VL = reshape(VL, [D, d, D]) ;

