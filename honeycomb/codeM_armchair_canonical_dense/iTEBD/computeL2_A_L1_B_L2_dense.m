function AB = computeL2_A_L1_B_L2_dense(parameter, MPS)
%* AB((a,c),(i,j)) = sum{b}_(Lambda2(a)*A(a,b,i)*Lambda1(b)*B(b,c,j)*Lambda2(c))

d = parameter.bondDimension^2 ;
% D = parameter.dim_MPS ;

A = MPS.A ;
B = MPS.B ;
Lambda = MPS.Lambda ;

D1 = length(Lambda{1}) ;
D2 = length(Lambda{2}) ;
%* A(a,b,i) = Lambda2(a)*A(a,b,i)
for a = 1 : D2
    A(a, :, :) = A(a, :, :) * Lambda{2}(a) ;
end
%* A(a,b,i) = A(a,b,i)*Lambda1(b)
for b = 1 : D1
    A(:, b, :) = A(:, b, :) * Lambda{1}(b) ;
end
%* B(b,c,j) = B(b,c,j)*Lambda2(c)
for c = 1 : D2
    B(:, c, :) = B(:, c, :) * Lambda{2}(c) ;
end
%* A(a,i,b)
A = permute(A, [1, 3, 2]) ;
%* A((a,i),b)
A = reshape(A, [D2 * d, D1]) ;

%* B(b,(c,j))
B = reshape(B, [D1, D2 * d]) ;

%* AB((a,i),(c,j))
AB = A * B ;

%* AB(a,i,c,j)
AB = reshape(AB, [D2, d, D2, d]) ;
%* AB(a,c,i,j)
AB = permute(AB, [1, 3, 2, 4]) ;
%* AB((a,c),(i,j))
AB = reshape(AB, [D2^2, d^2]) ;
