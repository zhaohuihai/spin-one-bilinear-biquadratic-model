function [wave, truncationError, coef] = projectBy1operator(parameter, projectionOperator, wave)


%* A(x,y1,z1,m1) B(x,y2,z2,m2)
[A, B] = computeLyLz_A_Lx_B_LyLz(parameter, wave) ;

%* Ta(x,l,y1,z1,m1) Tb(x,l,y2,z2,m2)
[Ta, Tb] = computeProjection(projectionOperator, A, B) ;

%* U((y1,z1,m1),x) V((y2,z2,m2),x) S(x)
[U, S, V, coef] = svd_projection(parameter, Ta, Tb) ;

[U, S, V, truncationError] = truncate(parameter, U, S, V) ;

wave = recover(parameter, wave, U, S, V) ;
