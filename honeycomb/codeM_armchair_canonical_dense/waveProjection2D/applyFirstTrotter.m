function [wave, truncationError, coef] = applyFirstTrotter(parameter, wave)

projectionOperator = createProjectionOperator(parameter) ;

c = zeros(1, 3) ;
truncErr_single = zeros(1, 3) ;
for j = 1 : 3
    
    [wave, truncErr_single(j), c(j)] = projectBy1operator(parameter, projectionOperator, wave) ;
    
    wave = rotate(wave) ;
end

truncationError = max(truncErr_single) ;
coef = prod(c) ;

