function saveWave(parameter, wave)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

dirName = ['result/D = ', num2str(D)] ;
[status,message,messageid] = mkdir(dirName) ;

waveFileName = [dirName, '/wavefunctionSite', num2str(M),'.mat'] ;
wave.parameter = parameter ;
wave.tauFinal = parameter.tau ;

id = exist(waveFileName, 'file') ;

if id == 2
    load (waveFileName, 'waveAll', 'thetaAll')
    index = find(thetaAll == theta) ;
    if isempty(index)
        thetaAll = [thetaAll, theta] ;
        waveAll = [waveAll, wave] ;
        [thetaAll, Order] = sort(thetaAll) ;
        waveAll = waveAll(Order) ;
        
    else
        waveAll(index) = wave ;
    end
else
    waveAll = wave ;
    thetaAll = theta ;
end
save(waveFileName, 'waveAll', 'thetaAll') ;