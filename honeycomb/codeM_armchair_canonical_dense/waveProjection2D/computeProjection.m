function [Ta, Tb] = computeProjection(projectionOperator, A, B)
%* Ta(x,l,y1,z1,m1) Tb(x,l,y2,z2,m2)


%* Pa(m1,n1,l)
Pa = projectionOperator.Pa ;

%* Ta(x,l,y1,z1,n1) = sum{m1}_[A(x,y1,z1,m1)*Pa(m1,n1,l)]
Ta = contractTensors(A, 4, 4, Pa, 3, 1, [1, 5, 2, 3, 4]) ;

%* Pb(m2,n2,l)
Pb = projectionOperator.Pb ;

%* Tb(x,l,y2,z2,n2) = sum{m2}_[B(x,y2,z2,m2)*Pb(m2,n2,l)]
Tb = contractTensors(B, 4, 4, Pb, 3, 1, [1, 5, 2, 3, 4]) ;