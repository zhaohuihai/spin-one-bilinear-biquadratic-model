function [wave, truncationError, coef] = applySecondTrotter(parameter, wave)

PO_1 = createProjectionOperator(parameter) ;

parameter.tau = parameter.tau / 2 ;
PO_half = createProjectionOperator(parameter) ;

c = zeros(1, 5) ;
truncErr_single = zeros(1, 5) ;
%* exp(- (tau / 2) * Hx)
[wave, truncErr_single(1), c(1)] = projectBy1operator(parameter, PO_half, wave) ;
wave = rotate(wave) ;

%* exp(- (tau / 2) * Hy)
[wave, truncErr_single(2), c(2)] = projectBy1operator(parameter, PO_half, wave) ;
wave = rotate(wave) ;

%* exp(- tau * Hz)
[wave, truncErr_single(3), c(3)] = projectBy1operator(parameter, PO_1, wave) ;
wave = reverseRotate(wave) ;

%* exp(- (tau / 2) * Hy)
[wave, truncErr_single(4), c(4)] = projectBy1operator(parameter, PO_half, wave) ;
wave = reverseRotate(wave) ;

%* exp(- (tau / 2) * Hx)
[wave, truncErr_single(5), c(5)] = projectBy1operator(parameter, PO_half, wave) ;

truncationError = max(truncErr_single) ;
coef = prod(c) ;

