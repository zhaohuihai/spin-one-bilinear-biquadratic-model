function wave = initializeWave(parameter)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
if parameter.sameInitialTensor == 1
    A = rand(D, D, D, M) ;
    L = sort(rand(D, 1), 'descend') ;
    L = L ./ L(1) ;
    wave.A = A ;
    wave.B = A ;
    for i = 1 : 3
        wave.Lambda{i} = L ;
    end
else %* == 0
    wave.A = rand(D, D, D, M) ;
    wave.B = rand(D, D, D, M) ;
    for i = 1 : 3
        wave.Lambda{i} = sort(rand(D, 1), 'descend') ;
        wave.Lambda{i} = wave.Lambda{i} ./ wave.Lambda{i}(1) ;
    end
end
