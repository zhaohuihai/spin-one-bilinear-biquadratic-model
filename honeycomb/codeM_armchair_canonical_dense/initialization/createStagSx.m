function stagSx = createStagSx(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

stagSx = zeros(M, M, M, M) ;
MM = M * M ;

%* stagSx(m1, m2, n1, n2) = <m1, m2| stagSx | n1, n2 >

%* assign values to off-diagonal entries
for q1 = 1 : M
    m1 = S - q1 + 1 ;
    %* x1 = (1/4)*(S1+)
    x1 = (1 / 4) * sqrt((S - m1) * (S + m1 + 1)) ;
    %* y1 = (1/4)*(S1-)
    y1 = (1 / 4) * sqrt((S + m1) * (S - m1 + 1)) ;
    for q2 = 1 : M
        m2 = S - q2 + 1 ;
        %* x2 = - (1/4)*(S2+)
        x2 = - (1 / 4) * sqrt((S - m2) * (S + m2 + 1)) ;
        %* y2 = - (1/4)*(S2-)
        y2 = - (1 / 4) * sqrt((S + m2) * (S - m2 + 1)) ;
        
        if x1 ~= 0
            stagSx(q1 - 1, q2, q1, q2) = stagSx(q1 - 1, q2, q1, q2) + x1 ;
        end
        if y1 ~= 0
            stagSx(q1 + 1, q2, q1, q2) = stagSx(q1 + 1, q2, q1, q2) + y1 ;
        end
        if x2 ~= 0
            stagSx(q1, q2 - 1, q1, q2) = stagSx(q1, q2 - 1, q1, q2) + x2 ;
        end
        if y2 ~= 0
            stagSx(q1, q2 + 1, q1, q2) = stagSx(q1, q2 + 1, q1, q2) + y2 ;
        end
    end
end

stagSx = reshape(stagSx, [MM, MM]) ;