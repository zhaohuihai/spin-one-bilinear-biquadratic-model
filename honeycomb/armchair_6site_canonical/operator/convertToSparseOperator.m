function H = convertToSparseOperator(parameter, operator)
%* operator((m1, m2), (n1, n2))
%* conservation law: m1 + m2 = n1 + n2

M = parameter.siteDimension ;
%* Hd(m1,m2,n1,n2)
Hd = reshape(operator, [M, M, M, M]) ;

subNo = 0 ;
quantNo = [] ;
tensor = cell(0) ;

for p1 = 1 : M
    m1 = M - 1 - p1 ;
    for p2 = 1 : M
        m2 = M - 1 - p2 ;
        for q1 = 1 : M
            n1 = M - 1 - q1 ;
            for q2 = 1 : M
                n2 = M - 1 - q2 ;
                if Hd(p1, p2, q1, q2) ~= 0
                    if (m1 + m2) ~= (n1 + n2)
                        warning('conservation law: m1 + m2 = n1 + n2 is not satisfied') ;
                    end
                    subNo = subNo + 1 ;
                    QN = [m1; m2; n1; n2] ;
                    quantNo(:, subNo) = QN ;
                    tensor{subNo} = Hd(p1, p2, q1, q2) ;
                end
            end
        end
    end
end
dim = ones(4, subNo) ;

H.indNo = 4 ;
H.subNo = subNo ;
H.quantNo = quantNo ;
H.dim = dim ;
H.tensor = tensor ;