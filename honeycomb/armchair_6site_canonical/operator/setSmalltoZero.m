function T = setSmalltoZero(T)

dim = size(T) ;

for i = 1 : dim(1)
    for j = 1 : dim(2)
        if abs(T(i, j)) < 1e-12
            T(i, j) = 0 ;
        end
    end
end

