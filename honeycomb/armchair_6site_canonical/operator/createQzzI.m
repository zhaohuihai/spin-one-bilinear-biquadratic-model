function QzzI = createQzzI(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

QzzI = zeros(M, M, M, M) ;
MM = M * M ;
%* QzzI = Sz1^2
%* QzzI(m1, -m2, n1, -n2) = <m1, -m2| Sz1^2 | n1, -n2 >

%* assign values to diagonal entries
for q2 = 1 : M
%     m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        QzzI(q1, q2, q1, q2) = QzzI(q1, q2, q1, q2) + m1^2 ;
    end
end

QzzI = reshape(QzzI, [MM, MM]) ;
