function ISz = createISz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

ISz = zeros(M, M, M, M) ;
MM = M * M ;

%* ISz(m1, -m2, n1, -n2) = <m1, -m2| ISz | n1, -n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
%         m1 = S - q1 + 1 ;
        ISz(q1, q2, q1, q2) = ISz(q1, q2, q1, q2) - m2 ;
    end
end

ISz = reshape(ISz, [MM, MM]) ;