function parameter = defineProjectionParameter(parameter)

parameter.polarization = 0 ;
parameter.polarField = 1 ;
parameter.field = 0 ;

parameter.fieldInitial = 0.01 ;
parameter.fieldFinal = 0.2 ;
parameter.fieldIncre = 0.01 ;

parameter.stagDipolarField_z = 1 ;

parameter.quadrupolarField_z = 0 ;

parameter.stagQuadrupolarField_z = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************
parameter.VBCinitial = 0 ;

parameter.VBCanisotropyRatio = 0.00 ;

parameter.plaquetteVBC = 0 ;
parameter.columnarVBC = 1 ;
parameter.staggeredVBC = 0 ;
parameter.convergenceCriterion_VBC = 1e-3 ;
%*****************************************************************
parameter.TrotterOrder = 1 ;

parameter.tauInitial = 0.1 ;
parameter.tauFinal = 1e-1 ; %*
parameter.tauChangeFactor = 40 ;
% disp(['initial tau = ', num2str(parameter.tauInitial)])

parameter.convergenceCriterion_projection = 1e-11 ;
parameter.maxProjectionStep = 1e4 ;
%* if the retaining dimension is smaller than 'svdRatio' of the total dimention, use svds
parameter.svdRatio_projection = 0.0 ;

