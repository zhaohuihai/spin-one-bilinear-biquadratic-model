function parameter = defineWaveParameter(parameter)

parameter.bondDimension = 4 ;
disp(['bond dimension = ', num2str(parameter.bondDimension)]) ;

% *********************************
%* virtualSpin must be INTEGER
parameter.virtualSpin = 20 ;

% if initial A and B are same
parameter.sameInitialTensor = 1 ;