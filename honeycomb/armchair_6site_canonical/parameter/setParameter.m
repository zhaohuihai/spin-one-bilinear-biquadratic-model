function parameter = setParameter

parameter = defineGlobalParameter ;

parameter = defineWaveParameter(parameter) ;
parameter = defineProjectionParameter(parameter) ;
parameter = defineTMparameter(parameter) ;
parameter = defineModelParameter(parameter) ;

%************************************************************************************
%* fields of a tensor:
%* 1. indNo: order of the tensors
%* 2. subNo: number of blocks
%* 3. quantNo: quantum number of blocks
%* 4. dim: size of blocks
%* 5. tensor: tensor blocks





