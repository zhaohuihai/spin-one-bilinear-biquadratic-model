function parameter = defineGlobalParameter

%* yes: 1, no: 0
parameter.loadPreviousWave = 0 ; %*

parameter.projection = 1 ; %*
%********************************************************************
parameter.computation.expectationValue = 0 ;
%* staggered spontaneous magnetization
parameter.computation.stagSz = 0 ;

%* z quadrupole
parameter.computation.Qzz = 0 ;

%* z staggered quadrupole
parameter.computation.stagQzz = 0 ;

%* energy computation
parameter.computation.energy = 1 ;
%--------------------------------------------------------------------
%* correlation: armchair direction
parameter.computation.correlation = 0 ;
parameter.computation.SzCorrelation = 1 ;
parameter.computation.QzzCorrelation = 1 ;
parameter.computation.maxCorrelationLength = 100 ;
%********************************************************************
%* Maximum number of iterations in svds
parameter.svdsOptions.maxit = 500 ;
%* tolerance in svds
parameter.svdsOptions.tol = 1e-10 ;

%* determine the maximum difference ratio of degenerate values
parameter.maxDiffRatio = 1 - 1e-15 ;
% parameter.maxDiffRatio = 1 ;
