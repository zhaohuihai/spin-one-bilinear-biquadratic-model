function [wave, parameter] = loadWave(parameter)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

resultFileName = ['result/D = ', num2str(D), '/wavefunctionSite', num2str(M),'.mat'] ;
id = exist(resultFileName, 'file') ;

if id == 2
    load (resultFileName, 'waveAll', 'thetaAll')
    index = find(thetaAll == theta) ;
    if isempty(index)
        disp(['Can not find wavefunction for theta = ', num2str(theta)]) ;
        disp('Create NEW wave instead.')
        parameter.loadPreviousWave = 0 ;
        wave = initializeWave(parameter) ;
    else
        disp('load old wave')
        wave = waveAll(index) ;
    end
else
    disp('No wavefunction file.')
    disp('Create NEW wave instead.') 
    parameter.loadPreviousWave = 0 ;
    wave = initializeWave(parameter) ;
end







