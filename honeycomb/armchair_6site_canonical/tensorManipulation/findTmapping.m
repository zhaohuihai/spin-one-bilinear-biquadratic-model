function Tmap = findTmapping(QN, dim)

subNo = length(QN) ;

n = 0 ;
while subNo > 0
    maxQN = max(QN) ;
    index = find(QN == maxQN) ;
    indexNo = length(index) ;
    
    d1 = dim(index(1)) ;
    n = n + 1 ;
    Tmap.quantNo(n) = maxQN ;
    Tmap.dim(n) = d1 ;
    
    QN(index) = [] ;
    dim(index) = [] ;
    subNo = subNo - indexNo ;
end
Tmap.subNo = n ;