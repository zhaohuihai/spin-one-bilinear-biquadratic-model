function sqD = computeSqrt(D)

sqD = D ;
for i = 1 : D.subNo
    sqD.tensor{i} = sqrt(D.tensor{i}) ;
end