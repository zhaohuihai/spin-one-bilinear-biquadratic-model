function XY = contractSparseTensors(varargin)
%* XY = contractSparseTensors(X, numindX, indX, Y, numindY, indY, parameter, XYdir, order)
%* optional input arguments: parameter, XYdir, order
%* possible nargin:
%* 6: X, numindX, indX, Y, numindY, indY
%* 7: X, numindX, indX, Y, numindY, indY, order
%* 8: X, numindX, indX, Y, numindY, indY, parameter, XYdir
%* 9: X, numindX, indX, Y, numindY, indY, parameter, XYdir, order

X = varargin{1} ;
numindX = varargin{2} ;
indX = varargin{3} ;
Y = varargin{4} ;
numindY = varargin{5} ;
indY = varargin{6} ;

if nargin == 8 || nargin == 9
    parameter = varargin{7} ;
    XYdir = varargin{8} ;
    [status,message,messageid] = mkdir(XYdir) ;
    XY.dirName = XYdir ;
else
    parameter.minSizeSaveInHD = Inf ;
end
indNo = numindX - length(indX) + numindY - length(indY) ;
XY.indNo = indNo ;
XY.subNo = 0 ;
XY.quantNo = zeros(indNo, 0) ;
XY.dim = zeros(indNo, 0) ;
XY.tensor = cell(0) ;

n = 0 ;
for i = 1 : X.subNo
    Xqn = X.quantNo(:, i) ;
    Xdim = X.dim(:, i) ;
    Xtensor = loadTensor(X, i) ;
    j = findMatchQN(Xqn, indX, Y.quantNo, indY) ;
    if ~isempty(j)
        for k = 1 : length(j)
            Yqn = Y.quantNo(:, j(k)) ;
            Ydim = Y.dim(:, j(k)) ;
            Ytensor = loadTensor(Y, j(k)) ;
            
            tensor = contractTensors(Xtensor, numindX, indX, Ytensor, numindY, indY) ;
            
            indXl = 1:numindX;
            indXl(indX) = [];
            indYr = 1:numindY;
            indYr(indY) = [];
            XYqn = [Xqn(indXl); Yqn(indYr)] ;
            XYdim = [Xdim(indXl); Ydim(indYr)] ;
            if nargin == 7
                order = varargin{7} ;
                tensor = permute(tensor, order) ;
                XYqn = XYqn(order) ;
                XYdim = XYdim(order) ;
            elseif nargin == 9
                order = varargin{9} ;
                tensor = permute(tensor, order) ;
                XYqn = XYqn(order) ;
                XYdim = XYdim(order) ;
            end
            
            index = findMatchQN(XYqn, XY.quantNo) ;
            if isempty(index)
                n = n + 1 ;
                XY.quantNo(:, n) = XYqn ;
                XY.dim(:, n) = XYdim ;               
                XY = saveTensor(XY, tensor, n, parameter) ;
            else
                XYtensor = loadTensor(XY, index) ;
                tensor = XYtensor + tensor ;
                XY = saveTensor(XY, tensor, index, parameter) ;
            end
        end
    end
end
XY.subNo = n ;



