function U = createU_projection(parameter, Ublock, U, blockQN, map, quantNo, dim)
%* U(y1,z1,m1,x)

n = size(quantNo, 2) ;
i = 1 ;
% eliminate duplicated quantNo
while i <= n
    QN = quantNo(:, i) ;
    ind = findMatchQN(QN, quantNo) ;
    ind(1) = [] ;
    quantNo(:, ind) = [] ;
    dim(:, ind) = [] ;
    i = i + 1 ;
    n = n - length(ind) ;
end

Udim = size(Ublock, 2) ;
for i = 1 : n
    QN = quantNo(1 : 2, i) ;
    index = findMatchQN(QN, map.quantNo) ;
    location = map.location(index) ;
    position = location : (location + map.dim(index) - 1) ;
    
    U.subNo = U.subNo + 1 ;
    U.quantNo(:, U.subNo) = [quantNo(:, i); blockQN] ;
    U.dim(:, U.subNo) = [dim(:, i); Udim] ;
    Utensor = Ublock(position, :) ;
    Utensor = reshape(Utensor, U.dim(:, U.subNo)') ;
    U = saveTensor(U, Utensor, U.subNo, parameter) ;
end