function X = tensorTimesScalar(X, coef, varargin)
%* optional input arguments: rule, parameter
%* possible nargin:
%* 2: X, coef
%* 3: X, coef, rule
%* 3: X, coef, parameter
%* 4: X, coef, rule, parameter
if nargin == 2
    rule = '*' ;
    parameter.minSizeSaveInHD = Inf ;
elseif nargin == 3
    if ischar(varargin{1})
        rule = varargin{1} ;
        parameter.minSizeSaveInHD = Inf ;
    else 
        rule = '*' ;
        parameter = varargin{1} ;
    end
elseif nargin == 4
    rule = varargin{1} ;
    parameter = varargin{2} ;
end
    
for i = 1 : X.subNo
    Xtensor = loadTensor(X, i) ;
    eval(['Xtensor = Xtensor .', rule, ' coef ;']) ;
    X = saveTensor(X, Xtensor, i, parameter) ;
end