function [Tblock, Tdim, Tmap] = combineTensor6to2(T)
%* T(y1,z1,m1,y2,z2,m2)

%* total dim
Tdim = zeros(1, 2) ;
%* Tmap: quantNo, location, dim
for x = 1 : 2
    i = 3 * x -2 ; % i = 1, 4
    threeDim = prod(T.dim(i : (i+2), :)) ;
    [Tmap(x), Tdim(x)] = findMap2(T.quantNo(i : (i + 1), :), threeDim) ;
end
Tblock = zeros(Tdim) ;

subNo = T.subNo ;

position = cell(1, 2) ;
dim = zeros(1, 2) ;
for y = 1 : subNo
    for x = 1 : 2
        i = 3 * x - 2 ;
        QN = T.quantNo(i : (i + 1), y) ;
        index = findMatchQN(QN, Tmap(x).quantNo) ;
        location = Tmap(x).location(index) ;
        dim(x) = Tmap(x).dim(index) ;
        position{x} = location : (location + dim(x) - 1 ) ;
    end
    
    tensor = loadTensor(T, y) ;
    tensor = reshape(tensor, dim) ;
    Tblock(position{1}, position{2}) = Tblock(position{1}, position{2}) + tensor ;
end