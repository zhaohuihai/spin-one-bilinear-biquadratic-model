function T = saveTensor(varargin)
% function T = saveTensor(T, tensor, index, parameter)
%* optional input argument: parameter

T = varargin{1} ;
tensor = varargin{2} ;
index = varargin{3} ;

if nargin == 4
    parameter = varargin{4} ;
    dim = T.dim(:, index) ;
    d = prod(dim) ;
    if d > parameter.minSizeSaveInHD && isfield(T, 'dirName')
        fileName = [T.dirName, 't', num2str(index), '.mat'] ;
        save(fileName, 'tensor', '-v7.3') ;
        T.tensor{index} = fileName ;
    else
        T.tensor{index} = tensor ;
    end
else
    T.tensor{index} = tensor ;
end




