function [ABblock, ABdim, ABmap] = combineTensor(AB)
%* AB(a,i,j,c)
%* conservation law: a - i = j + c

%* total dim
ABdim = zeros(1, 2) ;
%* ABmap: quantNo, location, dim
for x = 1 : 2
    i = 2 * x - 1 ;
    twoDim = AB.dim(i, :) .* AB.dim(i + 1, :) ;
    [ABmap(x), ABdim(x)] = findMap(AB.quantNo(i, :), twoDim) ;
end
ABblock = zeros(ABdim) ;

subNo = AB.subNo ;

position = cell(1, 2) ;
dim = zeros(1, 2) ;
for y = 1 : subNo
    for x = 1 : 2
        i = 2 * x - 1 ;
        index = find(ABmap(x).quantNo == AB.quantNo(i, y)) ;
        location = ABmap(x).location(index) ;
        dim(x) = ABmap(x).dim(index) ;
        position{x} = location : (location + dim(x) - 1 ) ;
    end
    
    tensor = loadTensor(AB, y) ;
    tensor = reshape(tensor, dim) ;
    ABblock(position{1}, position{2}) = ABblock(position{1}, position{2}) + tensor ;
end