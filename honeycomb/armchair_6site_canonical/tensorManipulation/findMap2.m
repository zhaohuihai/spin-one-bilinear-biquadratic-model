function [map, totDim] = findMap2(quantNo, dim)

s = size(quantNo, 2) ;
i = 0 ;
d = 0 ;
while s > 0
    QN = quantNo(:, 1) ;
    index = findMatchQN(QN, quantNo) ;
    subDim = dim(index(1)) ;
    
    i = i + 1 ;
    map.quantNo(:, i) = QN ;
    map.location(i) = d + 1 ; % start location
    map.dim(i) = subDim ;
    d = d + subDim ;
    
    quantNo(:, index) = [] ;
    dim(index) = [] ;
    s = s - length(index) ;
end
map.subNo = i ;
totDim = d ;