function A = recoverA(parameter, U, Lambda)
%* U(y1,z1,m1,x)

%* U(y1,z1,m1,x) -> A(x,y1,z1,m1)
A = permuteSparseTensor(U, [4, 1, 2, 3]) ;

%* A(x,y1,z1,m1) = A(x,y1,z1,m1) / Lambda(1)(y1)
A = tensorTimesVector(A, 4, 2, Lambda(1), 1, 1, '/', parameter, [1, 4, 2, 3]) ;

%* A(x,y1,z1,m1) = A(x,y1,z1,m1) / Lambda(2)(z1)
A = tensorTimesVector(A, 4, 3, Lambda(2), 1, 1, '/', parameter, [1, 2, 4, 3]) ;