function [U, S, V, truncationError, coef] = doTruncation(parameter, U, S, V)
%* U(y1,z1,m1,x) V(y2,z2,m2,x) S(x)
D = parameter.bondDimension ;

SNo = S.subNo ;
allS = [] ;
for iS = 1 : SNo
    allS = [allS; S.tensor{iS}] ;
end
allS = sort(allS, 'descend') ;
coef = allS(1) ;
SminRetain = allS(D) * parameter.maxDiffRatio ;
truncationError = 1 - sum(allS(1 : D).^2) / sum(allS.^2) ;
%* truncation
for iS = 1 : SNo
    %* keep the degenerate element
    cut = find(S.tensor{iS} < SminRetain) ;
    cutNo = length(cut) ;
    % normalization
    S.tensor{iS} = S.tensor{iS} ./ coef ;
    
    if ~isempty(cut)
        S.tensor{iS}(cut) = [] ;
        S.dim(iS) = S.dim(iS) - cutNo ;        
        QN = S.quantNo(iS) ;
        
        Uindex = find(U.quantNo(4, :) == QN) ;
        for i = 1 : length(Uindex)
            Utensor = loadTensor(U, Uindex(i)) ;
            U.dim(4, Uindex(i)) = U.dim(4, Uindex(i)) - cutNo ;
            Utensor(:, cut) = [] ;
            U.tensor{Uindex(i)} = Utensor ;
        end
        Vindex = find(V.quantNo(4, :) == QN) ;
        for j = 1 : length(Vindex)
            Vtensor = loadTensor(V, Vindex(j)) ;
            V.dim(4, Vindex(j)) = V.dim(4, Vindex(j)) - cutNo ;
            Vtensor(:, cut) = [] ;
            V.tensor{Vindex(j)} = Vtensor ;
        end
    end
end

allS = allS ./ coef ;
allS = allS(1 : D) ;