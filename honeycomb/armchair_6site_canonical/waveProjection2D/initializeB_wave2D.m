function B = initializeB_wave2D(parameter, mapping)
%* conservation law
%* B(x,y,z,m): x + y + z = - m

M = parameter.siteDimension ;

quantNo = mapping.quantNo ;
dim = mapping.dim ;

mapNo = length(quantNo) ;
Bdim = ones(4, 1) ;
Bqn = zeros(4, 1) ;
B.indNo = 4 ;
B.subNo = 0 ;
i = 0 ;
for iM = 1 : M
    site = M - 1 - iM ;
    Bqn(4) = site ;
    
    for iMap = 1 : mapNo
        Bqn(1) = quantNo(iMap) ;
        Bdim(1) = dim(iMap) ;
        
        for jMap = 1 : mapNo
            Bqn(2) = quantNo(jMap) ;
            Bdim(2) = dim(jMap) ;
            
            for kMap = 1 : mapNo
                Bqn(3) = quantNo(kMap) ;
                Bdim(3) = dim(kMap) ;
                
                if (Bqn(1) + Bqn(2) + Bqn(3)) == - Bqn(4)
                    i = i + 1 ;
                    B.quantNo(:, i) = Bqn ;
                    B.dim(:, i) = Bdim ;
                    B.tensor{i} = rand(Bdim') ;
                end
            end
        end
    end
end
B.subNo = i ;