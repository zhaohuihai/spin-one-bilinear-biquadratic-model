function [U, S, V] = eliminateEmptyElement(U, S, V)

S = eliminateEmptyS(S) ;
U = eliminateEmptyU(U) ;
V = eliminateEmptyU(V) ;
