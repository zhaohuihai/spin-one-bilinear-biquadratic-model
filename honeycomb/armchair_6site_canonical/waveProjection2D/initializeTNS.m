function wave = initializeTNS(parameter, mapping)
% 6 sites per unit cell
%* x. Lambda(1): 1-2; Lambda(2): 3-6; Lambda(3): 5-4
%* y. Lambda(4): 1-6; Lambda(5): 3-4; Lambda(6): 5-2
%* z. Lambda(7): 1-4; Lambda(8): 3-2; Lambda(9): 5-6

%* Control random number generation
rng(3) ;

if parameter.sameInitialTensor == 1
    %* A(s1,s4,s7,m1) : s1+s4+s7-m1=0 
    A = initializeA_wave2D(parameter, mapping) ;
    %* B(s1,s6,s8,m2) : s1+s6+s8+m2=0
    B = initializeB_wave2D(parameter, mapping) ;
    L = initializeLambda(mapping) ;
    for i = 1 : 3
        ia = 2 * i - 1 ;
        ib = 2 * i ;
        wave.A(ia) = A ;
        wave.A(ib) = B ;
    end
    for i = 1 : 9
        wave.Lambda(i) = L ;
    end
else %* == 0
    for i = 1 : 3
        ia = 2 * i - 1 ;
        ib = 2 * i ;
        wave.A(ia) = initializeA_wave2D(parameter, mapping) ;
        wave.A(ib) = initializeB_wave2D(parameter, mapping) ;
    end
    for i = 1 : 9
        wave.Lambda(i) = initializeLambda(mapping) ;
    end
end

wave.coef = 1 ;
wave.ABsingularValue = 1 ;




