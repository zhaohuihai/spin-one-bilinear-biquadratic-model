function A12 = recover(parameter, Lambda, U, V)


%* A(x,y1,z1,m1)
A = recoverA(parameter, U, Lambda([2, 4])) ;

%* B(x,y2,z2,m2)
B = recoverA(parameter, V, Lambda([3, 5])) ;

A12(1) = A ;
A12(2) = B ;