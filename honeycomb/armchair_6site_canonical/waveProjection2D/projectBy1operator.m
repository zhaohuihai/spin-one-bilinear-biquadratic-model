function [A, S, truncationError, coef] = projectBy1operator(parameter, A, Lambda)
%*************************************
%  L(4)\z1        y2/L(3)
%       \          /
%       A\__L(1)__/B
%        /   x    \
%       /          \
%  L(2)/y1        z2\L(5)
%*************************************
%* projectionOperator(n1,n2,m1,m2) dense -> sparse
parameter.projectionOperator = createProjectionOperator(parameter) ;
%* AB(y1,z1,y2,z2, m1,m2)
AB = computeLyLz_A_Lx_B_LyLz(parameter, A, Lambda) ;

%* T(y1,z1,m1,y2,z2,m2)
T = computeProjection(parameter, AB) ;
%====================================================
% T = permuteSparseTensor(AB, [1, 2, 5, 3, 4, 6]) ;
%====================================================
%* conservation law: - y1 - z1 + m1 = - y2 - z2 - m2 = x
T = partitionTensor(T, 6, [1, 2, 3], '--+') ;
%* U(y1,z1,m1,x) V(y2,z2,m2,x) S(x)
[U, S, V] = svd_projection(parameter, T) ;

[U, S, V, truncationError, coef] = truncate(parameter, U, S, V) ;

A = recover(parameter, Lambda, U, V) ;

