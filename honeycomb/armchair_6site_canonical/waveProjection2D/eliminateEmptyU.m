function U = eliminateEmptyU(U)

UsubNo = U.subNo ;
iUsub = 1 ;
while iUsub <= UsubNo
    if isempty(U.tensor{iUsub})
        U.quantNo(:, iUsub) = [] ;
        U.dim(:, iUsub) = [] ;
        U.tensor(iUsub) = [] ; 
        UsubNo = UsubNo - 1 ;
    else
        iUsub = iUsub + 1 ;
    end
end
U.subNo = UsubNo ;