function [U, S, V] = svd_projection(parameter, T)
%* T(y1,z1,m1,y2,z2,m2)
%* U(y1,z1,m1,x) V(y2,z2,m2,x) S(x)

U.indNo = 4 ;
V.indNo = 4 ;
TNo = length(T) ;
S.indNo = 1 ;
S.subNo = TNo ;


U.subNo = 0 ;
V.subNo = 0 ;
for i = 1 : TNo
    blockQN = T(i).blockQuantNo ;
    %* Tblock: matrix
    %* Tdim: 1X2
    %* Tmap: quantNo, location, dim
    [Tblock, Tdim, Tmap] = combineTensor6to2(T(i)) ;
    [Ublock, Sblock, Vblock] = svd(Tblock, 'econ') ;
    Sblock = diag(Sblock) ;
    %* U(y1,z1,m1,x)
    U = createU_projection(parameter, Ublock, U, blockQN, Tmap(1), T(i).quantNo(1 : 3, :), T(i).dim(1 : 3, :)) ;
    %* V(y2,z2,m2,x)
    V = createU_projection(parameter, Vblock, V, blockQN, Tmap(2), T(i).quantNo(4 : 6, :), T(i).dim(4 : 6, :)) ;
    
    S = createS_projection(Sblock, S, blockQN, i) ;

end

