function [U, S, V, truncationError, coef] = truncate(parameter, U, S, V)


[U, S, V, truncationError, coef] = doTruncation(parameter, U, S, V) ;

[U, S, V] = eliminateEmptyElement(U, S, V) ;

