function Lambda = initializeLambda(mapping)

quantNo = mapping.quantNo ;
dim = mapping.dim ;

mapNo = length(quantNo) ;

tensor = cell(1, mapNo) ;

for iMap = 1 : mapNo
    tensor{iMap} = sort(rand(dim(iMap), 1), 'descend') ;
    
end
Lambda.indNo = 1 ;
Lambda.subNo = mapNo ;
Lambda.quantNo = quantNo ;
Lambda.dim = dim ;
Lambda.tensor = tensor ;