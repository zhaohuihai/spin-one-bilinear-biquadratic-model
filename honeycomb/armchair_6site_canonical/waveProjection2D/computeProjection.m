function T = computeProjection(parameter, AB)

PO = parameter.projectionOperator ;
%* T(y1,z1,m1,y2,z2,m2) = sum{m1,m2}_(AB(y1,z1,y2,z2,n1,n2)*PO(n1,n2,m1,m2))
T = contractSparseTensors(AB, 6, [5, 6], PO, 4, [1, 2], [1, 2, 5, 3, 4, 6]) ;


