function wave = rotateWaveSite(wave)

% (1,2,3,4,5,6) -> (3,6,5,2,1,4)
wave.A = wave.A([3, 6, 5, 2, 1, 4]) ;
% 
wave.Lambda = wave.Lambda([2, 3, 1, 5, 6, 4, 8, 9, 7]) ;