function AB = computeLyLz_A_Lx_B_LyLz(parameter, A12, Lambda)
%*************************************
%  L(4)\z1        y2/L(3)
%       \          /
%       A\__L(1)__/B
%        /   x    \
%       /          \
%  L(2)/y1        z2\L(5)
%*************************************
M = parameter.siteDimension ;
D = parameter.bondDimension ;

A = A12(1) ;
B = A12(2) ;

%* A(x,y,z,m) = A(x,y,z,m)*Lambda(1)(x)
A = tensorTimesVector(A, 4, 1, Lambda(1), 1, 1, [4, 1, 2, 3]) ;

%* A(x,y,z,m) = A(x,y,z,m)*Lambda(2)(y)
A = tensorTimesVector(A, 4, 2, Lambda(2), 1, 1, [1, 4, 2, 3]) ;

%* A(x,y,z,m) = A(x,y,z,m)*Lambda(4)(z)
A = tensorTimesVector(A, 4, 3, Lambda(4), 1, 1, [1, 2, 4, 3]) ;

%* B(x,y,z,m) = B(x,y,z,m)*Lambda(3)(y)
B = tensorTimesVector(B, 4, 2, Lambda(3), 1, 1, [1, 4, 2, 3]) ;
%* B(x,y,z,m) = B(x,y,z,m)*Lambda(5)(z)
B = tensorTimesVector(B, 4, 3, Lambda(5), 1, 1, [1, 2, 4, 3]) ;

%* AB(y1,z1,y2,z2,m1,m2) = sum{x}_[A(x,y1,z1,m1)*B(x,y2,z2,m2)]
AB = contractSparseTensors(A, 4, 1, B, 4, 1, [1, 2, 4, 5, 3, 6]) ;



