function S = eliminateEmptyS(S)

SNo = S.subNo ;
iS = 1 ;
while iS <= SNo
    if isempty(S.tensor{iS})
        S.quantNo(iS) = [] ;
        S.dim(iS) = [] ;
        S.tensor(iS) = [] ;
        SNo = SNo - 1 ;
    else
        iS = iS + 1 ;
    end
end
S.subNo = SNo ;