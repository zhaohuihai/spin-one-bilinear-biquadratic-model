function A = initializeA_wave2D(parameter, mapping)
%* conservation law
%* A(x,y,z,m): x + y + z = m

M = parameter.siteDimension ;

quantNo = mapping.quantNo ;
dim = mapping.dim ;

mapNo = length(quantNo) ;
Adim = ones(4, 1) ;
Aqn = zeros(4, 1) ;
A.indNo = 4 ;
A.subNo = 0 ;
i = 0 ;
for iM = 1 : M
    site = M - 1 - iM ;
    Aqn(4) = site ;
    
    for iMap = 1 : mapNo
        Aqn(1) = quantNo(iMap) ;
        Adim(1) = dim(iMap) ;
        
        for jMap = 1 : mapNo
            Aqn(2) = quantNo(jMap) ;
            Adim(2) = dim(jMap) ;
            
            for kMap = 1 : mapNo
                Aqn(3) = quantNo(kMap) ;
                Adim(3) = dim(kMap) ;
                
                if (Aqn(1) + Aqn(2) + Aqn(3)) == Aqn(4)
                    i = i + 1 ;
                    A.quantNo(:, i) = Aqn ;
                    A.dim(:, i) = Adim ;
                    A.tensor{i} = rand(Adim') ;
                end
            end
        end
    end
end

A.subNo = i ;