function a = contract2vector(VL, indVL, VR, indVR)

numindVL = VL.indNo ;
numindVR = VR.indNo ;
a = 0 ;
for i = 1 : VL.subNo
    VLqn = VL.quantNo(:, i) ;
    VLtensor = loadTensor(VL, i) ;
    
    j = findMatchQN(VLqn, indVL, VR.quantNo, indVR) ;
    if ~isempty(j)
        VRtensor = loadTensor(VR, j) ;
        x = contractTensors(VLtensor, numindVL, indVL, VRtensor, numindVR, indVR) ;
        a = a + x ;
    end
end