function X = tensorTimesVector(X, numindX, indX, Y, numindY, indY, varargin)
%* optional input arguments: rule, parameter, order
%* possible nargin:
%* 6: X, numindX, indX, Y, numindY, indY
%* 7: X, numindX, indX, Y, numindY, indY, rule
%* 7: X, numindX, indX, Y, numindY, indY, parameter
%* 7: X, numindX, indX, Y, numindY, indY, order
%* 8: X, numindX, indX, Y, numindY, indY, rule, parameter
%* 8: X, numindX, indX, Y, numindY, indY, rule, order
%* 8: X, numindX, indX, Y, numindY, indY, parameter, order
%* 9: X, numindX, indX, Y, numindY, indY, rule, parameter, order

if nargin == 6
    rule = '*' ;
    parameter.minSizeSaveInHD = Inf ;
    order = 1 : numindX ;
elseif nargin == 7
    if ischar(varargin{1})
        rule = varargin{1} ;
        parameter.minSizeSaveInHD = Inf ;
        order = 1 : numindX ;
    elseif isstruct(varargin{1})
        rule = '*' ;
        parameter = varargin{1} ;
        order = 1 : numindX ;
    else
        rule = '*' ;
        parameter.minSizeSaveInHD = Inf ;
        order = varargin{1} ;
    end
elseif nargin == 8
    if ischar(varargin{1})
        rule = varargin{1} ;
        if isstruct(varargin{2})
            parameter = varargin{2} ;
            order = 1 : numindX ;
        else
            parameter.minSizeSaveInHD = Inf ;
            order = varargin{2} ;
        end
    else %*
        rule = '*' ;
        parameter = varargin{1} ;
        order = varargin{2} ;
    end
elseif nargin == 9
    rule = varargin{1} ;
    parameter = varargin{2} ;
    order = varargin{3} ;
end

for i = 1 : X.subNo
    QN = X.quantNo(:, i) ;
    Xtensor = loadTensor(X, i) ;
    j = findMatchQN(QN, indX, Y.quantNo, indY) ;
    Ydim = Y.dim(indY, j) ;
    Ytensor = loadTensor(Y, j) ;
    Ytensor = reshape(Ytensor, [prod(Ydim), 1]) ;
    eval(['Ytensor = 1 .', rule, ' Ytensor ;']) ;
    %* vector => diagonal matrix
    Ytensor = diag(Ytensor) ;
    
    tensor = contractTensors(Xtensor, numindX, indX, Ytensor, 2, 1, order) ;
    
    X = saveTensor(X, tensor, i, parameter) ;
end