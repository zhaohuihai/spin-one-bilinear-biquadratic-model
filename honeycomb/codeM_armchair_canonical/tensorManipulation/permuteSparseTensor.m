function A = permuteSparseTensor(A, order, varargin)

if nargin == 2
    parameter.minSizeSaveInHD = Inf ;
else
    parameter = varargin{1} ;
end

A.quantNo = A.quantNo(order, :) ;
A.dim = A.dim(order, :) ;
for i = 1 : A.subNo
    tensor = loadTensor(A, i) ;
    tensor = permute(tensor, order) ;
    A = saveTensor(A, tensor, i, parameter) ;
end