function P = partitionTensor(T, numindT, indT, partRule)
%* T(a,b,c,d,...) 

quantNo = T.quantNo ;
dim = T.dim ;
leftIndNo = length(indT) ;
%* left quantum number of block
leftQN = zeros(1, T.subNo) ;
for i = 1 : leftIndNo
    QN = quantNo(indT(i), :) ;
    eval(['leftQN = leftQN ', partRule(i), 'QN ;']) ;
end
%* partition T in a structure array by same block quantum number
iP = 0 ;
t = T.subNo ;
while t > 0
    max_leftQN = max(leftQN) ;
    index = find(leftQN == max_leftQN) ;
    indexNo = length(index) ;
    
    iP = iP + 1 ;
    P(iP).blockQuantNo = max_leftQN ;
    P(iP).indNo = numindT ;
    P(iP).subNo = indexNo ;
    P(iP).quantNo = quantNo(:, index) ;
    P(iP).dim = dim(:, index) ;
    P(iP).tensor = T.tensor(index) ;
    
    leftQN(index) = leftQN(index) - 1e9 ;
    t = t - indexNo ;
end
