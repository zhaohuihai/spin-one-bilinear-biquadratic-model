function tensor = loadTensor(T, index)

if ischar(T.tensor{index})
    fileName = T.tensor{index} ;
    load(fileName, 'tensor') ;
else
    tensor = T.tensor{index} ;
end