function [V, coef] = normalizeVector(V)
%* 2-norm
indNo = V.indNo ;
indV = 1 : indNo ;
a = contract2vector(V, indV, V, indV) ;
coef = sqrt(a) ;

for i = 1 : V.subNo
    Vtensor = loadTensor(V, i) ;
    Vtensor = Vtensor ./ coef ;
    V = saveTensor(V, Vtensor, i) ;
end