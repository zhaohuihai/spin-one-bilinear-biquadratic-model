function parameter = defineProjectionParameter(parameter)

parameter.polarization = 0 ;

parameter.field = 1 ;

parameter.fieldInitial = 0.01 ;
parameter.fieldFinal = 0.2 ;
parameter.fieldIncre = 0.01 ;

parameter.stagDipolarField_z = 0 ;

parameter.quadrupolarField_z = 1 ;

parameter.stagQuadrupolarField_z = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************
parameter.TrotterOrder = 2 ;

parameter.tauInitial = 0.1 ;
parameter.tauFinal = 1e-4 ; %*
parameter.tauChangeFactor = 40 ;
% disp(['initial tau = ', num2str(parameter.tauInitial)])

parameter.convergenceCriterion_projection = 1e-11 ;
parameter.maxProjectionStep = 1e4 ;
%* if the retaining dimension is smaller than 'svdRatio' of the total dimention, use svds
parameter.svdRatio_projection = 0.0 ;

