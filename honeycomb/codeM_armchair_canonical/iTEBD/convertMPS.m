function A1 = convertMPS(A, Lambda, bondFlag)
%* valid bondFlag value: 1, 3

A1 = rmfield(A, 'tensor3') ;
A1.indNo = 3 ;

if bondFlag == 1
    %* A(a,i,b) = A(a,i,b)*Lambda(a)
    for i = 1 : A.subNo
        QN = A.quantNo(1, i) ;
        tensor = A.tensor3{i} ;
        index = find(QN == Lambda.quantNo) ;
        L = Lambda.tensor1{index} ;
        for a = 1 : Lambda.dim(index)
            tensor(a, :, :) = tensor(a, :, :) * L(a) ;
        end
        A1.tensor{i} = tensor ;
    end
elseif bondFlag == 3
    %* A(a,i,b) = A(a,i,b)*Lambda(b)
    for i = 1 : A.subNo
        QN = A.quantNo(3, i) ;
        tensor = A.tensor3{i} ;
        index = find(QN == Lambda.quantNo) ;
        L = Lambda.tensor1{index} ;
        for b = 1 : Lambda.dim(index)
            tensor(:, :, b) = tensor(:, :, b) * L(b) ;
        end
        A1.tensor{i} = tensor ;
    end
end

