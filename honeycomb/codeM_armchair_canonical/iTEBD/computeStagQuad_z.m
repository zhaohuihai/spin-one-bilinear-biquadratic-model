function computeStagQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

parameter.H = createStagQzz(parameter) ;
disp('compute staggered quadrupole') ;
stagQuadBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;

stagQuadrupole = mean(stagQuadBond) ;

saveExpectationValue(parameter, stagQuadrupole, 'stagQuadrupole_z') ;