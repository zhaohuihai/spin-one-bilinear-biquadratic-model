function [AAblock, AAblockDim] = combineAAUsub_y(AA, index, mapping)


AAblockDim = zeros(3, 1) ;
for i = 1 : 3
    AAblockDim(i) = mapping(i).totDim ;
end


subNo = length(index) ;

quantNo(1, :) = AA.quantNo(1, index) ;
quantNo(2, :) = AA.quantNo(2, index) ;
quantNo(3, :) = AA.quantNo(4, index) ;
dim = AA.dim(:, index) ;

tensor3 = AA.tensor3(index) ;
AAblock = zeros(AAblockDim') ;

for i = 1 : subNo
    m = zeros(1, 3) ;
    for j = 1 : 3
        m(j) = find(mapping(j).map(1, :) == quantNo(j, i)) ;
    end

    k = cell(1, 3) ;
    for j = 1 : 3
        position = mapping(j).map(2, m(j)) ;
        k{j} = position : (position + dim(j, i) - 1) ;
    end

    AAblock(k{1}, k{2}, k{3}) = AAblock(k{1}, k{2}, k{3}) + tensor3{i} ;
end

