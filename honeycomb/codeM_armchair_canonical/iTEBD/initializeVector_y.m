function vector = initializeVector_y(PR, PL)
%* initialize a vector(c1,l,c2)
%* PR(a1,j,c1,i), PL(l,c2,i,a2) field: subNo, quantNo, dim

vector.indNo = 3 ;

c1Map = findTmapping(PR.quantNo(3, :), PR.dim(3, :)) ;
lMap = findTmapping(PL.quantNo(1, :), PL.dim(1, :)) ;
c2Map = findTmapping(PL.quantNo(2, :), PL.dim(2, :)) ;

vDim = zeros(3, 1) ;
vQN = zeros(3, 1) ;
n = 0 ;
for c1 = 1 : c1Map.subNo
    vDim(1) = c1Map.dim(c1) ;
    vQN(1) = c1Map.quantNo(c1) ;
    for l = 1 : lMap.subNo
        vDim(2) = lMap.dim(l) ;
        vQN(2) = lMap.quantNo(l) ;
        for c2 = 1 : c2Map.subNo
            vDim(3) = c2Map.dim(c2) ;
            vQN(3) = c2Map.quantNo(c2) ;
            n = n + 1 ;
            vector.quantNo(:, n) = vQN ;
            vector.dim(:, n) = vDim ;
            vector.tensor{n} = rand(vDim') ;
        end
    end
end
vector.subNo = n ;