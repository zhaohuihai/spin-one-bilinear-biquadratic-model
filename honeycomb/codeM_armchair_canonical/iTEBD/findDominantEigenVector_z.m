function [VR, VL, eta] = findDominantEigenVector_z(parameter, ABR, ABL, T)
%* ABR(a,c,i,j) ABL(a',c',k,l) T(i,j,k,l)
%* VR(c',l,c), VL(a,j,a')
%* PR(l,c,i,a), PL(a',j,c',i)

PRdir = 'tensor/PR/' ;
%* PR(l,c,k,a) = sum{i,j}_[ABR(a,c,i,j)*T(i,j,k,l)]
%* change notation: PR(l,c,i,a)
PR = contractSparseTensors(ABR, 4, [3, 4], T, 4, [1, 2], parameter, PRdir, [4, 2, 3, 1]) ;

PLdir = 'tensor/PL/' ;
%* PL(a',i,c',j) = sum{k,l}_[ABL(a',c',k,l) * T(i,j,k,l)]
%* change notation: PL(a',j,c',i)
PL = contractSparseTensors(ABL, 4, [3, 4], T, 4, [3, 4], parameter, PLdir, [1, 3, 2, 4]) ;

PRmap = rmfield(PR, 'tensor') ;
PLmap = rmfield(PL, 'tensor') ;
%* initialize a normalized vector(c',l,c)
V = initializeVector_y(PLmap, PRmap) ;
V = normalizeVector(V) ;

%* VR(a',j,a)=sum{c',i,l,c}_[PL(a',j,c',i)*VR(c',l,c)*PR(l,c,i,a)]
[VR, etaR] = findDominantRightEigenVector_y(parameter, PL, PR, V) ;

%* PR(l,c,i,a) -> PR(c,l,a,i)
PR = permuteSparseTensor(PR, [2, 1, 4, 3], parameter) ;
%* PL(a',j,c',i) -> PL(j,a',i,c')
PL = permuteSparseTensor(PL, [2, 1, 4, 3], parameter) ;

%* transpose: VR(a',j,a) -> VL(a,j,a')
VL = permuteSparseTensor(VR, [3, 2, 1], parameter) ;

%* VL(c,l,c') = sum{a,i,j,a'}_[PR(c,l,a,i)*VL(a,j,a')*PL(j,a',i,c')]
[VL, etaL] = findDominantRightEigenVector_y(parameter, PR, PL, VL) ;

eta = (etaR + etaL) / 2 ;
