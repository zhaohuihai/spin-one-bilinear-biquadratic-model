function [vector1, P] = applyPowerMethod_x(parameter, P, vector)
%* vector1(a1,a2) = sum{c1,c2}_(P((a1,a2),(c1,c2))*vector(c1,c2))

vector1.subNo = 0 ;
vector1.quantNo = zeros(2, 0) ;
vector1.dim = zeros(1, 0) ;
vector1.tensor1 = cell(0) ;
n = 0 ;
for i = 1 : P.subNo
    pQN = P.quantNo(:, i) ;
    pDim = P.dim(:, i) ;
    %* Ptensor((a1,a2),(c1,c2))
%     Ptensor = P.tensor2{i} ;
    Ptensor = loadTensor2(parameter, P, i) ;
    equalc1 = (pQN(3) == vector.quantNo(1, :)) ;
    equalc2 = (pQN(4) == vector.quantNo(2, :)) ;
    j = find(equalc1 & equalc2) ;
    if ~isempty(j)        
        for k = 1 : length(j)           
            %* vTensor(c1,c2)
            vTensor = vector.tensor1{j(k)} ;
            
            %* tensor((a1,a2))
            tensor = Ptensor * vTensor ;
            
            v1QN = [pQN(1); pQN(2)] ;
            v1Dim = pDim(1) * pDim(2) ;
            equalv1 = cell(1, 2) ;
            for m = 1 : 2
                equalv1{m} = (v1QN(m) == vector1.quantNo(m, :)) ;
            end
            index = find(equalv1{1} & equalv1{2}) ;
            
            if isempty(index)
                n = n + 1 ;
                vector1.quantNo(:, n) = v1QN ;
                vector1.dim(n) = v1Dim ;
                vector1.tensor1{n} = tensor ;
            else
                vector1.tensor1{index} = vector1.tensor1{index} + tensor ;
            end
        end
    end
end
vector1.subNo = n ;