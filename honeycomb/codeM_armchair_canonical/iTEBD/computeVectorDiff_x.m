function vD = computeVectorDiff_x(v1, v)
%* vD(c1,c2) = v1(c1,c2) - v(c1,c2)

vD.subNo = 0 ;
vD.quantNo = zeros(2, 0) ;
vD.dim = zeros(1, 0) ;
vD.tensor1 = cell(0) ;
n = 0 ;
for i = 1 : v1.subNo
    v1QN = v1.quantNo(:, i) ;
    v1Dim = v1.dim(i) ;
    %* v1(c1,c2)
    v1Tensor = v1.tensor1{i} ;
    equalc1 = (v1QN(1) == v.quantNo(1, :)) ;
    equalc2 = (v1QN(2) == v.quantNo(2, :)) ;
    
    j = find(equalc1 & equalc2) ;
    n = n + 1 ;
    vD.quantNo(:, n) = v1QN ;
    vD.dim(n) = v1Dim ;
    if isempty(j)
        vD.tensor1{n} = v1Tensor ;
    else
        vD.tensor1{n} = v1Tensor - v.tensor1{j} ;
        v.subNo = v.subNo - 1 ;
        v.quantNo(:, j) = [] ;
        v.dim(j) = [] ;
        v.tensor1(j) = [] ;
    end
end

for i = 1 : v.subNo
    vQN = v.quantNo(:, i) ;
    vDim = v.dim(i) ;
    vTensor = v.tensor1{i} ;
    
    n = n + 1 ;
    vD.quantNo(:, n) = vQN ;
    vD.dim(n) = vDim ;
    vD.tensor1{n} = - vTensor ;
end
vD.subNo = n ;