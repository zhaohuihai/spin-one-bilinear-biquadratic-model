function [MPS, T] = findDominantRightEigenMPS(parameter, T)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;
Tmapping = rmfield(T, 'tensor') ;
%* MPS: A B
[MPS] = initializeMPS(parameter, Tmapping) ;

TMPstep = 0 ;
[MPS, T, ~, ~, TMPstep] = iterateRightGate1D(parameter, T, MPS, TMPstep) ;

while parameter.dim_MPS < maxDim
    TMPstep = TMPstep + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, T] = applyTMP(parameter, T, MPS) ;
    [MPS, T] = applyTMP(parameter, T, MPS) ;
    toc
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;
[MPS, T, ~, ~, TMPstep] = iterateRightGate1D(parameter, T, MPS, TMPstep) ;
disp(['total TMP steps = ', num2str(TMPstep)]) ;