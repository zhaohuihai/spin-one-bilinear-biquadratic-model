function [vector, eta] = findDominantRightEigenVector_y(parameter, PR, PL, vector)
%* vector(a,j,a')
%* PR(a,j,c,i)
%* PL(l,c',i,a')

powerConvergence = 1 ;
eta = 1 ;
j = 0 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    %* vector1(a,j,a') = sum{c,c',l,i}_(PR(a,j,c,i)*vector(c,l,c')*PL(l,c',i,a'))
    vector1 = applyPowerMethod_y(PR, PL, vector) ;
    [vector1, eta1] = normalizeVector(vector1) ;
    
    powerConvergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;