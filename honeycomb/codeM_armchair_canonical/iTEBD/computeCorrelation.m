function [SzCorrelation, QzzCorrelation] = computeCorrelation(parameter, wave)
%*********************************************************************
%* Sz correlation: <Sz(i)*Sz(i+l)> - <Sz(i)><Sz(i+l)>
%* Qzz correlation: <Qzz(i)*Qzz(i+l)> - <Qzz(i)><Qzz(i+l)>
%* compute 3 bond directions: x, y, z
%* SzCorrelation and QzzCorrelation are maxCorrelationLength X 9 matrices
%*********************************************************************
SzCorrelation = [] ;
QzzCorrelation = [] ;

L = parameter.computation.maxCorrelationLength ;
l = 1 : L ;
l = l' ;

%* x, y, z are maxCorrelationLength X 1 column vectors
[SzCorrelation_x, SzCorrelation_xM, QzzCorrelation_x, QzzCorrelation_xM] = computeCorrelation_x(parameter, wave)

wave = rotate(parameter, wave) ;
[SzCorrelation_y, SzCorrelation_yM, QzzCorrelation_y, QzzCorrelation_yM] = computeCorrelation_x(parameter, wave)

wave = rotate(parameter, wave) ;
[SzCorrelation_z, SzCorrelation_zM, QzzCorrelation_z, QzzCorrelation_zM] = computeCorrelation_x(parameter, wave)


if parameter.computation.SzCorrelation == 1
    SzCorrelation = averageCorrelation(l, SzCorrelation_x, SzCorrelation_xM, SzCorrelation_y, SzCorrelation_yM, SzCorrelation_z, SzCorrelation_zM) ;
    saveExpectationValue(parameter, SzCorrelation, 'SzCorrelation')
end

if parameter.computation.QzzCorrelation == 1
    QzzCorrelation = averageCorrelation(l, QzzCorrelation_x, QzzCorrelation_xM, QzzCorrelation_y, QzzCorrelation_yM, QzzCorrelation_z, QzzCorrelation_zM) ;
    saveExpectationValue(parameter, QzzCorrelation, 'QzzCorrelation')
end


[status, message, messageid] = rmdir('tensor', 's') ;



