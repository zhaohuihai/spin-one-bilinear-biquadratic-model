function TH = createSpecialTensor_x(parameter, AA, BB)
%* TH(i,j,k,l) = sum{x,mi,mj,ni,nj}_[AA(x,i,k;mi,ni)*H(mi,mj,ni,nj)*BB(x,l,j;mj,nj)]

M = parameter.siteDimension ;

HBB = computeH_BB(parameter, BB) ;
for m1 = 1 : M
    for m2 = 1 : M
        HBB(m1, m2) = reduceTa(HBB(m1, m2)) ;
    end
end
THdir = 'tensor/TH/' ;
%* TH(i,j,k,l)=sum_{x,mi,ni}(AA(x,i,k;mi,ni)*HBB(x,l,j;mi,ni)
TH = computeAA_HBB(parameter, THdir, M, AA, HBB) ;
