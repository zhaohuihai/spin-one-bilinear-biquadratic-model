function AAU = contractAA_U_z(parameter, AA, U)
%* AAU(x,i,(k,n)) = sum{m1,m2}_(AA(x,i,k;m1,m2)*U((m1,m2),n))

M = parameter.siteDimension ;

n = 0 ;
AAU.subNo = 0 ;
AAU.quantNo = zeros(4, 0) ;
AAU.dim = zeros(3, 0) ;
AAU.tensor3 = cell(0) ;
for m1 = 1 : M
    site1 = M + 1 - 2 * m1 ;
    for m2 = 1 : M
        site2 = M + 1 - 2 * m2 ;
        for i = 1 : AA(m1, m2).subNo
            aQN = AA(m1, m2).quantNo(:, i) ;
            aDim = AA(m1, m2).dim(:, i) ;
            %* aTensor(x,i,k)
            aTensor = AA(m1, m2).tensor3{i} ;
            %* aTensor((x,i,k), 1)
            aTensor = reshape(aTensor, [aDim(1)*aDim(2)*aDim(3), 1]) ;
            
            equalm = cell(1, 2) ;
            equalm{1} = (site1 == U.quantNo(1, :)) ;
            equalm{2} = (site2 == U.quantNo(2, :)) ;
            j = find(equalm{1} & equalm{2}) ;
            if ~isempty(j)
                for k = 1 : length(j)
                    uQN = U.quantNo(:, j(k)) ;
                    uDim = U.dim(:, j(k)) ;
                    %* uTensor((m1,m2),n)
                    uTensor = U.tensor2{j(k)} ;
                    
                    %* tensor((x,i,k), n)
                    tensor = aTensor * uTensor ;
                    %* tensor(x,i,(k,n))
                    tensor = reshape(tensor, [aDim(1), aDim(2), aDim(3) * uDim(3)]) ;
                    
                    QN = [aQN(1); aQN(2); aQN(3); uQN(3)] ;
                    dim = [aDim(1); aDim(2); aDim(3) * uDim(3)] ;
                    equal = cell(1, 3) ;
                    for m = 1 : 3
                        equal{m} = (QN(m) == AAU.quantNo(m, :)) ;
                    end
                    index = find(equal{1} & equal{2} & equal{3}) ;
                    if isempty(index)
                        n = n + 1 ;
                        AAU.quantNo(:, n) = QN ;
                        AAU.dim(:, n) = dim ;
                        AAU.tensor3{n} = tensor ;
                    else
                        AAU.tensor3{index} = AAU.tensor3{index} + tensor ;
                    end
                end
            end
        end
    end
end
AAU.subNo = n ;