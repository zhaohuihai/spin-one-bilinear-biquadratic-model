function [correlation] = findCorrelation(distance, QR, QRa, QRb, QLa, QLb, VR, VL, eta, PR, PL, coef, normFactor)
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)
%* PR(j,a,i,c), PL(c',l,a',i)
%* VR(c,l,c') 
%* distance <= 3: VL(a',j,a); distance >= 4: VL(a',j,a,n)

%* k = 0,1,2,3
k = mod(distance, 4) ;

if distance == 1
    correlation = contractFinal(VL, 3, QR, 4, PL, 4, VR, 3) ;
elseif distance == 2
    correlation = contractFinal(VL, 3, QRa, 5, QLa, 5, VR, 3) ;
elseif distance == 3
    correlation = contractFinal(VL, 3, QRa, 5, QLb, 5, VR, 3) ;
elseif k == 0
    correlation = contractFinal(VL, 4, QRa, 5, PL, 4, VR, 3) ;
elseif k == 1
    correlation = contractFinal(VL, 4, QRb, 5, PL, 4, VR, 3) ;
elseif k == 2
    correlation = contractFinal(VL, 4, PR, 4, QLa, 5, VR, 3) ;
elseif k == 3
    correlation = contractFinal(VL, 4, PR, 4, QLb, 5, VR, 3) ;
end
    
n = floor(distance / 4) + 1 ;

correlation = correlation * coef / (eta^n * normFactor) ;
