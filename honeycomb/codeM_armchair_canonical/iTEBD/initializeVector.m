function V = initializeVector(T, indT)
%* initialize a V(c, c')
%* T(a,a',c,c')

indNo = length(indT) ;
V.indNo = indNo ;
V.subNo = 0 ;
V.quantNo = zeros(indNo, 0) ;
V.dim = zeros(indNo, 0) ;
V.tensor = cell(0) ;
 
n = 0 ;
for i = 1 : T.subNo
    Tqn = T.quantNo(indT, i) ;
    Tdim = T.dim(indT, i) ;
    j = findMatchQN(Tqn, V.quantNo) ;
    if isempty(j)
        n = n + 1 ;
        V.quantNo(:, n) = Tqn ;
        V.dim(:, n) = Tdim ;
        V.tensor{n} = rand(Tdim') ;
    end
end
V.subNo = n ;