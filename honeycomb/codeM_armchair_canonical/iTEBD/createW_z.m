function Wa = createW_z(parameter, AA, U)
%* Wa(x,i,kn) = sum{m1,m2}_(AA(x,i,k;m1,m2)*U((m1,m2),n))

%* AAU(x,i,(k,n))
AAU = contractAA_U_z(parameter, AA, U) ;

quantNo = AAU.quantNo(3, :) ;
dim = AAU.dim(3, :) ;
allTwoQN = AAU.quantNo(3, :) - AAU.quantNo(4, :) ;

mapping(1) = createTotMapping(AAU.quantNo(1, :), AAU.quantNo(1, :), AAU.dim(1, :)) ;
mapping(2) = createTotMapping(AAU.quantNo(2, :), AAU.quantNo(2, :), AAU.dim(2, :)) ;
mapping(3) = createTotMapping(allTwoQN, quantNo, dim) ;


Wa = blockAAUby2bond_z(AAU, mapping) ;
