function H = convertToSparseH(parameter)
%* Hd(mi,mj,mi',mj') => H(mi,mi',mj,mj')
%* conservation law: mi - mi' = mj - mj'

M = parameter.siteDimension ;
%* Hd(mi,mj,mi',mj')
Hd = reshape(parameter.H, [M, M, M, M]) ;
%* Hd(mi,mi',mj,mj')
Hd = permute(Hd, [1, 3, 2, 4]) ;

subNo = 0 ;
quantNo = [] ;
tensor = cell(0) ;

for p1 = 1 : M
    m1 = M + 1 - 2 * p1 ;
    for q1 = 1 : M
        n1 = M + 1 - 2 * q1 ;
        for p2 = 1 : M
            m2 = M + 1 - 2 * p2 ;
            for q2 = 1 : M
                n2 = M + 1 - 2 * q2 ;
                if Hd(p1, q1, p2, q2) ~= 0
                    
                    subNo = subNo + 1 ;
                    QN = [m1; n1; m2; n2] ;
                    quantNo(:, subNo) = QN ;
                    tensor{subNo} = Hd(p1, q1, p2, q2);
                end
            end
        end
    end
end
dim = ones(4, subNo) ;
%************************************************
% H.subNo = 6 ;
% 
% quantNo(:, 1) = [1; 1; 1; 1] ;
% tensor2{1} = Hd(1, 1, 1, 1) ;
% 
% quantNo(:, 2) = [1; -1; 1; -1] ;
% tensor2{2} = Hd(1, 2, 1, 2) ;
% 
% quantNo(:, 3) = [-1; -1; 1; 1] ;
% tensor2{3} = Hd(2, 2, 1, 1) ;
% 
% quantNo(:, 4) = [1; 1; -1; -1] ;
% tensor2{4} = Hd(1, 1, 2, 2) ;
% 
% quantNo(:, 5) = [-1; 1; -1; 1] ;
% tensor2{5} = Hd(2, 1, 2, 1) ;
% 
% quantNo(:, 6) = [-1; -1; -1; -1] ;
% tensor2{6} = Hd(2, 2, 2, 2) ;
% 
% dim = ones(4, 6) ;
%************************************************
H.indNo = 4 ;
H.subNo = subNo ;
H.quantNo = quantNo ;
H.dim = dim ;
H.tensor = tensor ;