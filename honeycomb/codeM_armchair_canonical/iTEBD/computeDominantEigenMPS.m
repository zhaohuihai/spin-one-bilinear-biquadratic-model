function [rightMPS, leftMPS] = computeDominantEigenMPS(parameter, T)
%* T(i,j,k,l)
%* conservation law of T: i - j = -k + l

% rand('twister', 1) ;
[rightMPS, T] = findDominantRightEigenMPS(parameter, T) ;

%* transpose to left transferMatrix
%* T(i,j,k,l) --> T(k,l,i,j)
T = permuteSparseTensor(T, [3, 4, 1, 2], parameter) ;

[leftMPS, T] = findDominantRightEigenMPS(parameter, T) ;

%* transpose to right transferMatrix
%* T(k,l,i,j) --> T(i,j,k,l)
T = permuteSparseTensor(T, [3, 4, 1, 2], parameter) ;