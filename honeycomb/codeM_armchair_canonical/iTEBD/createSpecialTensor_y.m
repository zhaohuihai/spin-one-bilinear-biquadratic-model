function [QR, QL] = createSpecialTensor_y(parameter, ABR, ABL, AA, BB, Ta, Tb)
%* Ta(x,i,k) Tb(x,l,j)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')
%* QR(j,a,i,c,n), QL(c',l,a',i,n)


%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, AA, BB) ;
%==============================================================
WaDir = 'tensor/Wa/' ;
%* Wa(i,k,n,l,j) = sum{x}_[TCa(x,i,k,n)*Tb(x,l,j)]
Wa = contractSparseTensors(TCa, 4, 1, Tb, 3, 1, parameter, WaDir) ;

QLdir = 'tensor/QL/' ;
%* QL(c',j,a',i,n) = sum{k,l}_[Wa(i,k,n,l,j)*ABL(a',c',k,l)]
QL = contractSparseTensors(Wa, 5, [2, 4], ABL, 4, [3, 4], parameter, QLdir, [5, 3, 4, 1, 2]) ;
%* change notation: QL(c',l,a',i,n)

WbDir = 'tensor/Wb/' ;
%* Wb(i,k,l,j,n) = sum{x}_[Ta(x,i,k)*TCb(x,l,j,n)]
Wb = contractSparseTensors(Ta, 3, 1, TCb, 4, 1, parameter, WbDir) ;

QRdir = 'tensor/QR/' ;
%* QR(k,a,l,c,n) = sum{i,j}_[Wb(i,k,l,j,n)*ABR(a,c,i,j)]
QR = contractSparseTensors(Wb, 5, [1, 4], ABR, 4, [3, 4], parameter, QRdir, [1, 4, 2, 5, 3]) ;
%* change notation: QRb(j,a,i,c,n)