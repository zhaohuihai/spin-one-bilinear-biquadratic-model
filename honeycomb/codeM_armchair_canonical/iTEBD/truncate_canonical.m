function [MPS, allS, truncationError] = truncate_canonical(parameter, MPS, Lambda)

D = parameter.dim_MPS ;

Lno = Lambda.subNo ;
allS = [] ;
for i = 1 : Lno
    allS = [allS; Lambda.tensor{i}] ;
end
allS = sort(allS, 'descend') ;

retainDim = min(length(allS), D) ;
truncationError = 1 - sum(allS(1 : retainDim).^2) / sum(allS.^2) ;
SminRetain = allS(retainDim) * parameter.maxDiffRatio ;
%* truncation: b
%* A(a,i,b)
A = MPS.A ;
%* B(b,j,c)
B = MPS.B ;

for iL = 1 : Lno
    %* keep the degenerate element
    cut = find(Lambda.tensor{iL} < SminRetain) ;
    cutNo = length(cut) ;
    if cutNo ~= 0
        QN = Lambda.quantNo(iL) ;
        A = truncateA_canonical(A, QN, cut) ;
        
        B = truncateB_canonical(B, QN, cut) ;
    end
end

allS = allS(1 : D) ;

MPS.A = loadAll(A) ;
MPS.B = loadAll(B) ;


