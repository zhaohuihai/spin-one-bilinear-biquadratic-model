function [U, S, V] = svd_Hamiltonian(parameter, H)

Hno = length(H) ;
S.subNo = Hno ;

U.subNo = 0 ;
V = U ;
for i = 1 : Hno
    Hblock = combineRsub(parameter, H(i)) ;
    [Ublock, Sblock, Vblock] = svd(Hblock) ;
    Sblock = diag(Sblock) ;
    
    cut = find(Sblock < 1e-30) ;
    Ublock(:, cut) = [] ;
    Sblock(cut) = [] ;
    Vblock(:, cut) = [] ;
    
    Sdim = length(Sblock) ;
    U = createU_TMP(parameter, Ublock, U, H(i), Sdim, 1) ;
    V = createU_TMP(parameter, Vblock, V, H(i), Sdim, 2) ;
    S = createS_TMP(Sblock, S, H(i), i) ;    
end
