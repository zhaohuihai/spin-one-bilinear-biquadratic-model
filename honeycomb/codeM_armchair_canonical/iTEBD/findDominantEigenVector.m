function [VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, ABR, ABL, T)
%* ABR(a,c,i,j) ABL(a',c',k,l) T(i,j,k,l)
%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)

PRdir = 'tensor/PR/' ;
%* PR(a,k,c,l) = sum{i,j}_(ABR(a,c,i,j)*T(i,j,k,l))
%* change notation: PR(a,j,c,i)
PR = contractSparseTensors(ABR, 4, [3, 4], T, 4, [1, 2], parameter, PRdir, [1, 3, 2, 4]) ;

PLdir = 'tensor/PL/' ;
%* PL(j,c',i,a') = sum{k,l}_(ABL(a',c',k,l)*T(i,j,k,l))
%* change notation: PL(l,c',i,a')
PL = contractSparseTensors(ABL, 4, [3, 4], T, 4, [3, 4], parameter, PLdir, [4, 2, 3, 1]) ;

PRmap = rmfield(PR, 'tensor') ;
PLmap = rmfield(PL, 'tensor') ;
%* initialize a normalized vector(c,l,c')
V = initializeVector_y(PRmap, PLmap) ;
V = normalizeVector(V) ;

%* VR(a,j,a')=sum{c,l,c'}_[PR(a,j,c,i)*V(c,l,c')*PL(l,c',i,a')]
[VR, etaR] = findDominantRightEigenVector_y(parameter, PR, PL, V) ;
%* change notation: VR(c,l,c')

%* PR(a,j,c,i) -> PR(j,a,i,c)
PR = permuteSparseTensor(PR, [2, 1, 4, 3], parameter) ;
%* PL(l,c',i,a') -> PL(c',l,a',i)
PL = permuteSparseTensor(PL, [2, 1, 4, 3], parameter) ;

%* transpose: VR(a,j,a') -> VL(a',j,a)
VL = permuteSparseTensor(VR, [3, 2, 1], parameter) ;

%* VL(c',l,c)=sum{a',j,a}_[PL(c',l,a',i)*VL(a',j,a)*PR(j,a,i,c)]
[VL, etaL] = findDominantRightEigenVector_y(parameter, PL, PR, VL) ;
%* change notation: VL(a',j,a)

eta = (etaR + etaL) / 2 ;

