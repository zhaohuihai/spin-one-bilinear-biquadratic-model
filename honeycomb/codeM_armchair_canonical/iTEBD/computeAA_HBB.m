function TH = computeAA_HBB(parameter, dirName, M, AA, HBB)
%* TH(i,j,k,l)=sum_{x,mi,ni}(AA(x,i,k;mi,ni)*HBB(x,l,j;mi,ni)

[status,message,messageid] = mkdir(dirName) ;
TH.dirName = dirName ;
TH.indNo = 4 ;
TH.subNo = 0 ;
TH.quantNo = zeros(4, 0) ;
TH.dim = zeros(4, 0) ;
TH.tensor = cell(0) ;
n = 0 ;
for m1 = 1 : M
    for m2 = 1 : M
        AAno = AA(m1, m2).subNo ;
        if HBB(m1, m2).subNo == 0
            continue
        end
        for i = 1 : AAno
            AquantNo = AA(m1, m2).quantNo(:, i) ;
            j = find(AquantNo(1) == HBB(m1, m2).quantNo(1, :)) ;
            Adim = AA(m1, m2).dim(:, i) ;
            Atensor = AA(m1, m2).tensor3{i} ; %* (x,i,k)

            for k = 1 : length(j)
                BquantNo = HBB(m1, m2).quantNo(:, j(k)) ;
                Bdim = HBB(m1, m2).dim(:, j(k)) ;
                
                Btensor = HBB(m1, m2).tensor{j(k)} ; %* (x,l,j)                
                
                %* tensor(i,j,k,l)
                tensor = contractTensors(Atensor, 3, 1, Btensor, 3, 1, [1, 4, 2, 3]) ;
                tQN = [AquantNo(2); BquantNo(3); AquantNo(3); BquantNo(2)] ;
                
                equalT = cell(1, 3) ;
                for m = 1 : 3
                    equalT{m} = (tQN(m) == TH.quantNo(m, :)) ;
                end
                index = find(equalT{1} & equalT{2} & equalT{3}) ;
                
                if isempty(index)
                    n = n + 1 ;
                    TH.quantNo(:, n) = tQN ;
                    TH.dim(:, n) = [Adim(2); Bdim(3); Adim(3); Bdim(2)] ;
                    TH = saveTensor(TH, tensor, n, parameter) ;
                else
                    THtensor = loadTensor(TH, index) ;
                    tensor = THtensor + tensor ;
                    TH = saveTensor(TH, tensor, index, parameter) ;
                end
            end
        end
    end
end
TH.subNo = n ;