function [R, AB, T] = contractABL_T_y(parameter, dirName, AB, T)
%* R((j,c),(i,a)) = sum{k,l}_(AB((a,c),(k,l))*T((i,j),(k,l)))

[status,message,messageid] = mkdir(dirName) ;
R.dirName = dirName ;
R.subNo = 0 ;
R.quantNo = zeros(4, 0) ;
R.dim = zeros(4, 0) ;
R.tensor2 = cell(0) ;
n = 0 ;
for i = 1 : AB.subNo
    ABqn = AB.quantNo(:, i) ;
    ABdim = AB.dim(:, i) ;
    %* ABtensor((a,c),(k,l))
%     ABtensor = AB.tensor2{i} ;
    ABtensor = loadTensor2(parameter, AB, i) ;
    equali = (ABqn(3) == T.quantNo(3, :)) ;
    equalj = (ABqn(4) == T.quantNo(4, :)) ;
    j = find(equali & equalj) ;
    if ~isempty(j)        
        for k = 1 : length(j)           
            Tqn = T.quantNo(:, j(k)) ;
            Tdim = T.dim(:, j(k)) ;
            %* Ttensor((i,j),(k,l))
%             Ttensor = T.tensor2{j(k)} ;
            Ttensor = loadTensor2(parameter, T, j(k)) ;
            %* tensor((a,c),(i,j))
            tensor = ABtensor * Ttensor' ;
            %* tensor(a,c,i,j)
            tensor = reshape(tensor, [ABdim(1), ABdim(2), Tdim(1), Tdim(2)]) ;
            %* tensor(j,c,i,a)
            tensor = permute(tensor, [4, 2, 3, 1]) ;
            %* tensor((j,c),(i,a))
            tensor = reshape(tensor, [Tdim(2) * ABdim(2), Tdim(1) * ABdim(1)]) ;
            
            rQN = [Tqn(2); ABqn(2); Tqn(1); ABqn(1)] ;
            rDim = [Tdim(2); ABdim(2); Tdim(1); ABdim(1)] ;
            equalR = cell(1, 3) ;
            for m = 1 : 3
                equalR{m} = (rQN(m) == R.quantNo(m, :)) ;
            end
            index = find(equalR{1} & equalR{2} & equalR{3}) ;
            
            if isempty(index)
                n = n + 1 ;
                R.quantNo(:, n) = rQN ;
                R.dim(:, n) = rDim ;
%                 R.tensor2{n} = tensor ;
                R = saveTensor2(parameter, R, tensor, n) ;
            else
                Rtensor = loadTensor2(parameter, R, index) ;
                tensor = Rtensor + tensor ;
%                 R.tensor2{index} = R.tensor2{index} + tensor ;
                R = saveTensor2(parameter, R, tensor, index) ;
            end
        end
    end
end
R.subNo = n ;
