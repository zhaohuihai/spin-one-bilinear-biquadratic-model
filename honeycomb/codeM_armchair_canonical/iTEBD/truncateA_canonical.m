function A = truncateA_canonical(A, QN, cut)

cutNo = length(cut) ;

Aindex = find(A.quantNo(3, :) == QN) ;
Ano = length(Aindex) ;
i = 1 ;
while i <= Ano
    a = Aindex(i) ;
    Adim = A.dim(3, a) - cutNo ;
    if Adim ~= 0
        A.dim(3, a) = Adim ;
        Atensor = loadTensor(A, a) ;
        Atensor(:, :, cut) = [] ;
        A.tensor{a} = Atensor ;
        i = i + 1 ;
    else %* eliminate sub tensor
        A.quantNo(:, a) = [] ;
        A.dim(:, a) = [] ;
        A.tensor(a) = [] ;
        A.subNo = A.subNo - 1 ;
        Ano = Ano - 1 ;
    end
    
end
