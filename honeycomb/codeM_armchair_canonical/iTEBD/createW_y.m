function Wa = createW_y(parameter, AA, U)
%* Wa(x,in,k) = sum{m1,m2}_(AA(x,i,k;m1,m2)*U((m1,m2),n))

%* AAU(x,(i,n),k)
AAU = contractAA_U_y(parameter, AA, U) ;

quantNo = AAU.quantNo(2, :) ;
dim = AAU.dim(2, :) ;
allTwoQN = AAU.quantNo(2, :) - AAU.quantNo(3, :) ;

mapping(1) = createTotMapping(AAU.quantNo(1, :), AAU.quantNo(1, :), AAU.dim(1, :)) ;
mapping(2) = createTotMapping(allTwoQN, quantNo, dim) ;
mapping(3) = createTotMapping(AAU.quantNo(4, :), AAU.quantNo(4, :), AAU.dim(3, :)) ;

Wa = blockAAUby2bond_y(AAU, mapping) ;
