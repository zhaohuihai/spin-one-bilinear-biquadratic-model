function [QR, QL] = createSpecialTensor_z(parameter, ABR, ABL, AA, BB, Ta, Tb)
%* Ta(x,i,k) Tb(x,l,j)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')
%* QR(c,l,a,k,n)    change notation: QR(c,l,a,i,n)
%* QL(i,a',j,c',n)  change notation: QL(j,a',i,c',n)

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, AA, BB) ;
%==============================================================

WaDir = 'tensor/Wa/' ;
%* Wa(i,k,n,l,j) = sum{x}_[TCa(x,i,k,n)*Tb(x,l,j)]
Wa = contractSparseTensors(TCa, 4, 1, Tb, 3, 1, parameter, WaDir) ;

WbDir = 'tensor/Wb/' ;
%* Wb(i,k,l,j,n) = sum{x}_[Ta(x,i,k)*TCb(x,l,j,n)]
Wb = contractSparseTensors(Ta, 3, 1, TCb, 4, 1, parameter, WbDir) ;

QRdir = 'tensor/QR/' ;
%* QR(c,l,a,k,n) = sum{i,j}_[Wa(i,k,n,l,j)*ABR(a,c,i,j)]
QR = contractSparseTensors(Wa, 5, [1, 5], ABR, 4, [3, 4], parameter, QRdir, [5, 3, 4, 1, 2]) ;
%* change notation: QR(c,l,a,i,n)

QLdir = 'tensor/QL/' ;
%* QL(i,a',j,c',n) = sum{k,l}_[Wb(i,k,l,j,n)*ABL(a',c',k,l)]
QL = contractSparseTensors(Wb, 5, [2, 3], ABL, 4, [3, 4], parameter, QLdir, [1, 4, 2, 5, 3]) ;
%* change notation: QL(j,a',i,c',n)

