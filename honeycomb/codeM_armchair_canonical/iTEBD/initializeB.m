function B = initializeB(mapping, iQN, iDim)
%* B(b,j,c): - b + c = j
%* T(i,j,k,l) field: subNo, quantNo, dim

Tmap = findTmapping(iQN, iDim) ;
B.indNo = 3 ;
Bdim = zeros(3, 1) ;
Bqn = zeros(3, 1) ;
n = 0 ;
for a = 1 : mapping.subNo
    Bdim(1) = mapping.dim(a) ;
    Bqn(1) = mapping.quantNo(a) ;
    for i = 1 : Tmap.subNo
        Bdim(2) = Tmap.dim(i) ;
        Bqn(2) = Tmap.quantNo(i) ;
        for b = 1 : mapping.subNo
            Bdim(3) = mapping.dim(b) ;
            Bqn(3) = mapping.quantNo(b) ;
            %* b + j - c = 0
            if (Bqn(1)+Bqn(2)-Bqn(3) == 0)
                n = n + 1 ;
                B.quantNo(:, n) = Bqn ;
                B.dim(:, n) = Bdim ;
                B.tensor{n} = rand(Bdim') ;
            end
        end
    end
end
B.subNo = n ;
