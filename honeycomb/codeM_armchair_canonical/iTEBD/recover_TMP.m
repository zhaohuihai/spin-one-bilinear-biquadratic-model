function MPS = recover_TMP(MPS, U, S, V)
%* A(a,k,b) = (1/Lambda(2)(a))*U((a,k),b)
%* B(b,l,c) = (1/Lambda(2)(c))*V((l,c),b)

Lambda(1) = S ;
Lambda(2) = MPS.Lambda(2) ;


invL = computeInvLambda(Lambda(2)) ;
%* A(a,k,b) = U((a,k),b)*invL(a)
A = recoverA_TMP(U, invL) ;
%* B(b,l,c) = V((l,c),b)*invL(c)
B = recoverB_TMP(V, invL) ;

MPS.A = A ;
MPS.B = B ;
MPS.Lambda = Lambda ;