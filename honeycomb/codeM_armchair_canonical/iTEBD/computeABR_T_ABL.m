function [P, T, ABR, ABL] = computeABR_T_ABL(parameter, Pdir, ABR, ABL, T)

%* P(a,a',c,c') = sum{i,j,k,l}_[ABR(a,c,i,j)*T(i,j,k,l)*ABL(a',c',k,l)]

RTdir = 'tensor/RT/' ;
%* RT(a,c,k,l) = ABR(a,c,i,j)*T(i,j,k,l)
RT = contractSparseTensors(ABR, 4, [3, 4], T, 4, [1, 2], parameter, RTdir, [1, 2, 3, 4]) ;

%* P(a,a',c,c') = RT(a,c,k,l)*ABL(a',c',k,l)
P = contractSparseTensors(RT, 4, [3, 4], ABL, 4, [3, 4], parameter, Pdir, [1, 3, 2, 4]) ;
clear RT
