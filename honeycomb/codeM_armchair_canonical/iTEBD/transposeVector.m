function vector = transposeVector(vector)
%* transpose: vector(a',j,a) -> vector(a,j,a')

% %* vectorLeft(a',j,a) col
% vectorLeft = reshape(vectorLeft, [D, d, D]) ;
% %* vectorLeft(a,j,a')
% vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
% %* vectorLeft(a,j,a') arr
% vectorLeft = reshape(vectorLeft, [1, D*d*D]) ;

for i = 1 : vector.subNo
    vDim = vector.dim(:, i) ;
    %* vTensor((a',j,a),1)
    vTensor = vector.tensor1{i} ;
    %* vTensor(a',j,a)
    vTensor = reshape(vTensor, vDim') ;
    %* vTensor(a,j,a')
    vTensor = permute(vTensor, [3, 2, 1]) ;
    %* vTensor((a,j,a'), 1)
    vTensor = reshape(vTensor, [vDim(1)*vDim(2)*vDim(3), 1]) ;
    
    vector.tensor1{i} = vTensor ;
end
vector.quantNo = vector.quantNo([3, 2, 1], :) ;
vector.dim = vector.dim([3, 2, 1], :) ;
