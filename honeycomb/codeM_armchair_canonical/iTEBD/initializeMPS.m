function [MPS] = initializeMPS(parameter, T)
%* T(i,j,k,l) field: subNo, quantNo, dim
mapping = initializeMPSmapping(parameter) ;
% RandStream.setDefaultStream(RandStream('mt19937ar', 'Seed', 5489))
%* A(a,i,b): a - b = i
MPS.A = initializeA(mapping, T.quantNo(1, :), T.dim(1, :)) ;
%* B(b,j,c): - b + c = j
MPS.B = initializeB(mapping, T.quantNo(2, :), T.dim(2, :)) ;
