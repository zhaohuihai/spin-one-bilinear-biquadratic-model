function [expectValue_z, T, ABR, ABL] = dealWithSpecialTensor_z(parameter, T, ABR, ABL, AA, BB, Ta, Tb)

%* VR(c',l,c), VL(a,j,a')
[VR, VL, eta] = findDominantEigenVector_z(parameter, ABR, ABL, T) ;

%* QR(c,l,a,k,n)    change notation: QR(c,l,a,i,n)
%* QL(i,a',j,c',n)  change notation: QL(j,a',i,c',n)
[QR, QL] = createSpecialTensor_z(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValue_z, QL, QR] = findExpectationValue_y(VR, VL, eta, QL, QR) ;
clear QL QR
