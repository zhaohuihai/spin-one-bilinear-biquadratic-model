function AB = contractA_B_TMP(parameter, dirName, A, B)
%* AB((a,c),(i,j)) = sum{b}_(A(a,i,b)*B(b,j,c))

[status,message,messageid] = mkdir(dirName) ;
AB.dirName = dirName ;
Ano = A.subNo ;
n = 0 ;
for i = 1 : Ano
    AquantNo = A.quantNo(:, i) ;
    j = find(AquantNo(3) == B.quantNo(1, :)) ;
    if ~isempty(j)
        Adim = A.dim(:, i) ;
        Atensor = A.tensor3{i} ; %* (a,i,b)
        Atensor = reshape(Atensor, [Adim(1) * Adim(2), Adim(3)]) ; %* ((a,i),b)
        
        for k = 1 : length(j)
            n = n + 1 ;
            BquantNo = B.quantNo(:, j(k)) ;
            Bdim = B.dim(:, j(k)) ;
            
            Btensor = B.tensor3{j(k)} ; %* (b,j,c)

            Btensor = reshape(Btensor, [Bdim(1), Bdim(2) * Bdim(3)]) ; %* (b,(j,c))
            %* tensor((a,i),(j,c))
            tensor = Atensor * Btensor ;
            %* tensor(a,i,j,c)
            tensor = reshape(tensor, [Adim(1), Adim(2), Bdim(2), Bdim(3)]) ;
            %* tensor(a,c,i,j)           
            tensor = permute(tensor, [1, 4, 2, 3]) ;
            %* tensor((a,c),(i,j))
            tensor = reshape(tensor, [Adim(1) * Bdim(3), Adim(2) * Bdim(2)]) ;
            
            AB.quantNo(:, n) = [AquantNo(1); BquantNo(3); AquantNo(2); BquantNo(2)] ;
            AB.dim(:, n) = [Adim(1); Bdim(3); Adim(2); Bdim(2)] ;
%             AB.tensor2{n} = tensor ;
            AB = saveTensor2(parameter, AB, tensor, n) ;
        end
    end
end
AB.subNo = n ;
