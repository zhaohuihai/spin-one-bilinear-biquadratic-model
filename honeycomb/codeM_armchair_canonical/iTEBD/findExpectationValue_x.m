function [expectValue, Q] = findExpectationValue_x(vectorRight, vectorLeft, etaRight, etaLeft, Q)

eta = (etaRight + etaLeft) / 2 ;

%* QvR(a1,a2) = sum{c1,c2}_(Q(a1,a2,c1,c2)*vectorRight(c1,c2))
QvR = contractSparseTensors(Q, 4, [3, 4], vectorRight, 2, [1, 2]) ;

%* h = sum{a1,a2}_(vectorLeft(a1,a2)*QvR(a1,a2))
h = contract2vector(vectorLeft, [1, 2], QvR, [1, 2]) ;

%* g = sum{a1,a2}_(vectorLeft(a1,a2)*vectorRight(a1,a2))
g = contract2vector(vectorLeft, [1, 2], vectorRight, [1, 2]) ;
g = eta .* g ;

expectValue = h / g ;