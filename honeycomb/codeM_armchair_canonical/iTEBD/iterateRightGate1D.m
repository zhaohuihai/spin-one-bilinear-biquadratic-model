function [MPS, T, coef, truncationError, TMPstep] = iterateRightGate1D(parameter, T, MPS, TMPstep)

TMconvergence = 1 ;
D = parameter.dim_MPS ;
coef0 = 1 ;
c = zeros(1, 2) ;
truncErr = zeros(1, 2) ;
j = 0 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(D), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, T, ~, c(1), truncErr(1)] = applyTMP(parameter, T, MPS) ;
    [MPS, T, ~, c(2), truncErr(2)] = applyTMP(parameter, T, MPS) ;
    toc
    coef = c(1) * c(2) ;
    truncationError = max(truncErr) ;
    TMconvergence = abs(coef0 - coef) / abs(coef) ;
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMconvergence)])
        disp(['truncation error = ', num2str(truncationError)])
    end
    coef0 = coef ;
end