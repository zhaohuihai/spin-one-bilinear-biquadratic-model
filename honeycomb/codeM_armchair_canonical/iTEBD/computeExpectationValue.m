function computeExpectationValue(parameter, wave)

%* Ta(x,i,k) Tb(x,l,j)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')
[Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave) ;

Tdir = 'tensor/T/' ;
%* T(i,j,k,l) right transfer matrix
%* T(i,j,k,l) = sum{x}_(Ta(x,i,k)*Tb(x,l,j))
%* conservation law: i - j = -k + l
T = contractSparseTensors(Ta, 3, 1, Tb, 3, 1, parameter, Tdir, [1, 4, 2, 3]) ;

[rightMPS, leftMPS] = computeDominantEigenMPS(parameter, T) ;
%* rightMPS A(a,i,b) B(b,j,c), conservation law: a - b = i, - b + c = j
%* leftMPS  A(a,k,b) B(b,l,c), conservation law: a - b = k, - b + c = l
%*============================================================================
computation = parameter.computation ;
%-----------------------------------------------------------------------------
% testDominantEigen(parameter, rightMPS)
%-----------------------------------------------------------------------------
if computation.stagSz == 1
    computeStagMag_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.Qzz == 1
    computeQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.stagQzz == 1
    computeStagQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.energy == 1
    computeEnergy(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

[status, message, messageid] = rmdir('tensor', 's') ;