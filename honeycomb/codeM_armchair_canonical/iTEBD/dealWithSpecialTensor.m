function expectValueBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

expectValueBond = zeros(1, 3) ;

[expectValueBond(1), T] = dealWithSpecialTensor_x(parameter, T, rightMPS, leftMPS, AA, BB) ;

%*=============================================================================
ABRdir = 'tensor/ABR/' ;
%* ABR(a,c,i,j) = sum{b}_[A(a,i,b)*B(b,j,c)]
%* conservation law: a - c = i - j
ABR = contractSparseTensors(rightMPS.A, 3, 3, rightMPS.B, 3, 1, parameter, ABRdir, [1, 4, 2, 3]) ;
ABLdir = 'tensor/ABL/' ;
%* ABL(a',c',k,l) = sum{b'}_[A(a',k,b')*B(b',l,c')]
%* conservation law: a' - c' = k - l
ABL = contractSparseTensors(leftMPS.A, 3, 3, leftMPS.B, 3, 1, parameter, ABLdir, [1, 4, 2, 3]) ;

[expectValueBond(2), T, ABR, ABL] = dealWithSpecialTensor_y(parameter, T, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValueBond(3), T, ABR, ABL] = dealWithSpecialTensor_z(parameter, T, ABR, ABL, AA, BB, Ta, Tb) ;

% [status, message, messageid] = rmdir('tensor', 's') ;