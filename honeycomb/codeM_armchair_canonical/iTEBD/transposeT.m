function T = transposeT(T)
%* T((i,j),(k,l)) --> T((k,l),(i,j))
%* unchanged field: subNo, dirName

T.quantNo = T.quantNo([3, 4, 1, 2], :) ;
T.dim = T.dim([3, 4, 1, 2], :) ;
for i = 1 : T.subNo
    if ischar(T.tensor2{i})
        fileName = T.tensor2{i} ;
        load(fileName, 'tensor') ;
        tensor = tensor' ;
        save(fileName, 'tensor', '-v7.3') ;
    else
        T.tensor2{i} = T.tensor2{i}' ;
    end
end
