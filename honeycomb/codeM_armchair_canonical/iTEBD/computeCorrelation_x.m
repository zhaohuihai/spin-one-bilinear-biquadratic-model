function [SzCorrelation, SzCorrelationM, QzzCorrelation, QzzCorrelationM] = computeCorrelation_x(parameter, wave)
%* correlation is maxCorrelationLength X 1 column vector

%* Ta(x,i,k) Tb(x,l,j)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')
[Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave) ;

Tdir = 'tensor/T/' ;
%* T(i,j,k,l) right transfer matrix
%* T(i,j,k,l) = sum{x}_(Ta(x,i,k)*Tb(x,l,j))
%* conservation law: i - j = -k + l
T = contractSparseTensors(Ta, 3, 1, Tb, 3, 1, parameter, Tdir, [1, 4, 2, 3]) ;

[rightMPS, leftMPS] = computeDominantEigenMPS(parameter, T) ;
%* rightMPS A(a,i,b) B(b,j,c), conservation law: a - b = i, - b + c = j
%* leftMPS  A(a,k,b) B(b,l,c), conservation law: a - b = k, - b + c = l
%*=============================================================================
ABRdir = 'tensor/ABR/' ;
%* ABR(a,c,i,j) = sum{b}_[A(a,i,b)*B(b,j,c)]
%* conservation law: a - c = i - j
ABR = contractSparseTensors(rightMPS.A, 3, 3, rightMPS.B, 3, 1, parameter, ABRdir, [1, 4, 2, 3]) ;
ABLdir = 'tensor/ABL/' ;
%* ABL(a',c',k,l) = sum{b'}_[A(a',k,b')*B(b',l,c')]
%* conservation law: a' - c' = k - l
ABL = contractSparseTensors(leftMPS.A, 3, 3, leftMPS.B, 3, 1, parameter, ABLdir, [1, 4, 2, 3]) ;

%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)
[VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, ABR, ABL, T) ;
%*=============================================================================

if parameter.computation.SzCorrelation == 1
    parameter.AsiteOperator = createSzI(parameter) ;
    parameter.BsiteOperator = createISz(parameter) ;
    parameter.correlationOperator = createSzSz(parameter) ;
    [SzCorrelation, SzCorrelationM] = findCorrelationFunction(parameter, ABR, ABL, AA, BB, Ta, Tb, VR, VL, PR, PL, eta) ;
else
    SzCorrelation = [] ;
    SzCorrelationM = [] ;
end
if parameter.computation.QzzCorrelation == 1
    parameter.AsiteOperator = createQzzI(parameter) ;
    parameter.BsiteOperator = createIQzz(parameter) ;
    parameter.correlationOperator = createQzzQzz(parameter) ;
    [QzzCorrelation, QzzCorrelationM] = findCorrelationFunction(parameter, ABR, ABL, AA, BB, Ta, Tb, VR, VL, PR, PL, eta) ;
else
    QzzCorrelation = [] ;
    QzzCorrelationM = [] ;
end

