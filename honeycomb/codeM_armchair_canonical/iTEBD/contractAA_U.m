function TCa = contractAA_U(parameter, AA, U)
%* TCa(x,i,k,n) = sum{mi,mi'}_[AA(x,i,k;mi,mi')*U(mi,mi',n)]

M = parameter.siteDimension ;

n = 0 ;
TCa.indNo = 4 ;
TCa.subNo = 0 ;
TCa.quantNo = zeros(4, 0) ;
TCa.dim = zeros(4, 0) ;
TCa.tensor = cell(0) ;
for m1 = 1 : M
    site1 = M + 1 - 2 * m1 ;
    for m2 = 1 : M
        site2 = M + 1 - 2 * m2 ;
        for i = 1 : AA(m1, m2).subNo
            aQN = AA(m1, m2).quantNo(:, i) ;
            aDim = AA(m1, m2).dim(:, i) ;
            %* aTensor(x,i,k)
            aTensor = AA(m1, m2).tensor3{i} ;
            %* aTensor((x,i,k), 1)
            aTensor = reshape(aTensor, [prod(aDim), 1]) ;
            
            equalm = cell(1, 2) ;
            equalm{1} = (site1 == U.quantNo(1, :)) ;
            equalm{2} = (site2 == U.quantNo(2, :)) ;
            j = find(equalm{1} & equalm{2}) ;
            if ~isempty(j)
                for k = 1 : length(j)
                    uQN = U.quantNo(:, j(k)) ;
                    uDim = U.dim(:, j(k)) ;
                    %* uTensor(m1,m2,n)
                    uTensor = U.tensor{j(k)} ;
                    %* uTensor((m1,m2),n)
                    uTensor = reshape(uTensor, [uDim(1) * uDim(2), uDim(3)]) ;
                    %* tensor((x,i,k), n)
                    tensor = aTensor * uTensor ;
                    %* tensor(x,i,k,n)
                    tensor = reshape(tensor, [aDim; uDim(3)]') ;
                    
                    QN = [aQN; uQN(3)] ;
                    dim = [aDim; uDim(3)] ;
                    equal = cell(1, 4) ;
                    for m = 1 : 4
                        equal{m} = (QN(m) == TCa.quantNo(m, :)) ;
                    end
                    index = find(equal{1} & equal{2} & equal{3} & equal{4}) ;
                    if isempty(index)
                        n = n + 1 ;
                        TCa.quantNo(:, n) = QN ;
                        TCa.dim(:, n) = dim ;
                        TCa.tensor{n} = tensor ;
                    else
                        TCa.tensor{index} = TCa.tensor{index} + tensor ;
                    end
                end
            end
        end
    end
end
TCa.subNo = n ;