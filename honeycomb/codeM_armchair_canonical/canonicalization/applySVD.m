function [U, S, V] = applySVD(parameter, AB, varargin)
%* [U, S, V] = applySVD(parameter, AB, Udir, Vdir)
%* optional input arguments: Udir, Vdir
%* possible nargin:
%* 2: parameter, AB
%* 4: parameter, AB, Udir, Vdir
%* AB(a,i,j,c) = sum{b}_[U(a,i,b)*S(b)*V(j,c,b)]
if nargin == 4
    Udir = varargin{1} ;
    Vdir = varargin{2} ;
    [status,message,messageid] = mkdir(Udir) ;
    [status,message,messageid] = mkdir(Vdir) ;
    U.dirName = Udir ;
    V.dirName = Vdir ;
end

U.indNo = 3 ;
V.indNo = 3 ;
ABno = length(AB) ;
S.subNo = ABno ;
S.indNo = 1 ;

U.subNo = 0 ;
V.subNo = 0 ;
for i = 1 : ABno
    blockQN = AB(i).blockQuantNo ;
    [ABblock, ABdim, ABmap] = combineTensor(AB(i)) ;
    [Ublock, Sblock, Vblock] = svd(ABblock, 'econ') ;
    Sblock = diag(Sblock) ;
    
    [~, Udim] = size(Ublock) ;
    %* U(a,i,b)
    U = createU_TMP(parameter, Ublock, U, blockQN, Udim, ABmap(1), AB(i).quantNo(1 : 2, :), AB(i).dim(1 : 2, :)) ;
    [~, Vdim] = size(Vblock) ;
    %* V(j,c,b)
    V = createU_TMP(parameter, Vblock, V, blockQN, Vdim, ABmap(2), AB(i).quantNo(3 : 4, :), AB(i).dim(3 : 4, :)) ;
    S = createS_TMP(Sblock, S, blockQN, i) ;
end
