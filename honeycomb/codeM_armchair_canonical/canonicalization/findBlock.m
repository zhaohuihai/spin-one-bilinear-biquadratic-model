function TA = findBlock(T, blockQuantNo)

for i = 1 : length(T)
    if blockQuantNo == T(i).blockQuantNo
        TA = T(i) ;
        return ;
    end
end