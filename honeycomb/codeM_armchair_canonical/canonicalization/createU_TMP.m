function U = createU_TMP(parameter, Ublock, U, blockQN, Udim, map, quantNo, dim)
%* U(a,i,b)

n = size(quantNo, 2) ;
i = 1 ;
while i <= n
    QN = quantNo(:, i) ;
    ind = findMatchQN(QN, quantNo) ;
    ind(1) = [] ;
    quantNo(:, ind) = [] ;
    dim(:, ind) = [] ;
    i = i + 1 ;
    n = n - length(ind) ;
end

for i = 1 : n
    index = find(map.quantNo == quantNo(1, i)) ;
    location = map.location(index) ;
    position = location : (location + map.dim(index) - 1) ;
    
    U.subNo = U.subNo + 1 ;
    U.quantNo(:, U.subNo) = [quantNo(:, i); blockQN] ;
    U.dim(:, U.subNo) = [dim(:, i); Udim] ;
    Utensor = Ublock(position, :) ;
    Utensor = reshape(Utensor, U.dim(:, U.subNo)') ;
    U = saveTensor(U, Utensor, U.subNo, parameter) ;
end


