function [map, totDim] = findMap(quantNo, dim)

s = length(quantNo) ;
i = 0 ;
d = 0 ;
while s > 0
    maxQN = max(quantNo) ;
    sameQN = find(maxQN == quantNo) ; %* index
    
    sameQNno = length(sameQN) ;
    subDim = dim(sameQN(1)) ;
    
    i = i + 1 ;
    map.quantNo(i) = maxQN ;
    map.location(i) = d + 1 ;
    map.dim(i) = subDim ;
    d = d + subDim ;
    
    quantNo(sameQN) = quantNo(sameQN) - 1e9 ;
    s = s - sameQNno ;
end
map.subNo = i ;
totDim = d ;