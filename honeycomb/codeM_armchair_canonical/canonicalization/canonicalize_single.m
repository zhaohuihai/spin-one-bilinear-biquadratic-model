function [A, L, coef] = canonicalize_single(parameter, A)
%* A(a,i,j,c)

c = zeros(1, 2) ;
[A, c(1)] = canoCondition_1(parameter, A) ;
[A, c(2)] = canoCondition_1(parameter, A) ;
coef = c(1) * c(2) ;
[A, L] = canoCondition_2(parameter, A) ;

%*****************************************************************************
%* verification of canonical condition
% 
% %* aa(a,a') = sum{i,j,c}_[A(a,i,j,c)*A(a',i,j,c)]
% aa = contractSparseTensors(A, 4, [2, 3, 4], A, 4, [2, 3, 4]) ;
% e1 = 0 ;
% for i = 1 : aa.subNo
%     x = norm(aa.tensor{i} - eye(aa.dim(:, i)')) ;
%     if x > e1
%        e1 = x ; 
%     end
% end
% e1
% 
% %* A2(a,i,j,c) = A(a,i,j,c) * L(a)
% A2 = tensorTimesVector(A, 4, 1, L, 1, 1, [4, 1, 2, 3]) ;
% %* cc(c,c') = sum{a,i,j}_[A2(a,i,j,c)*A2(a,i,j,c')]
% cc = contractSparseTensors(A2, 4, [1, 2, 3], A2, 4, [1, 2, 3]) ;
% e2 = 0 ;
% for i = 1 : cc.subNo
%     j = find(L.quantNo == cc.quantNo(1, i)) ;
%     Lam = diag(L.tensor{j} .* L.tensor{j}) ;
%     x = norm(cc.tensor{i} - Lam) ;
%     if x > e2
%         e2 = x ;
%     end
% end
% e2