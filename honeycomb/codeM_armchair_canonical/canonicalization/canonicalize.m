function [MPS, LA, coef] = canonicalize(parameter, AB)
%* AB(a,i,j,c)
%* conservation law: a + i = j + c

[AB, LB, coef] = canonicalize_single(parameter, AB) ;

%* AB(a,i,j,c) = LB(a)*AB(a,i,j,c)
%* conservation law: a + i = j + c
AB = tensorTimesVector(AB, 4, 1, LB, 1, 1, '*', parameter, [4, 1, 2, 3]) ;

AB = partitionTensor(AB, 4, [1, 2], '++') ;

Udir = 'tensor/U/' ;
Vdir = 'tensor/V/' ;
%* U(a,i,b), V(j,c,b), LA(b)
[U, LA, V] = applySVD(parameter, AB, Udir, Vdir) ;

%*****************************************************************************
% ss = 0 ;
% for i = 1 : LA.subNo
%     ss = ss + sum(LA.tensor{i}.^2) ;
% end
% ssb = 0 ;
% for i = 1 : LB.subNo
%     ssb = ssb + sum(LB.tensor{i}.^2) ;
% end
% xx = contractSparseTensors(U, 3, [1, 2], U, 3, [1, 2]) ;
% yy = contractSparseTensors(V, 3, [1, 2], V, 3, [1, 2]) ;
%*****************************************************************************

%* A(a,i,b) = U(a,i,b) / LB(a)
A = tensorTimesVector(U, 3, 1, LB, 1, 1, '/', parameter, [3, 1, 2]) ;
%* A(a,i,b) = A(a,i,b) * LA(b)
A = tensorTimesVector(A, 3, 3, LA, 1, 1, '*', parameter) ;

%* (j,c,b) => (b,j,c)
B = permuteSparseTensor(V, [3, 1, 2], parameter) ;
MPS.A = A ;
MPS.B = B ;
%******************************************************************************
%* verification of canonical condition

%* aa(a,a') = sum{i,b}_[A(a,i,b)*A(a',i,b)]
aa = contractSparseTensors(A, 3, [2, 3], A, 3, [2, 3]) ;
e1 = 0 ;
for i = 1 : aa.subNo
    x = norm(aa.tensor{i} - eye(aa.dim(:, i)')) ;
    if x > e1
       e1 = x ; 
    end
end

%* A2(a,i,b) = A(a,i,b) * LB(a)
A2 = tensorTimesVector(A, 3, 1, LB, 1, 1, [3, 1, 2]) ;
%* cc(b,b') = sum{a,i}_[A2(a,i,b)*A2(a,i,b')]
cc = contractSparseTensors(A2, 3, [1, 2], A2, 3, [1, 2]) ;
e2 = 0 ;
for i = 1 : cc.subNo
    j = find(LA.quantNo == cc.quantNo(1, i)) ;
    Lam = diag(LA.tensor{j} .* LA.tensor{j}) ;
    x = norm(cc.tensor{i} - Lam) ;
    if x > e2
        e2 = x ;
    end
end

%* aa(b,b') = sum{j,c}_[B(b,j,c)*B(b',j,c)]
aa = contractSparseTensors(B, 3, [2, 3], B, 3, [2, 3]) ;
e3 = 0 ;
for i = 1 : aa.subNo
    x = norm(aa.tensor{i} - eye(aa.dim(:, i)')) ;
    if x > e3
       e3 = x ; 
    end
end

%* B2(b,j,c) = B(b,j,c) * LA(b)
B2 = tensorTimesVector(B, 3, 1, LA, 1, 1, [3, 1, 2]) ;
%* cc(c,c') = sum{b,j}_[B2(b,j,c)*B2(b,j,c')]
cc = contractSparseTensors(B2, 3, [1, 2], B2, 3, [1, 2]) ;
e4 = 0 ;
for i = 1 : cc.subNo
    j = find(LB.quantNo == cc.quantNo(1, i)) ;
    Lam = diag(LB.tensor{j} .* LB.tensor{j}) ;
    x = norm(cc.tensor{i} - Lam) ;
    if x > e4
        e4 = x ;
    end
end

canoError = max([e1, e2, e3, e4]) ;
if canoError > 1e-10
    disp(['canonical condition error: ', num2str(canoError)])
end