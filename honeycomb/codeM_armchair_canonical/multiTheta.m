
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

parameter.theta = parameter.thetaInitial ;
i = 1 ;
while parameter.theta <= parameter.thetaFinal
    
    if parameter.loadPreviousWave == 0
        wave = initializeWave(parameter) ;
        disp('create new wave')
    else
        [wave, parameter] = loadWave(parameter) ;
    end
    
    parameter.polarization = 1 ;
    if parameter.projection == 1
        if parameter.TrotterOrder == 1
            disp('apply first order Trotter decomposition')
        elseif parameter.TrotterOrder == 2
            disp('apply second order Trotter decomposition')
        end
        wave = findGroundStateWave(parameter, wave) ;
    end
    
    parameter.polarization = 0 ;
    % parameter.theta = (0.0) ;
    
    fileName = [num2str(i), '.txt'] ;
    delete(fileName) ;
    diary(fileName) ;
    diary on
    
    computeExpectationValue(parameter, wave)
    
    
    diary off ;
    %* spin-spin correlation <S(i)*S(i+l)>
    % if parameter.computation.spinCorrelation == 1
    %     spinCorrelation = computeSpinCorrelation(parameter, wave) ;
    % end
    
    
    parameter.theta = parameter.theta + parameter.thetaIncre ;
    i = i + 1 ;
end
