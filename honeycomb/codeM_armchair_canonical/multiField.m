
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

parameter.field = parameter.fieldInitial ;
i = 1 ;
while parameter.field <= parameter.fieldFinal
    
    if parameter.loadPreviousWave == 0
        wave = initializeWave(parameter) ;
        disp('create new wave')
    else
        [wave, parameter] = loadWave(parameter) ;
    end
    
    if parameter.projection == 1
        wave = findGroundStateWave(parameter, wave) ;
    end
    
    
    fileName = [num2str(i), '.txt'] ;
    delete(fileName) ;
    diary(fileName) ;
    diary on
    
    computeExpectationValue(parameter, wave)
    
    
    diary off ;
    
    
    parameter.field = parameter.field + parameter.fieldIncre ;
    i = i + 1 ;
end
