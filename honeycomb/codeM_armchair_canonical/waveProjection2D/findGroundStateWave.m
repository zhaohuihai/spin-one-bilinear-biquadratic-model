function wave = findGroundStateWave(parameter, wave)

if parameter.loadPreviousWave == 1 && parameter.polarization == 1
    warning('Old wave is loaded, unnecessary to polarize')
    disp('The projection process will NOT polarize the wave function')
    parameter.polarization = 0 ;
end

if parameter.TrotterOrder == 1
    disp('apply first order Trotter decomposition')
elseif parameter.TrotterOrder == 2
    disp('apply second order Trotter decomposition')
end

if parameter.polarization == 1
    wave = polarizeWave(parameter, wave) ;
end
parameter.polarization = 0 ;
wave = applyWaveProjection(parameter, wave) ;


