clc
clear
format long
rs = RandStream('mt19937ar', 'Seed', 5489) ;
d = 4 ;
D = 30 ;
dim = [D, d, D] ;

for i = 1 : 2
    MPS.A(i).tensor3 = rand(dim) ;
    MPS.A(i).dim = dim ;
%     MPS.Lambda(i).tensor1 = rand(D, 1) ;
    MPS.Lambda(i).tensor1 = ones(D, 1) ;
    MPS.Lambda(i).dim = D ;
end
MPS = canonicalize_double(MPS) ;
%=================================================

% A = rand(rs, dim) ;
% Lambda = rand(rs, [D, 1]) ;
% MPS.A.tensor3 = A ;
% MPS.A.dim = dim ;
% MPS.Lambda.tensor1 = Lambda ;
% MPS.Lambda.dim = D ;
% 
% MPS = canonicalize_single(MPS) ;
