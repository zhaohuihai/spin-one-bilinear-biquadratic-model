function MPS = canonicalize_double(MPS)

%* A(a,i,b)
A = MPS.A(1).tensor3 ;
Adim = MPS.A(1).dim ;

%* LA(b)
LA = MPS.Lambda(1).tensor1 ;
LAdim = MPS.Lambda(1).dim ;
%* A(a,i,b) = A(a,i,b)*LA(b)
for b = 1 : LAdim
    A(:, :, b) = A(:, :, b) * LA(b) ;
end

%* B(b,j,c)
B = MPS.A(2).tensor3 ;
Bdim = MPS.A(2).dim ;

%* A((a,i),b)
A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
%* A(b,(j,c))
B = reshape(B, [Bdim(1), Bdim(2) * Bdim(3)]) ;

%* AB((a,i),(j,c))
AB = A * B ;
%* AB(a,(i,j),c)
ABdim = [Adim(1), Adim(2) * Bdim(2), Bdim(3)] ;
AB = reshape(AB, ABdim) ;

MPS_single.A.tensor3 = AB ;
MPS_single.A.dim = ABdim ;
MPS_single.Lambda = MPS.Lambda(2) ;

MPS_single = canonicalize_single(MPS_single) ;

%* LB(c)
LB = MPS_single.Lambda.tensor1 ;
LBdim = MPS_single.Lambda.dim ;

%* AB(a,(i,j),c)
AB = MPS_single.A.tensor3 ;
ABdim = MPS_single.A.dim ;

%* AB(a,(i,j),c) = LB(a)*AB(a,(i,j),c)
for a = 1 : LBdim
    AB(a, :, :) = LB(a) * AB(a, :, :) ;
end
%--------------------------------------------
% %* AB(a,(i,j),c) = AB(a,(i,j),c) * LB(c)
% for c = 1 : LBdim
%     AB(:, :, c) = AB(:, :, c) * LB(c) ;
% end
%--------------------------------------------
%* AB((a,i),(j,c))
AB = reshape(AB, [Adim(1) * Adim(2), Bdim(2) * Bdim(3)]) ;
%* U((a,i),b) LA(b,b) V((j,c),b)
[U, LA, V] = svd(AB, 'econ') ;
%* LA(b)
LA = diag(LA) ;
LAdim = length(LA) ;
Adim(3) = LAdim ;
Bdim(1) = LAdim ;
% U = U(:, 1 : LAdim) ;
% LA = LA(1 : LAdim) ;
% V = V(:, 1 : LAdim) ;

%* U(a,i,b)
U = reshape(U, [Adim(1 : 2), LAdim]) ;
A = zeros(Adim) ;
%* A(a,i,b) = U(a,i,b) / LB(a) ;
for a = 1 : LBdim
    A(a, :, :) = U(a, :, :) ./ LB(a) ;
end
%* V(b,(j,c))
V = V' ;
%* V(b,j,c)
V = reshape(V, [LAdim, Bdim(2 : 3)]) ;
% B = zeros(Bdim) ;
% %* B(b,j,c) = V(b,j,c) / LB(c)
% for c = 1 : LBdim
%     B(:, :, c) = V(:, :, c) ./ LB(c) ;
% end
B = V ;

MPS.A(1).tensor3 = A ;
MPS.A(1).dim = Adim ;
MPS.A(2).tensor3 = B ;
MPS.A(2).dim = Bdim ;
MPS.Lambda(1).tensor1 = LA ;
MPS.Lambda(1).dim = LAdim ;
MPS.Lambda(2).tensor1 = LB ;

%==========================================================
%* verification
A1 = zeros(Adim) ;
id = eye(Adim(1)) ;
%* A1(a,i,b) = A(a,i,b) * LA(b)
for b = 1 : LAdim
    A1(:, :, b) = A(:, :, b) .* LA(b) ;
end
%* A1(a,(i,b))
A1 = reshape(A1, [Adim(1), Adim(2)*Adim(3)]) ;
aa = A1 * A1' ;
norm(aa - id * aa(1,1))

A2 = zeros(Adim) ;
id = eye(Adim(3)) ;
%* A(a,i,b) = LB(a) * A(a,i,b)
for a = 1 : LBdim
    A2(a, :, :) = LB(a) .* A(a, :, :)  ;
end
%* A2((a,i),b)
A2 = reshape(A2, [Adim(1) * Adim(2), Adim(3)]) ;
aa = A2' * A2 ;
norm(aa - id * aa(1,1))


% B1 = zeros(Bdim) ;
id = eye(Bdim(1)) ;
% %* B1(b,j,c) = B(b,j,c) * LB(c)
% for c = 1 : LBdim
%     B1(:, :, c) = B(:, :, c) .* LB(c) ;
% end
B1 = B ;
%* B1(b,(j,c))
B1 = reshape(B1, [Bdim(1), Bdim(2) * Bdim(3)]) ;
bb = B1 * B1' ;
norm(bb - id * bb(1,1))

B2 = zeros(Bdim) ;
% id = eye(Bdim(3)) ;
%* B2(b,j,c) = LA(b) * B(b,j,c)
for b = 1 : LAdim
    B2(b, :, :) = LA(b) .* B(b, :, :)  ;
end
%* B2((b,j),c)
B2 = reshape(B2, [Bdim(1) * Bdim(2), Bdim(3)]) ;
bb = B2' * B2 ;
DB = diag(LB.^2) ;
norm(bb - DB)