function [V, eta] = computeDominantRightEigen(A, Adim)

dim = Adim(1) ;

V = rand(dim, dim) ;
V = (V + V') / 2 ;
V0 = V ./ trace(V) ;

powerConverge = 1 ;
step = 0 ;
while powerConverge > 1e-15
    step = step + 1 ;
    %* A((a,i),b)
    A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
    %* AV((a,i),b') = sum{b}_(A((a,i),b)*V0(b,b'))
    AV = A * V0 ;
    %* AV(a,(i,b'))
    AV = reshape(AV, [Adim(1), Adim(2) * Adim(3)]) ;
    %* A(a',(i,b'))
    A = reshape(A, [Adim(1), Adim(2) * Adim(3)]) ;
    %* V(a,a') = sum{i,b'}_(AV(a,(i,b'))*A(a',(i,b')))
    V = AV * A' ;
    
    eta = trace(V) ;
    V = V ./ eta ;
    V = (V + V') / 2 ;
    
    
    powerConverge = norm(V - V0) / sqrt(dim^2 - 1) ;
    V0 = V ;
end


