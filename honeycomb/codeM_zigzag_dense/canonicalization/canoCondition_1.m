function A = canoCondition_1(A, Adim)

%* Vright(b,b') 
[Vright, etaR] = computeDominantRightEigen(A, Adim) ;

%* Vright = Wr*Dr*Wr'
% [Wr, Dr] = decomposeV(Vright) ;
[ Wr, Dr, SymError ] = OrderEigOfPoSy ( Vright ) ;
[ EigdecomError, Dr ] = CheckEigError ( Wr, Dr, Vright ) ;

sqDr = diag(sqrt(diag(Dr))) ;
invSqDr = diag(1 ./ diag(sqDr)) ;

Aright = Wr * sqDr ./ sqrt(etaR) ;
Aleft = invSqDr * Wr' ;
%* A((a,i),b)
A = reshape(A, [Adim(1) * Adim(2), Adim(3)]) ;
A = A * Aright ;
%* A(a,(i,b))
A = reshape(A, [Adim(1), Adim(2) * Adim(3)]) ;
A = Aleft * A ;
%**************************************************
% aa = A * (A') ;
% norm(aa - eye(Adim(3)) * aa(1,1))
%**************************************************
%* A(a,i,b)
A = reshape(A, Adim) ;