function MPS = canonicalize(parameter, AB, Lambda)
%* AB((a,i),(c,j))

d = parameter.bondDimension^2 ;
D = round(size(AB, 1) / d) ;

%* AB(a,i,c,j)
AB = reshape(AB, [D, d, D, d]) ;
%* AB(a,i,j,c)
AB = permute(AB, [1, 2, 4, 3]) ;
%* AB(a,(i,j),c)
AB = reshape(AB, [D, d^2, D]) ;

MPS_single.A.tensor3 = AB ;
MPS_single.A.dim = [D, d^2, D] ;
MPS_single.Lambda.tensor1 = Lambda ;
MPS_single.Lambda.dim = D ;

MPS_single = canonicalize_single(MPS_single) ;

%* LB(c)
LB = MPS_single.Lambda.tensor1 ;

%* AB(a,(i,j),c)
AB = MPS_single.A.tensor3 ;

%* AB(a,(i,j),c) = LB(a)*AB(a,(i,j),c)
for a = 1 : D
    AB(a, :, :) = LB(a) * AB(a, :, :) ;
end
%---------------------------------------------
% %* AB(a,(i,j),c) = AB(a,(i,j),c) * LB(c)
% for c = 1 : D
%     AB(:, :, c) = AB(:, :, c) * LB(c) ;
% end
%----------------------------------------------
%* AB((a,i),(j,c))
AB = reshape(AB, [D * d, d * D]) ;
%* U((a,i),b) LA(b,b) V((j,c),b)
[U, LA, V] = svd(AB, 'econ') ;
%* LA(b)
LA = diag(LA) ;

%* U(a,i,b)
U = reshape(U, [D, d, D * d]) ;
A = zeros([D, d, D * d]) ;
%* A(a,i,b) = U(a,i,b) / LB(a) ;
for a = 1 : D
    A(a, :, :) = U(a, :, :) ./ LB(a) ;
end
%* A(a,b,i)
A = permute(A, [1, 3, 2]) ;

%* V(b,(j,c))
V = V' ;
%* V(b,j,c)
V = reshape(V, [D * d, d, D]) ;
B = zeros([D * d, d, D]) ;
%* B(b,j,c) = V(b,j,c) / LB(c)
for c = 1 : D
    B(:, :, c) = V(:, :, c) ./ LB(c) ;
end
%* B(b,c,j)
B = permute(B, [1, 3, 2]) ;

MPS.A = A ;
MPS.B = B ;
MPS.Lambda{1} = LA ;
MPS.Lambda{2} = LB ;

%==========================================================
% %* verification
% Error = zeros(1, 4) ;
% A1 = zeros([D, D*d, d]) ;
% id = eye(D) ;
% %* A1(a,b,i) = A(a,b,i) * LA(b)
% for b = 1 : D*d
%     A1(:, b, :) = A(:, b, :) .* LA(b) ;
% end
% %* A1(a,(b,i))
% A1 = reshape(A1, [D, D*d^2]) ;
% aa = A1 * A1' ;
% Error(1) = norm(aa - id * aa(1,1)) ;
% 
% A2 = zeros([D, D * d, d]) ;
% id = eye(D * d) ;
% %* A(a,b,i) = LB(a) * A(a,b,i)
% for a = 1 : D
%     A2(a, :, :) = LB(a) .* A(a, :, :)  ;
% end
% %* A2(a,i,b)
% A2 = permute(A2, [1, 3, 2]) ;
% %* A2((a,i),b)
% A2 = reshape(A2, [D * d, D * d]) ;
% aa = A2' * A2 ;
% Error(2) = norm(aa - id * aa(1,1)) ;
% 
% 
% B1 = zeros([D * d, D, d]) ;
% id = eye(D * d) ;
% %* B1(b,c,j) = B(b,c,j) * LB(c)
% for c = 1 : D
%     B1(:, c, :) = B(:, c, :) .* LB(c) ;
% end
% %* B1(b,(c,j))
% B1 = reshape(B1, [D*d, D*d]) ;
% bb = B1 * B1' ;
% Error(3) = norm(bb - id * bb(1,1)) ;
% 
% B2 = zeros([D * d, D, d]) ;
% id = eye(D) ;
% %* B2(b,c,j) = LA(b) * B(b,c,j)
% for b = 1 : D*d
%     B2(b, :, :) = LA(b) .* B(b, :, :)  ;
% end
% %* B2(b,j,c)
% B2 = permute(B2, [1, 3, 2]) ;
% %* B2((b,j),c)
% B2 = reshape(B2, [D*d^2, D]) ;
% bb = B2' * B2 ;
% Error(4) = norm(bb - id * bb(1,1)) ;
% canonicalError = max(Error) 