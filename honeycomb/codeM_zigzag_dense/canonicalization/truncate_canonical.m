function [MPS, truncationError_TMP] = truncate_canonical(parameter, MPS)

D = parameter.dim_MPS ;

%* A(a,b,i)
MPS.A = MPS.A(:, 1 : D, :) ;
%* B(b,c,j)
MPS.B = MPS.B(1 : D, :, :) ;

LA = MPS.Lambda{1} ;
truncationError_TMP = 1 - sum(LA(1 : D).^2) / sum(LA.^2) ;

MPS.Lambda{1} = LA(1 : D) ;
