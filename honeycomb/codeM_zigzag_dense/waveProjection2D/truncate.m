function [U, S, V, truncationError] = truncate(parameter, U, S, V)

D = parameter.bondDimension ;

truncationError = 1 - sum(S(1 : D)) / sum(S) ;

S = S(1 : D) ;
U = U(:, 1 : D) ;
V = V(:, 1 : D) ;


