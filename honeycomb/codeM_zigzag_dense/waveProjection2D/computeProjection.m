function T = computeProjection(parameter, AB)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* T((y1,z1,y2,z2), (m1,m2)) = sum{m1,m2}_(AB((y1,z1,y2,z2), (n1,n2))*projectionOperator((n1,n2),(m1,m2)))
T = AB * parameter.projectionOperator ;
%* T(y1,z1,y2,z2,m1,m2)
T = reshape(T, [D, D, D, D, M, M]) ;
%* T(y1,z1,m1,y2,z2,m2)
T = permute(T, [1, 2, 5, 3, 4, 6]) ;
%* T((y1,z1,m1),(y2,z2,m2))
T = reshape(T, [D^2 * M, D^2 * M]) ;

