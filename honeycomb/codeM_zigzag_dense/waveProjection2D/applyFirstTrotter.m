function [wave, truncationError] = applyFirstTrotter(parameter, wave)


for j = 1 : 3
    
    [wave, truncationError] = projectBy1operator(parameter, wave) ;
    
    wave = rotate(wave) ;
end
