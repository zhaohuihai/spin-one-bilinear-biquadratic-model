function AB = computeLyLz_A_Lx_B_LyLz(parameter, wave)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

A = wave.A ;
B = wave.B ;
Lambda = wave.Lambda ;
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{1}(x)
for x = 1 : D
    A(x, :, :, :) = A(x, :, :, :) * Lambda{1}(x) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    A(:, y, :, :) = A(:, y, :, :) * Lambda{2}(y) ;
end
%* A(x,y,z,m) = A(x,y,z,m)*Lambda{3}(z)
for z = 1 : D
    A(:, :, z, :) = A(:, :, z, :) * Lambda{3}(z) ;
end

%* B(x,y,z,m) = B(x,y,z,m)*Lambda{2}(y)
for y = 1 : D
    B(:, y, :, :) = B(:, y, :, :) * Lambda{2}(y) ;
end
%* B(x,y,z,m) = B(x,y,z,m)*Lambda{3}(z)
for z = 1 : D
    B(:, :, z, :) = B(:, :, z, :) * Lambda{3}(z) ;
end 

%* A(x,(y1,z1,m1))
A = reshape(A, [D, D^2 * M]) ;
%* B(x,(y2,z2,m2))
B = reshape(B, [D, D^2 * M]) ;
%* AB((y1,z1,m1), (y2,z2,m2))
AB = A' * B ;
%* AB(y1,z1,m1,y2,z2,m2)
AB = reshape(AB, [D, D, M, D, D, M]) ;
%* AB(y1,z1,y2,z2,m1,m2)
AB = permute(AB, [1, 2, 4, 5, 3, 6]) ;
%* AB((y1,z1,y2,z2), (m1,m2))
AB = reshape(AB, [D^4, M^2]) ;

