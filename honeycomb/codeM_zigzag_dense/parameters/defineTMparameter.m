function parameter = defineTMparameter(parameter)

%* canonicalization in transfer matrix projection
parameter.canonicalization = 0 ;

%* dimension of MPS
parameter.dim_MPS = 50 ;
parameter.dim_MPS_initial = 10 ;
parameter.dim_MPS_incre = 5 ;

parameter.convergenceCriterion_TMprojection = 1e-10 ;
parameter.convergenceCriterion_power = 1e-12 ;

parameter.maxTMstep = 1000 ;
parameter.maxPowerStep = 1e4 ;
