function [rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, T)



rightMPS = findDominantRightEigenMPS_dense(parameter, T) ;

%* transpose to left transferMatrix
T = T' ;

leftMPS = findDominantRightEigenMPS_dense(parameter, T) ;