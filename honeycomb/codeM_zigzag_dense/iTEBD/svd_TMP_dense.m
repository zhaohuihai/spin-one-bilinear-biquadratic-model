function [U, S, V] = svd_TMP_dense(R)

[U, S, V] = svd(R) ;
S = diag(S) ;
S = S ./ S(1) ;
