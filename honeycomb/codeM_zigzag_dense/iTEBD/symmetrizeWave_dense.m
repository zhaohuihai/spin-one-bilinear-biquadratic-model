function wave = symmetrizeWave_dense(wave)

Lambda = wave.Lambda ;

A = symmetrizeA_dense(wave.A) ;
B = symmetrizeA_dense(wave.B) ;

Lambda{1} = (Lambda{1} + Lambda{2} + Lambda{3}) / 3 ;
Lambda{2} = Lambda{1} ;
Lambda{3} = Lambda{1} ;

wave.Lambda = Lambda ;
wave.A = A ;
wave.B = B ;