function [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb)

%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian_dense(parameter, operator) ;

%* Wa(x,in,k)
Wa = createW_y_dense(parameter, A, U) ;
%* Wb(x,ln,j)
Wb = createW_y_dense(parameter, B, V) ;

%* THR((i,j),(k,ln))
THR = contractTa_Wb_y_dense(parameter, Ta, Wb) ;
%* THL((k,l),(in,j))
THL = contractTb_Wa_y_dense(parameter, Tb, Wa) ;

%* QR((a,k),(c,ln))
QR = contractABR_TH_dense(parameter, ABR, THR) ;
%* QL((j,c'),(in,a'))
QL = contractABL_TH_dense(parameter, ABL, THL) ;