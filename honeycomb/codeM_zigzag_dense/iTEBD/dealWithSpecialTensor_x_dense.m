function expectValueBond = dealWithSpecialTensor_x_dense(parameter, T, rightMPS, leftMPS, A, B)

%* do one step projection and exchange A, B
rightMPS = applyTMP_dense(parameter, T, rightMPS) ;

%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2_dense(parameter, rightMPS) ;

%* ABL((a',c'),(k,l))
ABL = computeA_L1_B_L2_dense(parameter, leftMPS) ;

%* P((c,c'),(e,e')) = ABR((c,e),(i,j))*T((i,j),(k,l))*ABL((c',e'),(k,l))
P = computeABR_T_ABL_dense(parameter, ABR, ABL, T) ;

D = parameter.dim_MPS ;
vector = rand(D^2, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

[vectorRight, etaRight] = applyPowerMethod_dense(parameter, P, vector) ;
P = P' ;
[vectorLeft, etaLeft] = applyPowerMethod_dense(parameter, P, vectorRight) ;
%*==================================================================================
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Sx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Sx = Sx ;
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Sy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Sy = Sy ;
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Sz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['Sz x bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagSx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagSx = stagSx ;
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagSy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagSy = stagSy ;
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagSz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagSz = stagSz ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qxx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qxx = Qxx ;
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qyy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qyy = Qyy ;
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qzz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qzz = Qzz ;
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qxy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qxy = Qxy ;
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qyz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qyz = Qyz ;
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    Qzx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.Qzx = Qzx ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQxx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxx = stagQxx ;
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQyy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQyy = stagQyy ;
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQzz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQzz = stagQzz ;
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQxx_yy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQxy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQxy = stagQxy ;
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQyz = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQyz = stagQyz ;
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    stagQzx = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    expectValueBond.stagQzx = stagQzx ;
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    Q = createSpecialTensor_x_dense(parameter, operator, A, B, ABR, ABL) ;
    energy = findExpectationValue_dense(vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
    disp(['energy x bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
end



