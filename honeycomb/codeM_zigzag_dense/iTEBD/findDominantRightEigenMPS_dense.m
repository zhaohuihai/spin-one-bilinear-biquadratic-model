function MPS = findDominantRightEigenMPS_dense(parameter, T)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;

%* MPS: A B Lambda{1} Lambda{2}
MPS = initializeMPS_dense(parameter) ;

TMPstep = 0 ;
TMconvergence = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(D), ', TMP step = ', num2str(TMPstep)]) ;
    Lambda0 = MPS.Lambda{1} ;
    tic
    [MPS, truncationError_TM] = applyTMP_dense(parameter, T, MPS) ;
    MPS = applyTMP_dense(parameter, T, MPS) ;
    toc
    Lambda1 = MPS.Lambda{1} ;
    TMconvergence = norm(Lambda1 - Lambda0) / sqrt(D - 1) ;
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMconvergence)])
        disp(['truncation error = ', num2str(truncationError_TM)])
    end
end

while parameter.dim_MPS < maxDim
    TMPstep = TMPstep + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS] = applyTMP_dense(parameter, T, MPS) ;
    [MPS] = applyTMP_dense(parameter, T, MPS) ;
    toc
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;
TMconvergence = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
while TMconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    Lambda0 = MPS.Lambda{1} ;
    tic
    [MPS, truncationError_TM] = applyTMP_dense(parameter, T, MPS) ;
    [MPS] = applyTMP_dense(parameter, T, MPS) ;
    toc
    Lambda1 = MPS.Lambda{1} ;
    if j > 1
        TMconvergence = norm(Lambda1 - Lambda0) / sqrt(D - 1) ;
    end
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMconvergence)])
        disp(['truncation error = ', num2str(truncationError_TM)])
    end
end
disp(['dim MPS = ', num2str(parameter.dim_MPS), ' TMP steps = ', num2str(j)]) ;

disp(['total TMP steps = ', num2str(TMPstep)]) ;