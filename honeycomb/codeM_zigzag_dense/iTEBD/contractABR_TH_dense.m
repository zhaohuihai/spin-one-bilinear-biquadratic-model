function QR = contractABR_TH_dense(parameter, ABR, THR)
%* QR((a,k),(c,ln))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
N = parameter.siteDimension^2 ;

%* QR((a,c),(k,ln)) = sum{i,j}_(ABR((a,c),(i,j))*THR((i,j),(k,ln)))
QR = ABR * THR ;

%* QR(a,c,k,ln)
QR = reshape(QR, [D, D, d, d*N]) ;

%* QR(a,k,c,ln)
QR = permute(QR, [1, 3, 2, 4]) ;

%* QR((a,k),(c,ln))
QR = reshape(QR, [D*d, D*d*N]) ;