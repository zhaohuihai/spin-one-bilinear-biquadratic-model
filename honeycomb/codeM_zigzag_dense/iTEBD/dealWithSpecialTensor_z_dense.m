function expectValueBond = dealWithSpecialTensor_z_dense(parameter, T, ABR, ABL, A, B, Ta, Tb)
%* T((i,j),(k,l))

%* PR((l,c),(k,a)) = sum{i,j}_(ABR((a,c),(i,j))*T((i,j),(k,l)))
%* change notation: PR((l,c),(i,a))
PR = contractABL_T_dense(parameter, ABR, T) ;

%* T((k,l),(i,j))
T = T' ;

%* PL((a',i),(c',j)) = sum{k,l}_(ABL((a',c'),(k,l))*T((k,l),(i,j)))
%* change notation: PL((a',j),(c',i))
PL = contractABR_T_dense(parameter, ABL, T) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,l,c')
vector = rand(D * d * D, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

%* vectorRight(a',j,a) = sum{c,c',l,i}_(PL((a',j),(c',i))*vectorRight(c',(l,c))*PR((l,c),(i,a)))
[vectorRight, etaRight] = applyPowerMethod_y_dense(parameter, PL, PR, vector) ;

%* PR((l,c),(i,a)) -> PR((c,l),(a,i))
%* PL((a',j),(c',i)) -> PL((j,a'),(i,c'))
[PL, PR] = transposeP_dense(parameter, PL, PR) ;

vectorLeft = reshape(vectorRight, [D, d, D]) ;
vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
vectorLeft = reshape(vectorLeft, [D * d * D, 1]) ;

%* vectorLeft(c,l,c') = sum{a,a',j,i}_(PR((c,l),(a,i))*vectorLeft(a,(j,a'))*PL((j,a'),(i,c')))
[vectorLeft, etaLeft] = applyPowerMethod_y_dense(parameter, PR, PL, vectorLeft) ;
%*==============================================================================================================
%* QR((l,c),(kn,a))    change notation: QR((l,c),(in,a))
%* QL((a',i),(c',jn))  change notation: QL((a',j),(c',in))
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Sx = Sx ;
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Sy = Sy ;
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['Sz z bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagSx = stagSx ;
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagSy = stagSy ;
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagSz = stagSz ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qxx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qxx = Qxx ;
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qyy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qyy = Qyy ;
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qzz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qzz = Qzz ;
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qxy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qxy = Qxy ;
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qyz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qyz = Qyz ;
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qzx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.Qzx = Qzx ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxx = stagQxx ;
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQyy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQyy = stagQyy ;
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQzz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQzz = stagQzz ;
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxx_yy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQxy = stagQxy ;
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQyz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQyz = stagQyz ;
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQzx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    expectValueBond.stagQzx = stagQzx ;
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_z_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    energy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
    disp(['energy z bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
end