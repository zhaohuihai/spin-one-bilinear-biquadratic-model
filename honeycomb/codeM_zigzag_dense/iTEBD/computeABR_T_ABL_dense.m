function P = computeABR_T_ABL_dense(parameter, ABR, ABL, T)
%* P((a,a'),(c,c')) = ABR((a,c),(i,j))*T((i,j),(k,l))*ABL((a',c'),(k,l))
D = parameter.dim_MPS ;


%* P((a,c),(k,l))
P = ABR * T ;
%* P((a,c),(a',c'))
P = P * ABL' ;
%* P(a,c,a',c')
P = reshape(P, [D, D, D, D]) ;
%* P(a,a',c,c')
P = permute(P, [1, 3, 2, 4]) ;
%* P((a,a'),(b,b'))
P = reshape(P, [D^2, D^2]) ;