function expectValueBond = dealWithSpecialTensor_y_dense(parameter, T, ABR, ABL, A, B, Ta, Tb)
%* T((i,j),(k,l))

%* PR((a,k),(c,l)) = sum{i,j}_(ABR((a,c),(i,j))*T((i,j),(k,l)))
%* change notation: PR((a,j),(c,i))
PR = contractABR_T_dense(parameter, ABR, T) ;

%* T((k,l),(i,j))
T = T' ;

%* PL((j,c'),(i,a')) = sum{k,l}_(ABL((a',c'),(k,l))*T((k,l),(i,j)))
%* change notation: PL((l,c'),(i,a'))
PL = contractABL_T_dense(parameter, ABL, T) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,l,c'): column vector
vector = rand(D * d * D, 1) ;
eta = norm(vector) ;
vector = vector ./ eta ;

%* vectorRight(a,j,a'): column vector
[vectorRight, etaRight] = applyPowerMethod_y_dense(parameter, PR, PL, vector) ;

%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))
[PR, PL] = transposeP_dense(parameter, PR, PL) ;

vectorLeft = reshape(vectorRight, [D, d, D]) ;
vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
vectorLeft = reshape(vectorLeft, [D * d * D, 1]) ;

%* vectorLeft(c',l,c): column vector
[vectorLeft, etaLeft] = applyPowerMethod_y_dense(parameter, PL, PR, vectorLeft) ;
%*==========================================================================================================
%* QR((a,k),(c,ln))    change notation: QR((a,j),(c,in))
%* QL((j,c'),(in,a'))  change notation: QL((l,c'),(in,a'))
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    operator = createSx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Sx = Sx ;
end
if computation.Sy == 1
    operator = createSy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Sy = Sy ;
end
if computation.Sz == 1
    operator = createSz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Sz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['Sz y bond = ', num2str(Sz)]) ;
    expectValueBond.Sz = Sz ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    operator = createStagSx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagSx = stagSx ;
end
if computation.stagSy == 1
    operator = createStagSy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagSy = stagSy ;
end
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagSz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagSz = stagSz ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    operator = createQxx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qxx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qxx = Qxx ;
end
if computation.Qyy == 1
    operator = createQyy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qyy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qyy = Qyy ;
end
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qzz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qzz = Qzz ;
end
if computation.Qxy == 1
    operator = createQxy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qxy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qxy = Qxy ;
end
if computation.Qyz == 1
    operator = createQyz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qyz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qyz = Qyz ;
end
if computation.Qzx == 1
    operator = createQzx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    Qzx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.Qzx = Qzx ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    operator = createStagQxx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxx = stagQxx ;
end
if computation.stagQyy == 1
    operator = createStagQyy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQyy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQyy = stagQyy ;
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQzz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQzz = stagQzz ;
end
if computation.stagQxx_yy == 1
    operator = createStagQxx_yy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxx_yy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxx_yy = stagQxx_yy ;
end
if computation.stagQxy == 1
    operator = createStagQxy(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQxy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQxy = stagQxy ;
end
if computation.stagQyz == 1
    operator = createStagQyz(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQyz = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQyz = stagQyz ;
end
if computation.stagQzx == 1
    operator = createStagQzx(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    stagQzx = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    expectValueBond.stagQzx = stagQzx ;
end
% energy computation
if computation.energy == 1
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_y_dense(parameter, operator, ABR, ABL, A, B, Ta, Tb) ;
    energy = findExpectationValue_y_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
    disp(['energy y bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
end