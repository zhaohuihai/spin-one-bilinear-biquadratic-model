function T = computeTensorProductA_A_dense(parameter, A)

D = parameter.bondDimension ;
M = parameter.siteDimension ;
%* A((xyz),m)
A = reshape(A, [D^3, M]) ;
%* T((xyz), (x'y'z'))
T = A * A' ;
%* T(x,y,z,x',y',z')
T = reshape(T, [D, D, D, D, D, D]) ;
%* T(x, x', y, y', z, z')
T = permute(T, [1, 4, 2, 5, 3, 6]) ;
%* T((x,x'), (y,y'), (z,z'))
T = reshape(T, [D^2, D^2, D^2]) ;
