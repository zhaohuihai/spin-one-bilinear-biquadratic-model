function [QR, QL] = createSpecialTensor(parameter, ABR, ABL, A, B, Ta, Tb)
%* QR(j,a,i,c), QL(c',l,a',i,n)

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* ABR(a,c,i,j)
ABR = reshape(ABR, [D, D, d, d]) ;
%* ABL(a',c',k,l)
ABL = reshape(ABL, [D, D, d, d]) ;

%* TCa(x,l,k,n), TCb(x,i,j,n)
[TCa, TCb] = createTCa_TCb(parameter, A, B) ;
%==============================================================
%* Wa(l,k,n,i,j) = sum{x}_[TCa(x,l,k,n)*Tb(x,i,j)]
Wa = contractTensors(TCa, 4, 1, Tb, 3, 1) ;

%* Wb(l,k,i,j,n) = sum{x}_[Ta(x,l,k)*TCb(x,i,j,n)]
Wb = contractTensors(Ta, 3, 1, TCb, 4, 1) ;
%==============================================================
%* QR(k,a,l,c,n) = sum{i,j}_[Wa(l,k,n,i,j)*ABR(a,c,i,j)]
QR = contractTensors(Wa, 5, [4, 5], ABR, 4, [3, 4], [2, 4, 1, 5, 3]) ;
%* change notation: QR(j,a,i,c,n)

%* QL(c',j,a',i,n) = sum{k,l}_[Wb(l,k,i,j,n)*ABL(a',c',k,l)]
QL = contractTensors(Wb, 5, [2, 1], ABL, 4, [3, 4], [5, 2, 4, 1, 3]) ;
%* change notation: QL(c',l,a',i,n)


