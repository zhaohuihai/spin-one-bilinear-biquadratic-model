function [MPS, truncationError_TMP] = applyTMP_dense(parameter, T, MPS)

if parameter.canonicalization == 0
    %* AB((a,c),(i,j))
    AB = computeL2_A_L1_B_L2_dense(parameter, MPS) ;
    %* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
    R = projectTransferMatrix_dense(parameter, T, AB) ;
    
    [U, S, V] = svd_TMP_dense(R) ;
    [U, S, V, truncationError_TMP] = truncate_TMP_dense(parameter, U, S, V) ;
    
    MPS = recover_TMP_dense(parameter, MPS, U, S, V) ;
    
    MPS = exchangeAandB_TMP_dense(MPS) ;
    
elseif parameter.canonicalization == 1
    %* AB((a,c),(i,j))
    AB = computeA_L1_B_dense(parameter, MPS) ;
    %* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
    R = projectTransferMatrix_dense(parameter, T, AB) ;
    
    MPS = canonicalize(parameter, R, MPS.Lambda{2}) ;
    
    [MPS, truncationError_TMP] = truncate_canonical(parameter, MPS) ;
    
    MPS = exchangeAandB_TMP_dense(MPS) ;
end
