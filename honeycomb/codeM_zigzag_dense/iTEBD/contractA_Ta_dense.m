function A = contractA_Ta_dense(parameter, A, Ta)
%* A((a,b),(x,i)) = sum{k}_(A(a,b,k)*Ta(x,k,i))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* A((a,b),k)
A = reshape(A , [D^2, d]) ;

%* Ta(k,x,i)
Ta = permute(Ta, [2, 1, 3]) ;
%* Ta(k,(x,i))
Ta = reshape(Ta, [d, d^2]) ;
%* A((a,b),(x,i))
A = A * Ta ;