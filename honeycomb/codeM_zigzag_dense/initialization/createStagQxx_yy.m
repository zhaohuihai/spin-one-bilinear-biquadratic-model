function stagQxx_yy = createStagQxx_yy(parameter)

stagQxx = createStagQxx(parameter) ;
stagQyy = createStagQyy(parameter) ;

stagQxx_yy = stagQxx - stagQyy ;