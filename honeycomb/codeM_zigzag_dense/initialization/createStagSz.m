function stagSz = createStagSz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

stagSz = zeros(M, M, M, M) ;
MM = M * M ;

%* stagSz(m1, m2, n1, n2) = <m1, m2| stagSz | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        stagSz(q1, q2, q1, q2) = stagSz(q1, q2, q1, q2) + (m1 - m2) / 2 ;
    end
end

stagSz = reshape(stagSz, [MM, MM]) ;