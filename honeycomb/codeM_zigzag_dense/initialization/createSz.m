function Sz = createSz(parameter)

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Sz = zeros(M, M, M, M) ;
MM = M * M ;

%* Sz(m1, m2, n1, n2) = <m1, m2| Sz | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        Sz(q1, q2, q1, q2) = Sz(q1, q2, q1, q2) + (m1 + m2) / 2 ;
    end
end

Sz = reshape(Sz, [MM, MM]) ;