function parameter = defineModelParameter(parameter)
%* Bilinear biquadratic model parameters

parameter.theta = atan(2) ;
% parameter.theta = (0) * pi ;

% parameter.thetaInitial = (-0.51) *pi ;
% parameter.thetaFinal = (-0.49) *pi ;
% parameter.thetaIncre = (0.005) *pi ;


if parameter.theta <= (-pi) || parameter.theta > (pi)
    error('the value of theta is out of parameter range') ;
end
parameter.siteDimension = 3 ;