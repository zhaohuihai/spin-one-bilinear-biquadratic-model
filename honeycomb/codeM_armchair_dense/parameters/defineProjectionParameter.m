function parameter = defineProjectionParameter(parameter)

parameter.polarization = 0 ;

parameter.field = 0.1 ;

parameter.fieldInitial = 0.1 ;
parameter.fieldFinal = 3 ;
parameter.fieldIncre = 0.1 ;

parameter.dipolarField_x = 0 ;
parameter.dipolarField_z = 1 ;

parameter.stagDipolarField_x = 0 ;
parameter.stagDipolarField_z = 0 ;

parameter.quadrupolarField_x = 0 ;
parameter.quadrupolarField_z = 0 ;

parameter.stagQuadrupolarField_x = 0 ;
parameter.stagQuadrupolarField_z = 0 ;
parameter.stagQuadrupolarField_xx_yy = 0 ;

parameter.convergenceCriterion_polarization = 1e-3 ;
%*****************************************************************

parameter.VBC = 0 ;
parameter.convergenceCriterion_VBC = 1e-3 ;
%*****************************************************************
parameter.TrotterOrder = 1 ;

parameter.tauInitial = 0.1 ;
parameter.tauFinal = 1e-4 ; %*
parameter.tauChangeFactor = 50 ;

parameter.convergenceCriterion_projection = 1e-11 ;
parameter.maxProjectionStep = 2e4 ;