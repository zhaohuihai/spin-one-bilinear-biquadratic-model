function wave = recover(parameter, wave, U, S, V)

wave.Lambda{1} = S ;
Lambda = wave.Lambda ;


%* A(x,y1,z1,m1)
A = recoverA(parameter, U, Lambda) ;

%* B(x,y2,z2,m2)
B = recoverA(parameter, V, Lambda) ;


wave.A = A ;
wave.B = B ;