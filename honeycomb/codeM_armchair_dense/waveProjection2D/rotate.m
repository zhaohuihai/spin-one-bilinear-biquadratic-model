function wave = rotate(wave)
%* (x, y, z) --> (y, z, x)

%* A(x,y,z,m) -> A(y,z,x,m)
wave.A = permute(wave.A, [2, 3, 1, 4]) ;
%* B(x,y,z,m) -> B(y,z,x,m)
wave.B = permute(wave.B, [2, 3, 1, 4]) ;

wave.Lambda = wave.Lambda([2, 3, 1]) ;

