function [wave, truncationError, coef] = projectBy1operator(parameter, wave)

parameter.projectionOperator = createProjectionOperator(parameter) ;
%* AB((y1,z1,y2,z2), (m1,m2))
AB = computeLyLz_A_Lx_B_LyLz(parameter, wave) ;

%* T((y1,z1,m1),(y2,z2,m2))
T = computeProjection(parameter, AB) ;

%* U((y1,z1,m1),x) V((y2,z2,m2),x)
[U, S, V] = svd(T) ;
%* S(x)
S = diag(S) ;
coef = S(1) ;
S = S ./ coef ;

[U, S, V, truncationError] = truncate(parameter, U, S, V) ;

wave = recover(parameter, wave, U, S, V) ;
