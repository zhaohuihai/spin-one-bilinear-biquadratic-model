function A = recoverA(parameter, U, Lambda)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* U(x,(y1,z1,m1))
U = U' ;
%* A(x,y1,z1,m1)
A = reshape(U, [D, D, D, M]) ;

for y1 = 1 : D
    A(:, y1, :, :) = A(:, y1, :, :) ./ Lambda{2}(y1) ;
end
for z1 = 1 : D
    A(:, :, z1, :) = A(:, :, z1, :) ./ Lambda{3}(z1) ;
end