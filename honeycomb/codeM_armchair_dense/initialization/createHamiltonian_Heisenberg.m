function H = createHamiltonian_Heisenberg(parameter)
%* AF Heisenberg model Hamiltonian

M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

H = zeros(M, M, M, M) ;
MM = M * M ;

%* H(m1, m2, n1, n2) = <m1, m2| H | n1, n2 >

%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        H(q1, q2, q1, q2) = H(q1, q2, q1, q2) + m1 * m2 ;
    end
end

%* assign values to off-diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        m1 = S - q1 + 1 ;
        %* h1 = (1/2)((S1+)(S2-))
        h1 = (1 / 2) * sqrt((S - m1) * (S + m1 + 1) * (S + m2) * (S - m2 + 1)) ;
        
        if h1 ~= 0
            p1 = round(S - m1) ;
            p2 = round(S - m2 + 2) ;
            H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h1 ;
        end
        %* h2 = (1/2)((S1-)(S2+))
        h2 = (1 / 2) * sqrt((S + m1) * (S - m1 + 1) * (S - m2) * (S + m2 + 1)) ;
        
        if h2 ~= 0
            p1 = round(S - m1 + 2) ;
            p2 = round(S - m2) ;
            H(p1, p2, q1, q2) = H(p1, p2, q1, q2) + h2 ;
        end
    end
end


H = reshape(H, [MM, MM]) ; %* H((m1, m2), (n1, n2))