function [W, D] = decomposeV(V)
%* V = X * X'

dim = sqrt(length(V)) ;
V = reshape(V, [dim, dim]) ;
% norm(V - V')
% V = (V + V') / 2 ;
[W, D] = eig(V) ; %* V = W * D * W'
D = abs(D) ;
% X = W * sqrt(D) ;
norm(W'*V*W - D)
norm(W'*W - eye(dim))
% norm(V - X*X')