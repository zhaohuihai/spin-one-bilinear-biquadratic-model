function [U, S, V, truncationError_TMP] = truncate_TMP_dense(parameter, U, S, V)

D = parameter.dim_MPS ;

allS = sum(S.^2) ;
S = S(1 : D) ;
savedS = sum(S.^2) ;
truncationError_TMP = 1 - savedS / allS ;

U = U(:, 1 : D) ;
V = V(:, 1 : D) ;