function T = createTransferMatrix_dense(parameter, Ta, Tb)
%* T((i,j),(k,l)) = sum{x}_(Ta(x,i,k) * Tb(x,j,l))

%* Tb(x,j,l)
Tb = permute(Tb, [1, 3, 2]) ;

d = parameter.bondDimension^2 ;

%* Ta(x,(i,k))
Ta = reshape(Ta, [d, d^2]) ;
%* Tb(x,(j,l))
Tb = reshape(Tb, [d, d^2]) ;
%* T((i,k),(j,l))
T = Ta' * Tb ;

%* T(i,k,j,l)
T = reshape(T, [d, d, d, d]) ;

%* T(i,j,k,l)
T = permute(T, [1, 3, 2, 4]) ;

%* T((i,j),(k,l))
T = reshape(T, [d^2, d^2]) ;

