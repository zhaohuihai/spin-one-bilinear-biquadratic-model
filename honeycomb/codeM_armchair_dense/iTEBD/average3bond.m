function average3bond(parameter, expectValueBond)

computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    Sx = 0 ;
    for i = 1 : 3
        Sx = Sx + expectValueBond(i).Sx / 3 ;
    end
    saveExpectationValue(parameter, Sx, 'Sx') ;
end
if computation.Sy == 1
    Sy = 0 ;
    for i = 1 : 3
        Sy = Sy + expectValueBond(i).Sy / 3 ;
    end
    saveExpectationValue(parameter, Sy, 'Sy') ;
end
if computation.Sz == 1
    Sz = 0 ;
    for i = 1 : 3
        Sz = Sz + expectValueBond(i).Sz / 3 ;
    end
    saveExpectationValue(parameter, Sz, 'Sz') ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    stagSx = 0 ;
    for i = 1 : 3
        stagSx = stagSx + expectValueBond(i).stagSx / 3 ;
    end
    saveExpectationValue(parameter, stagSx, 'stagSx') ;
end
if computation.stagSy == 1
    stagSy = 0 ;
    for i = 1 : 3
        stagSy = stagSy + expectValueBond(i).stagSy / 3 ;
    end
    saveExpectationValue(parameter, stagSy, 'stagSy') ;
end
if computation.stagSz == 1
    stagSz = 0 ;
    for i = 1 : 3
        stagSz = stagSz + expectValueBond(i).stagSz / 3 ;
    end
    saveExpectationValue(parameter, stagSz, 'stagSz') ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    Qxx = 0 ;
    for i = 1 : 3
        Qxx = Qxx + expectValueBond(i).Qxx / 3 ;
    end
    saveExpectationValue(parameter, Qxx, 'Qxx') ;
end
if computation.Qyy == 1
    Qyy = 0 ;
    for i = 1 : 3
        Qyy = Qyy + expectValueBond(i).Qyy / 3 ;
    end
    saveExpectationValue(parameter, Qyy, 'Qyy') ;
end
if computation.Qzz == 1
    Qzz = 0 ;
    for i = 1 : 3
        Qzz = Qzz + expectValueBond(i).Qzz / 3 ;
    end
    saveExpectationValue(parameter, Qzz, 'Qzz') ;
end
if computation.Qxy == 1
    Qxy = 0 ;
    for i = 1 : 3
        Qxy = Qxy + expectValueBond(i).Qxy / 3 ;
    end
    saveExpectationValue(parameter, Qxy, 'Qxy') ;
end
if computation.Qyz == 1
    Qyz = 0 ;
    for i = 1 : 3
        Qyz = Qyz + expectValueBond(i).Qyz / 3 ;
    end
    saveExpectationValue(parameter, Qyz, 'Qyz') ;
end
if computation.Qzx == 1
    Qzx = 0 ;
    for i = 1 : 3
        Qzx = Qzx + expectValueBond(i).Qzx / 3 ;
    end
    saveExpectationValue(parameter, Qzx, 'Qzx') ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    stagQxx = 0 ;
    for i = 1 : 3
        stagQxx = stagQxx + expectValueBond(i).stagQxx / 3 ;
    end
    saveExpectationValue(parameter, stagQxx, 'stagQxx') ;
end
if computation.stagQyy == 1
    stagQyy = 0 ;
    for i = 1 : 3
        stagQyy = stagQyy + expectValueBond(i).stagQyy / 3 ;
    end
    saveExpectationValue(parameter, stagQyy, 'stagQyy') ;
end
if computation.stagQzz == 1
    stagQzz = 0 ;
    for i = 1 : 3
        stagQzz = stagQzz + expectValueBond(i).stagQzz / 3 ;
    end
    saveExpectationValue(parameter, stagQzz, 'stagQzz') ;
end
if computation.stagQxx_yy == 1
    stagQxx_yy = 0 ;
    for i = 1 : 3
        stagQxx_yy = stagQxx_yy + expectValueBond(i).stagQxx_yy / 3 ;
    end
    saveExpectationValue(parameter, stagQxx_yy, 'stagQxx_yy') ;
end
if computation.stagQxy == 1
    stagQxy = 0 ;
    for i = 1 : 3
        stagQxy = stagQxy + expectValueBond(i).stagQxy / 3 ;
    end
    saveExpectationValue(parameter, stagQxy, 'stagQxy') ;
end
if computation.stagQyz == 1
    stagQyz = 0 ;
    for i = 1 : 3
        stagQyz = stagQyz + expectValueBond(i).stagQyz / 3 ;
    end
    saveExpectationValue(parameter, stagQyz, 'stagQyz') ;
end
if computation.stagQzx == 1
    stagQzx = 0 ;
    for i = 1 : 3
        stagQzx = stagQzx + expectValueBond(i).stagQzx / 3 ;
    end
    saveExpectationValue(parameter, stagQzx, 'stagQzx') ;
end
% energy computation
if computation.energy == 1
    energy = 0 ;
    for i = 1 : 3
        energy = energy + expectValueBond(i).energy / 3 ;
    end
    energy = energy * 1.5 ;
    saveExpectationValue(parameter, energy, 'energy') ;
end