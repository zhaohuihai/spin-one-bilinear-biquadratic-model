function THR = contractTb_Wa_z_dense(parameter, Tb, Wa)
%* THR((i,j),(kn,l)) = sum{x}_(Wa(x,i,kn)*Tb(x,l,j))

d = parameter.bondDimension^2 ;
N = parameter.siteDimension^2 ;

%* Tb(x,(l,j))
Tb = reshape(Tb, [d, d^2]) ;

%* Wa(x,(i,kn))
Wa = reshape(Wa, [d, d^2*N]) ;

%* THR((i,kn),(l,j))
THR = Wa' * Tb ;

%* THR(i,kn,l,j)
THR = reshape(THR, [d, d*N, d, d]) ;

%* THR(i,j,kn,l)
THR = permute(THR, [1, 4, 2, 3]) ;

%* THR((i,j),(kn,l))
THR = reshape(THR, [d^2, d*N*d]) ;