function [U, V] = decomposeHamiltonian(parameter, H)

%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
[U, S, V] = svd_Hamiltonian(parameter, H) ;

sqrtS = computeSqrtS(S) ;
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
U = UtimesSqrtS(U, sqrtS) ;

%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
V = UtimesSqrtS(V, sqrtS) ;

