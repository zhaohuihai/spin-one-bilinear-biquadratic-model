function V = reform1to3(V0)
%* VR(c,l,c')

V = rmfield(V0, 'tensor1') ;
V.indNo = 3 ;

for i = 1 : V0.subNo
    vDim = V0.dim(:, i) ;
    %* vTensor((c,l,c'),1)
    vTensor = V0.tensor1{i} ;
    %* VR(c,l,c')
    vTensor = reshape(vTensor, vDim') ;
    
    V.tensor{i} = vTensor ;
end