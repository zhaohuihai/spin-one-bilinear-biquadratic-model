function [U, S, V, R] = svd_TMP(parameter, Udir, Vdir, R)

[status,message,messageid] = mkdir(Udir) ;
[status,message,messageid] = mkdir(Vdir) ;
U.dirName = Udir ;
V.dirName = Vdir ;

ratio = parameter.svdRatio_TMP ;
svdsOptions = parameter.svdsOptions ;

D = parameter.dim_MPS + 1 ;
Rno = length(R) ;
decomposingDim = zeros(1, Rno) ;
S.subNo = Rno ;

U.subNo = 0 ;
V.subNo = 0 ;
for i = 1 : Rno
    Rblock = combineRsub(parameter, R(i)) ;
    Rdim = length(Rblock) ;
    decomposingDim(i) = Rdim ;
    if (Rdim * ratio) < D
        [Ublock, Sblock, Vblock] = svd(Rblock, 'econ') ;
        Sblock = diag(Sblock) ;
        if length(Sblock) > D
            Ublock = Ublock(:, 1 : D) ;
            Sblock = Sblock(1 : D) ;
            Vblock = Vblock(:, 1 : D) ;
        end
    else
        [Ublock, Sblock, Vblock, convergence] = svds(Rblock, D, 'L', svdsOptions) ;
        Sblock = diag(Sblock) ;
        if convergence == 1
            disp('svds is not convergent. Use svd instead') ;
            [Ublock, Sblock, Vblock] = svd(Rblock, 'econ') ;
            Sblock = diag(Sblock) ;
            Ublock = Ublock(:, 1 : D) ;
            Sblock = Sblock(1 : D) ;
            Vblock = Vblock(:, 1 : D) ;
        end
    end
    [na, Udim] = size(Ublock) ;
    U = createU_TMP(parameter, Ublock, U, R(i), Udim, 1) ;
    [na, Vdim] = size(Vblock) ;
    V = createU_TMP(parameter, Vblock, V, R(i), Vdim, 2) ;
    S = createS_TMP(Sblock, S, R(i), i) ;    
end
% disp(['max TMP decomposition dim = ', num2str(max(decomposingDim)) ]) ;
