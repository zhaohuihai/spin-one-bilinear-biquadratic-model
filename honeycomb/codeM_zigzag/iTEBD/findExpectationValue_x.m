function [expectValue, Q] = findExpectationValue_x(parameter, vectorRight, vectorLeft, etaRight, etaLeft, Q)

eta = (etaRight + etaLeft) / 2 ;



%* QvR(a1,a2) = sum{c1,c2}_(Q((a1,a2),(c1,c2))*vectorRight(c1,c2))
[QvR, Q] = applyPowerMethod_x(parameter, Q, vectorRight) ;

%* h = sum{a1,a2}_(vectorLeft(a1,a2)*QvR(a1,a2))
h = contract2vector_x(vectorLeft, QvR) ;

%* g = sum{a1,a2}_(vectorLeft(a1,a2)*vectorRight(a1,a2))
g = contract2vector_x(vectorLeft, vectorRight) ;
g = eta .* g ;

expectValue = h / g ;