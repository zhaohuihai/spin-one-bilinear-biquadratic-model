function P = transposeP_y(parameter, P)
%* P((a,j),(c,i)) -> P((j,a),(i,c))

for i = 1 : P.subNo
    pDim = P.dim(:, i) ;
    %* P((a,j),(c,i))
%     pTensor = P.tensor2{i} ;
    pTensor = loadTensor2(parameter, P, i) ;
    %* P(a,j,c,i)
    pTensor = reshape(pTensor, pDim') ;
    %* P(j,a,i,c)
    pTensor = permute(pTensor, [2, 1, 4, 3]) ;
    %* P((j,a),(i,c))
    pTensor = reshape(pTensor, [pDim(2) * pDim(1), pDim(4) * pDim(3)]) ;
    
%     P.tensor2{i} = pTensor ;
    P = saveTensor2(parameter, P, pTensor, i) ;
end
P.quantNo = P.quantNo([2, 1, 4, 3], :) ;
P.dim = P.dim([2, 1, 4, 3], :) ;