function U = UtimesSqrtS(U0, S)
%* U((mi,mi'),n) = U((mi,mi'),n)*S(n))

U = rmfield(U0, 'tensor2') ;
for i = 1 : U.subNo
    tensor = U0.tensor2{i} ; %* ((mi,mi'),n)
    U0.tensor2{i} = [] ;
    xQN = U0.quantNo(3, i) ;
    
    index = find(xQN == S.quantNo) ;
    Sx = diag(S.tensor1{index}) ;
    
    tensor = tensor * Sx ;
    
    U.tensor2{i} = tensor ;
end