function [expectValue_z, T, ABR, ABL] = dealWithSpecialTensor_z(parameter, T, ABR, ABL, AA, BB, Ta, Tb)

%* T((k,l),(i,j))
T = transposeT(T) ;

PRdir = 'tensor/PR/' ;
%* PR((l,c),(k,a)) = sum{i,j}_(ABR((a,c),(i,j))*T((k,l),(i,j)))
%* change notation: PR((l,c),(i,a))
[PR, ABR, T] = contractABL_T_y(parameter, PRdir, ABR, T) ;

PLdir = 'tensor/PL/' ;
%* PL((a',i),(c',j)) = sum{k,l}_(ABL((a',c'),(k,l))*T((k,l),(i,j)))
%* change notation: PL((a',j),(c',i))
[PL, ABL, T] = contractABR_T_y(parameter, PLdir, ABL, T) ;

PRmap = rmfield(PR, 'tensor2') ;
PLmap = rmfield(PL, 'tensor2') ;
%* initialize a normalized vector(c',l,c)
vector = initializeVector_y(PLmap, PRmap) ;
vector = normalize_x(vector) ;

%* vectorRight(a',j,a) = sum{c,c',l,i}_(PL((a',j),(c',i))*vectorRight(c',(l,c))*PR((l,c),(i,a)))
[vectorRight, etaRight, PL, PR] = findDominantRightEigenVector_y(parameter, PL, PR, vector) ;

%* PR((l,c),(i,a)) -> PR((c,l),(a,i))
PR = transposeP_y(parameter, PR) ;
%* PL((a',j),(c',i)) -> PL((j,a'),(i,c'))
PL = transposeP_y(parameter, PL) ;

%* transpose: vectorRight(a',j,a) -> vectorLeft(a,j,a')
vectorLeft = transposeVector(vectorRight) ;

%* vectorLeft(c,l,c') = sum{a,a',j,i}_(PR((c,l),(a,i))*vectorLeft(a,(j,a'))*PL((j,a'),(i,c')))
[vectorLeft, etaLeft, PR, PL] = findDominantRightEigenVector_y(parameter, PR, PL, vectorLeft) ;
clear PR PL
%* QR((l,c),(k,a))    change notation: QR((l,c),(i,a))
%* QL((a',i),(c',j))  change notation: QL((a',j),(c',i))
[QR, QL, ABR, ABL] = createSpecialTensor_z(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValue_z, QL, QR] = findExpectationValue_y(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QL, QR) ;
clear QL QR

%* T((i,j),(k,l))
T = transposeT(T) ;