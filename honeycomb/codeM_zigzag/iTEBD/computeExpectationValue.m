function computeExpectationValue(parameter, wave)

%* Ta(x,l,k) Tb(x,i,j)
%* AA(x,l,k;mi,mi') BB(x,i,j;mj,mj')
[Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave) ;

Tdir = 'tensor/T/' ;
%* T((i,j),(k,l)) right transfer matrix
%* conservation law: i + j = k + l
T = createTransferMatrix(parameter, Tdir, Ta, Tb) ;

[rightMPS, leftMPS, T] = computeDominantEigenMPS(parameter, T) ;

computation = parameter.computation ;
%=======================================================================
% testDominantEigen(parameter, rightMPS)
%=======================================================================
if computation.stagSz == 1
    computeStagMag_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.Qzz == 1
    computeQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.stagQzz == 1
    computeStagQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

if computation.energy == 1
    computeEnergy(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;
end

[status, message, messageid] = rmdir('tensor', 's') ;