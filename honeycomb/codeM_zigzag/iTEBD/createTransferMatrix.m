function T = createTransferMatrix(parameter, dirName, Ta, Tb)
%* field of Ta,Tb: subNo, quantNo, dim, tensor3
%* T((i,j),(k,l)) = sum{x}_(Ta(x,l,k)*Tb(x,i,j))
%* conservation law: i + j = k + l

[status,message,messageid] = mkdir(dirName) ;
T.dirName = dirName ;
TaNo = Ta.subNo ;
n = 0 ;
for i = 1 : TaNo
    AquantNo = Ta.quantNo(:, i) ;
    j = find(AquantNo(1) == Tb.quantNo(1, :)) ;
    if ~isempty(j)
        Adim = Ta.dim(:, i) ;
        TaTensor = Ta.tensor3{i} ; %* (x,l,k)
        
        for k = 1 : length(j)
            n = n + 1 ;
            BquantNo = Tb.quantNo(:, j(k)) ;
            Bdim = Tb.dim(:, j(k)) ;
            
            TbTensor = Tb.tensor3{j(k)} ; %* (x,i,j)

            %* tensor(i,j,k,l) = sum{x}_(Ta(x,l,k)*Tb(x,i,j))
            tensor = contractTensors(TaTensor, 3, 1, TbTensor, 3, 1, [3, 4, 2, 1]) ;
            %* tensor((i,j),(k,l))
            tensor = reshape(tensor, [Bdim(2) * Bdim(3), Adim(3) * Adim(2)]) ;
            
            T.quantNo(:, n) = [BquantNo(2); BquantNo(3); AquantNo(3); AquantNo(2)] ;
            T.dim(:, n) = [Bdim(2); Bdim(3); Adim(3); Adim(2)] ;
%             T.tensor2{n} = tensor ;
            T = saveTensor2(parameter, T, tensor, n) ;
        end
    end
end
T.subNo = n ;
