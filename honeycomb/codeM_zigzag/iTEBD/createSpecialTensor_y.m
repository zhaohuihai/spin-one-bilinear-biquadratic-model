function [QR, QL, ABR, ABL] = createSpecialTensor_y(parameter, ABR, ABL, AA, BB, Ta, Tb)

%* H((mi,mi'),(mj,mj'))
%* conservation law: mi - mi' = mj - mj'
H = convertToSparseH(parameter) ;
H = partitionHby2quantNo(H) ;

%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian(parameter, H) ;

%* Wa(x,i,k)
Wa = createW_y(parameter, AA, U) ;
%* Wb(x,l,j)
Wb = createW_y(parameter, BB, V) ;

THRdir = 'tensor/THR/' ;
%* THR((i,j),(k,l))=sum{x}_(Ta(x,i,k)*Wb(x,l,j))
THR = createTransferMatrix(parameter, THRdir, Ta, Wb) ;
THLdir = 'tensor/THL/' ;
%* THL((i,j),(k,l))=sum{x}_(Wa(x,i,k)*Tb(x,l,j))
THL = createTransferMatrix(parameter, THLdir, Wa, Tb) ;

QRdir = 'tensor/QR/' ;
%* QR((a,k),(c,l))
[QR, ABR, THR] = contractABR_T_y(parameter, QRdir, ABR, THR) ;
clear THR
QLdir = 'tensor/QL/' ;
%* QL((j,c'),(i,a'))
[QL, ABL, THL] = contractABL_T_y(parameter, QLdir, ABL, THL) ;
clear THL