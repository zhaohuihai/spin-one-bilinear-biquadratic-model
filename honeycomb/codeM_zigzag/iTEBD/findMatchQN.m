function index = findMatchQN(varargin)
%* index = findMatchQN(QN, indX, yAllQN, indY)
%* optional input arguments: indX, indY

if nargin == 2
    QN = varargin{1} ;
    yAllQN = varargin{2} ;
    indX = 1 : length(QN) ;
    indY = indX ;
else
    QN = varargin{1} ;
    indX = varargin{2} ;
    yAllQN = varargin{3} ;
    indY = varargin{4} ;
end
indNo = length(indX) ;
sameInd = true(1, size(yAllQN, 2)) ;

for i = 1 : indNo
    sameInd = sameInd & (QN(indX(i)) == yAllQN(indY(i), :)) ;
end
index = find(sameInd) ;
