function [Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave)

M = parameter.siteDimension ;

Lambda = wave.Lambda ;
%* A(x, y, z) B(x, y, z)
[A, B] = absorbLambda(M, wave) ;

%* AA(x,l,k;mi,mi')
AA = createAA(M, A, Lambda) ;

%* BB(x,i,j;mj,mj')
BB = createAA(M, B, Lambda) ;

%* Ta(x,l,k)
%* conservation law: x + l + k = 0
Ta = createTa(M, AA) ;

%* Tb(x,i,j)
%* conservation law: x + i + j = 0
Tb = createTa(M, BB) ;