function [U, S, V, allS, truncationError] = doTruncation_TMP(parameter, U, S, V)

D = parameter.dim_MPS ;

Sno = S.subNo ;
allS = [] ;
for iS = 1 : Sno
    allS = [allS; S.tensor1{iS}] ;
end
allS = sort(allS, 'descend') ;
coef = allS(1) ;

retainDim = min(length(allS), D) ;
truncationError = 1 - sum(allS(1 : retainDim).^2) / sum(allS.^2) ;
SminRetain = allS(retainDim) * parameter.maxDiffRatio ;
%* truncation
for iS = 1 : Sno
    %* keep the degenerate element
    cut = find(S.tensor1{iS} < SminRetain) ;
    cutNo = length(cut) ;
    % normalization
    S.tensor1{iS} = S.tensor1{iS} ./ coef ;
    
    if ~isempty(cut)
        S.tensor1{iS}(cut) = [] ;
        S.dim(iS) = S.dim(iS) - cutNo ;
        quantNo = S.quantNo(iS) ;
        
        Uindex = find(U.quantNo(3, :) == quantNo) ;
        for i = 1 : length(Uindex)
%             U.tensor2{Uindex(i)}(:, cut) = [] ;
            Utensor = loadTensor2(parameter, U, Uindex(i)) ;
            U.dim(3, Uindex(i)) = U.dim(3, Uindex(i)) - cutNo ;
            Utensor(:, cut) = [] ;
            U.tensor2{Uindex(i)} = Utensor ;
        end
        Vindex = find(V.quantNo(3, :) == quantNo) ;
        for j = 1 : length(Vindex)
%             V.tensor2{Vindex(j)}(:, cut) = [] ;
            Vtensor = loadTensor2(parameter, V, Vindex(j)) ;
            V.dim(3, Vindex(j)) = V.dim(3, Vindex(j)) - cutNo ;
            Vtensor(:, cut) = [] ;
            V.tensor2{Vindex(j)} = Vtensor ;
        end
    end
end
allS = allS(1 : D) ./ coef ;