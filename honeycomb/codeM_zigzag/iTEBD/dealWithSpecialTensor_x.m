function [expectValue_x, T] = dealWithSpecialTensor_x(parameter, T, rightMPS, leftMPS, AA, BB)

%* do one step projection and exchange A, B
[rightMPS, T] = applyTMP(parameter, T, rightMPS) ;

%* TH((i,j), (k,l))
%* i + j = k + l
TH = createSpecialTensor_x(parameter, AA, BB) ;
%* expectValue = trace(QPPP...)
[P, Q, TH, T] = createSpecialChain_x(parameter, TH, T, rightMPS, leftMPS) ;
clear TH

Pmap = rmfield(P, 'tensor2') ;
%* initialize a normalized vector(c1, c2)
vector = initializeVector_x(Pmap) ;
vector = normalize_x(vector) ;

[vectorRight, etaRight, P] = findDominantRightEigenVector_x(parameter, P, vector) ;

P = transposeT(P) ;

[vectorLeft, etaLeft, P] = findDominantRightEigenVector_x(parameter, P, vectorRight) ;
clear P
[expectValue_x, Q] = findExpectationValue_x(parameter, vectorRight, vectorLeft, etaRight, etaLeft, Q) ;
clear Q