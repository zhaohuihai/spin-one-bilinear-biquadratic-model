function U = createU_TMP(parameter, Ublock, U, R, Sdim, UorV)
%* U((a,k),b)

twoQuantNo = R.twoQuantNo ;
s = R.subNo ;
i = 1 ;
k = [2 * UorV - 1, 2 * UorV ] ; 
quantNo = R.quantNo ;
aQuantNo = R.quantNo(k(1), :) ;
dim = R.dim ;
while s > 0
    max_aQN = max(aQuantNo) ;
    same_aQN = find(max_aQN == aQuantNo) ;
    same_aQNno = length(same_aQN) ;
    index = same_aQN(1) ;
    
    U.subNo = U.subNo + 1 ;
    %* ((a,k),b)
    U.quantNo(:, U.subNo) = [quantNo(k, index); twoQuantNo] ;
    aDim = dim(k(1), index) ;
    kDim = dim(k(2), index) ;
    U.dim(:, U.subNo) = [aDim; kDim; Sdim] ;
    akDim = aDim * kDim ;
    j = i + akDim - 1 ;
%     U.tensor2{U.subNo} = Ublock(i : j, :) ; %* ((a,k),b)
    Utensor = Ublock(i : j, :) ; %* ((a,k),b)
    U = saveTensor2(parameter, U, Utensor, U.subNo) ;
    i = j + 1 ;
    
    aQuantNo(same_aQN) = aQuantNo(same_aQN) - 1e9 ;
    s = s - same_aQNno ;
end