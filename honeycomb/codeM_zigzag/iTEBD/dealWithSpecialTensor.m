function expectValueBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

expectValueBond = zeros(1, 3) ;

[expectValueBond(1), T] = dealWithSpecialTensor_x(parameter, T, rightMPS, leftMPS, AA, BB) ;

ABRdir = 'tensor/ABR/' ;
%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2(parameter, ABRdir, rightMPS) ;
ABLdir = 'tensor/ABL/' ;
%* ABL((a',c'),(k,l))
ABL = computeA_L1_B_L2(parameter, ABLdir, leftMPS) ;

[expectValueBond(2), T, ABR, ABL] = dealWithSpecialTensor_y(parameter, T, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValueBond(3), T, ABR, ABL] = dealWithSpecialTensor_z(parameter, T, ABR, ABL, AA, BB, Ta, Tb) ;

% [status, message, messageid] = rmdir('tensor', 's') ;