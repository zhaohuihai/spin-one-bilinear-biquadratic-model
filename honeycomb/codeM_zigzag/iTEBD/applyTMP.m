function [MPS, T, TMPsingularValue, truncationError] = applyTMP(parameter, T, MPS)

ABdir = 'tensor/AB/' ;
%* AB((a,c),(i,j))
AB = computeL2_A_L1_B_L2(parameter, ABdir, MPS) ;

Rdir = 'tensor/R/' ;
%* R((a, k), (l, c)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
[R, AB, T] = projectTransferMatrix(parameter, Rdir, AB, T) ;
clear AB
R = partitionRby2quantNo(R) ;

Udir = 'tensor/U/' ;
Vdir = 'tensor/V/' ;
%* U((a,k),b), V((l,c),b)
[U, S, V, R] = svd_TMP(parameter, Udir, Vdir, R) ;
clear R
[U, S, V, TMPsingularValue, truncationError] = truncate_TMP(parameter, U, S, V) ;

MPS = recover_TMP(MPS, U, S, V) ;
clear U S V
MPS = exchangeAandB_TMP(MPS) ;