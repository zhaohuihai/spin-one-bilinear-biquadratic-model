function [P, T, ABR, ABL] = computeABR_T_ABL(parameter, Pdir, ABR, ABL, T)
%* P((a,a'),(c,c')) = ABR((a,c),(i,j))*T((i,j),(k,l))*ABL((a',c'),(k,l))

RTdir = 'tensor/RT/' ;
%* RT((a,c), (k,l)) = ABR((a,c),(i,j))*T((i,j),(k,l))
[RT, ABR, T] = contractABR_T(parameter, RTdir, ABR, T) ;

%* P((a,a'),(c,c')) = RT((a,c), (k,l))*ABL((a',c'),(k,l))
[P, RT, ABL] = contractRT_ABL(parameter, Pdir, RT, ABL) ;
clear RT