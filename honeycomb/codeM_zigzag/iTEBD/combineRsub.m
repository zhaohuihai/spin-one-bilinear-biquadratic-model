function Rblock = combineRsub(parameter, R)
%* R((a, k), (l, c))

aQuantNo = R.quantNo(1, :) ;
aDim = R.dim(1, :) .* R.dim(2, :) ;
lQuantNo = R.quantNo(3, :) ;
lDim = R.dim(3, :) .* R.dim(4, :) ;

% tensor2 = R.tensor2 ;
%* total dim
dim = zeros(1, 2) ;
[aMap, dim(1)] = createMapping(aQuantNo, aDim) ;
[lMap, dim(2)] = createMapping(lQuantNo, lDim) ;

Rblock = zeros(dim) ;

subNo = R.subNo ;

for i = 1 : subNo
    aIndex = find(aMap(1, :) == aQuantNo(i)) ;
    lIndex = find(lMap(1, :) == lQuantNo(i)) ;
    
    a = aMap(2, aIndex) : (aMap(2, aIndex) + aDim(i) - 1) ;
    l = lMap(2, lIndex) : (lMap(2, lIndex) + lDim(i) - 1) ;
    tensor = loadTensor2(parameter, R, i) ;
    Rblock(a, l) = Rblock(a, l) + tensor ;
end