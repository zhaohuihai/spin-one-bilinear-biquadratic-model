function TH = computeAA_HBB(parameter, dirName, M, AA, HBB)
%* TH((i,j),(k,l))=sum_{x,m1,m2}(AA(m1,m2)(x,l,k)*HBB(m1,m2)(x,i,j))

[status,message,messageid] = mkdir(dirName) ;
TH.dirName = dirName ;
TH.subNo = 0 ;
TH.quantNo = zeros(4, 0) ;
TH.dim = zeros(4, 0) ;
TH.tensor2 = cell(0) ;
n = 0 ;
for m1 = 1 : M
    for m2 = 1 : M
        AAno = AA(m1, m2).subNo ;
        if HBB(m1, m2).subNo == 0
            continue
        end
        for i = 1 : AAno
            AquantNo = AA(m1, m2).quantNo(:, i) ;
            j = find(AquantNo(1) == HBB(m1, m2).quantNo(1, :)) ;
            Adim = AA(m1, m2).dim(:, i) ;
            Atensor = AA(m1, m2).tensor3{i} ; %* (x,l,k)
            for k = 1 : length(j)
                BquantNo = HBB(m1, m2).quantNo(:, j(k)) ;
                Bdim = HBB(m1, m2).dim(:, j(k)) ;
                
                Btensor = HBB(m1, m2).tensor3{j(k)} ; %* (x,i,j)
                
                %* tensor(i,j,k,l)
                tensor = contractTensors(Atensor, 3, 1, Btensor, 3, 1, [3, 4, 2, 1]) ;                               
                %* tensor((i,j),(k,l))
                tensor = reshape(tensor, [Bdim(2) * Bdim(3), Adim(3) * Adim(2)]) ;
                tQN = [BquantNo(2); BquantNo(3); AquantNo(3); AquantNo(2)] ;
                
                equalT = cell(1, 3) ;
                for m = 1 : 3
                    equalT{m} = (tQN(m) == TH.quantNo(m, :)) ;
                end
                index = find(equalT{1} & equalT{2} & equalT{3}) ;
                
                if isempty(index)
                    n = n + 1 ;
                    TH.quantNo(:, n) = tQN ;
                    TH.dim(:, n) = [Bdim(2); Bdim(3); Adim(3); Adim(2)] ;
%                     TH.tensor2{n} = tensor ;
                    TH = saveTensor2(parameter, TH, tensor, n) ;
                else
                    THtensor = loadTensor2(parameter, TH, index) ;
                    tensor = THtensor + tensor ;
%                     TH.tensor2{index} = TH.tensor2{index} + tensor ;
                    TH = saveTensor2(parameter, TH, tensor, index) ;
                end
            end
        end
    end
end
TH.subNo = n ;