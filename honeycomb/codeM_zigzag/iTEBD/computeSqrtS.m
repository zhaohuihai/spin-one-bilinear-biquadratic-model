function sqrtS = computeSqrtS(S)

sqrtS = rmfield(S, 'tensor1') ;

for j = 1 : S.subNo
    tensor1 = S.tensor1{j} ;
    sqrtS.tensor1{j} = sqrt(tensor1) ;
end
