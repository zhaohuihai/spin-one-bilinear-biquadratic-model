function [U, S, V, TMPsingularValue, truncationError] = truncate_TMP(parameter, U, S, V)


[U, S, V, TMPsingularValue, truncationError] = doTruncation_TMP(parameter, U, S, V) ;

U = loadAll(U) ;
V = loadAll(V) ;

[U, S, V] = eliminateEmptyElement_TMP(U, S, V) ;

