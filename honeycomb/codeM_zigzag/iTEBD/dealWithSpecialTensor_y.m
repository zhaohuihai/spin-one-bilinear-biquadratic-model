function [expectValue_y, T, ABR, ABL] = dealWithSpecialTensor_y(parameter, T, ABR, ABL, AA, BB, Ta, Tb)
%* T((i,j),(k,l))

PRdir = 'tensor/PR/' ;
%* PR((a,k),(c,l)) = sum{i,j}_(ABR((a,c),(i,j))*T((i,j),(k,l)))
%* change notation: PR((a,j),(c,i))
[PR, ABR, T] = contractABR_T_y(parameter, PRdir, ABR, T) ;

PLdir = 'tensor/PL/' ;
%* PL((j,c'),(i,a')) = sum{k,l}_(ABL((a',c'),(k,l))*T((i,j),(k,l)))
%* change notation: PL((l,c'),(i,a'))
[PL, ABL, T] = contractABL_T_y(parameter, PLdir, ABL, T) ;

PRmap = rmfield(PR, 'tensor2') ;
PLmap = rmfield(PL, 'tensor2') ;
%* initialize a normalized vector(c,l,c')
vector = initializeVector_y(PRmap, PLmap) ;
vector = normalize_x(vector) ;

%* vectorRight(a,j,a')
[vectorRight, etaRight, PR, PL] = findDominantRightEigenVector_y(parameter, PR, PL, vector) ;

%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
PR = transposeP_y(parameter, PR) ;
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))
PL = transposeP_y(parameter, PL) ;

%* transpose: vectorRight(a,j,a') -> vectorLeft(a',j,a)
vectorLeft = transposeVector(vectorRight) ;

%* vectorLeft(c',l,c)
[vectorLeft, etaLeft, PL, PR] = findDominantRightEigenVector_y(parameter, PL, PR, vectorLeft) ;
clear PL PR

%* QR((a,k),(c,l))    change notation: QR((a,j),(c,i))
%* QL((j,c'),(i,a'))  change notation: QL((l,c'),(i,a'))
[QR, QL, ABR, ABL] = createSpecialTensor_y(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

[expectValue_y, QR, QL] = findExpectationValue_y(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL) ;
clear QR QL