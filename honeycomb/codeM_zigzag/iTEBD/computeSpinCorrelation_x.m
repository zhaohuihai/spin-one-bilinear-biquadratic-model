function [correlation, correlationM] = computeSpinCorrelation_x(parameter, wave)
%* correlation is maxCorrelationLength X 1 column vector

%* Ta(x,l,k) Tb(x,i,j)
%* AA(x,l,k;mi,mi') BB(x,i,j;mj,mj')
[Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave) ;

Tdir = 'tensor/T/' ;
%* T((i,j),(k,l)) right transfer matrix
%* conservation law: i + j = k + l
T = createTransferMatrix(parameter, Tdir, Ta, Tb) ;

[rightMPS, leftMPS, ~] = computeDominantEigenMPS(parameter, T) ;

%*=============================================================================
ABRdir = 'tensor/ABR/' ;
%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2(parameter, ABRdir, rightMPS) ;
ABLdir = 'tensor/ABL/' ;
%* ABL((a',c'),(k,l))
ABL = computeA_L1_B_L2(parameter, ABLdir, leftMPS) ;

%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)
[VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, ABR, ABL, T) ;
%*=============================================================================
%* QR(j,a,i,c,n), QL(c',l,a',i,n)
[QR, QL] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb) ;

L = parameter.computation.maxCorrelationLength ;
L = L + 2 ;
correlation = zeros(L, 1) ;

%* normFactor = sum{c',l,c}_[VL(c',l,c)*VR(c,l,c')]
normFactor = contract2vector(VL, [1, 2, 3], VR, [3, 2, 1]) ;

coef = 1 ;

for i = 1 : L
    %* distance <= 1: VL(a',j,a); distance >= 2: VL(a',j,a,n)
    if i == 2
        [VL, c] = updateVector(VL, 3, QR, 5, PL, 4) ;
        coef = coef * c ;
    elseif mod(i, 2) == 0
        [VL, c] = updateVector(VL, 4, PR, 4, PL, 4) ;
        coef = coef * c ;
    end
    [correlation(i)] = findSpinCorrelation(i, QR, QL, VR, VL, eta, PR, PL, coef, normFactor) ;
end
%===================================================================================================
correlationM = zeros(L, 1) ;
sc1 = correlation(1 : 2 : L) ;
n1 = length(sc1) ;
correlationM(1 : 2 : L) = correlation(1 : 2 : L) - sc1(n1) ;

sc2 = correlation(2 : 2 : L) ;
n2 = length(sc2) ;
correlationM(2 : 2 : L) = correlation(2 : 2 : L) - sc2(n2) ;

correlation = correlation(1 : (L - 2)) ;
correlationM = correlationM(1 : (L - 2)) ;

