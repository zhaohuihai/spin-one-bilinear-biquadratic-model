function [MPS, TMPsingularValue1, T] = findDominantRightEigenMPS(parameter, T, MPS, TMPsingularValue0)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;
Tmapping = rmfield(T, 'tensor2') ;
%* MPS: A B Lambda(1) Lambda(2)
[MPS, TMPsingularValue0] = initializeMPS(parameter, Tmapping) ;

TMPstep = 0 ;
TMPconvergence = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
while TMPconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, T] = applyTMP(parameter, T, MPS) ;
    [MPS, T, TMPsingularValue1, truncationError] = applyTMP(parameter, T, MPS) ;
    toc
    if j > 1
        TMPconvergence = norm(TMPsingularValue0 - TMPsingularValue1) / sqrt(D - 1) ;
    end
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMPconvergence)])
        disp(['truncation error = ', num2str(truncationError)])
    end
    TMPsingularValue0 = TMPsingularValue1 ;
end

while parameter.dim_MPS < maxDim
    TMPstep = TMPstep + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, T] = applyTMP(parameter, T, MPS) ;
    [MPS, T, TMPsingularValue1] = applyTMP(parameter, T, MPS) ;
    toc
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;
TMPconvergence = 1 ;
D = parameter.dim_MPS ;
j = 0 ;
while TMPconvergence >= parameter.convergenceCriterion_TMprojection && j <= parameter.maxTMstep
    TMPstep = TMPstep + 1 ;
    j = j + 1 ;
    disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP step = ', num2str(TMPstep)]) ;
    tic
    [MPS, T] = applyTMP(parameter, T, MPS) ;
    [MPS, T, TMPsingularValue1, truncationError] = applyTMP(parameter, T, MPS) ;
    toc
    if j > 1
        TMPconvergence = norm(TMPsingularValue0 - TMPsingularValue1) / sqrt(D - 1) ;
    end
    flag = TMPstep / 1 ;
    if flag == floor(flag)
        disp(['TMP convergence error = ', num2str(TMPconvergence)])
        disp(['truncation error = ', num2str(truncationError)])
    end
    TMPsingularValue0 = TMPsingularValue1 ;
end
disp(['dim MPS = ', num2str(parameter.dim_MPS), ' TMP steps = ', num2str(j)]) ;
disp(['total TMP steps = ', num2str(TMPstep)]) ;

%*********************************************************************************************
% TMPstep = 0 ;
% while parameter.dim_MPS <= maxDim
%
%     TMPconvergence = 1 ;
%     D = parameter.dim_MPS ;
%     j = 0 ;
%     while TMPconvergence >= parameter.convergenceCriterion_TMprojection
%         TMPstep = TMPstep + 1 ;
%         j = j + 1 ;
% %         disp(['TMP step = ', num2str(TMPstep)]) ;
% %         tic
%         [MPS, T] = applyTMP(parameter, T, MPS) ;
%         [MPS, T, TMPsingularValue1] = applyTMP(parameter, T, MPS) ;
% %         toc
%         if j > 1
%             TMPconvergence = norm(TMPsingularValue0 - TMPsingularValue1) / sqrt(D - 1) ;
%         end
%         flag = TMPstep / 100 ;
%         if flag == floor(flag)
%             disp(['TMP convergence error = ', num2str(TMPconvergence)])
%         end
%         TMPsingularValue0 = TMPsingularValue1 ;
%     end
%     disp(['dim MPS = ', num2str(parameter.dim_MPS), ', TMP steps = ', num2str(j)]) ;
%     parameter.dim_MPS = parameter.dim_MPS + 20 ;
% end
% disp(['total TMP steps = ', num2str(TMPstep)]) ;