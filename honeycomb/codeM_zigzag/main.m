clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

if parameter.loadPreviousWave == 0
    wave = initializeWave(parameter) ;
    disp('create new wave')
else
    [wave, parameter] = loadWave(parameter) ;
end

if parameter.projection == 1
    wave = findGroundStateWave(parameter, wave) ;
end

parameter.polarization = 0 ;
% parameter.theta = 0 ;
% computeExpectationValue(parameter, wave)

%* spin-spin correlation <S(i)*S(i+l)>
if parameter.computation.spinCorrelation == 1
    spinCorrelation = computeSpinCorrelation(parameter, wave) ;
end
