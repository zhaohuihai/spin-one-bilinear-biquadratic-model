function [wave, truncationError, coef] = applySecondTrotter(parameter, wave)

projectionSingularValue0 = wave.projectionSingularValue1 ;
tauSaved = parameter.tau ;

parameter.tau = tauSaved / 2 ;

c = zeros(1, 5) ;
truncErr_single = zeros(1, 5) ;
%* exp(- (tau / 2) * Hx)
[wave, projectionSingularValue, truncErr_single(1), c(1)] = projectBy1operator(parameter, wave) ;
wave = rotate(parameter, wave) ;

%* exp(- (tau / 2) * Hy)
[wave, ~, truncErr_single(2), c(2)] = projectBy1operator(parameter, wave) ;
wave = rotate(parameter, wave) ;

%* exp(- tau * Hz)
parameter.tau = tauSaved ;
[wave, ~, truncErr_single(3), c(3)] = projectBy1operator(parameter, wave) ;
wave = reverseRotate(parameter, wave) ;

%* exp(- (tau / 2) * Hy)
parameter.tau = tauSaved / 2 ;
[wave, ~, truncErr_single(4), c(4)] = projectBy1operator(parameter, wave) ;
wave = reverseRotate(parameter, wave) ;

%* exp(- (tau / 2) * Hx)
[wave, ~, truncErr_single(5), c(5)] = projectBy1operator(parameter, wave) ;


wave.projectionSingularValue0 = projectionSingularValue0 ;
wave.projectionSingularValue1 = projectionSingularValue ;

truncationError = max(truncErr_single) ;
coef = 1 ;
for i = 1 : 5
    coef = coef * c(i) ;
end
