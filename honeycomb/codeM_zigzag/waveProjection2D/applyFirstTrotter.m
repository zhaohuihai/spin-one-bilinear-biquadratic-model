function wave = applyFirstTrotter(parameter, wave)

projectionSingularValue0 = wave.projectionSingularValue1 ;
for j = 1 : 3
    
    [wave, projectionSingularValue] = projectBy1operator(parameter, wave) ;
    
    wave = rotate(parameter, wave) ;
end

wave.projectionSingularValue0 = projectionSingularValue0 ;
wave.projectionSingularValue1 = projectionSingularValue ;