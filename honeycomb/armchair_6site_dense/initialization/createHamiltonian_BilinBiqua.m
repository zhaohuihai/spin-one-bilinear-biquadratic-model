function H = createHamiltonian_BilinBiqua(parameter)
%* Bilinear biquadratic model Hamiltonian

theta = parameter.theta ;

H_bl = createHamiltonian_Heisenberg(parameter) ;

H = cos(theta) * H_bl + sin(theta) * (H_bl * H_bl) ;

H = setSmalltoZero(H) ;

if parameter.polarization == 1
    if parameter.dipolarField_x ~= 0
        disp('dipolarize X')
        H_polar = createSx(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.dipolarField_z ~= 0
        disp('dipolarize Z')
        H_polar = createSz(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.stagDipolarField_x ~= 0
        disp('stagger dipolarize X')
        H_polar = createStagSx(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.stagDipolarField_z ~= 0
        disp('stagger dipolarize Z')
        H_polar = createStagSz(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.quadrupolarField_x ~= 0
        disp('quadrupolarize X')
        H_polar = createQxx(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.quadrupolarField_z ~= 0
        disp('quadrupolarize Z')
        H_polar = createQzz(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.stagQuadrupolarField_x ~= 0
        disp('stagger quadrupolarize X')
        H_polar = createStagQxx(parameter) ;
        H = H - H_polar * parameter.field ;
        
    elseif parameter.stagQuadrupolarField_z ~= 0
        disp('stagger quadrupolarize Z')
        H_polar = createStagQzz(parameter) ;
        H = H - H_polar * parameter.field ;
    elseif parameter.stagQuadrupolarField_xx_yy ~= 0
        disp('stagger quadrupolarize XX-YY')
        H_polar = createStagQxx_yy(parameter) ;
        H = H - H_polar * parameter.field ;
    end
    disp(['field = ', num2str(parameter.field)]) ;
end