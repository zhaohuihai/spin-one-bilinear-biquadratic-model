function wave = initializeWave(parameter)
% 6 sites per unit cell
%* x. Lambda{1}: 1-2; Lambda{2}: 3-6; Lambda{3}: 5-4
%* y. Lambda{4}: 1-6; Lambda{5}: 3-4; Lambda{6}: 5-2
%* z. Lambda{7}: 1-4; Lambda{8}: 3-2; Lambda{9}: 5-6
M = parameter.siteDimension ;
D = parameter.bondDimension ;
if parameter.sameInitialTensor == 1
    A = rand(D, D, D, M) ;
    L = sort(rand(D, 1), 'descend') ;
    L = L ./ L(1) ;
    for i = 1 : 6
        wave.A{i} = A ;
    end
    for i = 1 : 9
        wave.Lambda{i} = L ;
    end
else %* == 0
    for i = 1 : 6
        wave.A{i} = rand(D, D, D, M) ;
    end
    for i = 1 : 9
        L = sort(rand(D, 1), 'descend') ;
        wave.Lambda{i} = L ./ L(1) ;
    end
end
