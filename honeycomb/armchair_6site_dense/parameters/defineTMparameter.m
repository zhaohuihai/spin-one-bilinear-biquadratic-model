function parameter = defineTMparameter(parameter)

%* dimension of MPS
parameter.dim_MPS = 10 ;
parameter.dim_MPS_initial = 4 ;
parameter.dim_MPS_incre = 2 ;

parameter.convergenceCriterion_TMprojection = 1e-11 ;
parameter.convergenceCriterion_power = 1e-11 ;

parameter.maxTMstep = 1e3 ;
parameter.maxPowerStep = 1e3 ;
