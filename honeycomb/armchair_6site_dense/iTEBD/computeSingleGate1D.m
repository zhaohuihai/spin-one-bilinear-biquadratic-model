function [MPS, coef, truncationError] = computeSingleGate1D(parameter, T, MPS)

%* AB((a,c),(i,j))
AB = computeL2_A_L1_B_L2_dense(parameter, MPS) ;
%* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))
R = projectTransferMatrix_dense(parameter, T, AB) ;

[U, S, V, coef] = svd_TMP_dense(R) ;
[U, S, V, truncationError] = truncate_TMP_dense(parameter, U, S, V) ;

MPS = recover_TMP_dense(parameter, MPS, U, S, V) ;

MPS = exchangeAandB_TMP_dense(MPS) ;