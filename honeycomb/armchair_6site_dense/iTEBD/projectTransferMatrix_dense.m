function R = projectTransferMatrix_dense(parameter, T, AB)
%* R((a, k), (c, l)) = sum{i,j}_(AB((a,c),(i,j))*T((i,j),(k,l)))

d = parameter.bondDimension^2 ;
D = round(sqrt(size(AB, 1))) ;

%* R((a,c),(k,l))
R = AB * T ;
%* R(a,c,k,l)
R = reshape(R, [D, D, d, d]) ;

%* R(a,k,c,l)
R = permute(R, [1, 3, 2, 4]) ;

%* R((a, k), (c, l))
R = reshape(R, [D * d, D * d]) ;
