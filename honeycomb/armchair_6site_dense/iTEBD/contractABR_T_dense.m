function PR = contractABR_T_dense(parameter, ABR, T)
%* PR((a,k),(c,l)) = sum{i,j}_(ABR((a,c),(i,j))*T((i,j),(k,l)))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

%* PR((a,c),(k,l))
PR = ABR * T ;

%* PR(a,c,k,l)
PR = reshape(PR, [D, D, d, d]) ;

%* PR(a,k,c,l)
PR = permute(PR, [1, 3, 2, 4]) ;

%* PR((a,k),(c,l))
PR = reshape(PR, [D * d, D * d]) ;