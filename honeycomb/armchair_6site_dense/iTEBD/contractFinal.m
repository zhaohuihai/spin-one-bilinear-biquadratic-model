function correlation = contractFinal(VL, numindVL, PR, numindPR, PL, numindPL, VR, ~)
%* VR(c,l,c') 

%* VL(c',l,c)
[VL, coef] = updateVector(VL, numindVL, PR, numindPR, PL, numindPL) ;

%* correlation = sum{c',l,c}_[VL(c',l,c)*VR(c,l,c')]
correlation = contractTensors(VL, 3, [1, 2, 3], VR, 3, [3, 2, 1]) ;

correlation = correlation * coef ;