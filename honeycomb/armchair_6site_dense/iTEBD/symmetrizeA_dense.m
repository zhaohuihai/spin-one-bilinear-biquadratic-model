function A = symmetrizeA_dense(A)

%* A1(y,z,x,m)
A1 = permute ( A, [ 2, 3, 1, 4 ] ) ;
%* A2(z,x,y,m)
A2 = permute ( A, [ 3, 1, 2, 4 ] ) ;

A = (A + A1 + A2) / 3 ;