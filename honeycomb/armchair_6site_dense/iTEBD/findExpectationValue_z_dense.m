function expectValue = findExpectationValue_z_dense(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL)
%* QR((l,c),(in,a))
%* QL((a',j),(c',in))
%* vectorRight(c',l,c)
%* vectorLeft(a,j,a')

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
N = parameter.siteDimension^2 ;

eta = (etaRight + etaLeft) / 2 ;

%* VR(c',(l,c))
VR = reshape(vectorRight, [D, d * D]) ;

%* h(c',(in,a)) = sum{l,c}_(VR(c',(l,c))*QR((l,c),(in,a)))
h = VR * QR ;
%* h((c',in),a)
h = reshape(h, [D*d*N, D]) ;
%* h((a',j),a) = sum{c',in}_(QL((a',j),(c',in))*h((c',in),a))
h = QL * h ;
%* h(a',j,a) col
h = reshape(h, [D*d*D, 1]) ;

%* vectorLeft(a,j,a') col
vectorLeft = reshape(vectorLeft, [D, d, D]) ;
%* vectorLeft(a',j,a)
vectorLeft = permute(vectorLeft, [3, 2, 1]) ;
%* vectorLeft(a',j,a) arr
vectorLeft = reshape(vectorLeft, [1, D*d*D]) ;

h = vectorLeft * h ;
g = eta .* vectorLeft * vectorRight ;

expectValue = h / g ;