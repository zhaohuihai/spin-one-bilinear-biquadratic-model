function T = transposeTransferMatrix1D(T0)

T = cell(1, 3) ;

j = 3 ;
for i = 1 : 3
    T{j} = T0{i}' ;
    j = j - 1 ;
end