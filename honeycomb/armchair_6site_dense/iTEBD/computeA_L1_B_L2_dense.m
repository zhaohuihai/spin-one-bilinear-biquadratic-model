function AB = computeA_L1_B_L2_dense(parameter, MPS)
%* AB((a,c),(i,j)) = sum{b}_(A(a,b,i)*Lambda1(b)*B(b,c,j)*Lambda2(c))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;

A = MPS.A ;
B = MPS.B ;


Lambda = MPS.Lambda ;
%* A(a,b,i) = A(a,b,i)*Lambda1(b)
for b = 1 : D
    A(:, b, :) = A(:, b, :) * Lambda{1}(b) ;
end
%* B(b,c,j) = B(b,c,j)*Lambda2(c)
for c = 1 : D
    B(:, c, :) = B(:, c, :) * Lambda{2}(c) ;
end


%* A(a,i,b)
A = permute(A, [1, 3, 2]) ;
%* A((a,i),b)
A = reshape(A, [D * d, D]) ;

%* B(b,(c,j))
B = reshape(B, [D, D * d]) ;

%* AB((a,i),(c,j))
AB = A * B ;

%* AB(a,i,c,j)
AB = reshape(AB, [D, d, D, d]) ;
%* AB(a,c,i,j)
AB = permute(AB, [1, 3, 2, 4]) ;
%* AB((a,c),(i,j))
AB = reshape(AB, [D^2, d^2]) ;
