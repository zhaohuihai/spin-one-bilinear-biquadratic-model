function expectValueBond = dealWithSpecialTensor_z_dense(parameter, T, rightMPS, leftMPS, A, t)

rightMPS = computeSingleGate1D(parameter, T{3}, rightMPS) ;
rightMPS_3 = computeSingleGate1D(parameter, T{2}, rightMPS) ;
rightMPS_2 = computeSingleGate1D(parameter, T{1}, rightMPS_3) ;
rightMPS_1 = computeSingleGate1D(parameter, T{3}, rightMPS_2) ;

leftMPS_1 = leftMPS ;
leftMPS_2 = computeSingleGate1D(parameter, T{1}', leftMPS_1) ;
leftMPS_3 = computeSingleGate1D(parameter, T{2}', leftMPS_2) ;

expectValueBond(1) = contractSpecialTensor_z(parameter, T([1, 2]), rightMPS_1, leftMPS_1, A{3}, A{2}, t{1}, t{6}) ;
expectValueBond(2) = contractSpecialTensor_z(parameter, T([2, 3]), rightMPS_2, leftMPS_2, A{5}, A{6}, t{3}, t{4}) ;
expectValueBond(3) = contractSpecialTensor_z(parameter, T([3, 1]), rightMPS_3, leftMPS_3, A{1}, A{4}, t{5}, t{2}) ;
