function T = createTransferMatrix_dense(parameter, t)
%* T((i,j),(k,l)) = sum{x}_(t1(x,i,k) * t2(x,l,j))

d = parameter.bondDimension^2 ;

% T(i,j,k,l)
T = contractTensors(t{1}, 3, 1, t{2}, 3, 1, [1, 4, 2, 3]) ;

%* T((i,j),(k,l))
T = reshape(T, [d^2, d^2]) ;

