function [MPS, coef, truncationError] = applyRightGateOperation1D(parameter, T, MPS)
% T{1},T{2},T{3}

c = zeros(1, 6) ;
truncErr_single = zeros(1, 6) ;

n = 1 ;
for j = 1 : 2
    for i = 3 : (-1) : 1
        [MPS, c(n), truncErr_single(n)] = computeSingleGate1D(parameter, T{i}, MPS) ;
        n = n + 1 ;
    end
end

truncationError = max(truncErr_single) ;
coef = prod(c) ;
