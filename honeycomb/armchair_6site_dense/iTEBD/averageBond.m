function averageBond(parameter, expectValueBond)

bondNo = length(expectValueBond) ;
computation = parameter.computation ;
%* spontaneous magnetization
if computation.Sx == 1
    Sx = 0 ;
    for i = 1 : bondNo
        Sx = Sx + expectValueBond(i).Sx / bondNo ;
    end
    saveExpectationValue(parameter, Sx, 'Sx') ;
end
if computation.Sy == 1
    Sy = 0 ;
    for i = 1 : bondNo
        Sy = Sy + expectValueBond(i).Sy / bondNo ;
    end
    saveExpectationValue(parameter, Sy, 'Sy') ;
end
if computation.Sz == 1
    Sz = 0 ;
    for i = 1 : bondNo
        Sz = Sz + expectValueBond(i).Sz / bondNo ;
    end
    saveExpectationValue(parameter, Sz, 'Sz') ;
end
%* staggered spontaneous magnetization
if computation.stagSx == 1
    stagSx = 0 ;
    for i = 1 : bondNo
        stagSx = stagSx + expectValueBond(i).stagSx / bondNo ;
    end
    saveExpectationValue(parameter, stagSx, 'stagSx') ;
end
if computation.stagSy == 1
    stagSy = 0 ;
    for i = 1 : bondNo
        stagSy = stagSy + expectValueBond(i).stagSy / bondNo ;
    end
    saveExpectationValue(parameter, stagSy, 'stagSy') ;
end
if computation.stagSz == 1
    stagSz = 0 ;
    for i = 1 : bondNo
        stagSz = stagSz + expectValueBond(i).stagSz / bondNo ;
    end
    saveExpectationValue(parameter, stagSz, 'stagSz') ;
end
%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Qxx == 1
    Qxx = 0 ;
    for i = 1 : bondNo
        Qxx = Qxx + expectValueBond(i).Qxx / bondNo ;
    end
    saveExpectationValue(parameter, Qxx, 'Qxx') ;
end
if computation.Qyy == 1
    Qyy = 0 ;
    for i = 1 : bondNo
        Qyy = Qyy + expectValueBond(i).Qyy / bondNo ;
    end
    saveExpectationValue(parameter, Qyy, 'Qyy') ;
end
if computation.Qzz == 1
    Qzz = 0 ;
    for i = 1 : bondNo
        Qzz = Qzz + expectValueBond(i).Qzz / bondNo ;
    end
    saveExpectationValue(parameter, Qzz, 'Qzz') ;
end
if computation.Qxy == 1
    Qxy = 0 ;
    for i = 1 : bondNo
        Qxy = Qxy + expectValueBond(i).Qxy / bondNo ;
    end
    saveExpectationValue(parameter, Qxy, 'Qxy') ;
end
if computation.Qyz == 1
    Qyz = 0 ;
    for i = 1 : bondNo
        Qyz = Qyz + expectValueBond(i).Qyz / bondNo ;
    end
    saveExpectationValue(parameter, Qyz, 'Qyz') ;
end
if computation.Qzx == 1
    Qzx = 0 ;
    for i = 1 : bondNo
        Qzx = Qzx + expectValueBond(i).Qzx / bondNo ;
    end
    saveExpectationValue(parameter, Qzx, 'Qzx') ;
end
%* staggered quadrupole
if computation.stagQxx == 1
    stagQxx = 0 ;
    for i = 1 : bondNo
        stagQxx = stagQxx + expectValueBond(i).stagQxx / bondNo ;
    end
    saveExpectationValue(parameter, stagQxx, 'stagQxx') ;
end
if computation.stagQyy == 1
    stagQyy = 0 ;
    for i = 1 : bondNo
        stagQyy = stagQyy + expectValueBond(i).stagQyy / bondNo ;
    end
    saveExpectationValue(parameter, stagQyy, 'stagQyy') ;
end
if computation.stagQzz == 1
    stagQzz = 0 ;
    for i = 1 : bondNo
        stagQzz = stagQzz + expectValueBond(i).stagQzz / bondNo ;
    end
    saveExpectationValue(parameter, stagQzz, 'stagQzz') ;
end
if computation.stagQxx_yy == 1
    stagQxx_yy = 0 ;
    for i = 1 : bondNo
        stagQxx_yy = stagQxx_yy + expectValueBond(i).stagQxx_yy / bondNo ;
    end
    saveExpectationValue(parameter, stagQxx_yy, 'stagQxx_yy') ;
end
if computation.stagQxy == 1
    stagQxy = 0 ;
    for i = 1 : bondNo
        stagQxy = stagQxy + expectValueBond(i).stagQxy / bondNo ;
    end
    saveExpectationValue(parameter, stagQxy, 'stagQxy') ;
end
if computation.stagQyz == 1
    stagQyz = 0 ;
    for i = 1 : bondNo
        stagQyz = stagQyz + expectValueBond(i).stagQyz / bondNo ;
    end
    saveExpectationValue(parameter, stagQyz, 'stagQyz') ;
end
if computation.stagQzx == 1
    stagQzx = 0 ;
    for i = 1 : bondNo
        stagQzx = stagQzx + expectValueBond(i).stagQzx / bondNo ;
    end
    saveExpectationValue(parameter, stagQzx, 'stagQzx') ;
end
% energy computation
if computation.energy == 1
    energyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyBond(i) = expectValueBond(i).energy ;
    end
    energyBond
    energy = mean(energyBond) ;
    energy = energy * 1.5 ;
    saveExpectationValue(parameter, energy, 'energy') ;
end