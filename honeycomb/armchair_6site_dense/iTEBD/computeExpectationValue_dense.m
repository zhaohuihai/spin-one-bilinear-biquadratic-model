function computeExpectationValue_dense(parameter, wave)
%* compute expectation value of local operator

%* (x, y, z, m)
A = absorbLambda_dense(parameter, wave) ;

t = cell(1, 6) ;
%* t1,t3,t5: (x,i,k); t2,t4,t6: (x,l,j)
for i = 1 : 6
    %* t((x,x'),(y,y'),(z,z')) = sum{m}_[A(x,y,z,m)*A(x',y',z',m)]
    t{i} = computeTensorProductA_A_dense(parameter, A{i}) ;
end

T = cell(1, 3) ;
%* T((i,j),(k,l)) right transfer matrix(from bottom to top)
T{1} = createTransferMatrix_dense(parameter, t([1, 2])) ;
T{2} = createTransferMatrix_dense(parameter, t([3, 6])) ;
T{3} = createTransferMatrix_dense(parameter, t([5, 4])) ;


[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, T) ;

%*=========================================================================================
expectValueBond([1, 2, 3]) = dealWithSpecialTensor_x_dense(parameter, T, rightMPS, leftMPS, A) ;

expectValueBond([4, 5, 6]) = dealWithSpecialTensor_y_dense(parameter, T, rightMPS, leftMPS, A, t) ;

expectValueBond([7, 8, 9]) = dealWithSpecialTensor_z_dense(parameter, T, rightMPS, leftMPS, A, t) ;

% print the 9 bonds values and calculate average value of the 9 bonds
averageBond(parameter, expectValueBond) ;