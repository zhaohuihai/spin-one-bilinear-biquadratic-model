function [U, S, V, coef] = svd_TMP_dense(R)

[U, S, V] = svd(R) ;
S = diag(S) ;
coef = S(1) ;
S = S ./ coef ;
