function [rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, T)

% project from bottom to top
rightMPS = findDominantRightEigenMPS_dense(parameter, T) ;

%* transpose to left transferMatrix
T = transposeTransferMatrix1D(T) ;

leftMPS = findDominantRightEigenMPS_dense(parameter, T) ;