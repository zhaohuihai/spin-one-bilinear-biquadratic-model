function MPS = initializeMPS_dense(parameter)

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* A(a, b, i)
A = rand(D, D, d) ;
%* B(b, c, j)
B = A ;

MPS.A = A ;
MPS.B = B ;

Lambda = cell(1, 2) ;
Lambda{1} = rand(D, 1) ;
Lambda{2} = Lambda{1} ;
MPS.Lambda = Lambda ;