function expectValueBond = dealWithSpecialTensor_x_dense(parameter, T, rightMPS, leftMPS, A)

for i = 3 : (-1) : 1
    rightMPS = computeSingleGate1D(parameter, T{i}, rightMPS) ;
end

rightMPS_3 = rightMPS ;
rightMPS_2 = computeSingleGate1D(parameter, T{3}, rightMPS_3) ;
rightMPS_1 = computeSingleGate1D(parameter, T{2}, rightMPS_2) ;

leftMPS_1 = leftMPS ;
leftMPS_2 = computeSingleGate1D(parameter, T{1}', leftMPS_1) ;
leftMPS_3 = computeSingleGate1D(parameter, T{2}', leftMPS_2) ;

expectValueBond(1) = contractSpecialTensor_x(parameter, T{1}, rightMPS_1, leftMPS_1, A{1}, A{2}) ;
expectValueBond(2) = contractSpecialTensor_x(parameter, T{2}, rightMPS_2, leftMPS_2, A{3}, A{6}) ;
expectValueBond(3) = contractSpecialTensor_x(parameter, T{3}, rightMPS_3, leftMPS_3, A{5}, A{4}) ;