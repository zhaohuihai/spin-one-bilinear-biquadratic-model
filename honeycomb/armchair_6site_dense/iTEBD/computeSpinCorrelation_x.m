function [correlation, correlationM] = computeSpinCorrelation_x(parameter, wave)
%* correlation is maxCorrelationLength X 1 column vector

%* A(x, y, z, mi) B(x, y, z, mj)
[A, B] = absorbLambda_dense(parameter, wave) ;

%* Ta(x,i,k) 
Ta = computeTensorProductA_A_dense(parameter, A) ;
%* Tb(x,l,j)
Tb = computeTensorProductA_A_dense(parameter, B) ;

%* T((i,j),(k,l)) right transfer matrix
T = createTransferMatrix_dense(parameter, Ta, Tb) ;

[rightMPS, leftMPS] = computeDominantEigenMPS_dense(parameter, T) ;

%*=============================================================================
%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2_dense(parameter, rightMPS) ;
%* ABL((a',c'),(k,l))
ABL = computeA_L1_B_L2_dense(parameter, leftMPS) ;

%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)
[VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, ABR, ABL, T) ;
%*=============================================================================
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)
[QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, ABR, ABL, A, B, Ta, Tb) ;

L = parameter.computation.maxCorrelationLength ;
L = L + 2 ;
correlation = zeros(L, 1) ;

normFactor = contractTensors(VL, 3, [1, 2, 3], VR, 3, [3, 2, 1]) ;

coef = 1 ;

for i = 1 : L
    %* distance <= 3: VL(a',j,a); distance >= 4: VL(a',j,a,n)
    if i == 4
        [VL, c] = updateVector(VL, 3, QRa, 5, PL, 4) ;
        coef = coef * c ;
    elseif mod(i, 4) == 0
        [VL, c] = updateVector(VL, 4, PR, 4, PL, 4) ;
        coef = coef * c ;
    end
    [correlation(i)] = findSpinCorrelation(i, QR, QRa, QRb, QLa, QLb, VR, VL, eta, PR, PL, coef, normFactor) ;
end
%===================================================================================================
correlationM = zeros(L, 1) ;
sc1 = correlation(1 : 2 : L) ;
n1 = length(sc1) ;
correlationM(1 : 2 : L) = correlation(1 : 2 : L) - sc1(n1) ;

sc2 = correlation(2 : 2 : L) ;
n2 = length(sc2) ;
correlationM(2 : 2 : L) = correlation(2 : 2 : L) - sc2(n2) ;

correlation = correlation(1 : (L - 2)) ;
correlationM = correlationM(1 : (L - 2)) ;

