function [QL, THL] = contractABL_TH_dense(parameter, ABL, THL)
%* QL((j,c'),(in,a'))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
N = parameter.siteDimension^2 ;

%* QL((a',c'),(in,j)) = sum{k,l}_(ABL((a',c'),(k,l))*THL((k,l),(in,j)))
QL = ABL * THL ;

%* QL(a',c',in,j)
QL = reshape(QL, [D, D, d*N, d]) ;

%* QL(j,c',in,a')
QL = permute(QL, [4, 2, 3, 1]) ;

%* QL((j,c'),(in,a'))
QL = reshape(QL, [d*D, d*N*D]) ;