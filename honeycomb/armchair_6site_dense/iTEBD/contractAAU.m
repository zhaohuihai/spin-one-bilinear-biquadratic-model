function TCa = contractAAU(parameter, A, U)
%* TCa((x,x'),(y,y'),(z,z'),n) = sum{mi,mi'}_[A(x,y,z,mi)*A(x',y',z',mi')*U((mi,mi'),n)]

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = M^2 ;
%* A((x,y,z),mi)
A = reshape(A, [D^3, M]) ;
%* AA((x,y,z,x',y',z'),(mi,mi'))
AA = kron(A, A) ;

%* TCa((x,y,z,x',y',z'), n) = sum{mi,mi'}_[AA((x,y,z,x',y',z'),(mi,mi'))*U((mi,mi'),n)]
TCa = AA * U ;

%* TCa(x,y,z,x',y',z', n)
TCa = reshape(TCa, [D, D, D, D, D, D, N]) ;

%* TCa(x,x',y,y',z,z',n)
TCa = permute(TCa, [1, 4, 2, 5, 3, 6, 7]) ;

%* TCa((x,x'),(y,y'),(z,z'),n)
TCa = reshape(TCa, [D^2, D^2, D^2, N]) ;