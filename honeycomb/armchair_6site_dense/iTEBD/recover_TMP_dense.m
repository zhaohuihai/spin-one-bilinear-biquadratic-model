function MPS = recover_TMP_dense(parameter, MPS, U, S, V)
%* A(a,b,k) = (1/Lambda2(a))*U((a,k),b)
%* B(b,c,l) = (1/Lambda2(c))*V((c,l),b)


d = parameter.bondDimension^2 ;
D1 = parameter.dim_MPS ;
D2 = length(MPS.Lambda{2}) ;

Lambda = cell(1, 2) ;

Lambda{1} = S ;
Lambda{2} = MPS.Lambda{2} ;

%* U(a, k, b)
U = reshape(U, [D2, d, D1]) ;
%* A(a, b, k)
A = permute(U, [1, 3, 2]) ;

%* A(a,b,k) = A(a,b,k) / Lambda2(a)
for a = 1 : D2
    A(a, :, :) = A(a, :, :) ./ Lambda{2}(a) ;
end
%* V(c,l,b)
V = reshape(V, [D2, d, D1]) ;
%* B(b,c,l)
B = permute(V, [3, 1, 2]) ;

%* B(b,c,l) = B(b,c,l) / Lambda2(c)
for c = 1 : D2
    B(:, c, :) = B(:, c, :) ./ Lambda{2}(c) ;
end

MPS.A = A ;
MPS.B = B ;
MPS.Lambda = Lambda ;