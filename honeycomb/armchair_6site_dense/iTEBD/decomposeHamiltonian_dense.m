function [U, V] = decomposeHamiltonian_dense(parameter, operator)

M = parameter.siteDimension ;
MM = M * M ;
%* H(mi',mj',mi,mj)
H = reshape(operator, [M, M, M, M]) ;

%* H(mi,mi',mj,mj')
H = permute(H, [3, 1, 4, 2]) ;

%* H((mi,mi'),(mj,mj'))
H = reshape(H, [MM, MM]) ;

[U, S, V] = svd(H) ;

%* U((mi,mi'),n)
U = U * sqrt(S) ;

%* V((mj,mj'),n)
V = V * sqrt(S) ;

