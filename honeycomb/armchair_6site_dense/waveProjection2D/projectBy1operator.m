function [A, S, truncationError, coef] = projectBy1operator(parameter, A, Lambda)
%*************************************
%  L(4)\z1        y2/L(3)
%       \          /
%       A\__L(1)__/B
%        /   x    \
%       /          \
%  L(2)/y1        z2\L(5)
%*************************************
%* projectionOperator((n1,n2),(m1,m2))
parameter.projectionOperator = createProjectionOperator(parameter) ;
%* AB((y1,z1,y2,z2), (m1,m2))
AB = computeLyLz_A_Lx_B_LyLz(parameter, A, Lambda) ;

%* T((y1,z1,m1),(y2,z2,m2))
T = computeProjection(parameter, AB) ;

%* U((y1,z1,m1),x) V((y2,z2,m2),x)
[U, S, V] = svd(T) ;
%* S(x)
S = diag(S) ;
coef = S(1) ;
S = S ./ coef ;

[U, S, V, truncationError] = truncate(parameter, U, S, V) ;

A = recover(parameter, Lambda, U, V) ;
