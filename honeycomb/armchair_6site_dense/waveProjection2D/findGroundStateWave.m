function wave = findGroundStateWave(parameter, wave)

if parameter.loadPreviousWave == 1 && parameter.VBCinitial == 1
    warning('Old wave is loaded, unnecessary to initialize VBC wave') ;
    disp('The projection process will NOT initialize a VBC wave') ;
    parameter.VBCinitial = 0 ;
end
if parameter.VBCinitial == 1
    wave = initializeVBCwave(parameter, wave) ;
end

if parameter.loadPreviousWave == 1 && parameter.polarization == 1
    warning('Old wave is loaded, unnecessary to polarize')
    disp('The projection process will NOT polarize the wave function')
    parameter.polarization = 0 ;
end
if parameter.polarization == 1
    wave = polarizeWave(parameter, wave) ;
end

parameter.polarization = 0 ;
%============================================
% parameter.stagQuadrupolarField_z = parameter.field ;
%============================================
wave = applyWaveProjection(parameter, wave) ;


