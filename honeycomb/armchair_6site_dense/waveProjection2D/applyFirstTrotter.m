function [wave, truncationError, coef] = applyFirstTrotter(parameter, wave)
%* x. Lambda{1}: 1-2; Lambda{2}: 3-6; Lambda{3}: 5-4
%* y. Lambda{4}: 1-6; Lambda{5}: 3-4; Lambda{6}: 5-2
%* z. Lambda{7}: 1-4; Lambda{8}: 3-2; Lambda{9}: 5-6
%  L(7)\            /L(6)
%       \          /
%       A\__L(1)__/B
%        /        \
%       /          \
%  L(4)/            \L(8)
%

c = zeros(1, 9) ;
truncErr_single = zeros(1, 9) ;
%====================================================================================
n = 1 ;
for i = 1 : 3
    for j = 1 : 3
        
        [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(n), c(n)] = projectBy1operator(parameter, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
        % (1,2,3,4,5,6) -> (3,6,5,2,1,4)
        wave = rotateWaveSite(wave) ;
        n = n + 1 ;
    end
    % (x,y,z) -> (y,z,x)
    wave = rotateWaveTensor(wave) ;
end
%=====================================================================================
% n = 1 ;
% for i = 1 : 3
%     for j = 1 : 3
%         
%         [wave.A([1, 2]), wave.Lambda{1}, truncErr_single(n), c(n)] = projectBy1operator(parameter, wave.A([1, 2]), wave.Lambda([1, 4, 6, 7, 8])) ;
%         % (x,y,z) -> (y,z,x)
%         wave = rotateWaveTensor(wave) ;
%         n = n + 1 ;
%     end
%     % (1,2,3,4,5,6) -> (3,6,5,2,1,4)
%     wave = rotateWaveSite(wave) ;
% end
%===================================================================================
truncationError = max(truncErr_single) ;
coef = prod(c) ;