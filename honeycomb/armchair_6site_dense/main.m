
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

if parameter.loadPreviousWave == 0
    wave = initializeWave(parameter) ;
    disp('create new wave')
else    
    [wave, parameter] = loadWave(parameter) ;
end

if parameter.projection == 1
    if parameter.TrotterOrder == 1
        disp('apply first order Trotter decomposition')
    elseif parameter.TrotterOrder == 2
        disp('apply second order Trotter decomposition')
    end
    wave = findGroundStateWave(parameter, wave) ;
end

parameter.polarization = 0 ;
% parameter.theta = (0.0) ;
computeExpectationValue_dense(parameter, wave)

%* spin-spin correlation <S(i)*S(i+l)>
% if parameter.computation.spinCorrelation == 1
%     spinCorrelation = computeSpinCorrelation(parameter, wave) ;
% end
