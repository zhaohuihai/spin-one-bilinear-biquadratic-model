function MPS = canonicalize_single(MPS)

%* A(a,i,b)
A = MPS.A.tensor3 ;
Adim = MPS.A.dim ;
%* L(b)
L = MPS.Lambda.tensor1 ;
Ldim = MPS.Lambda.dim ;

%* A(a,i,b) = A(a,i,b) * L(b)
for b = 1 : Ldim
    A(:, :, b) = A(:, :, b) .* L(b) ;
end

%==============================================================
A = canoCondition_1(A, Adim) ;
%==============================================================
A = canoCondition_1(A, Adim) ;
[A, L] = canoCondition_2(A, Adim) ;
%*--------------------------------------------------------------
% A2 = zeros(Adim) ;
% for a = 1 : Ldim
%     A2(a, :, :) = L(a) * A(a, :, :) ;
% end
% %* A2((a,i),b)
% A2 = reshape(A2, [Adim(1) * Adim(2), Adim(3)]) ;
% Dl = diag(L.^2) ;
% norm(A2' * A2 - Dl)
%*--------------------------------------------------------------
%* A(a,i,b) = A(a,i,b) / L(b)
for b = 1 : Ldim
    A(:, :, b) = A(:, :, b) ./ L(b) ;
end
MPS.A.tensor3 = A ;
MPS.Lambda.tensor1 = L ;

%*************************************************
% %* verification
% A1 = zeros(Adim) ;
% id = eye(Adim(1)) ;
% %* A1(a,i,b) = A(a,i,b) * L(b)
% for b = 1 : Ldim
%     A1(:, :, b) = A(:, :, b) .* L(b) ;
% end
% %* A1(a,(i,b))
% A1 = reshape(A1, [Adim(1), Adim(2)*Adim(3)]) ;
% aa = A1 * A1' ;
% norm(A1 * A1' - id * aa(1,1))
% 
% A2 = zeros(Adim) ;
% id = eye(Adim(3)) ;
% %* A(a,i,b) = S(a) * A(a,i,b)
% for a = 1 : Ldim
%     A2(a, :, :) = L(a) .* A(a, :, :)  ;
% end
% %* A2((a,i),b)
% A2 = reshape(A2, [Adim(1) * Adim(2), Adim(3)]) ;
% aa = A2' * A2 ;
% norm(A2' * A2 - id * aa(1,1))