function [Vright, Vleft, eta] = findDominantEigen(T)

%* Find the dominant right eigenvector of T: Vright
%* Find the dominant left eigenvector of T: Vleft
[Vright, etaR] = computeDominantRightEigen(T) ;

T = T' ;
[Vleft, etaL] = computeDominantRightEigen(T) ;

eta = (etaL + etaR) / 2 ;