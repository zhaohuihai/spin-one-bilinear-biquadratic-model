function T = loadAll(T)

for i = 1 : T.subNo
    if ischar(T.tensor2{i})
        fileName = T.tensor2{i} ;
        load(fileName, 'tensor') ;
        T.tensor2{i} = tensor ;
    end
end