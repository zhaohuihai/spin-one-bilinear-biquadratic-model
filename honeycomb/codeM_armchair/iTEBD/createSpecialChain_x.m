function [P, Q, TH, T] = createSpecialChain_x(parameter, TH, T, rightMPS, leftMPS)

ABRdir = 'tensor/ABR/' ;
%* ABR((a,c),(i,j))
ABR = computeA_L1_B_L2(parameter, ABRdir, rightMPS) ;

ABLdir = 'tensor/ABL/' ;
%* ABL((a',c'),(k,l))
ABL = computeA_L1_B_L2(parameter, ABLdir, leftMPS) ;

Pdir = 'tensor/P/' ;
%* P((c,c'),(e,e')) = ABR((c,e),(i,j))*T((i,j),(k,l))*ABL((c',e'),(k,l))
[P, T, ABR, ABL] = computeABR_T_ABL(parameter, Pdir, ABR, ABL, T) ;

Qdir = 'tensor/Q/' ;
%* Q((a,a'),(c,c')) = ABR((a,c),(i,j))*TH((i,j),(k,l))*ABL((a',c'),(k,l))
[Q, TH, ABR, ABL] = computeABR_T_ABL(parameter, Qdir, ABR, ABL, TH) ;
clear ABR ABL


