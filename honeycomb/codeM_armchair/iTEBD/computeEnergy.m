function computeEnergy(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

parameter.H = createHamiltonian_BilinBiqua(parameter) ;
disp('compute energy')
energyBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;

energyBond = energyBond .* 1.5 
energy = mean(energyBond) ;

saveExpectationValue(parameter, energy, 'energy') ;