function A = initializeA(mapping, T)
%* A(a,i,b): S[a]-S[b]=S[i]
%* T((i,j),(k,l)) field: subNo, quantNo, dim

iQN = T.quantNo(1, :) ;
iDim = T.dim(1, :) ;

Tmap = findTmapping(iQN, iDim) ;

Adim = zeros(3, 1) ;
Aqn = zeros(3, 1) ;
n = 0 ;
for a = 1 : mapping.subNo
    Adim(1) = mapping.dim(a) ;
    Aqn(1) = mapping.quantNo(a) ;
    for i = 1 : Tmap.subNo
        Adim(2) = Tmap.dim(i) ;
        Aqn(2) = Tmap.quantNo(i) ;
        for b = 1 : mapping.subNo
            Adim(3) = mapping.dim(b) ;
            Aqn(3) = mapping.quantNo(b) ;
            %* S[a]-S[i]-S[b]=0
            if (Aqn(1)-Aqn(2)-Aqn(3) == 0)
                n = n + 1 ;
                A.quantNo(:, n) = Aqn ;
                A.dim(:, n) = Adim ;
                A.tensor3{n} = rand(Adim') ;
            end
        end
    end
end
A.subNo = n ;