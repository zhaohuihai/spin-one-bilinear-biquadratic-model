function vector = initializeVector_x(P)
%* initialize a vector(c1, c2)
%* P((a1,a2),(c1,c2)) field: subNo, quantNo, dim

cQN = P.quantNo([3, 4], :) ;
cDim = P.dim([3, 4], :) ;
for i = 1 : 2
    cMap(i) = findTmapping(cQN(i, :), cDim(i, :)) ;
end

vDim = zeros(2, 1) ;
vQN = zeros(2, 1) ;
n = 0 ;
for c1 = 1 : cMap(1).subNo
    vDim(1) = cMap(1).dim(c1) ;
    vQN(1) = cMap(1).quantNo(c1) ;
    for c2 = 1 : cMap(2).subNo
        vDim(2) = cMap(2).dim(c2) ;
        vQN(2) = cMap(2).quantNo(c2) ;
        d = vDim(1) * vDim(2) ;
        n = n + 1 ;
        vector.quantNo(:, n) = vQN ;
        vector.dim(n) = d ;
        vector.tensor1{n} = rand(d, 1) ;
        
    end
end
vector.subNo = n ;