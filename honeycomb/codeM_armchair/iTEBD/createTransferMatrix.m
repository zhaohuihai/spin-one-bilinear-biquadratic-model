function T = createTransferMatrix(parameter, dirName, Ta, Tb)
%* field of Ta,Tb: subNo, quantNo, dim, tensor3
%* T((i,j),(k,l)) = sum{x}_(Ta(x,i,k)*Tb(x,l,j))
%* conservation law: i - j = -k + l

[status,message,messageid] = mkdir(dirName) ;
T.dirName = dirName ;
TaNo = Ta.subNo ;
n = 0 ;
for i = 1 : TaNo
    AquantNo = Ta.quantNo(:, i) ;
    j = find(AquantNo(1) == Tb.quantNo(1, :)) ;
    if ~isempty(j)
        Adim = Ta.dim(:, i) ;
        TaTensor = Ta.tensor3{i} ; %* (x,i,k)
        TaTensor = permute(TaTensor, [2, 3, 1]) ; %* (i,k,x)
        TaTensor = reshape(TaTensor, [Adim(2) * Adim(3), Adim(1)]) ; %* ((i,k),x)
        
        for k = 1 : length(j)
            n = n + 1 ;
            BquantNo = Tb.quantNo(:, j(k)) ;
            Bdim = Tb.dim(:, j(k)) ;
            
            TbTensor = Tb.tensor3{j(k)} ; %* (x,l,j)

            TbTensor = reshape(TbTensor, [Bdim(1), Bdim(2) * Bdim(3)]) ; %* (x,(l,j))
            %* tensor((i,k),(l,j))
            tensor = TaTensor * TbTensor ;
            %* tensor(i,k,l,j)
            tensor = reshape(tensor, [Adim(2), Adim(3), Bdim(2), Bdim(3)]) ;
            %* tensor(i,j,k,l)           
            tensor = permute(tensor, [1, 4, 2, 3]) ;
            %* tensor((i,j),(k,l))
            tensor = reshape(tensor, [Adim(2) * Bdim(3), Adim(3) * Bdim(2)]) ;
            
            T.quantNo(:, n) = [AquantNo(2); BquantNo(3); AquantNo(3); BquantNo(2)] ;
            T.dim(:, n) = [Adim(2); Bdim(3); Adim(3); Bdim(2)] ;
%             T.tensor2{n} = tensor ;
            T = saveTensor2(parameter, T, tensor, n) ;
        end
    end
end
T.subNo = n ;
