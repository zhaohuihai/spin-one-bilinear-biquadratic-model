function B = initializeB(mapping, T)
%* B(b,j,c): -S[b]+S[c]=S[j]
%* T((i,j),(k,l)) field: subNo, quantNo, dim

jQN = T.quantNo(2, :) ;
jDim = T.dim(2, :) ;

Tmap = findTmapping(jQN, jDim) ;

Bdim = zeros(3, 1) ;
Bqn = zeros(3, 1) ;
n = 0 ;
for b = 1 : mapping.subNo
    Bdim(1) = mapping.dim(b) ;
    Bqn(1) = mapping.quantNo(b) ;
    for j = 1 : Tmap.subNo
        Bdim(2) = Tmap.dim(j) ;
        Bqn(2) = Tmap.quantNo(j) ;
        for c = 1 : mapping.subNo
            Bdim(3) = mapping.dim(c) ;
            Bqn(3) = mapping.quantNo(c) ;
            %* -S[b]-S[j]+S[c]=0
            if (-Bqn(1)-Bqn(2)+Bqn(3) == 0)
                n = n + 1 ;
                B.quantNo(:, n) = Bqn ;
                B.dim(:, n) = Bdim ;
                B.tensor3{n} = rand(Bdim') ;
            end
        end
    end
end
B.subNo = n ;