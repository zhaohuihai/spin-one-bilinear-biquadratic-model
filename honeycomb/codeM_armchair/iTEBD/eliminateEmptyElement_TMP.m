function [U, S, V] = eliminateEmptyElement_TMP(U, S, V)

S = eliminateEmptyS_TMP(S) ;

U = eliminateEmptyU_TMP(U) ;

V = eliminateEmptyU_TMP(V) ;
