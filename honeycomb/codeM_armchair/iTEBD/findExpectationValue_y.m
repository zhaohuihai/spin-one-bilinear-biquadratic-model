function [expectValue, QR, QL] = findExpectationValue_y(parameter, vectorRight, vectorLeft, etaRight, etaLeft, QR, QL)
%* QR((a,j),(c,i))
%* QL((l,c'),(i,a'))
%* vectorRight(c,l,c')
%* vectorLeft(a',j,a)

eta = (etaRight + etaLeft) / 2 ;

%* QvR(a,j,a') = sum{c,c',l,i}_(QR((a,j),(c,i))*vectorRight(c,l,c')*QL((l,c'),(i,a')))
[QvR, QR, QL] = applyPowerMethod_y(parameter, QR, QL, vectorRight) ;

%* transpose: vectorLeft(a',j,a) -> vectorLeft(a,j,a')
vectorLeft = transposeVector(vectorLeft) ;

%* h = sum{a,j,a'}_(vectorLeft(a,j,a')*QvR(a,j,a'))
h = contract2vector_y(vectorLeft, QvR) ;
%* g = sum{a,j,a'}_(vectorLeft(a,j,a')*vectorRight(a,j,a'))
g = contract2vector_y(vectorLeft, vectorRight) ;

g = eta .* g ;

expectValue = h / g ;