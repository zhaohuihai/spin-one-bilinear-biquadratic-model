function [QR, QL, ABR, ABL] = createSpecialTensor_z(parameter, ABR, ABL, AA, BB, Ta, Tb)

%* H((mi,mi'),(mj,mj'))
%* conservation law: mi - mi' = mj - mj'
H = convertToSparseH(parameter) ;
H = partitionHby2quantNo(H) ;

%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian(parameter, H) ;

%* Wa(x,i,k)
Wa = createW_z(parameter, AA, U) ;
%* Wb(x,l,j)
Wb = createW_z(parameter, BB, V) ;

THRdir = 'tensor/THR/' ;
%* THR((i,j),(k,l))=sum{x}_(Wa(x,i,k)*Tb(x,l,j))
THR = createTransferMatrix(parameter, THRdir, Wa, Tb) ;
%* THR((k,l),(i,j))
THR = transposeT(THR) ;
THLdir = 'tensor/THL/' ;
%* THL((i,j),(k,l))=sum{x}_(Ta(x,i,k)*Wb(x,l,j))
THL = createTransferMatrix(parameter, THLdir, Ta, Wb) ;
%* THL((k,l),(i,j))
THL = transposeT(THL) ;

QRdir = 'tensor/QR/' ;
%* QR((l,c),(k,a)) = sum{i,j}_(ABR((a,c),(i,j))*THR((k,l),(i,j)))
%* change notation: QR((l,c),(i,a))
[QR, ABR, THR] = contractABL_T_y(parameter, QRdir, ABR, THR) ;
clear THR
QLdir = 'tensor/QL/' ;
%* QL((a',i),(c',j)) = sum{k,l}_(ABL((a',c'),(k,l))*THL((k,l),(i,j)))
%* change notation: QL((a',j),(c',i))
[QL, ABL, THL] = contractABR_T_y(parameter, QLdir, ABL, THL) ;
clear THL