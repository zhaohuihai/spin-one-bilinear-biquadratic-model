function S = createS_TMP(Sblock, S, R, iR)

S.quantNo(iR) = R.twoQuantNo ;
S.dim(iR) = length(Sblock) ;
S.tensor1{iR} = Sblock ;