function [VR, VL, eta, PR, PL] = findDominantEigenVector(parameter, ABR, ABL, T)
%* VR(c,l,c'), VL(a',j,a)
%* PR(j,a,i,c), PL(c',l,a',i)

PRdir = 'tensor/PR/' ;
%* PR((a,k),(c,l)) = sum{i,j}_(ABR((a,c),(i,j))*T((i,j),(k,l)))
%* change notation: PR((a,j),(c,i))
[PR, ~, ~] = contractABR_T_y(parameter, PRdir, ABR, T) ;

PLdir = 'tensor/PL/' ;
%* PL((j,c'),(i,a')) = sum{k,l}_(ABL((a',c'),(k,l))*T((i,j),(k,l)))
%* change notation: PL((l,c'),(i,a'))
[PL, ~, ~] = contractABL_T_y(parameter, PLdir, ABL, T) ;

PRmap = rmfield(PR, 'tensor2') ;
PLmap = rmfield(PL, 'tensor2') ;
%* initialize a normalized vector(c,l,c')
V = initializeVector_y(PRmap, PLmap) ;
V = normalize_x(V) ;

%* VR(a,j,a')=sum{c,l,c'}_[PR((a,j),(c,i))*VR(c,l,c')*PL((l,c'),(i,a'))]
[VR, etaR, ~, ~] = findDominantRightEigenVector_y(parameter, PR, PL, V) ;
%* change notation: VR(c,l,c')

%* PR((a,j),(c,i)) -> PR((j,a),(i,c))
PR = transposeP_y(parameter, PR) ;
%* PL((l,c'),(i,a')) -> PL((c',l),(a',i))
PL = transposeP_y(parameter, PL) ;

%* transpose: VR(a,j,a') -> VL(a',j,a)
VL = transposeVector(VR) ;

%* VL(c',l,c)=sum{a',j,a}_[PL((c',l),(a',i))*VL(a',j,a)*PR((j,a),(i,c))]
[VL, etaL, PL, PR] = findDominantRightEigenVector_y(parameter, PL, PR, VL) ;
%* change notation: VL(a',j,a)

eta = (etaR + etaL) / 2 ;

%===========================================================================
%* change the fields of structure to uniform type: 
%* 1. dirName
%* 2. indNo: order of the tensors
%* 3. subNo: number of blocks
%* 4. quantNo: quantum number of blocks
%* 5. dim: size of blocks
%* 6. tensor: tensor blocks 

%* PR((j,a),(i,c)) -> PR(j,a,i,c)
PR = reform2to4(parameter, PR) ;
%* PL((c',l),(a',i)) -> PL(c',l,a',i)
PL = reform2to4(parameter, PL) ;

%* VR(c,l,c')
VR = reform1to3(VR) ;
%* VL(a',j,a)
VL = reform1to3(VL) ;

