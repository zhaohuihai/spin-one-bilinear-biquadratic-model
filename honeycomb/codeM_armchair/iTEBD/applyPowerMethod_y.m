function [vector, PR, PL] = applyPowerMethod_y(parameter, PR, PL, vector)
%* vector1(a,j,a') = sum{c,c',l,i}_(PR((a,j),(c,i))*vector(c,l,c')*PL((l,c'),(i,a')))


%* vector(c,i,a') = sum{l,c'}_(vector(c,l,c')*PL((l,c'),(i,a')))
[vector, PL] = contractVector_PL(parameter, vector, PL) ;

%* vector(a,j,a') = sum{c,i}_(PR((a,j),(c,i))*vector(c,i,a'))
[vector, PR] = contractPR_vector(parameter, PR, vector) ;
