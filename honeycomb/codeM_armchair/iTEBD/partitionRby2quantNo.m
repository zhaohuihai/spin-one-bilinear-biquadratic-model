function R = partitionRby2quantNo(ABT)
%* ABT((a, k), (l, c))
%* conservation law: S[a] + S[k] = S[l] + S[c]

quantNo = ABT.quantNo ;
dim = ABT.dim ;
akQN = quantNo(1, :) + quantNo(2, :) ;
iR = 0 ;
t = ABT.subNo ;
while t > 0
    max_akQN = max(akQN) ;
    index = find(akQN == max_akQN) ;
    indexNo = length(index) ;
    
    iR = iR + 1 ;
    R(iR).twoQuantNo = max_akQN ;
    
    R(iR).subNo = indexNo ;
    R(iR).quantNo = quantNo(:, index) ;
    R(iR).dim = dim(:, index) ;
    R(iR).tensor2 = ABT.tensor2(index) ;
    
    akQN(index) = akQN(index) - 1e9 ;
    t = t - indexNo ;
end
