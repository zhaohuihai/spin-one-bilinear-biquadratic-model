function [TCa, TCb] = createTCa_TCb(parameter, AA, BB)
%* TCa(x,i,k,n), TCb(x,l,j,n)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')

parameter.H = createHamiltonian_Heisenberg(parameter) ;
%* H((mi,mi'),(mj,mj'))
%* conservation law: mi - mi' = mj - mj'
H = convertToSparseH(parameter) ;
H = partitionHby2quantNo(H) ;


%* H((mi', mj'),(mi, mj)) = sum{n}_(U((mi,mi'),n)*S(n)*V((mj,mj'),n))
%* U((mi,mi'),n) = U((mi,mi'),n)*sqrt(S(n))
%* V((mj,mj'),n) = V((mj,mj'),n)*sqrt(S(n))
[U, V] = decomposeHamiltonian(parameter, H) ;

%* TCa(x,i,k,n) = sum{mi,mi'}_[AA(x,i,k;mi,mi')*U((mi,mi'),n)]
TCa = contractAA_U(parameter, AA, U) ;

%* TCb(x,l,j,n) = sum{mj,mj'}_[BB(x,l,j;mj,mj')*V((mj,mj'),n)]
TCb = contractAA_U(parameter, BB, V) ;