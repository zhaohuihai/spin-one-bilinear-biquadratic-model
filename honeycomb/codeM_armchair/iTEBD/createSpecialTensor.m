function [QR, QRa, QRb, QLa, QLb] = createSpecialTensor(parameter, ABR, ABL, AA, BB, Ta, Tb)
%* Ta(x,i,k) Tb(x,l,j)
%* AA(x,i,k;mi,mi') BB(x,l,j;mj,mj')
%* QR(j,a,i,c), QRa(j,a,i,c,n), QRb(j,a,i,c,n), QLa(c',l,a',i,n), QLb(c',l,a',i,n)


%* ABR((a,c),(i,j)) -> ABR(a,c,i,j)
ABR = reform2to4(parameter, ABR) ;
%* ABL((a',c'),(k,l)) -> ABL(a',c',k,l)
ABL = reform2to4(parameter, ABL) ;

%* field: dirName(if needed), indNo, subNo, quantNo, dim, tensor
Ta = reformTensor3(Ta) ;
Tb = reformTensor3(Tb) ;

%* TCa(x,i,k,n), TCb(x,l,j,n)
[TCa, TCb] = createTCa_TCb(parameter, AA, BB) ;
%==============================================================
Wdir = 'tensor/W/' ;
%* W(i,k,l,j) = sum{x,n}_[TCa(x,i,k,n)*TCb(x,l,j,n)]
W = contractSparseTensors(TCa, 4, [1, 4], TCb, 4, [1, 4], parameter, Wdir) ;

QRdir = 'tensor/QR/' ;
%* QR(k,a,l,c) = sum{i,j}_[ABR(a,c,i,j)*W(i,k,l,j)]
QR = contractSparseTensors(ABR, 4, [3, 4], W, 4, [1, 4], parameter, QRdir, [3, 1, 4, 2]) ;
%* change notation: QR(j,a,i,c)

%==============================================================
WaDir = 'tensor/Wa/' ;
%* Wa(i,k,n,l,j) = sum{x}_[TCa(x,i,k,n)*Tb(x,l,j)]
Wa = contractSparseTensors(TCa, 4, 1, Tb, 3, 1, parameter, WaDir) ;

WbDir = 'tensor/Wb/' ;
%* Wb(i,k,l,j,n) = sum{x}_[Ta(x,i,k)*TCb(x,l,j,n)]
Wb = contractSparseTensors(Ta, 3, 1, TCb, 4, 1, parameter, WbDir) ;

QRaDir = 'tensor/QRa/' ;
%* QRa(k,a,l,c,n) = sum{i,j}_[Wa(i,k,n,l,j)*ABR(a,c,i,j)]
QRa = contractSparseTensors(Wa, 5, [1, 5], ABR, 4, [3, 4], parameter, QRaDir, [1, 4, 3, 5, 2]) ;
%* change notation: QRa(j,a,i,c,n)

QRbDir = 'tensor/QRb/' ;
%* QRb(k,a,l,c,n) = sum{i,j}_[Wb(i,k,l,j,n)*ABR(a,c,i,j)]
QRb = contractSparseTensors(Wb, 5, [1, 4], ABR, 4, [3, 4], parameter, QRbDir, [1, 4, 2, 5, 3]) ;
%* change notation: QRb(j,a,i,c,n)

QLaDir = 'tensor/QLa/' ;
%* QLa(c',j,a',i,n) = sum{k,l}_[Wa(i,k,n,l,j)*ABL(a',c',k,l)]
QLa = contractSparseTensors(Wa, 5, [2, 4], ABL, 4, [3, 4], parameter, QLaDir, [5, 3, 4, 1, 2]) ;
%* change notation: QLa(c',l,a',i,n)

QLbDir = 'tensor/QLb/' ;
%* QLb(c',j,a',i,n) = sum{k,l}_[Wb(i,k,l,j,n)*ABL(a',c',k,l)]
QLb = contractSparseTensors(Wb, 5, [2, 3], ABL, 4, [3, 4], parameter, QLbDir, [5, 2, 4, 1, 3]) ;
%* change notation: QLb(c',l,a',i,n)


