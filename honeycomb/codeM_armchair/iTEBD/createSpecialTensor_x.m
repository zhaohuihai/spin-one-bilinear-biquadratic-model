function TH = createSpecialTensor_x(parameter, AA, BB)
%* TH((yi,zj),(zi,yj)) = 
%* sum{x,mi,mj,mi',mj'}_(AA(x,yi,zi;mi,mi')*H(mi,mj,mi',mj')*BB(x,yj,zj;mj,mj')

M = parameter.siteDimension ;

HBB = computeH_BB(parameter, BB) ;
for m1 = 1 : M
    for m2 = 1 : M
        HBB(m1, m2) = reduceTa(HBB(m1, m2)) ;
    end
end
THdir = 'tensor/TH/' ;
TH = computeAA_HBB(parameter, THdir, M, AA, HBB) ;
