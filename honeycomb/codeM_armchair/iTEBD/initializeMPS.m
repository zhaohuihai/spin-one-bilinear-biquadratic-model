function [MPS, allS] = initializeMPS(parameter, T)
%* T((i,j),(k,l)) field: subNo, quantNo, dim
mapping = initializeMPSmapping(parameter) ;

%* A(a,i,b): S[a]-S[b]=S[i]
A = initializeA(mapping, T) ;
%* B(b,j,c): -S[b]+S[c]=S[j]
B = initializeB(mapping, T) ;

Lambda = initializeLambda(mapping) ;

MPS.A = A ;
MPS.B = B ;

MPS.Lambda(1) = Lambda ;
MPS.Lambda(2) = Lambda ;

allS = [] ;
for i = 1 : Lambda.subNo 
    allS = [allS; Lambda.tensor1{i}] ;
end
allS = sort(allS, 'descend') ;
allS = allS ./ allS(1) ;