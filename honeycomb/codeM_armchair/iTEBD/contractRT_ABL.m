function [P, RT, ABL] = contractRT_ABL(parameter, dirName, RT, ABL)
%* P((a,a'),(c,c')) = RT((a,c), (k,l))*ABL((a',c'),(k,l))

[status,message,messageid] = mkdir(dirName) ;
P.dirName = dirName ;
P.subNo = 0 ;
P.quantNo = zeros(4, 0) ;
P.dim = zeros(4, 0) ;
P.tensor2 = cell(0) ;
n = 0 ;
for i = 1 : RT.subNo
    RTqn = RT.quantNo(:, i) ;
    RTdim = RT.dim(:, i) ;
    %* RTtensor((a,c), (k,l))
%     RTtensor = RT.tensor2{i} ;
    RTtensor = loadTensor2(parameter, RT, i) ;
    equali = (RTqn(3) == ABL.quantNo(3, :)) ;
    equalj = (RTqn(4) == ABL.quantNo(4, :)) ;
    j = find(equali & equalj) ;
    if ~isempty(j)        
        for k = 1 : length(j)           
            ABLqn = ABL.quantNo(:, j(k)) ;
            ABLdim = ABL.dim(:, j(k)) ;
            %* ABLtensor((a',c'),(k,l))
%             ABLtensor = ABL.tensor2{j(k)} ;
            ABLtensor = loadTensor2(parameter, ABL, j(k)) ;
            
            %* tensor((a,c),(a',c'))
            tensor = RTtensor * ABLtensor' ;
            %* tensor(a,c,a',c')
            tensor = reshape(tensor, [RTdim(1), RTdim(2), ABLdim(1), ABLdim(2)]) ;
            %* tensor(a,a',c,c')
            tensor = permute(tensor, [1, 3, 2, 4]) ;
            %* tensor((a, a'), (c, c'))
            tensor = reshape(tensor, [RTdim(1) * ABLdim(1), RTdim(2) * ABLdim(2)]) ;
            
            pQN = [RTqn(1); ABLqn(1); RTqn(2); ABLqn(2)] ;
            pDim = [RTdim(1); ABLdim(1); RTdim(2); ABLdim(2)] ;
            equalP = cell(1, 3) ;
            for m = 1 : 3
                equalP{m} = (pQN(m) == P.quantNo(m, :)) ;
            end
            index = find(equalP{1} & equalP{2} & equalP{3}) ;
            
            if isempty(index)
                n = n + 1 ;
                P.quantNo(:, n) = pQN ;
                P.dim(:, n) = pDim ;
%                 P.tensor2{n} = tensor ;
                P = saveTensor2(parameter, P, tensor, n) ;
            else
                Ptensor = loadTensor2(parameter, P, index) ;
                tensor = Ptensor + tensor ;
%                 P.tensor2{index} = P.tensor2{index} + tensor ;
                P = saveTensor2(parameter, P, tensor, index) ;
            end
        end
    end
end
P.subNo = n ;
