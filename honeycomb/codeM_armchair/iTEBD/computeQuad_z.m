function computeQuad_z(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb)

parameter.H = createQzz(parameter) ;
disp('compute quadrupole') ;
quadBond = dealWithSpecialTensor(parameter, T, rightMPS, leftMPS, AA, BB, Ta, Tb) ;

quadrupole = mean(quadBond) ;

saveExpectationValue(parameter, quadrupole, 'quadrupole_z') ;