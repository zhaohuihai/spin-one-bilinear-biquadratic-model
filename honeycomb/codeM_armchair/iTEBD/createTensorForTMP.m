function [Ta, Tb, AA, BB] = createTensorForTMP(parameter, wave)

M = parameter.siteDimension ;

Lambda = wave.Lambda ;
%* A(x, y, z) B(x, y, z)
[A, B] = absorbLambda(M, wave) ;

%* AA(x,y,z;mi,mi')
AA = createAA(M, A, Lambda) ;

%* BB(x,y,z;mj,mj')
BB = createAA(M, B, Lambda) ;

%* Ta(x,i,k) 
%* conservation law: x + i + k = 0
Ta = createTa(M, AA) ;

%* Tb(x,l,j)
%* conservation law: x + l + j = 0
Tb = createTa(M, BB) ;