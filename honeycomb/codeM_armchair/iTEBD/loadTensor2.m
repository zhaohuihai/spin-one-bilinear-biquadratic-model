function tensor = loadTensor2(parameter, T, index)

dim = T.dim(:, index) ;
d = 1 ;
for i = 1 : length(dim)
    d = d * dim(i) ;
end

if d <= parameter.minSizeSaveInHD
    tensor = T.tensor2{index} ;
else
    fileName = T.tensor2{index} ;
    load(fileName, 'tensor') ;
end