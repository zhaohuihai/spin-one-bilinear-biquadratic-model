function MPS = exchangeAandB_TMP(MPS0)

MPS.A = MPS0.B ;
MPS.B = MPS0.A ;

Lambda(1) = MPS0.Lambda(2) ;
Lambda(2) = MPS0.Lambda(1) ;
MPS.Lambda = Lambda ;
