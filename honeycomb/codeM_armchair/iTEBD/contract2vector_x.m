function h = contract2vector_x(vL, vR)
%* h = sum{a1,a2}_(vL(a1,a2)*vR(a1,a2))

h = 0 ;
for i = 1 : vL.subNo
    lQN = vL.quantNo(:, i) ;
    lTensor = vL.tensor1{i} ;
    equala1 = (lQN(1) == vR.quantNo(1, :)) ;
    eqaula2 = (lQN(2) == vR.quantNo(2, :)) ;
    
    j = find(equala1 & eqaula2) ;
    if ~isempty(j)
        h = h + lTensor' * vR.tensor1{j} ;
    end
end