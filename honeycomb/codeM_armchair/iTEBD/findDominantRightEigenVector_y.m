function [vector, eta, PR, PL] = findDominantRightEigenVector_y(parameter, PR, PL, vector)
%* vector(a,j,a')
%* PR((a,j),(c,i))
%* PL((l,c'),(i,a'))

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;



powerConvergence = 1 ;
j = 0 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    %* vector1(a,j,a') = sum{c,c',l,i}_(PR((a,j),(c,i))*vector(c,l,c')*PL((l,c'),(i,a')))
    [vector1, PR, PL] = applyPowerMethod_y(parameter, PR, PL, vector) ;
    [vector1, eta1] = normalize_x(vector1) ;
    %* vectorDiff(c,l,c') = vector1(c,l,c') - vector(c,l,c')
    vectorDiff = computeVectorDiff_y(vector1, vector) ;
    vector1 = eliminateEmptyVector(vector1) ;
    [vectorDiff, powerConvergence] = normalize_x(vectorDiff) ;
    powerConvergence = powerConvergence / sqrt(D * d * D - 1) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;