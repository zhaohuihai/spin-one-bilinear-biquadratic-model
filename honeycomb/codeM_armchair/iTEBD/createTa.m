function Ta = createTa(M, AA)


subNo = 0 ;
quantNo = [] ;
dim = [] ;
tensor3 = cell(0) ;

for i = 1 : M
    subNo = subNo + AA(i, i).subNo ;
    quantNo = [quantNo, AA(i, i).quantNo] ;
    dim = [dim, AA(i, i).dim] ;
    tensor3 = [tensor3, AA(i, i).tensor3] ;
end

Ta.subNo = subNo ;
Ta.quantNo = quantNo ;
Ta.dim = dim ;
Ta.tensor3 = tensor3 ;
Ta = reduceTa(Ta) ;
