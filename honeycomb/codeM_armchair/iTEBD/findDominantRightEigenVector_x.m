function [vector, eta, P] = findDominantRightEigenVector_x(parameter, P, vector)

D = parameter.dim_MPS ;


powerConvergence = 1 ;
j = 0 ;
while powerConvergence >= parameter.convergenceCriterion_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    %* vector1(a1,a2) = sum{c1,c2}_(P((a1,a2),(c1,c2))*vector(c1,c2))
    [vector1, P] = applyPowerMethod_x(parameter, P, vector) ;
    [vector1, eta1] = normalize_x(vector1) ;
    vector1 = eliminateEmptyVector(vector1) ;
    %* vectorDiff(c1,c2) = vector1(c1,c2) - vector(c1,c2)
    vectorDiff = computeVectorDiff_x(vector1, vector) ;
    
    [vectorDiff, powerConvergence] = normalize_x(vectorDiff) ;
    powerConvergence = powerConvergence / sqrt(D^2 - 1) ;
    flag = j / 100 ;
    if flag == floor(flag)
        powerConvergence
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;