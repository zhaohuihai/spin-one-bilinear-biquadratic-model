function [rightMPS, leftMPS, T] = computeDominantEigenMPS(parameter, T)
%* conservation law of T: i - j = -k + l

Tmapping = rmfield(T, 'tensor2') ;
%* MPS: A B Lambda(1) Lambda(2)
[MPS, TMPsingularValue0] = initializeMPS(parameter, Tmapping) ;

% rand('twister', 1) ;
[rightMPS, TMPsingularValue, T] = findDominantRightEigenMPS(parameter, T, MPS, TMPsingularValue0) ;

%* transpose to left transferMatrix
%* T((i,j),(k,l)) --> T((k,l),(i,j))
T = transposeT(T) ;

[leftMPS, TMPsingularValue, T] = findDominantRightEigenMPS(parameter, T, rightMPS, TMPsingularValue) ;

%* transpose to right transferMatrix
%* T((k,l),(i,j)) --> T((i,j),(k,l))
T = transposeT(T) ;