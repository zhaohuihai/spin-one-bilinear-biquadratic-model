function [vector1, PR] = contractPR_vector(parameter, PR, vector)
%* vector1(a,j,a') = sum{c,i}_(PR((a,j),(c,i))*vector(c,i,a'))

vector1.subNo = 0 ;
vector1.quantNo = zeros(3, 0) ;
vector1.dim = zeros(3, 0) ;
vector1.tensor1 = cell(0) ;
n = 0 ;
for i = 1 : PR.subNo
    pQN = PR.quantNo(:, i) ;
    pDim = PR.dim(:, i) ;
    %* pTensor((a,j),(c,i))
%     pTensor = PR.tensor2{i} ;
    pTensor = loadTensor2(parameter, PR, i) ;
    equal = cell(1, 2) ;
    equal{1} = (pQN(3) == vector.quantNo(1, :)) ;
    equal{2} = (pQN(4) == vector.quantNo(2, :)) ;
    j = find(equal{1} & equal{2}) ;
    if ~isempty(j)
        for k = 1 : length(j)
            vQN = vector.quantNo(:, j(k)) ;
            vDim = vector.dim(:, j(k)) ;
            %* vTensor(c,i,a')
            vTensor = vector.tensor1{j(k)} ;
            %* vTensor((c,i), a')
            vTensor = reshape(vTensor, [vDim(1) * vDim(2), vDim(3)]) ;
            %* tensor((a,j),a'))
            tensor = pTensor * vTensor ;
            
            v1QN = [pQN(1); pQN(2); vQN(3)] ;
            v1Dim = [pDim(1); pDim(2); vDim(3)] ;
            v1d = v1Dim(1) * v1Dim(2) * v1Dim(3) ;
            %* tensor(a,j,a')
            tensor = reshape(tensor, [v1d, 1]) ;
            equalv1 = cell(1, 3) ;
            for m = 1 : 3
                equalv1{m} = (v1QN(m) == vector1.quantNo(m, :)) ;
            end
            index = find(equalv1{1} & equalv1{2} & equalv1{3}) ;
            
            if isempty(index)
                n = n + 1 ;
                vector1.quantNo(:, n) = v1QN ;
                vector1.dim(:, n) = v1Dim ;
                vector1.tensor1{n} = tensor ;
            else
                vector1.tensor1{index} = vector1.tensor1{index} + tensor ;
            end
        end
    end
end
vector1.subNo = n ;