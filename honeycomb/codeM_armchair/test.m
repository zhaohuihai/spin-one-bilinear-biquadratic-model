clc
clear
% createPath
D= 3000 ;
A = rand(D) ;
L = rand(1, D) ;

tic
for i = 1 : D
    A(:, i) = A(:, i) * L(i) ;
end
toc

L = diag(L) ;
% L = sparse(L) ;
tic
A = A * L ;
toc