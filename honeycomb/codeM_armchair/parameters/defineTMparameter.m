function parameter = defineTMparameter(parameter)
%* 0: sparse, 1: dense
parameter.denseTensor = 0 ;

%* parallel parameter
parameter.parallelTMP = 0 ;

%* dimension of MPS
parameter.dim_MPS = 20 ;
parameter.dim_MPS_initial = 10 ;
parameter.dim_MPS_incre = 2 ;
parameter.virtualSpin_MPS = 100 ;

%* If the entries of tensor is more than 'minSizeSaveInHD', then save it in hard disk.
parameter.minSizeSaveInHD = 1e6 ;

parameter.convergenceCriterion_TMprojection = 1e-11 ;
parameter.convergenceCriterion_power = 1e-13 ;


parameter.maxTMstep = 1e4 ;
parameter.maxPowerStep = 1e4 ;


parameter.svdRatio_TMP = 0 ;