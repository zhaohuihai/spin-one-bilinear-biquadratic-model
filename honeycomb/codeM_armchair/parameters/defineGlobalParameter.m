function parameter = defineGlobalParameter

%* yes: 1, no: 0
parameter.loadPreviousWave = 1 ; %*

parameter.projection = 1 ; %*
%********************************************************************
%* staggered spontaneous magnetization
parameter.computation.stagSz = 0 ;

%* z quadrupole
parameter.computation.Qzz = 0 ;

%* z staggered quadrupole
parameter.computation.stagQzz = 0 ;

%* energy computation
parameter.computation.energy = 1 ;

%* spin-spin correlation: armchair direction
parameter.computation.spinCorrelation = 1 ;
parameter.computation.maxCorrelationLength = 40 ;
%********************************************************************
%* Maximum number of iterations in svds
parameter.svdsOptions.maxit = 500 ;
%* tolerance in svds
parameter.svdsOptions.tol = 1e-10 ;

%* determine the maximum difference ratio of degenerate values
parameter.maxDiffRatio = 1 - 1e-15 ;
% parameter.maxDiffRatio = 1 ;
