function wave = initializeWave(parameter)


mapping = initializeMapping(parameter) ;
wave = initializeTNS(parameter, mapping) ;


