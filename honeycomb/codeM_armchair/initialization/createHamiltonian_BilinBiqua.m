function H = createHamiltonian_BilinBiqua(parameter)
%* Bilinear biquadratic model Hamiltonian

theta = parameter.theta ;

H_bl = createHamiltonian_Heisenberg(parameter) ;

H = cos(theta) * H_bl + sin(theta) * (H_bl * H_bl) ;

H = setSmalltoZero(H) ;


if parameter.polarization == 1
    if parameter.stagDipolarField_z ~= 0
        disp(['stagger dipolarize Z, field = ', num2str(parameter.stagDipolarField_z)]) 
        H_polar = createStagSz(parameter) ;
        H = H - H_polar * parameter.stagDipolarField_z ;
    elseif parameter.quadrupolarField_z ~= 0
        disp(['quadrupolarize Z, field = ', num2str(parameter.quadrupolarField_z)])
        H_polar = createQzz(parameter) ;
        H = H - H_polar * parameter.quadrupolarField_z ;
    elseif parameter.stagQuadrupolarField_z ~= 0
        disp(['stagger quadrupolarize Z, field = ', num2str(parameter.stagQuadrupolarField_z)])
        H_polar = createStagQzz(parameter) ;
        H = H - H_polar * parameter.stagQuadrupolarField_z ;
    end
end