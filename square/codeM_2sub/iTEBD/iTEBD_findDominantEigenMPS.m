function [rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T)
%* T{1}(z,w,y,x); T{2}(x,y,w,z)

% project from bottom to top
[rightMPS, T] = iTEBD_computeDominantRightEigenMPS(parameter, T) ;

%* transpose to left transferMatrix
%* exchange T{1} and T{2}
%* T{2}(x,y,w,z) -> T{1}(w,z,x,y)
%* T{1}(z,w,y,x) -> T{2}(y,x,z,w)
T = transposeTransferMatrix1D(T) ;

[leftMPS, T] = iTEBD_computeDominantRightEigenMPS(parameter, T) ;


T = transposeTransferMatrix1D(T) ;