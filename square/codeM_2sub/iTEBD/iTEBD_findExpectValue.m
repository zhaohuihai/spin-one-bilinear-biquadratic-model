function iTEBD_findExpectValue(parameter, wave)
%* compute expectation value of local operator

parameter.dim_MPS = parameter.expectValue.dim_MPS ;
parameter.dim_MPS_initial = parameter.expectValue.dim_MPS_initial ;
parameter.dim_MPS_incre = parameter.expectValue.dim_MPS_incre ;

parameter.tol_iTEBD = parameter.expectValue.tol_iTEBD ;
parameter.tol_power = parameter.expectValue.tol_power ;

parameter.maxITEBDstep = parameter.expectValue.maxITEBDstep ;
parameter.maxPowerStep = parameter.expectValue.maxPowerStep ;
%---------------------------------------------------------------------------------------------
A = wave.A ;

%* A{1}(x,y,z,w,m) -> A{1}(z,w,y,x,m)
A{1} = permute(A{1}, [3, 4, 2, 1, 5]) ;
%* A{2}(x,y,z,w,m) -> A{2}(x,y,w,z,m)
A{2} = permute(A{2}, [1, 2, 4, 3, 5]) ;
%---------------------------------------------------------------------------------------------
T = cell(1, 2) ;
%* T{1}(z,w,y,x); T{2}(x,y,w,z)
for i = 1 : 2
    %* T{1}((z,z'),(w,w'),(y,y'),(x,x')) = sum{m}_[conj(A{1}(z,w,y,x,m))*A{1}(z',w',y',x',m)]
    %* T{2}((x,x'),(y,y'),(w,w'),(z,z')) = sum{m}_[conj(A{2}(x,y,w,z,m))*A{2}(x',y',w',z',m)]
    T{i} = computeTensorProductA_A(A{i}) ;
end

[rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T) ;

if parameter.computation.OneSiteOperator == 1
    [expectValueSite, T] = dealWithSpecialTensor_1site(parameter, T, rightMPS, leftMPS, A) ;
    averageSite(parameter, expectValueSite) ;
end
if parameter.computation.twoSiteOperator == 1
    %*=========================================================================================
    [expectValueBond([1, 3]), T] = dealWithSpecialTensor_xz(parameter, T, rightMPS, leftMPS, A) ;
    [expectValueBond([2, 4]), T] = dealWithSpecialTensor_yw(parameter, T, rightMPS, leftMPS, A) ;
    
    %==========================================================================================
    % print the 4 bonds values and calculate average value of the 4 bonds
    averageBond(parameter, expectValueBond) ;
end