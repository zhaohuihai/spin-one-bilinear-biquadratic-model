function [expectValueSite, T] = contractSpecialTensor_1site(parameter, T, rightMPS, leftMPS, A)
%* T{1}(z,w,y,x); T{2}(x,y,w,z)

%* BBR(a,z,w,c) = sum{b}_[rightMPS.B1(a,z,b)*rightMPS.B2(b,w,c)]
BBR = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;
clear  rightMPS ;
%* BBL(a',w,z,c') = sum{b'}_[leftMPS.B1(a',w,b') * leftMPS.B2(b',z,c')]
BBL = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;
clear leftMPS ;
%* PR(a,c,y,x) = sum{z,w}_(BBR(a,z,w,c)*T{1}(z,w,y,x))
%* change notation: PR(a,c,y',x)
PR = contractTensors(BBR, 4, [2, 3], T{1}, 4, [1, 2]) ;

%* PL(a',c',x,y) = sum{w,z}_(BBL(a',w,z,c')*T{2}(x,y,w,z))
PL = contractTensors(BBL, 4, [2, 3], T{2}, 4, [3, 4]) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,y,c') vector(a,y',a')
if parameter.complexWave == 1
    vector = rand(D, d, D) + 1i * rand(D, d, D) ;
else
    vector = rand(D, d, D) ;
end
vector = normalizeVector(vector) ;

%* vectorR(a,y',a')
vectorR = applyPowerMethod_x_right(parameter, PR, PL, vector) ;
%*  change notation: vectorR(c,y,c')


%* vectorL(c,y,c')
vectorL = applyPowerMethod_x_left(parameter, PR, PL, vector) ;
%* change notation: vectorL(a,y',a')
%*==================================================================================
%* QR(a,c,y,xn)    change notation: QR(a,c,y',xn)
%* QL(a',c',xn,y)
computation = parameter.computation ;
%* spontaneous magnetization
if computation.S == 1
    
    operator = createSzI(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [Sz, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    clear QR QL
    expectValueSite.Sz = Sz ;
end

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Q == 1
    
    operator = createQzz(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Qzz = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Qzz = Qzz ;
end
