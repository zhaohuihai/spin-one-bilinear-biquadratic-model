function [U, V] = decomposeOperator(parameter, operator)
%* H((m1, m2),(m1', m2')) = sum{n}_(U((m1,m1'),n)*S(n)*V((m2,m2'),n))
%* U((m1,m1'),n) = U((m1,m1'),n)*sqrt(S(n))
%* V((m2,m2'),n) = V((m2,m2'),n)*sqrt(S(n))

M = parameter.siteDimension ;
MM = M * M ;
%* H(m1,m2,m1',m2')
H = reshape(operator, [M, M, M, M]) ;

%* H(m1,m1',m2,m2')
H = permute(H, [1, 3, 2, 4]) ;

%* H((m1,m1'),(m2,m2'))
H = reshape(H, [MM, MM]) ;

[U, S, V] = svd(H) ;
%************************************
%* truncation
S = diag(S) ;
cut = find(S < 1e-12) ;
S(cut) = [] ;
U(:, cut) = [] ;
V(:, cut) = [] ;
S = diag(S) ;
%************************************
%* U((m1,m1'),n)
U = U * sqrt(S) ;

%* V((m2,m2'),n)
V = V * sqrt(S) ;

