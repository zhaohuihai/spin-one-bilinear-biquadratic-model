function energy = averageBond(parameter, expectValueBond)

bondNo = length(expectValueBond) ;
computation = parameter.computation ;

%* staggered spontaneous magnetization
if computation.stagSz == 1
    stagSzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagSzBond(i) = expectValueBond(i).stagSz ;
    end
    disp ('stagSz of every bond = ') ;
    disp(stagSzBond) ;
    stagSz = mean(stagSzBond) ;
    saveExpectationValue(parameter, stagSz, 'stagSz') ;
end
% energy computation
if computation.energy == 1
    energyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyBond(i) = expectValueBond(i).energy ;
    end
    if parameter.display == 1
        disp ('energy of every bond = ') ;
        disp(energyBond) ;
        disp (['std of every bond energy: ', num2str(std(energyBond))]) ;
    end
    energy = mean(energyBond) ;
    energy = energy * 2 ;
    saveExpectationValue(parameter, energy, 'energy') ;
end
%*
if computation.Qzz == 1
    QzzBond  = zeros(1, bondNo) ;
    for i = 1 : bondNo
        QzzBond(i) = expectValueBond(i).Qzz ;
    end
    disp('Qzz of every bond = ') ;
    disp(QzzBond) ;
    Qzz = mean(QzzBond) ;
    saveExpectationValue(parameter, Qzz, 'Qzz') ;
end
%*
if computation.stagQzz == 1
    stagQzzBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        stagQzzBond(i) = expectValueBond(i).stagQzz ;
    end
    disp('stagQzz of every bond = ') ;
    disp(stagQzzBond) ;
    stagQzz = mean(stagQzzBond) ;
    saveExpectationValue(parameter, stagQzz, 'stagQzz') ;
end
%* energy derivative
if computation.energyDerivative == 1
    energyDerivativeBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyDerivativeBond(i) = expectValueBond(i).energyDerivative ;
    end
    disp ('energy derivative of every bond = ') ;
    disp(energyDerivativeBond) ;
    energyDerivative = mean(energyDerivativeBond) ;
    energyDerivative = energyDerivative * 1.5 ;
    saveExpectationValue(parameter, energyDerivative, 'energyDerivative') ;
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    nearSpinCorrelationBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        nearSpinCorrelationBond(i) = expectValueBond(i).nearSpinCorrelation ;
    end
    disp ('nearest neighbor spin correlation of every bond = ') ;
    disp(nearSpinCorrelationBond) ;
    P = computePlaquetteOrder(nearSpinCorrelationBond) ;
    disp(['plaquette order = ', num2str(P, 12)])
end