function TH1 = createTH1_y(parameter, A, U)
%* TH1((z,z'),(w,w'),(y,y',n),(x,x')) = sum{m1,m1'}_[A(z,w,y,x,m1)*A(z',w',y',x',m1')*U((m1,m1'),n)]

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = size(U, 2) ;
%* A((z,w,y,x),m1)
A = reshape(A, [D^4, M]) ;
%* AA((z,w,y,x,z',w',y',x'),(m1,m1'))
AA = kron(A, A) ;

%* TH1((z,w,y,x,z',w',y',x'),n) = sum{m1,m1'}_[AA((z,w,y,x,z',w',y',x'),(m1,m1'))*U((m1,m1'),n)]
TH1 = AA * U ;

%* TH1(z,w,y,x,z',w',y',x',n)
TH1 = reshape(TH1, [D, D, D, D, D, D, D, D, N]) ;

%* TH1(z,z',w,w',y,y',n,x,x')
TH1 = permute(TH1, [1, 5, 2, 6, 3, 7, 9, 4, 8]) ;

%* TH1((z,z'),(w,w'),(y,y',n),(x,x'))
TH1 = reshape(TH1, [D^2, D^2, D^2 * N, D^2]) ;