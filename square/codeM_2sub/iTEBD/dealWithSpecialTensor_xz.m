function [expectValueBond, T] = dealWithSpecialTensor_xz(parameter, T, rightMPS_x, leftMPS_x, A)
%*  A{1}(z,w,y,x,m) A{2}(x,y,w,z,m)

%* T{1}(z,w,y,x); T{2}(x,y,w,z)
rightMPS_z = iTEBD_computeSingleGate1D(parameter, T{1}, rightMPS_x) ;


T = transposeTransferMatrix1D(T) ;

leftMPS_z = iTEBD_computeSingleGate1D(parameter, T{1}, leftMPS_x) ;

T = transposeTransferMatrix1D(T) ;

parameter.orderSign = 1 ; 
%* x
[expectValueBond(1), T] = contractSpecialTensor_x(parameter, T, rightMPS_x, leftMPS_x, A) ;

%* z
parameter.orderSign = - 1 ; 
T = T([2, 1]) ;
[expectValueBond(2), T] = contractSpecialTensor_x(parameter, T, rightMPS_z, leftMPS_z, A([2, 1])) ;
T = T([2, 1]) ;