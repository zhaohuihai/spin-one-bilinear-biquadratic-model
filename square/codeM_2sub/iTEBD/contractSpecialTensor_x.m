function [expectValueBond, T] = contractSpecialTensor_x(parameter, T, rightMPS, leftMPS, A)
%* T{1}(z,w,y,x); T{2}(x,y,w,z)

%* BBR(a,z,w,c) = sum{b}_[rightMPS.B1(a,z,b)*rightMPS.B2(b,w,c)]
BBR = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;
clear rightMPS ;
%* BBL(a',w,z,c') = sum{b'}_[leftMPS.B1(a',w,b') * leftMPS.B2(b',z,c')]
BBL = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;
clear leftMPS ;
%* PR(a,c,y,x) = sum{z,w}_(BBR(a,z,w,c)*T{1}(z,w,y,x))
%* change notation: PR(a,c,y',x)
PR = contractTensors(BBR, 4, [2, 3], T{1}, 4, [1, 2]) ;

%* PL(a',c',x,y) = sum{w,z}_(BBL(a',w,z,c')*T{2}(x,y,w,z))
PL = contractTensors(BBL, 4, [2, 3], T{2}, 4, [3, 4]) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,y,c') vector(a,y',a')
if parameter.complexWave == 1
    vector = rand(D, d, D) + 1i * rand(D, d, D) ;
else
    vector = rand(D, d, D) ;
end
vector = normalizeVector(vector) ;

%* vectorR(a,y',a')
vectorR = applyPowerMethod_x_right(parameter, PR, PL, vector) ;
%*  change notation: vectorR(c,y,c')


%* vectorL(c,y,c')
vectorL = applyPowerMethod_x_left(parameter, PR, PL, vector) ;
%* change notation: vectorL(a,y',a')
%*==========================================================================================================
%* QR(a,c,y,xn)    change notation: QR(a,c,y',xn)
%* QL(a',c',xn,y)
computation = parameter.computation ;
%* staggered spontaneous magnetization
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [stagSz, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.stagSz = stagSz * parameter.orderSign ;
    clear QR QL
end
%* 
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [Qzz, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.Qzz = Qzz ;
    clear QR QL
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [stagQzz, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.stagQzz = stagQzz * parameter.orderSign ;
    clear QR QL
end
% energy computation
if computation.energy == 1
    parameter.project.field = 0 ;
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [energy, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.energy = energy ;
    clear QR QL
end
%* energy derivative
if computation.energyDerivative == 1
    operator = createEnergyDerivative(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [energyDerivative, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.energyDerivative = energyDerivative ;
    clear QR QL
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    [QR, QL] = createSpecialTensor_x(parameter, operator, BBR, BBL, A) ;
    [nearSpinCorrelation, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.nearSpinCorrelation = nearSpinCorrelation ;
    clear QR QL
end