function expectValue = findExpectationValue_1site(vectorR, vectorL, P, Q)


% h = vectorL.' * Q * vectorR ;
% 
% g = vectorL.' * P * vectorR ;

h = vectorL' * Q * vectorR ;

g = vectorL' * P * vectorR ;

% expectValue = real(h / g) ;
expectValue = h / g ;
expectValue = real(expectValue) ;