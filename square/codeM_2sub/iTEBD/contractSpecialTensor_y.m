function [expectValueBond, T] = contractSpecialTensor_y(parameter, T, rightMPS, leftMPS, A)
%* T{1}(z,w,y,x); T{2}(x,y,w,z)

%* BBR(a,z,w,c) = sum{b}_[rightMPS.B1(a,z,b)*rightMPS.B2(b,w,c)]
BBR = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;
clear rightMPS ;
%* BBL(a',w,z,c') = sum{b'}_[leftMPS.B1(a',w,b') * leftMPS.B2(b',z,c')]
BBL = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;
clear leftMPS ;
%* PR(a,c,y,x) = sum{z,w}_(BBR(a,z,w,c)*T{1}(z,w,y,x))
PR = contractTensors(BBR, 4, [2, 3], T{1}, 4, [1, 2]) ;

%* PL(a',c',x,y) = sum{w,z}_(BBL(a',w,z,c')*T{2}(x,y,w,z))
%* change notation: PL(a',c',x,y) -> PL(a',c',x',y)
PL = contractTensors(BBL, 4, [2, 3], T{2}, 4, [3, 4]) ;

d = parameter.bondDimension^2 ;
D = parameter.dim_MPS ;
%* vector(c,x,c') vector(a,x',a')
vector = rand(D, d, D) ;
vector = normalizeVector(vector) ;

%* vectorR(a,x',a')
vectorR = applyPowerMethod_y_right(parameter, PR, PL, vector) ;
%*  change notation: vectorR(c,x,c')


%* vectorL(c,x,c')
vectorL = applyPowerMethod_y_left(parameter, PR, PL, vector) ;
%* change notation: vectorL(a,x',a')
%*==========================================================================================================
%* QR(a,c,yn,x)    
%* QL(a',c',x,yn) change notation: QL(a',c',x',yn)
computation = parameter.computation ;
%* staggered spontaneous magnetization
if computation.stagSz == 1
    operator = createStagSz(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [stagSz, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
%     disp(['stagSz y bond = ', num2str(stagSz)]) ;
    expectValueBond.stagSz = stagSz * parameter.orderSign ;
    clear QR QL
end
%*
if computation.Qzz == 1
    operator = createQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [Qzz, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.Qzz = Qzz ;
    clear QR QL
end
if computation.stagQzz == 1
    operator = createStagQzz(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [stagQzz, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.stagQzz = stagQzz * parameter.orderSign ;
    clear QR QL
end
% energy computation
if computation.energy == 1
    parameter.project.field = 0 ;
    operator = createHamiltonian_BilinBiqua(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [energy, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
%     disp(['energy y bond = ', num2str(energy)]) ;
    expectValueBond.energy = energy ;
    clear QR QL
end
%* energy derivative
if computation.energyDerivative == 1
    operator = createEnergyDerivative(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [energyDerivative, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
%     disp(['energy derivative y bond = ', num2str(energyDerivative)]) ;
    expectValueBond.energyDerivative = energyDerivative ;
    clear QR QL
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    operator = createHamiltonian_Heisenberg(parameter) ;
    [QR, QL] = createSpecialTensor_y(parameter, operator, BBR, BBL, A) ;
    [nearSpinCorrelation, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL) ;
    expectValueBond.nearSpinCorrelation = nearSpinCorrelation ;
    clear QR QL
end