function T = transposeTransferMatrix1D(T)
%* transpose to left transfer matrix
%* exchange T{1} and T{2}
%* T{2}(x,y,w,z) -> T{1}(w,z,x,y)
T1 = permute(T{2}, [3, 4, 1, 2]) ;

%* T{1}(z,w,y,x) -> T{2}(y,x,z,w)
T2 = permute(T{1}, [3, 4, 1, 2]) ;

T{1} = T1 ;
T{2} = T2 ;
