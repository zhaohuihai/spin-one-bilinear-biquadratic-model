function [MPS, T, truncationError] = iTEBD_applyRightGateOperation1D(parameter, T, MPS)
%* T{1}(z,w,y,x); T{2}(x,y,w,z)

truncErr_single = zeros(1, 2) ;

%* apply T{1} gate; then exchange A <--> B
%* apply T{2} gate; then exchange A <--> B
for i = 1 : 2
    [MPS, truncErr_single(i)] = iTEBD_computeSingleGate1D(parameter, T{i}, MPS) ;
end

truncationError = max(truncErr_single) ;
