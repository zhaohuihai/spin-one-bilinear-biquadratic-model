function [expectValueSite, T] = dealWithSpecialTensor_1site(parameter, T, rightMPS_1, leftMPS_1, A)


rightMPS_2 = iTEBD_computeSingleGate1D(parameter, T{1}, rightMPS_1) ;

%* transpose to left transferMatrix
%* exchange T{1} and T{2}
%* T{2}(x,y,w,z) -> T{1}(w,z,x,y)
%* T{1}(z,w,y,x) -> T{2}(y,x,z,w)
T = transposeTransferMatrix1D(T) ;

leftMPS_2 = iTEBD_computeSingleGate1D(parameter, T{1}, leftMPS_1) ;

%* transpose to right transferMatrix
T = transposeTransferMatrix1D(T) ;

[expectValueSite(1), T] = contractSpecialTensor_1site(parameter, T, rightMPS_1, leftMPS_1, A) ;

T = T([2, 1]) ;
[expectValueSite(2), T] = contractSpecialTensor_1site(parameter, T, rightMPS_2, leftMPS_2, A([2, 1])) ;
T = T([2, 1]) ;

