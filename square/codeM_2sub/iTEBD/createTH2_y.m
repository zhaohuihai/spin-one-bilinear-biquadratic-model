function TH2 = createTH2_y(parameter, A, V)
%* TH2((x,x'),(y,y',n),(w,w'),(z,z')) = sum{m2,m2'}_[A{2}(x,y,w,z,m2)*A{2}(x',y',w',z',m2')*V((m2,m2'),n)]

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = size(V, 2) ;
%* A((x,y,w,z),m2)
A = reshape(A, [D^4, M]) ;
%* AA((x,y,w,z,x',y',w',z'),(m2,m2'))
AA = kron(A, A) ;

%* TH2((x,y,w,z,x',y',w',z'),n) = sum{m2,m2'}_[AA((x,y,w,z,x',y',w',z'),(m2,m2'))*V((m2,m2'),n)]
TH2 = AA * V ;

%* TH2(x,y,w,z,x',y',w',z',n)
TH2 = reshape(TH2, [D, D, D, D, D, D, D, D, N]) ;

%* TH2(x,x',y,y',n,w,w',z,z')
TH2 = permute(TH2, [1, 5, 2, 6, 9, 3, 7, 4, 8]) ;

%* TH2((x,x'),(y,y',n),(w,w'),(z,z')))
TH2 = reshape(TH2, [D^2, D^2 * N, D^2, D^2]) ;