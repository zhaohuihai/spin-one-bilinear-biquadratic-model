function [vector, PR, PL] = updateVector_x_right(PR, PL, vector)
%* vector(a,y',a') = sum{c',y,c,x}_[PR(a,c,y',x)*PL(a',c',x,y)*vector(c,y,c')]


%* vector(a',x,c) = sum{c',y}_(PL(a',c',x,y)*vector(c,y,c'))
vector = contractTensors(PL, 4, [2, 4], vector, 3, [3, 2]) ;


%* vector(a,y',a') = sum{c,x}_(PR(a,c,y',x)*vector(a',x,c))
vector = contractTensors(PR, 4, [2, 4], vector, 3, [3, 2]) ;
