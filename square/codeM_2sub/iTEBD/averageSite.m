function averageSite(parameter, expectValueSite)

computation = parameter.computation ;

precision = 12 ;
if computation.S == 1
    
    disp(['Sz1: ', num2str(expectValueSite(1).Sz, precision)]) ;
    disp(['Sz2: ', num2str(expectValueSite(2).Sz, precision)]) ;
    
    stagSz = (expectValueSite(1).Sz - expectValueSite(2).Sz) / 2 ;
    saveExpectationValue(parameter, stagSz, 'stagSz') ;
end

if computation.Q == 1
    
    for i = 1 : 6
        disp(['Qxx', num2str(i), ': ', num2str(expectValueSite(i).Qxx, precision)]) ;
        disp(['Qyy', num2str(i), ': ', num2str(expectValueSite(i).Qyy, precision)]) ;
        disp(['Qzz', num2str(i), ': ', num2str(expectValueSite(i).Qzz, precision)]) ;
        disp(' ') ;
    end
    
    AFQ3 = (expectValueSite(1).Qxx + expectValueSite(2).Qyy + expectValueSite(3).Qzz + ...
        expectValueSite(4).Qxx + expectValueSite(5).Qyy + expectValueSite(6).Qzz) / 6 ;

    saveExpectationValue(parameter, AFQ3, 'AFQ3') ;
end