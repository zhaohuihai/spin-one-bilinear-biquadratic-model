function [MPS, truncErr_bond] = computeSingleGate1D_doubleLayer(parameter, AA, MPS)
%* AA{1}(w,z,x,y); AA{2}(y,x,z,w)

d0 = parameter.siteDimension ;
D = parameter.bondDimension ;
Chi = parameter.dim_MPS ;

%* A((a,z),y,(b,x)) = sum{w}_[A(a,w,b)*AA{1}(w,z,x,y)]
MPS.A = contractGate_doubleLayer(parameter, MPS.A, AA{1}) ;

%* B((b,x),w,(c,z)) = sum{y}_[B(b,y,c)*AA{2}(y,x,z,w)] 
MPS.B = contractGate_doubleLayer(parameter, MPS.B, AA{2}) ;


%* MPS.A(a,y,b) MPS.B(b,w,c)
[MPS, truncErr_bond] = canonicalize_double(parameter, MPS) ;

%----------------------------------------------------------------------------




