function [VRa, VRb, VLa, VLb, eta] = contractCluster0D(parameter, AA, Lx, Lz, bottomMPS, topMPS)
%* AA{1}(w,z,x,y)
%* AA{2}(y,x,z,w)

%* AAb(a,w,b)
AAb = bottomMPS.A ;
%* BBb(b,y,c)
BBb = bottomMPS.B ;

%* AAt(a',y,b')
AAt = topMPS.A ;
%* BBt(b',w,c')
BBt = topMPS.B ;

SAb = bottomMPS.SA ;
SAt = topMPS.SA ;
SBb = bottomMPS.SB ;
SBt = topMPS.SB ;

[VRa, VRb, etaR] = applyPowerMethod_doubleLayer(parameter, Lx, Lz, SAb, SAt, SBb, SBt, AAb, BBb, AAt, BBt, AA{1}, AA{2})  ;

%* AAb(a,w,b) -> AAb(b,w,a)
AAb = permute(AAb, [3, 2, 1]) ;
%* BBb(b,y,c) -> BBb(c,y,b)
BBb = permute(BBb, [3, 2, 1]) ;

%* AAt(a',y,b') -> AAt(b',y,a')
AAt = permute(AAt, [3, 2, 1]) ;
%* BBt(b',w,c') -> BBt(c',w,b')
BBt = permute(BBt, [3, 2, 1]) ;

%* AA1(w,z,x,y) -> AA1(w,x,z,y)
AA{1} = permute(AA{1}, [1, 3, 2, 4]) ;
%* AA2(y,x,z,w) -> AA2(y,z,x,w)
AA{2} = permute(AA{2}, [1, 3, 2, 4]) ;

[VLa, VLb, etaL] = applyPowerMethod_doubleLayer(parameter, Lx, Lz, SAb, SAt, SBb, SBt, BBb, AAb, BBt, AAt, AA{2}, AA{1}) ;

eta = (etaR + etaL) / 2 ;

