function [U, S, V, coef] = applySVD(M)
%* M(x',x") = sum{x}_[U(x',x)*S(x)*conj(V(x",x))]

[U, S, V] = svd(M) ;
S = diag(S) ;

coef = S(1) ;
% S = S ./ coef ;