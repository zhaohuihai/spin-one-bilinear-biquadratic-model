function [wave, truncationError, coef] = simpleUpdate(parameter, projectionOperator, wave)
%* Tensor to update: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* x. Lambda{1}
%* y. Lambda{2}
%* z. Lambda{3}
%* w. Lambda{4}

%* T1(x,y1,z1,w1,m1) T2(x,y2,z2,w2,m2)
[T1, T2] = computeProjection(parameter, projectionOperator, wave.A{1}, wave.A{2}) ;
% ===============truncation============

alpha = parameter.project.alpha ;

%* step A: absorb lambdas
%* M1((y1,z1,w1,m1),x) = T1(x,y1,z1,w1,m1)*sqrt(L2(y1))*sqrt(L3(z1))*sqrt(L4(w1))
M1 = computeA1_sqrtLy_sqrtLz_sqrtLw(T1, wave.Lambda{2}, wave.Lambda{3}, wave.Lambda{4}, alpha) ;
%* M2((y2,z2,w2,m2),x) = T2(x,y2,z2,w2,m2)*sqrt(L2(y2))*sqrt(L3(z2))*sqrt(L4(w2))
M2 = computeA1_sqrtLy_sqrtLz_sqrtLw(T2, wave.Lambda{2}, wave.Lambda{3}, wave.Lambda{4}, alpha) ;

%* step B: QR factorization
%* M1((y1,z1,w1,m1),x) = sum{x'}_[Q1((y1,z1,w1,m1),x')*R1(x',x)]
[~, R1] = qr(M1, 0) ;
R1 = R1 ./ norm(R1) ;

%* M2((y2,z2,w2,m2),x) = sum{x"}_[Q2((y2,z2,w2,m2),x")*R2(x",x)]
[~, R2] = qr(M2, 0) ;
R2 = R2 ./ norm(R2) ;

%* step C: SVD and truncation
%* M(x',x") = sum{x}_[R1(x',x)*R2(x",x)]
M = R1 * R2.' ;

%* M(x',x") = sum{x}_[U(x',x)*S(x)*conj(V(x",x))]
[U0, S0, V0, coef] = applySVD(M) ;

Dcut = parameter.bondDimension ;
% Dcut = 27 ;
[U, S, V, truncationError] = truncate(U0, S0, V0, Dcut) ;

sqrtS = sqrt(S) ;
%* step D: Create Projector
%* change index notation: R1(x',x) -> R1(x",x'); R2(x",x) -> R2(x",x')
%* PR(x',x) = sum{x"}_[R2(x",x')*V(x",x)/sqrtS(x)]
PR = createProjector(R2, V, sqrtS) ;

%* PL(x',x) = sum{x"}_[R1(x",x')*conj(U(x",x))/sqrtS(x)]
U = conj(U) ;
PL = createProjector(R1, U, sqrtS) ;

%* step E: Form new system sites: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* A1(x,y1,z1,w1,m1) = sum{x'}_[PR(x',x)*T1(x',y1,z1,w1,m1)]
wave.A{1} = contractTensors(PR, 2, 1, T1, 5, 1) ;

% wave.A{1} = wave.A{1} ./ max(max(max(max(abs(wave.A{1}))))) ;

%* A2(x,y2,z2,w2,m2) = sum{x'}_[PL(x',x)*T2(x',y2,z2,w2,m2)]
wave.A{2} = contractTensors(PL, 2, 1, T2, 5, 1) ;

% wave.A{2} = wave.A{2} ./ max(max(max(max(abs(wave.A{2}))))) ;

wave.Lambda{1} = S ./ coef ;
% wave.Lambda{1} = S ;

wave.PR{1} = PR ;
wave.PL{1} = PL ;
