function M2 = computeK3_T2_sqrtLy_sqrtLz(K3, T2, Ly, Lz)
%* M2((y2,z2,w,m2),x) = sum{w'}_[K3(w,w')*T2(x,y2,z2,w',m2)*sqrt(Ly(y2))*sqrt(Lz(z2))]

[dx, dy, dz, dw, dm] = size(T2) ;

%* T2(x,y2,z2,w',m2) = T2(x,y2,z2,w',m2)*sqrt(Ly(y2))
for y2 = 1 : dy
    T2(:, y2, :, :, :) = T2(:, y2, :, :, :) * sqrt(Ly(y2)) ;
end

%* T2(x,y2,z2,w',m2) = T2(x,y2,z2,w',m2)*sqrt(Lz(z2))
for z2 = 1 : dz
    T2(:, :, z2, :, :) = T2(:, :, z2, :, :) * sqrt(Lz(z2)) ;
end

%* M2(w,x,y2,z2,m2) = sum{w'}_[K3(w,w')*T2(x,y2,z2,w',m2)]
M2 = contractTensors(K3, 2, 2, T2, 5, 4) ;

%* M2(w,x,y2,z2,m2) -> M2(y2,z2,w,m2,x)
M2 = permute(M2, [3, 4, 1, 5, 2]) ;

%* M2(y2,z2,w,m2,x) -> M2((y2,z2,w,m2),x)
M2 = reshape(M2, [dy * dz * dw * dm, dx]) ;