function A = computeA_sqrtLx(A, Lx, varargin )

[dx, ~, ~, ~, ~] = size(A) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A(x,y,z,w,m) = A(x,y,z,w,m)*sqrt(Lx(x))
for x = 1 : dx
    A(x, :, :, :, :) = A(x, :, :, :, :) * Lx(x).^alpha ;
end
