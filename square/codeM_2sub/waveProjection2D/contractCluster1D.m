function [bottomMPS, topMPS] = contractCluster1D(parameter, AA, Lx, Ly, Lz, Lw)


SA = kron(Lz, Lz) ;
SB = kron(Lx, Lx) ;

%* AA{1}((w,w'),(z,z'),(x,x'),(y,y'))
%* AA{2}((y,y'),(x,x'),(z,z'),(w,w'))
% project from bottom to top
%* bottomMPS A((a1,a1'),(w,w'),(b1,b1')) B((b1,b1'),(y,y'),(c1,c1'))
bottomMPS = computeBottomMPS(parameter, AA, Ly, Lw, SA, SB) ;

%* transpose to left transferMatrix
%* A{1}(w,z1,x,y,n1) -> A{1}(y,z1,x,w,n1)
AA{1} = permute(AA{1}, [4, 2, 3, 1, 5]) ;
%* A{2}(y,x,z2,w,n2) -> A{2}(w,x,z2,y,n2)
AA{2} = permute(AA{2}, [4, 2, 3, 1, 5]) ;

%* topMPS A((a2,a2'),(y,y'),(b2,b2')) B((b2,b2'),(w,w'),(c2,c2'))
topMPS = computeBottomMPS(parameter, AA, Lw, Ly, SA, SB) ;