function M4 = computeK3_A4_sqrtLw_sqrtLx( K3, A4, Lw, Lx, varargin )
%* M4((z,w4,x4,m4),y) = sum{z'}_[K3(z,z')*A4(x4,y,z',w4,m4)*sqrt(Lw(w4))*sqrt(Lx(x4))]

[dx, dy, dz, dw, dm] = size(A4) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A4(x4,y,z',w4,m4) = A4(x4,y,z',w4,m4)*sqrt(Lw(w4))
for w4 = 1 : dw
    A4(:, :, :, w4, :) = A4(:, :, :, w4, :) * Lw(w4).^alpha ;
end

%* A4(x4,y,z',w4,m4) = A4(x4,y,z',w4,m4)*sqrt(Lx(x4))
for x4 = 1 : dx
    A4(x4, :, :, :, :) = A4(x4, :, :, :, :) * Lx(x4).^alpha ;
end

%* M4(z,x4,y,w4,m4) = sum{z'}_[K3(z,z')*A4(x4,y,z',w4,m4)]
M4 = contractTensors(K3, 2, 2, A4, 5, 3) ;

%* M4(z,x4,y,w4,m4) -> M4(z,w4,x4,m4,y)
M4 = permute(M4, [1, 4, 2, 5, 3]) ;

%* M4(z,w4,x4,m4,y) -> M4((z,w4,x4,m4),y)
M4 = reshape(M4, [dz * dw * dx * dm, dy]) ;