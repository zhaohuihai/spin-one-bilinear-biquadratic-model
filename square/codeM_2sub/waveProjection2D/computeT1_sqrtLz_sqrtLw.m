function M1 = computeT1_sqrtLz_sqrtLw( T1, Lz, Lw )
%* M1((z1,w1,x,m1),y) = T1(x,y,z1,w1,m1)*sqrt(Lz(z1))*sqrt(Lw(w1))

[dx, dy, dz, dw, dm] = size(T1) ;

%* T1(x,y,z1,w1,m1) = T1(x,y,z1,w1,m1)*sqrt(Lz(z1))
for z1 = 1 : dz
    T1(:, :, z1, :, :) = T1(:, :, z1, :, :) * sqrt(Lz(z1)) ;
end

%* T1(x,y,z1,w1,m1) = T1(x,y,z1,w1,m1)*sqrt(Lw(w1))
for w1 = 1 : dw
    T1(:, :, :, w1, :) = T1(:, :, :, w1, :) * sqrt(Lw(w1)) ;
end

%* T1(x,y,z1,w1,m1) -> T1(z1,w1,x,m1,y)
T1 = permute(T1, [3, 4, 1, 5, 2]) ;

%* T1(z1,w1,x,m1,y) -> M1((z1,w1,x,m1),y)
M1 = reshape(T1, [dz * dw * dx * dm, dy]) ;