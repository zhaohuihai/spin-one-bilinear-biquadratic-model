function [T1, T2, c1, c2] = computeProjection(parameter, projectionOperator, A1, A2)
%* T1((x,l),y1,z1,w1,m1) T2((x,l),y2,z2,w2,m2)


%* Pa(m1,n1,l)
Pa = projectionOperator.Pa ;

%* T1(x,l,y1,z1,w1,m1) = sum{n1}_[A1(x,y1,z1,w1,n1)*Pa(m1,n1,l)]
T1 = contractTensors(A1, 5, 5, Pa, 3, 2, [1, 6, 2, 3, 4, 5]) ;

%* Pb(m2,n2,l)
Pb = projectionOperator.Pb ;

%* T2(x,l,y2,z2,w2,m2) = sum{n2}_[A2(x,y2,z2,w2,n2)*Pb(m2,n2,l)]
T2 = contractTensors(A2, 5, 5, Pb, 3, 2, [1, 6, 2, 3, 4, 5]) ;

M = parameter.siteDimension ;
D = parameter.bondDimension ;

%* T1(x,l,y1,z1,w1,m1) -> T1((x,l),y1,z1,w1,m1)
T1 = reshape(T1, [D * M^2, D, D, D, M]) ;

%* T2(x,l,y2,z2,w2,m2) -> T2((x,l),y2,z2,w2,m2)
T2 = reshape(T2, [D * M^2, D, D, D, M]) ;

c1 = max(max(max(max(max(abs(T1)))))) ;
c2 = max(max(max(max(max(abs(T2)))))) ;

T1 = T1 ./ c1 ;
T2 = T2 ./ c2 ;