function [wave, truncationError, coef] = applyFirstTrotter(parameter, wave)

c = zeros(1, 4) ;
truncErr_single = zeros(1, 4) ;

% parameter.project.tau = 0 ;

PO = createProjectionOperator(parameter) ;

for j = 1 : 4
    
    if parameter.project.method == 1
        [wave, truncErr_single(j), c(j)] = simpleUpdate(parameter, PO, wave) ;
    elseif parameter.project.method == 3
        [wave, truncErr_single(j), c(j)] = clusterUpdate2X2(parameter, PO, wave) ;
    elseif parameter.project.method == 5
        [wave, truncErr_single(j), c(j)] = clusterUpdate3X2(parameter, PO, wave) ;
    elseif parameter.project.method == 9
        [wave, truncErr_single(j), c(j)] = clusterUpdate_variation(parameter, PO, wave) ;
    elseif parameter.project.method == 11
        [wave, truncErr_single(j), c(j)] = clusterUpdate_doubleLayer(parameter, PO, wave) ;
    end
    % A(x,y,z,w,m) -> A(y,z,w,x,m)
    wave = rotate(wave) ;
    
    
end

if parameter.project.SuperOrth == 1
    wave = superOrthogonal(parameter, wave) ;
end
% wave.Lambda{1}
truncationError = max(truncErr_single) ;
% coef = prod(c) ;

coef = mean(c) ;