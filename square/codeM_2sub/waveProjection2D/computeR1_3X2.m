function R1 = computeR1_3X2(wave, T1, T2)
%* compute R1(x',x)

%* compute K4(y,y')
K4 = computeK4(wave, T2) ;


%---------------------------------------------
%* T2(x,y,z2,w,m2) -> T2(x,w,z2,y,m2)
T2 = permute(T2, [1, 4, 3, 2, 5]) ;

%* 
for i = 1 : 2
    wave.A{i} = permute(wave.A{i}, [1, 4, 3, 2, 5]) ;
end

%* (x,y,z,w) -> (x,w,z,y)
wave.Lambda = wave.Lambda([1, 4, 3, 2]) ;
%---------------------------------------------


%* compute K6(w,w')
K6 = computeK4(wave, T2) ;


%* M1((y,z1,w,m1),x) = sum{y',w'}_[K4(y,y')*K6(w,w')*T1(x,y',z1,w',m1)*sqrt(L3(z1))]
M1 = computeK4_K6_T1_sqrtLz(K4, K6, T1, wave.Lambda{3}, wave.alpha) ;

%* M1((y,z1,w1,m1),x) = sum{x'}_[Q1((y,z1,w1,m1),x')*R1(x',x)]
[~, R1] = qr(M1, 0) ;
R1 = R1 ./ norm(R1) ;

