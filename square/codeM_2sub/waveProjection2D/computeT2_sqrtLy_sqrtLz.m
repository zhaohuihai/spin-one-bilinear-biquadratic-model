function M2 = computeT2_sqrtLy_sqrtLz(T2, Ly, Lz)
%* M2((x,y2,z2,m2),w) = T2(x,y2,z2,w,m2)*sqrt(Ly(y2))*sqrt(Lz(z2))

[dx, dy, dz, dw, dm] = size(T2) ;

%* T2(x,y2,z2,w,m2) = T2(x,y2,z2,w,m2)*sqrt(Ly(y2))
for y2 = 1 : dy
    T2(:, y2, :, :, :) = T2(:, y2, :, :, :) * sqrt(Ly(y2)) ;
end

%* T2(x,y2,z2,w,m2) = T2(x,y2,z2,w,m2)*sqrt(Lz(z2))
for z2 = 1 : dz
    T2(:, :, z2, :, :) = T2(:, :, z2, :, :) * sqrt(Lz(z2)) ;
end

%* T2(x,y2,z2,w,m2) -> T2(x,y2,z2,m2,w)
T2 = permute(T2, [1, 2, 3, 5, 4]) ;

%* T2(x,y2,z2,m2,w) -> M2((x,y2,z2,m2),w)
M2 = reshape(T2, [dx * dy * dz * dm, dw]) ;