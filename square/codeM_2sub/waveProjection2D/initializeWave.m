function wave = initializeWave(parameter)

% rng(1) ;

M = parameter.siteDimension ;
D = parameter.bondDimension ;
if parameter.sameInitialTensor == 1
    %* A(x,y,z,w,m)
    if parameter.complexWave == 1
        A = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
    else
        A = rand(D, D, D, D, M) ;
    end
    L = sort(rand(D, 1), 'descend') ;
    L = L ./ L(1) ;
    for i = 1 : 2
        wave.A{i} = A ;
    end
    for i = 1 : 4
        wave.Lambda{i} = L ;
    end
else %* == 0
    for i = 1 : 2
        if parameter.complexWave == 1
            wave.A{i} = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
        else
            wave.A{i} = rand(D, D, D, D, M) ;
        end
    end
    for i = 1 : 4
        L = sort(rand(D, 1), 'descend') ;
        wave.Lambda{i} = L ./ L(1) ;
    end
end

wave.PR = cell(1, 4) ;
wave.PL = cell(1, 4) ;
for i = 1 : 2
    wave.PR{i} = rand(M^2*D, D) ;
    wave.PL{i} = rand(M^2*D, D) ;
end