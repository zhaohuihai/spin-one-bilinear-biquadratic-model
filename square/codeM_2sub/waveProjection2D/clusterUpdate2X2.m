function [wave, truncationError, coef] = clusterUpdate2X2(parameter, projectionOperator, wave)
%* Tensor to update: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* x. Lambda{1}
%* y. Lambda{2}
%* z. Lambda{3}
%* w. Lambda{4}

%* T1(x,y1,z1,w1,m1) T2(x,y2,z2,w2,m2)
[T1, T2] = computeProjection(parameter, projectionOperator, wave.A{1}, wave.A{2}) ;
% ===============truncation============
%* step 1: compute R1
%* step 1.1: absorb lambda of site 2
%* M2((x,y2,z2,m2),w) = T2(x,y2,z2,w,m2)*sqrt(L2(y2))*sqrt(L3(z2))
M2 = computeT2_sqrtLy_sqrtLz(T2, wave.Lambda{2}, wave.Lambda{3}) ;

%* step 1.2: QR factorization of site 2
%* M2((x,y2,z2,m2),w') = sum{w}_[Q2((x,y2,z2,m2),w)*K2(w,w')]
[~, K2] = qr(M2, 0) ;
K2 = K2 ./ norm(K2) ;

%* step 1.3: move from site 2 to site 3
%* M3((w,x3,y3,m3),z) = sum{w'}_[K2(w,w')*A1(x3,y3,z,w',m3)*sqrt(L1(x3))*sqrt(L2(y3))]
M3 = computeK2_A3_sqrtLx_sqrtLy(K2, wave.A{1}, wave.Lambda{1}, wave.Lambda{2}) ;

%* step 1.4: QR factorization of site 3
%* M3((w,x3,y3,m3),z') = sum{z}_[Q3((w,x3,y3,m3),z)*K3(z,z')]
[~, K3] = qr(M3, 0) ;
K3 = K3 ./ norm(K3) ;

%* step 1.5: move from site 3 to site 4
%* M4((z,w4,x4,m4),y) = sum{z'}_[K3(z,z')*A2(x4,y,z',w4,m4)*sqrt(L4(w4))*sqrt(L1(x4))]
M4 = computeK3_A4_sqrtLw_sqrtLx(K3, wave.A{2}, wave.Lambda{4}, wave.Lambda{1}) ;

%* step 1.6: QR factorization of site 4
%* M4((z,w4,x4,m4),y') = sum{y}_[Q4((z,w4,x4,m4),y)*K4(y,y')]
[~, K4] = qr(M4, 0) ;
K4 = K4 ./ norm(K4) ;

%* step 1.7: move from site 4 to site 1
%* M1((y,z1,w1,m1),x) = sum{y'}_[K4(y,y')*T1(x,y',z1,w1,m1)*sqrt(L3(z1))*sqrt(L4(w1))]
M1 = computeK4_T1_sqrtLz_sqrtLw(K4, T1, wave.Lambda{3}, wave.Lambda{4}) ;

%* step 1.8: QR factorization of site 1
%* M1((y,z1,w1,m1),x) = sum{x'}_[Q1((y,z1,w1,m1),x')*R1(x',x)]
[~, R1] = qr(M1, 0) ;
c1 = norm(R1) ;
R1 = R1 ./ c1 ;
%---------------------------------------------------------------------------------------------
%* step 2: compute R2
%* step 2.1: absorb lambda of site 1
%* M1((z1,w1,x,m1),y) = T1(x,y,z1,w1,m1)*sqrt(L3(z1))*sqrt(L4(w1))
M1 = computeT1_sqrtLz_sqrtLw(T1, wave.Lambda{3}, wave.Lambda{4}) ;

%* step 2.2: QR factorization of site 1
%* M1((z1,w1,x,m1),y') = sum{y}_[Q1((z1,w1,x,m1),y)*K1(y,y')]
[~, K1] = qr(M1, 0) ;
K1 = K1 ./ norm(K1) ;

%* step 2.3: move from site 1 to site 4
%* M4((w4,x4,y,m4),z) = sum{y'}_[K1(y,y')*A2(x4,y',z,w4,m4)*sqrt(L4(w4))*sqrt(L1(x4))]
M4 = computeK1_A4_sqrtLw_sqrtLx(K1, wave.A{2}, wave.Lambda{4}, wave.Lambda{1}) ;

%* step 2.4: QR factorization of site 4
%* M4((w4,x4,y,m4),z') = sum{z}_[Q4((w4,x4,y,m4),z)*K4(z,z')]
[~, K4] = qr(M4, 0) ;
K4 = K4 ./ norm(K4) ;

%* step 2.5: move from site 4 to site 3
%* M3((x3,y3,z,m3),w) = sum{z'}_[K4(z,z')*A1(x3,y3,z',w,m3)*sqrt(L1(x3))*sqrt(L2(y3))]
M3 = computeK4_A3_sqrtLx_sqrtLy(K4, wave.A{1}, wave.Lambda{1}, wave.Lambda{2}) ;

%* step 2.6: QR factorization of site 3
%* M3((x3,y3,z,m3),w') = sum{w}_[Q3((x3,y3,z,m3),w)*K3(w,w')]
[~, K3] = qr(M3, 0) ;
K3 = K3 ./ norm(K3) ;

%* step 2.7: move from site 3 to site 2
%* M2((y2,z2,w,m2),x) = sum{w'}_[K3(w,w')*T2(x,y2,z2,w',m2)*sqrt(L2(y2))*sqrt(L3(z2))]
M2 = computeK3_T2_sqrtLy_sqrtLz(K3, T2, wave.Lambda{2}, wave.Lambda{3}) ;

%* step 2.8: QR factorization of site 2
%* M2((y2,z2,w,m2),x) = sum{x"}_[Q2((y2,z2,w,m2),x")*R2(x",x)]
[~, R2] = qr(M2, 0) ;
c2 = norm(R2) ;
R2 = R2 ./ c2 ;
%-------------------------------------------------------------------------------------------
%* step 3: SVD and truncation
%* M(x',x") = sum{x}_[R1(x',x)*R2(x",x)]
M = R1 * R2.' ;

%* M(x',x") = sum{x}_[U(x',x)*S(x)*conj(V(x",x))]
[U0, S0, V0, coef] = applySVD(M) ; 
% S0 = S0 ./ coef ;
% coef = coef * c1 * c2 ;

Dcut = parameter.bondDimension ;
[U, S, V, truncationError] = truncate(U0, S0, V0, Dcut) ;
%-------------------------------------------------------------------------------------------
sqrtS = sqrt(S) ;
%* step 4: Create Projector
%* change index notation: R1(x',x) -> R1(x",x'); R2(x",x) -> R2(x",x')
%* PR(x',x) = sum{x"}_[R2(x",x')*V(x",x)/sqrtS(x)]
PR = createProjector(R2, V, sqrtS) ;

%* PL(x',x) = sum{x"}_[R1(x",x')*conj(U(x",x))/sqrtS(x)]
U = conj(U) ;
PL = createProjector(R1, U, sqrtS) ;
%-----------------------------------------------------------------------------------------------
wave.Lambda{1} = S ./ coef ;
%*************************************************************************
% [PR, PL] = variation2X2(parameter, wave, T1, T2, PR, PL) ;
%*************************************************************************
%* step 5: Form new system sites: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* A1(x,y1,z1,w1,m1) = sum{x'}_[PR(x',x)*T1(x',y1,z1,w1,m1)]
wave.A{1} = contractTensors(PR, 2, 1, T1, 5, 1) ;

%* A2(x,y2,z2,w2,m2) = sum{x'}_[PL(x',x)*T2(x',y2,z2,w2,m2)]
wave.A{2} = contractTensors(PL, 2, 1, T2, 5, 1) ;



