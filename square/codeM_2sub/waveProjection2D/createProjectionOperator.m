function projectionOperator = createProjectionOperator(parameter)
%* input arguments: parameter.siteDimension, parameter.tau, parameter.H
%* output arguments: Pa(m1,n1,l) Pb(m2,n2,l)

%* H((m1,m2),(n1,n2)) = V*D*V'
[V, D] = eig(parameter.H) ;

expD = diag(exp(- parameter.project.tau * diag(D))) ;
PO = V * expD * (V') ;

%* PO((m1,m2),(n1,n2))
PO = setSmalltoZero(PO) ;
%-----------------------------------------------------------
M = parameter.siteDimension ;
MM = M * M ;
%* PO(m1,m2,n1,n2)
PO = reshape(PO, [M, M, M, M]) ;

%* PO(m1,n1,m2,n2)
PO = permute(PO, [1, 3, 2, 4]) ;

%* PO((m1,n1),(m2,n2))
PO = reshape(PO, [MM, MM]) ;

[U, S, V] = svd(PO) ;

%* Pa((m1,n1),l)
Pa = U * sqrt(S) ;

%* Pb((m2,n2),l)
Pb = V * sqrt(S) ;

%* Pa(m1,n1,l)
projectionOperator.Pa = reshape(Pa, [M, M, MM]) ;

%* Pb(m2,n2,l)
projectionOperator.Pb = reshape(Pb, [M, M, MM]) ;