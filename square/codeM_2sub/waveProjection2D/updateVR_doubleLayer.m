function VR = updateVR_doubleLayer(VR, BBb, TT2, BBt)
%* input: VR((c1,c1'),(z2,z2'),(c2,c2'))

%* VR((z2,z2'),(c2,c2'),(b1,b1'),(y2,y2')) = sum{c1,c1'}_
%* [VR((c1,c1'),(z2,z2'),(c2,c2')) * BBb((b1,b1'),(y2,y2'),(c1,c1'))]
VR = contractTensors(VR, 3, 1, BBb, 3, 3) ;

%* VR((c2,c2'),(b1,b1'),x2,w2,x2',w2') = sum{z2,z2',y2,y2'}_
%* [VR((z2,z2'),(c2,c2'),(b1,b1'),(y2,y2')) * TT2(x2,y2,z2,w2,x2',y2',z2',w2')]
VR = contractTensors(VR, 4, [1, 4], TT2, 8, [3, 7, 2, 6]) ;

%* VR((b1,b1'),x2,x2',(b2,b2')) = sum{w2,w2',c2,c2'}_
%* [VR((c2,c2'),(b1,b1'),x2,w2,x2',w2') * BBt((b2,b2'),(w2,w2'),(c2,c2'))]
VR = contractTensors(VR, 6, [4, 6, 1], BBt, 3, [2, 3]) ;