function K4 = computeK4(wave, T2)

%* step 1.1: absorb lambda of site 2
%* M2((x,y,z2,m2),w) = T2(x,y,z2,w,m2)*sqrt(L2(y2))*sqrt(L3(z2))

% M2 = computeT2_sqrtLy_sqrtLz(T2, wave.Lambda{2}, wave.Lambda{3}) ;
M2 = computeT2_sqrtLz(T2, wave.Lambda{3}, wave.alpha) ;

%* step 1.2: QR factorization of site 2
%* M2((x,y2,z2,m2),w') = sum{w}_[Q2((x,y2,z2,m2),w)*K2(w,w')]
[~, K2] = qr(M2, 0) ;
K2 = K2 ./ norm(K2) ;

%* step 1.3: move from site 2 to site 3
%* M3((w,x3,y3,m3),z) = sum{w'}_[K2(w,w')*A1(x3,y3,z,w',m3)*sqrt(L1(x3))*sqrt(L2(y3))]
M3 = computeK2_A3_sqrtLx_sqrtLy(K2, wave.A{1}, wave.Lambda{1}, wave.Lambda{2}, wave.alpha) ;

%* step 1.4: QR factorization of site 3
%* M3((w,x3,y3,m3),z') = sum{z}_[Q3((w,x3,y3,m3),z)*K3(z,z')]
[~, K3] = qr(M3, 0) ;
K3 = K3 ./ norm(K3) ;

%* step 1.5: move from site 3 to site 4
%* M4((z,w4,x4,m4),y) = sum{z'}_[K3(z,z')*A2(x4,y,z',w4,m4)*sqrt(L4(w4))*sqrt(L1(x4))]
M4 = computeK3_A4_sqrtLw_sqrtLx(K3, wave.A{2}, wave.Lambda{4}, wave.Lambda{1}, wave.alpha) ;

%* step 1.6: QR factorization of site 4
%* M4((z,w4,x4,m4),y') = sum{y}_[Q4((z,w4,x4,m4),y)*K4(y,y')]
[~, K4] = qr(M4, 0) ;
K4 = K4 ./ norm(K4) ;