function [wave, truncationError, coef] = clusterUpdate3X2(parameter, projectionOperator, wave)
%* Tensor to update: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* x. Lambda{1}
%* y. Lambda{2}
%* z. Lambda{3}
%* w. Lambda{4}

%* T1(x,y1,z1,w1,m1) T2(x,y2,z2,w2,m2)
[T1, T2] = computeProjection(parameter, projectionOperator, wave.A{1}, wave.A{2}) ;
% ===============truncation============

wave.alpha = parameter.project.alpha ;

%* step 1: compute R1(x',x)
R1 = computeR1_3X2(wave, T1, T2) ;
%* step 2: compute R2(x",x)
R2 = computeR2_3X2(wave, T1, T2) ;
%-------------------------------------------------------------------------------------------
%* step 3: SVD and truncation
%* M(x',x") = sum{x}_[R1(x',x)*R2(x",x)]
M = R1 * R2.' ;

%* M(x',x") = sum{x}_[U(x',x)*S(x)*V(x",x)]
[U0, S0, V0, coef] = applySVD(M) ; 
% S0 = S0 ./ coef ;
% coef = coef * c1 * c2 ;

Dcut = parameter.bondDimension ;
[U, S, V, truncationError] = truncate(U0, S0, V0, Dcut) ;
%-------------------------------------------------------------------------------------------
sqrtS = sqrt(S) ;
%* step 4: Create Projector
%* change index notation: R1(x',x) -> R1(x",x'); R2(x",x) -> R2(x",x')
%* PR(x',x) = sum{x"}_[R2(x",x')*V(x",x)/sqrtS(x)]
PR = createProjector(R2, V, sqrtS) ;

%* PL(x',x) = sum{x"}_[R1(x",x')*conj(U(x",x))/sqrtS(x)]
U = conj(U) ;
PL = createProjector(R1, U, sqrtS) ;
%-----------------------------------------------------------------------------------------------
%* step 5: Form new system sites: wave.A{1}, wave.A{2}, wave.Lambda{1}
%* A1(x,y1,z1,w1,m1) = sum{x'}_[PR(x',x)*T1(x',y1,z1,w1,m1)]
wave.A{1} = contractTensors(PR, 2, 1, T1, 5, 1) ;

%* A2(x,y2,z2,w2,m2) = sum{x'}_[PL(x',x)*T2(x',y2,z2,w2,m2)]
wave.A{2} = contractTensors(PL, 2, 1, T2, 5, 1) ;

wave.Lambda{1} = S ./ coef ;

