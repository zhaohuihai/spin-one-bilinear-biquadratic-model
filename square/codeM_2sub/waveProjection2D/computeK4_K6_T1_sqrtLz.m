function M1 = computeK4_K6_T1_sqrtLz( K4, K6, T1, Lz, varargin )
%* M1((y,z1,w,m1),x) = sum{y',w'}_[K4(y,y')*K6(w,w')*T1(x,y',z1,w',m1)*sqrt(Lz(z1))]

[dx, dy, dz, dw, dm] = size(T1) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* T1(x,y',z1,w',m1) = T1(x,y',z1,w',m1)*sqrt(Lz(z1))
for z1 = 1 : dz
    T1(:, :, z1, :, :) = T1(:, :, z1, :, :) * Lz(z1).^alpha ;
end


%* M1(y,x,z1,w',m1) = sum{y'}_[K4(y,y')*T1(x,y',z1,w',m1)]
M1 = contractTensors(K4, 2, 2, T1, 5, 2) ;

%* M1(w,y,x,z1,m1) = sum{w'}_[K6(w,w')*M1(y,x,z1,w',m1)]
M1 = contractTensors(K6, 2, 2, M1, 5, 4) ;

%* M1(w,y,x,z1,m1) -> M1(y,z1,w,m1,x)
M1 = permute(M1, [2, 4, 1, 5, 3]) ;

%* M1(y,z1,w,m1,x) -> M1((y,z1,w,m1),x)
M1 = reshape(M1, [dy * dz * dw * dm, dx]) ;