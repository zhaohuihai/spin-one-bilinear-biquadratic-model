function M1 = computeT1_sqrtLz( T1, Lz, varargin )
%* M1((z1,w1,x,m1),y) = T1(x,y,z1,w1,m1)*sqrt(Lz(z1))

[dx, dy, dz, dw, dm] = size(T1) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* T1(x,y,z1,w1,m1) = T1(x,y,z1,w1,m1)*sqrt(Lz(z1))
for z1 = 1 : dz
    T1(:, :, z1, :, :) = T1(:, :, z1, :, :) * Lz(z1).^alpha ;
end

%* T1(x,y,z1,w1,m1) -> T1(z1,w1,x,m1,y)
T1 = permute(T1, [3, 4, 1, 5, 2]) ;

%* T1(z1,w1,x,m1,y) -> M1((z1,w1,x,m1),y)
M1 = reshape(T1, [dz * dw * dx * dm, dy]) ;