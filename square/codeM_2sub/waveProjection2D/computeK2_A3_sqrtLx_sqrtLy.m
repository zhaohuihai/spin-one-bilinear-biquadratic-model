function M3 = computeK2_A3_sqrtLx_sqrtLy( K2, A3, Lx, Ly, varargin)
%* M3((w,x3,y3,m3),z) = sum{w'}_[K2(w,w')*A3(x3,y3,z,w',m3)*sqrt(Lx(x3))*sqrt(Ly(y3))]

[dx, dy, dz, dw, dm] = size(A3) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A3(x3,y3,z,w',m3) = A3(x3,y3,z,w',m3)*sqrt(Lx(x3))
for x3 = 1 : dx
    A3(x3, :, :, :, :) = A3(x3, :, :, :, :) * Lx(x3).^alpha ;
end

%* A3(x3,y3,z,w',m3) = A3(x3,y3,z,w',m3)*sqrt(Ly(y3))
for y3 = 1 : dy
    A3(:, y3, :, :, :) = A3(:, y3, :, :, :) * Ly(y3).^alpha ;
end

%* M3(w,x3,y3,z,m3) = sum{w'}_[K2(w,w')*A3(x3,y3,z,w',m3)]
M3 = contractTensors(K2, 2, 2, A3, 5, 4) ;

%* M3(w,x3,y3,z,m3) -> M3(w,x3,y3,m3,z)
M3 = permute(M3, [1, 2, 3, 5, 4]) ;

%* M3(w,x3,y3,m3,z) -> M3((w,x3,y3,m3),z)
M3 = reshape(M3, [dw * dx * dy * dm, dz]) ;