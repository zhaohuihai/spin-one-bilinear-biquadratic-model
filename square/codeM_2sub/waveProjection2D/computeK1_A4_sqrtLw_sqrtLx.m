function M4 = computeK1_A4_sqrtLw_sqrtLx( K1, A4, Lw, Lx, varargin )
%* M4((w4,x4,y,m4),z) = sum{y'}_[K1(y,y')*A4(x4,y',z,w4,m4)*sqrt(Lw(w4))*sqrt(Lx(x4))]

[dx, dy, dz, dw, dm] = size(A4) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A4(x4,y',z,w4,m4) = A4(x4,y',z,w4,m4)*sqrt(Lw(w4))
for w4 = 1 : dw
    A4(:, :, :, w4, :) = A4(:, :, :, w4, :) * Lw(w4).^alpha ;
end

%* A4(x4,y',z,w4,m4) = A4(x4,y',z,w4,m4)*sqrt(Lx(x4))
for x4 = 1 : dx
    A4(x4, :, :, :, :) = A4(x4, :, :, :, :) * Lx(x4).^alpha ;
end

%* M4(y,x4,z,w4,m4) = sum{y'}_[K1(y,y')*A4(x4,y',z,w4,m4)]
M4 = contractTensors(K1, 2, 2, A4, 5, 2) ;

%* M4(y,x4,z,w4,m4) -> M4(w4,x4,y,m4,z)
M4 = permute(M4, [4, 2, 1, 5, 3]) ;

%* M4(w4,x4,y,m4,z) -> M4((w4,x4,y,m4),z)
M4 = reshape(M4, [dw * dx * dy * dm, dz]) ;