function coef = computePOvalue(wave, bottomMPS, topMPS, VR, VL, eta, T1, T2)

D = parameter.bondDimension ;
Chi = parameter.dim_MPS ;

%* AAb((a1,a1'),(w1,w1'),(b1,b1'))
AAb = createReduceMPS(bottomMPS.A) ;
%* AAb((a1,a1'),(w1,w1'),(b1,b1')) -> AAb(a1,a1',w1,w1',b1,b1')
AAb = reshape(AAb, [Chi, Chi, D, D, Chi, Chi]) ;

%* BBb((b1,b1'),(y2,y2'),(c1,c1'))
BBb = createReduceMPS(bottomMPS.B) ;
%* BBb((b1,b1'),(y2,y2'),(c1,c1')) -> BBb(b1,b1',y2,y2',c1,c1')
BBb = reshape(BBb, [Chi, Chi, D, D, Chi, Chi]) ;

%* AAt((a2,a2'),(y1,y1'),(b2,b2'))
AAt = createReduceMPS(topMPS.A) ;
%* AAt((a2,a2'),(y1,y1'),(b2,b2')) -> AAt(a2,a2',y1,y1',b2,b2')
AAt = reshape(AAt, [Chi, Chi, D, D, Chi, Chi]) ;

%* BBt((b2,b2'),(w2,w2'),(c2,c2'))
BBt = createReduceMPS(topMPS.B) ;
%* BBt((b2,b2'),(w2,w2'),(c2,c2')) -> BBt(b2,b2',w2,w2',c2,c2')
BBt = reshape(BBt, [Chi, Chi, D, D, Chi, Chi]) ;
%--------------------------------------------------------

%* AT(x,y1,z1,w1,x',y1',z1',w1') = sum{m1}_[wave.A(x,y1,z1,w1,m1) * T1(x',y1',z1',w1',m1)]
AT = contractTensors(wave.A, 5, 5, T1, 5, 5) ;

%* BT(x,y2,z2,w2,x',y2',z2',w2') = sum{m2}_[wave.B(x,y2,z2,w2,m2) * T2(x',y2',z2',w2',m2)]
BT = contractTensors(wave.B, 5, 5, T2, 5, 5) ;

%-----------------------------------------------------------
%* h(z2,z2',c2,c2',b1,b1',y2,y2') = sum{c1,c1'}_[VR(c1,c1',z2,z2',c2,c2') * BBb(b1,b1',y2,y2',c1,c1')]
h = contractTensors(VR, 6, [1, 2], BBb, 6, [5, 6]) ;

%* h(c2,c2',b1,b1',x,w2,x',w2') = sum{z2,z2',y2,y2'}_[h(z2,z2',c2,c2',b1,b1',y2,y2') * BT(x,y2,z2,w2,x',y2',z2',w2')]
h = contractTensors(h, 8, [1, 2, 7, 8], BT, 8, [3, 7, 2, 6]) ;

%* h(b1,b1',x,x',b2,b2') = sum{w2,w2',c2,c2'}_[h(c2,c2',b1,b1',x,w2,x',w2') * BBt(b2,b2',w2,w2',c2,c2') ]
h = contractTensors(h, 8, [6, 8, 1, 2], BBt, 6, [3, 4, 5, 6]) ;

%* h(x,x',b2,b2',a1,a1',w1,w1') = sum{b1,b1'}_[h(b1,b1',x,x',b2,b2') * AAb(a1,a1',w1,w1',b1,b1')]
h = contractTensors(h, 6, [1, 2], AAb, 6,  [5, 6]) ;

%* h(b2,b2',a1,a1',y1,z1,y1',z1') = sum{x,x',w1,w1'}_[h(x,x',b2,b2',a1,a1',w1,w1') * AT(x,y1,z1,w1,x',y1',z1',w1')]
h = contractTensors(h, 8, [1, 2, 7, 8], AT, 8, [1, 5, 4, 8]) ;

%* h = sum{}_[h(b2,b2',a1,a1',y1,z1,y1',z1') * AT]