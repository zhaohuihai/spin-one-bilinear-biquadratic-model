function VL = updateVL_doubleLayer(VL, AAb, TT1, AAt)
%* input: VL((a1,a1'),(z1,z1'),(a2,a2'))

%* VL((z1,z1'),(a2,a2'),(w1,w1'),(b1,b1')) = sum{a1,a1'}_
%* [VL((a1,a1'),(z1,z1'),(a2,a2')) * AAb((a1,a1'),(w1,w1'),(b1,b1'))]
VL = contractTensors(VL, 3, 1, AAb, 3, 1) ;

%* VL((a2,a2'),(b1,b1'),x1,y1,x1',y1')  = sum{z1,z1',w1,w1'}_
%* [VL((z1,z1'),(a2,a2'),(w1,w1'),(b1,b1')) * TT1(x1,y1,z1,w1,x1',y1',z1',w1')]
VL = contractTensors(VL, 4, [1, 3], TT1, 8, [3, 7, 4, 8]) ;

%* VL((b1,b1'),x1,x1',(b2,b2')) = sum{a2,a2',y1,y1'}_
%* [VL((a2,a2'),(b1,b1'),x1,y1,x1',y1')*AAt((a2,a2'),(y1,y1'),(b2,b2'))]
VL = contractTensors(VL, 6, [1, 4, 6], AAt, 3, [1, 2]) ;