function VR = initializeVR(AAb, SAb, AAt, SAt, AA1, Lx)

%* SAb(b1,b1')
SAb = diag(SAb) ;
%* bot((a1,a1'),(w,w')) = sum{b1,b1'}_[AAb((a1,a1'),(w,w'),(b1,b1'))*SAb(b1,b1')]
bot = contractTensors(AAb, 3, 3, SAb, 2, [1, 2]) ;


%* Lx(x,x')
Lx = diag(Lx) ;
%* mid((w,w'),(z,z'),(y,y')) = sum{x,x'}_[AA1((w,w'),(z,z'),(x,x'),(y,y'))*Lx(x,x')]
mid = contractTensors(AA1, 4, 3, Lx, 2, [1, 2]) ;

%* SAt(b2,b2')
SAt = diag(SAt) ;
%* top((a2,a2'),(y,y')) = sum{}_[b2,b2']_[AAt((a2,a2'),(y,y'),(b2,b2'))*SAt(b2,b2')]
top = contractTensors(AAt, 3, 3, SAt, 2, [1, 2]) ;


%* VR((a1,a1'),(z,z'),(y,y')) = sum{w,w'}_[bot((a1,a1'),(w,w'))*mid((w,w'),(z,z'),(y,y'))]
VR = contractTensors(bot, 2, 2, mid, 3, 1) ;

%* VR((a1,a1'),(z,z'),(a2,a2')) = sum{y,y'}_[VR((a1,a1'),(z,z'),(y,y'))*top((a2,a2'),(y,y'))]
VR = contractTensors(VR, 3, 3, top, 2, 2) ;