function M2 = computeT2_sqrtLz(T2, Lz, varargin)
%* M2((x,y2,z2,m2),w) = T2(x,y2,z2,w,m2)*sqrt(Lz(z2))

[dx, dy, dz, dw, dm] = size(T2) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* T2(x,y2,z2,w,m2) = T2(x,y2,z2,w,m2)*sqrt(Lz(z2))
for z2 = 1 : dz
    T2(:, :, z2, :, :) = T2(:, :, z2, :, :) * Lz(z2).^alpha ;
end

%* T2(x,y2,z2,w,m2) -> T2(x,y2,z2,m2,w)
T2 = permute(T2, [1, 2, 3, 5, 4]) ;

%* T2(x,y2,z2,m2,w) -> M2((x,y2,z2,m2),w)
M2 = reshape(T2, [dx * dy * dz * dm, dw]) ;