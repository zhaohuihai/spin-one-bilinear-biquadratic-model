function VR = computeVR(wave, T2)
%* VR(z3,z3',x2,x2',z5,z5')

alpha = wave.alpha ;
%--------------------------------------
%* A10(x3,y,z,w,m) = A10(x3,y,z,w,m)*sqrt(L3(z))
A10 = computeA_sqrtLz(wave.A{2}, wave.Lambda{3}, alpha) ;

%* A10(x3,y,z,w,m) = A10(x3,y,z,w,m)*sqrt(L4(w))
A10 = computeA_sqrtLw(A10, wave.Lambda{4}, alpha) ;

%* AA10(x3,y,x3',y') = sum{z,w,m}_[A10(x3,y,z,w,m)*A10(x3',y',z,w,m)]
AA10 = contractTensors(A10, 5, [3, 4, 5], A10, 5, [3, 4, 5]) ;
%--------------------------------------------------------
%* A11(x,y,z2,w,m) = A11(x,y,z2,w,m)*sqrt(L1(x))
A11 = computeA_sqrtLx(wave.A{1}, wave.Lambda{1}, alpha) ;

%* AA11(y,z2,w,y',z2',w') = sum{x,m}_[A11(x,y,z2,w,m)*A11(x,y',z2',w',m)]
AA11 = contractTensors(A11, 5, [1, 5], A11, 5, [1, 5]) ;
%--------------------------------------
%* A12(x5,y,z,w,m) = A12(x5,y,z,w,m)*sqrt(L2(y))
A12 = computeA_sqrtLy(wave.A{2}, wave.Lambda{2}, alpha) ;

%* A12(x5,y,z,w,m) = A12(x5,y,z,w,m)*sqrt(L3(z))
A12 = computeA_sqrtLz(A12, wave.Lambda{3}, alpha) ;

%* AA12(x5,w,x5',w') = sum{y,z,m}_[A12(x5,y,z,w,m)*A12(x5',y,z,w',m)]
AA12 = contractTensors(A12, 5, [2, 3, 5], A12, 5, [2, 3, 5]) ;
%--------------------------------------
%* A3(x3,y,z3,w2,m) = A3(x3,y,z3,w2,m)*sqrt(L2(y))
A3 = computeA_sqrtLy(wave.A{1}, wave.Lambda{2}, alpha) ;

%* AA3(x3,z3,w2,x3',z3',w2') = sum{y,m}_[A3(x3,y,z3,w2,m)*A3(x3',y,z3',w2',m)]
AA3 = contractTensors(A3, 5, [2, 5], A3, 5, [2, 5]) ;
%-----------------------------------------------------------------------
%* TT2(x2,y2,z2,w2,x2',y2',z2',w2') = sum{m}_[T2(x2,y2,z2,w2,m)*T2(x2',y2',z2',w2',m)]
TT2 = contractTensors(T2, 5, 5, T2, 5, 5) ;
%-------------------------------------------------------------------------------------
%* A5(x5,y2,z5,w,m) = A5(x5,y2,z5,w,m)*sqrt(L4(w))
A5 = computeA_sqrtLw(wave.A{1}, wave.Lambda{4}, alpha) ;

%* AA5(x5,y2,z5,x5',y2',z5') = sum{w,m}_[A5(x5,y2,z5,w,m)*A5(x5',y2',z5',w,m)]
AA5 = contractTensors(A5, 5, [4, 5], A5, 5, [4, 5]) ;
%--------------------------------------------------------
%* VR(x3,x3',z2,w,z2',w') = sum{y,y'}_[AA10(x3,y,x3',y')*AA11(y,z2,w,y',z2',w')]
VR = contractTensors(AA10, 4, [2, 4], AA11, 6, [1, 4]) ;

%* VR(x3,x3',z2,z2',x5,x5') = sum{w,w'}_[VR(x3,x3',z2,w,z2',w')*AA12(x5,w,x5',w')]
VR = contractTensors(VR, 6, [4, 6], AA12, 4, [2, 4]) ;

%* VR(z3,w2,z3',w2',z2,z2',x5,x5') = sum{x3,x3'}_[AA3(x3,z3,w2,x3',z3',w2')*VR(x3,x3',z2,z2',x5,x5')]
VR = contractTensors(AA3, 6, [1, 4], VR, 6, [1, 2]) ;

%VR(z3,z3',x5,x5',x2,y2,x2',y2')=sum{z2,z2',w2,w2'}_[VR(z3,w2,z3',w2',z2,z2',x5,x5')*TT2(x2,y2,z2,w2,x2',y2',z2',w2')]
VR = contractTensors(VR, 8, [5, 6, 2, 4], TT2, 8, [3, 7, 4, 8]) ;

%* VR(z3,z3',x2,x2',z5,z5') = sum{x5,x5',y2,y2'}_[VR(z3,z3',x5,x5',x2,y2,x2',y2')*AA5(x5,y2,z5,x5',y2',z5')]
VR = contractTensors(VR, 8, [3, 4, 6, 8], AA5, 6, [1, 4, 2, 5]) ;