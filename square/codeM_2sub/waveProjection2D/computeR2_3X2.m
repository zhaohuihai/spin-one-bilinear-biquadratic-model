function R2 = computeR2_3X2(wave, T1, T2)
%* compute R2(x",x)

%* K3(w,w')
K3 = computeK3(wave, T1) ;

%--------------------------------------------
%* T1(x,y,z1,w,m1) -> T1(x,w,z1,y,m1)
T1 = permute(T1, [1, 4, 3, 2, 5]) ;

%* 
for i = 1 : 2
    wave.A{i} = permute(wave.A{i}, [1, 4, 3, 2, 5]) ;
end

%* (x,y,z,w) -> (x,w,z,y)
wave.Lambda = wave.Lambda([1, 4, 3, 2]) ;
%-------------------------------------------------

%* K5(y,y')
K5 = computeK3(wave, T1) ;

%* M2((y,z2,w,m2),x) = sum{y',w'}_[K5(y,y')*K3(w,w')*T2(x,y',z2,w',m2)*sqrt(L3(z2))]
M2 = computeK4_K6_T1_sqrtLz(K5, K3, T2, wave.Lambda{3}, wave.alpha) ;


%* R2(x",x)
[~, R2] = qr(M2, 0) ;
R2 = R2 ./ norm(R2) ;
