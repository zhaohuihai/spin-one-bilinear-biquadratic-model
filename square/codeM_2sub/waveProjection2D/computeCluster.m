function [bottomMPS, topMPS, VR, VL, eta] = computeCluster(parameter, wave)
%* VR(c1,c1',z2,z2',c2,c2') VL(a1,a1',z1,z1',a2,a2')

A = wave.A ;

%* A{1}(x,y,z1,w,n1) -> A{1}(w,z1,x,y,n1)
A{1} = permute(A{1}, [4, 3, 1, 2, 5]) ;
%* A{2}(x,y,z2,w,n2) -> A{2}(y,x,z2,w,n2)
A{2} = permute(A{2}, [2, 1, 3, 4, 5]) ;

AA = cell(1, 2) ;
%* AA{1}((w,w'),(z,z'),(x,x'),(y,y'))
AA{1} = createReduceTNS(A{1}) ;
%* AA{2}((y,y'),(x,x'),(z,z'),(w,w'))
AA{2} = createReduceTNS(A{2}) ;
%*
Lx = wave.Lambda{1} ;
Ly = wave.Lambda{2} ;
Lz = wave.Lambda{3} ;
Lw = wave.Lambda{4} ;

%-----------------------------------------------------------------------------
%* bottomMPS A((a1,a1'),(w,w'),(b1,b1')) B((b1,b1'),(y,y'),(c1,c1'))
%* topMPS A((a2,a2'),(y,y'),(b2,b2')) B((b2,b2'),(w,w'),(c2,c2'))
[bottomMPS, topMPS] = contractCluster1D(parameter, AA, Lx, Ly, Lz, Lw) ;
%-----------------------------------------------------------------------------

%* VR((c1,c1'),(z2,z2'),(c2,c2'))
%* VL((a1,a1'),(z1,z1'),(a2,a2'))
[VR, ~, VL, ~, eta] = contractCluster0D(parameter, AA, Lx, Lz, bottomMPS, topMPS) ;
%------------------------------------------------------------------------------
