function M3 = computeK4_A3_sqrtLx_sqrtLy( K4, A3, Lx, Ly, varargin )
%* M3((x3,y3,z,m3),w) = sum{z'}_[K4(z,z')*A3(x3,y3,z',w,m3)*sqrt(Lx(x3))*sqrt(Ly(y3))]

[dx, dy, dz, dw, dm] = size(A3) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A3(x3,y3,z',w,m3) = A3(x3,y3,z',w,m3)*sqrt(Lx(x3))
for x3 = 1 : dx
    A3(x3, :, :, :, :) = A3(x3, :, :, :, :) * Lx(x3).^alpha ;
end

%* A3(x3,y3,z',w,m3) = A3(x3,y3,z',w,m3)*sqrt(Ly(y3))
for y3 = 1 : dy
    A3(:, y3, :, :, :) = A3(:, y3, :, :, :) * Ly(y3).^alpha ;
end

%* M3(z,x3,y3,w,m3) = sum{z'}_[K4(z,z')*A3(x3,y3,z',w,m3)]
M3 = contractTensors(K4, 2, 2, A3, 5, 3) ;

%* M3(z,x3,y3,w,m3) -> M3(x3,y3,z,m3,w) 
M3 = permute(M3, [2, 3, 1, 5, 4]) ;

%* M3(x3,y3,z,m3,w) -> M3((x3,y3,z,m3),w)
M3 = reshape(M3, [dx * dy * dz * dm, dw]) ;