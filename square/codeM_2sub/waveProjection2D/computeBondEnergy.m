function energy = computeBondEnergy(parameter, wave, bottomMPS, topMPS, VR, VL, eta)

A = wave.A ;

%* A{1}(x,y,z1,w,n1) -> A{1}(w,z1,x,y,n1)
A{1} = permute(A{1}, [4, 3, 1, 2, 5]) ;
%* A{2}(x,y,z2,w,n2) -> A{2}(y,x,z2,w,n2)
A{2} = permute(A{2}, [2, 1, 3, 4, 5]) ;

Chi = parameter.dim_MPS ;
D = parameter.bondDimension ;

%* VR((c1,c1'),(z2,z2'),(c2,c2'))
VR = reshape(VR, [Chi*Chi, D*D, Chi*Chi]) ;
%* VL((a1,a1'),(z1,z1'),(a2,a2'))
VL = reshape(VL, [Chi*Chi, D*D, Chi*Chi]) ;

%--------------------------------------------
operator = createHamiltonian_Heisenberg(parameter) ;
[TH1, TH2] = createSpecialTensor(parameter, operator, A) ;
energy = contractFinal(bottomMPS, topMPS, VR, VL, eta, TH1, TH2) ;