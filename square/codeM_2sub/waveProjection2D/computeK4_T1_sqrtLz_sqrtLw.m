function M1 = computeK4_T1_sqrtLz_sqrtLw( K4, T1, Lz, Lw )
%* M1((y,z1,w1,m1),x) = sum{y'}_[K4(y,y')*T1(x,y',z1,w1,m1)*sqrt(Lz(z1))*sqrt(Lw(w1))]

[dx, dy, dz, dw, dm] = size(T1) ;

%* T1(x,y',z1,w1,m1) = T1(x,y',z1,w1,m1)*sqrt(Lz(z1))
for z1 = 1 : dz
    T1(:, :, z1, :, :) = T1(:, :, z1, :, :) * sqrt(Lz(z1)) ;
end

%* T1(x,y',z1,w1,m1) = T1(x,y',z1,w1,m1)*sqrt(Lw(w1))
for w1 = 1 : dw
    T1(:, :, :, w1, :) = T1(:, :, :, w1, :) * sqrt(Lw(w1)) ;
end

%* M1(y,x,z1,w1,m1) = sum{y'}_[K4(y,y')*T1(x,y',z1,w1,m1)]
M1 = contractTensors(K4, 2, 2, T1, 5, 2) ;

%* M1(y,x,z1,w1,m1) -> M1(y,z1,w1,m1,x)
M1 = permute(M1, [1, 3, 4, 5, 2]) ;

%* M1(y,z1,w1,m1,x) -> M1((y,z1,w1,m1),x)
M1 = reshape(M1, [dy * dz * dw * dm, dx]) ;