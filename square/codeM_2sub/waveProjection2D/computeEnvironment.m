function Me = computeEnvironment(parameter, bottomMPS, topMPS, VR, VL, T1, T2)
%* VR(c1,c1',z2,z2',c2,c2') VL(a1,a1',z1,z1',a2,a2')
%* T1(x1,y1,z1,w1,m1) T2(x2,y2,z2,w2,m2)

D = parameter.bondDimension ;
Chi = size(bottomMPS.A, 1) ;

%* AAb((a1,a1'),(w1,w1'),(b1,b1'))
AAb = createReduceMPS(bottomMPS.A) ;
%* AAb((a1,a1'),(w1,w1'),(b1,b1')) -> AAb(a1,a1',w1,w1',b1,b1')
AAb = reshape(AAb, [Chi, Chi, D, D, Chi, Chi]) ;

%* BBb((b1,b1'),(y2,y2'),(c1,c1'))
BBb = createReduceMPS(bottomMPS.B) ;
%* BBb((b1,b1'),(y2,y2'),(c1,c1')) -> BBb(b1,b1',y2,y2',c1,c1')
BBb = reshape(BBb, [Chi, Chi, D, D, Chi, Chi]) ;

%* AAt((a2,a2'),(y1,y1'),(b2,b2'))
AAt = createReduceMPS(topMPS.A) ;
%* AAt((a2,a2'),(y1,y1'),(b2,b2')) -> AAt(a2,a2',y1,y1',b2,b2')
AAt = reshape(AAt, [Chi, Chi, D, D, Chi, Chi]) ;

%* BBt((b2,b2'),(w2,w2'),(c2,c2'))
BBt = createReduceMPS(topMPS.B) ;
%* BBt((b2,b2'),(w2,w2'),(c2,c2')) -> BBt(b2,b2',w2,w2',c2,c2')
BBt = reshape(BBt, [Chi, Chi, D, D, Chi, Chi]) ;
%--------------------------------------------------------
%* TT1(x1,y1,z1,w1,x1',y1',z1',w1') = sum{m}_[T1(x1,y1,z1,w1,m)*T1(x1',y1',z1',w1',m)]
TT1 = contractTensors(T1, 5, 5, T1, 5, 5) ;
%----------------------------------------------------------------------------------------
%* TT2(x2,y2,z2,w2,x2',y2',z2',w2') = sum{m}_[T2(x2,y2,z2,w2,m)*T2(x2',y2',z2',w2',m)]
TT2 = contractTensors(T2, 5, 5, T2, 5, 5) ;
%-------------------------------------------------------------------------------------

%* VR(b1,b1',x2,x2',b2,b2')
VR = updateVR(VR, BBb, TT2, BBt) ;

%* VL(b1,b1',x1,x1',b2,b2')
VL = updateVL(VL, AAb, TT1, AAt) ;

%* Me(x1,x1',x2,x2') = sum{b1,b1',b2,b2'}_[VL(b1,b1',x1,x1',b2,b2') * VR(b1,b1',x2,x2',b2,b2')]
Me = contractTensors(VL, 6, [1, 2, 5, 6], VR, 6, [1, 2, 5, 6]) ;

Me = Me ./ max(max(max(max(abs(Me))))) ;


