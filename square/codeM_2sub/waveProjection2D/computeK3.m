function K3 = computeK3(wave, T1)

%* step 2.1: absorb lambda of site 1
%* M1((z1,w1,x,m1),y) = T1(x,y,z1,w1,m1)*sqrt(L3(z1))*sqrt(L4(w1))
% M1 = computeT1_sqrtLz_sqrtLw(T1, wave.Lambda{3}, wave.Lambda{4}) ;

M1 = computeT1_sqrtLz(T1, wave.Lambda{3}, wave.alpha) ;

%* step 2.2: QR factorization of site 1
%* M1((z1,w1,x,m1),y') = sum{y}_[Q1((z1,w1,x,m1),y)*K1(y,y')]
[~, K1] = qr(M1, 0) ;
K1 = K1 ./ norm(K1) ;

%* step 2.3: move from site 1 to site 4
%* M4((w4,x4,y,m4),z) = sum{y'}_[K1(y,y')*A2(x4,y',z,w4,m4)*sqrt(L4(w4))*sqrt(L1(x4))]
M4 = computeK1_A4_sqrtLw_sqrtLx(K1, wave.A{2}, wave.Lambda{4}, wave.Lambda{1}, wave.alpha) ;

%* step 2.4: QR factorization of site 4
%* M4((w4,x4,y,m4),z') = sum{z}_[Q4((w4,x4,y,m4),z)*K4(z,z')]
[~, K4] = qr(M4, 0) ;
K4 = K4 ./ norm(K4) ;

%* step 2.5: move from site 4 to site 3
%* M3((x3,y3,z,m3),w) = sum{z'}_[K4(z,z')*A1(x3,y3,z',w,m3)*sqrt(L1(x3))*sqrt(L2(y3))]
M3 = computeK4_A3_sqrtLx_sqrtLy(K4, wave.A{1}, wave.Lambda{1}, wave.Lambda{2}, wave.alpha) ;

%* step 2.6: QR factorization of site 3
%* M3((x3,y3,z,m3),w') = sum{w}_[Q3((x3,y3,z,m3),w)*K3(w,w')]
[~, K3] = qr(M3, 0) ;
K3 = K3 ./ norm(K3) ;