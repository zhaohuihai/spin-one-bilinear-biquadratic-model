function parameter = defineGlobalParameter

%* yes: 1, no: 0
parameter.loadPreviousWave = 0 ; %*
%* yes: 1, no: 0
parameter.projection = 1 ; 
%********************************************************************
parameter.numCPUthreads = 48 ;

%********************************************************************
parameter.display = 1 ;
%********************************************************************
parameter.computation.expectationValue = 1 ;
%--------------------------------------------
parameter.computation.OneSiteOperator = 1 ;
parameter.computation.S = 1 ;
parameter.computation.Q = 0 ;
%--------------------------------------------
parameter.computation.twoSiteOperator = 1 ;
%* staggered spontaneous magnetization
parameter.computation.stagSz = 0 ;

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
parameter.computation.Qzz = 0 ;

%* staggered quadrupole
parameter.computation.stagQzz = 0 ;

% energy computation
parameter.computation.energy = 1 ;
parameter.computation.energyDerivative = 0 ;

%* nearest neighbor spin correlation
parameter.computation.nearSpinCorrelation = 0 ;
%----------------------------------------------
