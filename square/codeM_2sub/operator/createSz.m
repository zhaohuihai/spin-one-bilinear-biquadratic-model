function Sz = createSz(parameter)
%* Sz(m,n)
M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Sz = zeros(M, M) ;


%* assign values to diagonal entries
for q1 = 1 : M
    m1 = S - q1 + 1 ;
    Sz(q1, q1) = Sz(q1, q1) + m1 ;
end
