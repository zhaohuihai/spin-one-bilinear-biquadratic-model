function AA65 = computeAA65(wave)
%* AA65(w1,w1',y2,y2')

alpha = wave.alpha ;

%* A5(x5,y2,z,w5,m5) = A5(x5,y2,z,w5,m5)*sqrt(L4(w5))
A5 = computeA_sqrtLw(wave.A{1}, wave.Lambda{4}, alpha) ;

%* A5(x5,y2,z,w5,m5) = A5(x5,y2,z,w5,m5)*sqrt(L1(x5))
A5 = computeA_sqrtLx(A5, wave.Lambda{1}, alpha) ;

%* AA5(y2,z,y2',z') = sum{x5,w5,m5}_[A5(x5,y2,z,w5,m5)*A5(x5,y2',z',w5,m5)]
AA5 = contractTensors(A5, 5, [1, 4, 5], A5, 5, [1, 4, 5]) ;

%* A6(x6,y6,z,w1,m6) = A6(x6,y6,z,w1,m6)*sqrt(L1(x6))
A6 = computeA_sqrtLx(wave.A{2}, wave.Lambda{1}, alpha) ;

%* A6(x6,y6,z,w1,m6) = A6(x6,y6,z,w1,m6)*sqrt(L2(y6))
A6 = computeA_sqrtLy(A6, wave.Lambda{2}, alpha) ;

%* AA6(z,w1,z',w1') = sum{x6,y6,m6}_[A6(x6,y6,z,w1,m6)*A6(x6,y6,z',w1',m6)]
AA6 = contractTensors(A6, 5, [1, 2, 5], A6, 5, [1, 2, 5]) ;

%* AA65(w1,w1',y2,y2') = sum{z,z'}_[AA6(z,w1,z',w1')*AA5(y2,z,y2',z')]
AA65 = contractTensors(AA6, 4, [1, 3], AA5, 4, [2, 4]) ;