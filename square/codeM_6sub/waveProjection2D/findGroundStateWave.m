function wave = findGroundStateWave(parameter, wave)


if parameter.loadPreviousWave == 1
    if parameter.project.polarization == 1
        warning('Old wave is loaded, unnecessary to polarize')
        disp('The projection process will NOT polarize the wave function')
        parameter.project.polarization = 0 ;
    end
end

% use simple update to polarize
if parameter.project.polarization == 1
    wave = polarizeWave(parameter, wave) ;
end
%-------------------------------------------------------------------------------------------------------------
if parameter.project.method == 1
    disp('simple update') ;
    wave = applyWaveProjection(parameter, wave) ;
elseif parameter.project.method == 9
    clusterLength = parameter.project.clusterLength ;
    clusterWidth = parameter.project.clusterWidth ;
    disp(['variational cluster update, cluster size: ', num2str(clusterLength), ' X ' , num2str(clusterWidth)]) ;
    
    parameter.project.eachStep = parameter.project.eachStep_variation ;
    parameter.project.tol_projection = parameter.project.tol_projection_variation ;
    parameter.project.maxProjectionStep = parameter.project.maxProjectionStep_variation ;
    %----------------------------------------------------------    
    wave = applyWaveProjection(parameter, wave) ;
elseif parameter.project.method == 10
    parameter.project.method = 1 ;
    disp('simple update') ;    
    wave = applyWaveProjection(parameter, wave) ;
    %==========================================================
    parameter.project.method = 9 ;
    clusterLength = parameter.project.clusterLength ;
    clusterWidth = parameter.project.clusterWidth ;
    disp(['variational cluster update, cluster size: ', num2str(clusterLength), ' X ' , num2str(clusterWidth)]) ;
    
    
    parameter.project.tauInitial = parameter.project.tauInitial_variation ;
    parameter.project.eachStep = parameter.project.eachStep_variation ;
    parameter.project.tol_projection = parameter.project.tol_projection_variation ;
    parameter.project.maxProjectionStep = parameter.project.maxProjectionStep_variation ;
    %----------------------------------------------------------
    wave = applyWaveProjection(parameter, wave) ;
    
elseif parameter.project.method == 11
    disp('double layer cluster update') ;
    wave = applyWaveProjection(parameter, wave) ;
elseif parameter.project.method == 12
    parameter.project.method = 1 ;
    disp('simple update') ;    
    wave = applyWaveProjection(parameter, wave) ;
    %--------------------------------------------------
    parameter.project.method = 11 ;
    parameter.project.tauInitial = parameter.project.tauInitial_variation ;
    disp('double layer cluster update') ;
    wave = applyWaveProjection(parameter, wave) ;
end




