function converge = compareLambda(wave0, wave)

n = length(wave.Lambda{1}) ;

bondNo = length(wave.Lambda) ;

c = zeros(1, bondNo) ;
for i = 1 : bondNo
    c(i) = norm(wave0.Lambda{i} - wave.Lambda{i}) / sqrt(n - 1) ;
end

converge = max(c) ;
