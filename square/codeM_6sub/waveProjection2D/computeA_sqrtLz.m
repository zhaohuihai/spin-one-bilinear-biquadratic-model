function A = computeA_sqrtLz(A, Lz, varargin )

[~, ~, dz, ~, ~] = size(A) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A(x,y,z,w,m) = A(x,y,z,w,m)*sqrt(Lz(z))
for z = 1 : dz
    A(:, :, z, :, :) = A(:, :, z, :, :) * Lz(z).^alpha ;
end
