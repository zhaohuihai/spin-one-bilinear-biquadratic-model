function [PR, PL] = variation2X2(parameter, wave, T1, T2, PR, PL)

wave.alpha = parameter.project.alpha_variation ;

%* AA43(y1,y1',w2,w2')
AA43 = computeAA43(wave) ;

AA43 = AA43 ./ max(max(max(max(abs(AA43))))) ;

alpha = wave.alpha ;

%* T1(x1,y1,z1,w1,m1) = T1(x1,y1,z1,w1,m1)*sqrt(L3(z1))
T1 = computeA_sqrtLz(T1, wave.Lambda{3}, alpha) ;

%* T1(x1,y1,z1,w1,m1) = T1(x1,y1,z1,w1,m1)*sqrt(L4(w1))
T1 = computeA_sqrtLw(T1, wave.Lambda{4}, alpha) ;

T1 = T1 ./ max(max(max(max(max(abs(T1)))))) ;

%* T2(x2,y2,z2,w2,m2) = T2(x2,y2,z2,w2,m2)*sqrt(L2(y2))
T2 = computeA_sqrtLy(T2, wave.Lambda{2}, alpha) ;

%* T2(x2,y2,z2,w2,m2) = T2(x2,y2,z2,w2,m2)*sqrt(L3(z2))
T2 = computeA_sqrtLz(T2, wave.Lambda{3}, alpha) ;

T2 = T2 ./ max(max(max(max(max(abs(T2)))))) ;

%* TT1(x1,y1,x1',y1') = sum{z1,w1,m1}_[T1(x1,y1,z1,w1,m1)*T1(x1',y1',z1,w1,m1)]
TT1 = contractTensors(T1, 5, [3, 4, 5], T1, 5, [3, 4, 5]) ;

%* TT2(x2,w2,x2',w2') = sum{y2,z2,m2}_[T2(x2,y2,z2,w2,m2)*T2(x2',y2,z2,w2',m2)]
TT2 = contractTensors(T2, 5, [2, 3, 5], T2, 5, [2, 3, 5]) ;

%* Me(x1,x1',w2,w2') = sum{y1,y1'}_[TT1(x1,y1,x1',y1')*AA43(y1,y1',w2,w2')]
Me = contractTensors(TT1, 4, [2, 4], AA43, 4, [1, 2]) ;

%* Me(x1,x1',x2,x2') = sum{w2,w2'}_[Me(x1,x1',w2,w2')*TT2(x2,w2,x2',w2')]
Me = contractTensors(Me, 4, [3, 4], TT2, 4, [2, 4]) ;

Me = Me ./ max(max(max(max(abs(Me))))) ;

step = 0 ;
converge = 1 ;
while converge > parameter.project.tol_variation && step < parameter.project.maxVariationStep
    step = step + 1 ;
    PR1 = optimizePR(Me, PL) ;

    PL1 = optimizePL(Me, PR1) ;

    converge = max(norm(PR - PR1), norm(PL - PL1)) ;
    
    
    
    PR = PR1 ;
    PL = PL1 ;
end
% disp(['total variation steps = ', num2str(step), ', variation error = ', num2str(converge)]) ;


