function wave = initializeWave(parameter)
%* 6 sublattice wave function

randSeed = parameter.randSeed ;
if randSeed >= 0 
    rng(randSeed) ;
else
    rng('shuffle') ;
end

M = parameter.siteDimension ;
D = parameter.bondDimension ;
if parameter.sameInitialTensor == 1
    
    if parameter.complexWave == 1
        A = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
    else
        A = rand(D, D, D, D, M) ;
    end
    L = sort(rand(D, 1), 'descend') ;
    L = L ./ L(1) ;
    %* A1(a6,a12,a7,a1,m1) A2(a1,a7,a8,a2,m2) A3(a2,a8,a9,a3,m3)
    %* A4(a3,a9,a10,a4,m4) A5(a4,a10,a11,a5,m5) A6(a5,a11,a12,a6)
    for i = 1 : 6
        wave.A{i} = A ;
    end
    for i = 1 : 12
        wave.Lambda{i} = L ;
    end
else %* == 0
    for i = 1 : 6
        if parameter.complexWave == 1
            wave.A{i} = rand(D, D, D, D, M) + 1i * rand(D, D, D, D, M) ;
        else
            wave.A{i} = rand(D, D, D, D, M) ;
        end
    end
    for i = 1 : 12
        L = sort(rand(D, 1), 'descend') ;
        wave.Lambda{i} = L ./ L(1) ;
    end
end