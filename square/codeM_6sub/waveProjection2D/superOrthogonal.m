function wave = superOrthogonal(parameter, wave)

wave0 = wave ;

step = 0 ;
converge = 1 ;
while converge > parameter.project.tol_SO && step < parameter.project.maxSOstep
    step = step + 1 ;
    
    for i = 1 : 4
        wave = Orthogonal_bond(parameter, wave) ;
        
        % A(x,y,z,w,m) -> A(y,z,w,x,m)
        wave = rotate(wave) ;
    end
    converge = compareLambda(wave0, wave) ;
    wave0 = wave ;
end