function M2 = computeA2_sqrtL7_sqrtL8_sqrtL2( A2, L7, L8, L2, varargin )
%* M2((a7,a8,a2,m2),a1) = A2(a1,a7,a8,a2,m2)*sqrt(L7(a7))*sqrt(L8(a8))*sqrt(L2(a2))

[d1, d7, d8, d2, dm] = size(A2) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A2(a1,a7,a8,a2,m2)*sqrt(L7(a7))
for a7 = 1 : d7 
    A2(:, a7, :, :, :) = A2(:, a7, :, :, :) * L7(a7).^alpha ;
end

%* A2(a1,a7,a8,a2,m2)*sqrt(L8(a8))
for a8 = 1 : d8
    A2(:, :, a8, :, :) = A2(:, :, a8, :, :) * L8(a8).^alpha ;
end

%* A2(a1,a7,a8,a2,m2)*sqrt(L2(a2))
for a2 = 1 : d2
    A2(:, :, :, a2, :) = A2(:, :, :, a2, :) * L2(a2).^alpha ;
end

%* A2(a1,a7,a8,a2,m2) -> A2(a7,a8,a2,m2,a1)
A2 = permute(A2, [2, 3, 4, 5, 1]) ;

%* A2(a7,a8,a2,m2,a1) -> M2((A2(a7,a8,a2,m2),a1))
M2 = reshape(A2, [d7 * d8 * d2 * dm, d1]) ;