function wave = applyWaveProjection(parameter, wave)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

disp(['site dimension = ', num2str(M)]) ;
disp(['wave function bond dimension = ', num2str(D)]) ;
disp(['theta = ', num2str(theta)]) ;

if parameter.loadPreviousWave == 0
    parameter.project.tau = parameter.project.tauInitial ;
    wave.polarization = parameter.project.polarization ;
end

tauChangeFactor = parameter.project.tauChangeFactor ;

if isfield(wave, 'totalProjectionTimes')
    j = wave.totalProjectionTimes ;
else
    j = 0 ;
end
%**********************************************************************************
%* create polarized hamiltonian
%* hamiltonian is 1 X 6 cell array: h12, h23, h34, h45, h56, h61
hamiltonian = createHamiltonian_polarized(parameter) ;
%**********************************************************************************

coef0 = 1 ;
wave0 = wave ;
eachStep = parameter.project.eachStep ;

tauFinal = parameter.project.tauFinal ;

n = 1 ; % tau change times
tau = parameter.project.tau ;
while tau >= parameter.project.tauFinal
    parameter.project.tau = tau ;
    tol_projection = parameter.project.tol_projection * (tau / tauFinal)
    maxProjectionStep = parameter.project.maxProjectionStep / (tau / tauFinal) ;
    disp(['tau = ', num2str(tau)])
    
    converge = 1000 ;
    step = 0 ;
    t1 = 0 ;
    %-------------------------------------------------------------------------------------------------------------------
    while converge > tol_projection && step <= maxProjectionStep
        
        j = j + 1 ;
        step = step + 1 ;
        tic
        [wave, truncationError, coef] = applyFirstTrotter(parameter, hamiltonian, wave) ;
        t1 = t1 + toc ;
        if (j / eachStep) == floor(j / eachStep)
            
            tic
%             if parameter.project.method == 1 && parameter.estimate.method == 1
                
                converge = compareLambda(wave0, wave) ;
%             else
%                 coef = estimateEnergy(parameter, wave) ;
%                 converge = abs(coef0 - coef) / abs(coef) ;
%             end
            t2 = toc ;
            
            if (3 * t2) > t1
                eachStep = eachStep * 2 ;
                disp('convergence checking cost too much time!')
                disp(['increase eachStep to: ', num2str(eachStep)])
            end
            
            disp(['projection step = ', num2str(step), ', tau = ', num2str(tau)])
            disp(['truncation error = ', num2str(truncationError)])
            disp(['projection convergence error = ', num2str(converge)]) ;
            disp(['average projection time: ', num2str(t1 / eachStep), ' s']) ;
            coef0 = coef ;
            wave0 = wave ;
            t1 = 0 ;
        end
    end
    %-------------------------------------------------------------------------------------------------------------------
    wave.totalProjectionTimes = j ;
    
    saveWave(parameter, wave) ;
    
    tau = 1 / ((1 / tau) + tauChangeFactor) ;
    %     tauChangeFactor = parameter.project.tauChangeFactor * n^2 ;
    tauChangeFactor = parameter.project.tauChangeFactor * n ;
    if parameter.project.tau > parameter.project.tauFinal && tau < parameter.project.tauFinal
        tau = parameter.project.tauFinal ;
    end
    n = n + 1 ;
end
totalProjectionTimes = j
saveWave(parameter, wave) ;
