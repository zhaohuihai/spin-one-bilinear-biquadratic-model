function M1 = computeA1_sqrtL6_sqrtL12_sqrtL7( A1, L6, L12, L7, varargin )
%* M1((a6,a12,a7,m1),a1) = A1(a6,a12,a7,a1,m1)*sqrt(L6(a6))*sqrt(L12(a12))*sqrt(L7(a7))

[d6, d12, d7, d1, dm] = size(A1) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A1(a6,a12,a7,a1,m1)*sqrt(L6(a6))
for a6 = 1 :  d6 
    A1(a6, :, :, :, :) = A1(a6, :, :, :, :) * L6(a6).^alpha ;
end

%* A1(a6,a12,a7,a1,m1)*sqrt(L12(a12))
for a12 = 1 : d12
    A1(:, a12, :, :, :) = A1(:, a12, :, :, :) * L12(a12).^alpha ;
end

%* A1(a6,a12,a7,a1,m1)*sqrt(L7(a7))
for a7 = 1 : d7
    A1(:, :, a7, :, :) = A1(:, :, a7, :, :) * L7(a7).^alpha ;
end

%* A1(a6,a12,a7,a1,m1) -> A1(a6,a12,a7,m1,a1)
A1 = permute(A1, [1, 2, 3, 5, 4]) ;

%* A1(a6,a12,a7,m1,a1) -> M1((a6,a12,a7,m1),a1)
M1 = reshape(A1, [d6 * d12 * d7 * dm, d1]) ;