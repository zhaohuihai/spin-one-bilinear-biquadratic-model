function M1 = computeA1_sqrtLy_sqrtLz_sqrtLw( A1, Ly, Lz, Lw, varargin )
%* M1((y,z,w,m),x) = A1(x,y,z,w,m)*sqrt(Ly(y))*sqrt(Lz(z))*sqrt(Lw(w))

[dx, dy, dz, dw, dm] = size(A1) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A1(x,y,z,w,m) = A1(x,y,z,w,m)*sqrt(Ly(y))
for y = 1 :  dy 
    A1(:, y, :, :, :) = A1(:, y, :, :, :) * Ly(y).^alpha ;
end

%* A1(x,y,z,w,m) = A1(x,y,z,w,m)*sqrt(Lz(z))
for z = 1 : dz
    A1(:, :, z, :, :) = A1(:, :, z, :, :) * Lz(z).^alpha ;
end

%* A1(x,y,z,w,m) = A1(x,y,z,w,m)*sqrt(Lw(w))
for w = 1 : dw
    A1(:, :, :, w, :) = A1(:, :, :, w, :) * Lw(w).^alpha ;
end

%* A1(x,y,z,w,m) -> A1(y,z,w,m,x)
A1 = permute(A1, [2, 3, 4, 5, 1]) ;

%* A1(y,z,w,m,x) -> M1((y,z,w,m),x)
M1 = reshape(A1, [dy * dz * dw * dm, dx]) ;