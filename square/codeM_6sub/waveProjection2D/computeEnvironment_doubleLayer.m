function Me = computeEnvironment_doubleLayer(parameter, bottomMPS, topMPS, VR, VL, T1, T2)
%* VR((c1,c1'),(z2,z2'),(c2,c2')) VL((a1,a1'),(z1,z1'),(a2,a2'))
%* T1(x1,y1,z1,w1,m1) T2(x2,y2,z2,w2,m2)

D = parameter.bondDimension ;

%* AAb((a1,a1'),(w1,w1'),(b1,b1'))
AAb = bottomMPS.A ;

%* BBb((b1,b1'),(y2,y2'),(c1,c1'))
BBb = bottomMPS.B ;

%* AAt((a2,a2'),(y1,y1'),(b2,b2'))
AAt = topMPS.A ;

%* BBt((b2,b2'),(w2,w2'),(c2,c2'))
BBt = topMPS.B ;
%--------------------------------------------------------
%* TT1(x1,y1,z1,w1,x1',y1',z1',w1') = sum{m}_[T1(x1,y1,z1,w1,m)*T1(x1',y1',z1',w1',m)]
TT1 = contractTensors(T1, 5, 5, T1, 5, 5) ;
%----------------------------------------------------------------------------------------
%* TT2(x2,y2,z2,w2,x2',y2',z2',w2') = sum{m}_[T2(x2,y2,z2,w2,m)*T2(x2',y2',z2',w2',m)]
TT2 = contractTensors(T2, 5, 5, T2, 5, 5) ;
%-------------------------------------------------------------------------------------

%* VR((b1,b1'),x2,x2',(b2,b2'))
VR = updateVR_doubleLayer(VR, BBb, TT2, BBt) ;

%* VL((b1,b1'),x1,x1',(b2,b2'))
VL = updateVL_doubleLayer(VL, AAb, TT1, AAt) ;

%* Me(x1,x1',x2,x2') = sum{b1,b1',b2,b2'}_[VL((b1,b1'),x1,x1',(b2,b2'))*VR((b1,b1'),x2,x2',(b2,b2'))]
Me = contractTensors(VL, 4, [1, 4], VR, 4, [1, 4]) ;

Me = Me ./ max(max(max(max(abs(Me))))) ;


