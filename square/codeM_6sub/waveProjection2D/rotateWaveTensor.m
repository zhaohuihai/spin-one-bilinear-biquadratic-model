function wave = rotateWaveTensor(wave)
%* Lambda(1,2,3,4,5,6,7,8,9,10,11,12) -> Lambda(7,8,9,10,11,12,1,2,3,4,5,6)
%* A1(a6,a12,a7,a1,m1) -> A1(a12,a6,a1,a7,m1)


%* A1(a6,a12,a7,a1,m1) -> A1(a12,a6,a1,a7,m1)
for i = 1 : 6
    wave.A{i} = permute(wave.A{i}, [2, 1, 4, 3, 5]) ;
end

%* 
wave.Lambda = wave.Lambda([7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6]) ;