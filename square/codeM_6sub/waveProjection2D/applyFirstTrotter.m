function [wave, truncationError, coef] = applyFirstTrotter(parameter, hamiltonian, wave)

c = zeros(1, 12) ;
truncErr_single = zeros(1, 12) ;

for i = 1 : 6
    PO(i) = createProjectionOperator(parameter, hamiltonian{i}) ;
end

n = 1 ;
for j = 1 : 2
    for i = 1 : 6
        [wave, truncErr_single(n), c(n)] = simpleUpdate(parameter, PO(i), wave) ;

        % A(1,2,3,4,5,6) -> A(2,3,4,5,6,1)
        %* Lambda(1,2,3,4,5,6,7,8,9,10,11,12) -> Lambda(2,3,4,5,6,1,8,9,10,11,12,7)
        wave = rotateWave234561(wave) ;
        
        n = n + 1 ;
    end
    %* Lambda(1,2,3,4,5,6,7,8,9,10,11,12) -> Lambda(7,8,9,10,11,12,1,2,3,4,5,6)
    %* A1(a6,a12,a7,a1,m1) -> A1(a12,a6,a1,a7,m1)
    wave = rotateWaveTensor(wave) ;
end

truncationError = max(truncErr_single) ;

coef = mean(c) ;