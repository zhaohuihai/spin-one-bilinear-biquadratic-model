function VR = initializeVR_doubleLayer(AAb, SAb, AAt, SAt, AA1, Lx)

alpha = 0.5 ;

%* AAb((a1,a1'),(w,w'),(b,b')) = AAb((a1,a1'),(w,w'),(b,b'))*SAb(b,b')
for i = 1 : length(SAb)
    AAb(:, :, i) = AAb(:, :, i) * SAb(i).^alpha ;
end


%* Lx(x,x')
Lx = diag(Lx) ;
%* mid((w,w'),(z,z'),(y,y')) = sum{x,x'}_[AA1((w,w'),(z,z'),(x,x'),(y,y'))*Lx(x,x')]
mid = contractTensors(AA1, 4, 3, Lx, 2, [1, 2]) ;


%* AAt((a2,a2'),(y,y'),(b,b')) = AAt((a2,a2'),(y,y'),(b,b'))*SAt(b,b')
for i = 1 : length(SAt)
    AAt(:, :, i) = AAt(:, :, i) * SAt(i).^alpha ;
end


%* VR((a1,a1'),(b,b'),(z,z'),(y,y')) = sum{w,w'}_[AAb((a1,a1'),(w,w'),(b,b'))*mid((w,w'),(z,z'),(y,y'))]
VR = contractTensors(AAb, 3, 2, mid, 3, 1) ;

%* VR((a1,a1'),(z,z'),(a2,a2')) = sum{(y,y'),(b,b')}_
%* [VR((a1,a1'),(b,b'),(z,z'),(y,y'))*AAt((a2,a2'),(y,y'),(b,b'))]
VR = contractTensors(VR, 4, [4, 2], AAt, 3, [2, 3]) ;