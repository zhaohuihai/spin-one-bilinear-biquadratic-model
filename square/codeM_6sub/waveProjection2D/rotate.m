function wave = rotate(wave)
%* A(x,y,z,w,m) -> A(y,z,w,x,m)

for i = 1 : 2
    wave.A{i} = permute(wave.A{i}, [2, 3, 4, 1, 5]) ;
end


%* (x,y,z,w) -> (y,z,w,x)
wave.Lambda = wave.Lambda([2, 3, 4, 1]) ;

if isfield(wave, 'PR') && isfield(wave, 'PL')
    wave.PR = wave.PR([2, 3, 4, 1]) ;
    wave.PL = wave.PL([2, 3, 4, 1]) ;
end
