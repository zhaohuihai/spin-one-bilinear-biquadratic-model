function [PR, PL, energy] = variationFull(parameter, wave, T1, T2, PR, PL)
%* T1(x,y1,z1,w1,m1) T2(x,y2,z2,w2,m2)

parameter.dim_MPS = parameter.project.dim_MPS ;
parameter.dim_MPS_initial = parameter.project.dim_MPS_initial ;
parameter.dim_MPS_incre = parameter.project.dim_MPS_incre ;

parameter.dim_phys = parameter.project.dim_phys ;

parameter.tol_iSLTE = parameter.project.tol_iSLTE ;
parameter.tol_SLTE = parameter.project.tol_SLTE ;

parameter.maxISLTEstep = parameter.project.maxISLTEstep ;
parameter.maxSLTEstep = parameter.project.maxSLTEstep ;
%----------------------------------------------------------------------

%* VR(c1,c1',z2,z2',c2,c2') VL(a1,a1',z1,z1',a2,a2')
[bottomMPS, topMPS, VR, VL, eta] = contractTNS(parameter, wave) ;

%* Me(x1,x1',x2,x2')
Me = computeEnvironment(parameter, bottomMPS, topMPS, VR, VL, T1, T2) ;

Me = Me ./ max(max(max(max(abs(Me))))) ;

step = 0 ;
converge = 1 ;
while converge > parameter.project.convergenceCriterion_variation && step < parameter.project.maxVariationStep
    step = step + 1 ;
    PR1 = optimizePR(Me, PL) ;

    PL1 = optimizePL(Me, PR1) ;

    converge = max(norm(PR - PR1), norm(PL - PL1)) ;
    
    
    
    PR = PR1 ;
    PL = PL1 ;
end
%--------------------------------------------------------------------
energy = 1 ;
% energy = computeBondEnergy(parameter, wave, bottomMPS, topMPS, VR, VL, eta) ;