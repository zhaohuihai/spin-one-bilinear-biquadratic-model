function A = computeA_sqrtLw(A, Lw, varargin)

[~, ~, ~, dw, ~] = size(A) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A(x,y,z,w,m) = A(x,y,z,w,m)*sqrt(Lw(w))
for w = 1 : dw
    A(:, :, :, w, :) = A(:, :, :, w, :) * Lw(w).^alpha ;
end
