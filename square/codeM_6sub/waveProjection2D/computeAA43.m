function AA43 = computeAA43(wave)
%* AA43(y1,y1',w2,w2')

alpha = wave.alpha ;

%* A4(x4,y1,z,w4,m4) = A4(x4,y1,z,w4,m4)*sqrt(L4(w4))
A4 = computeA_sqrtLw(wave.A{2}, wave.Lambda{4}, alpha) ;

%* A4(x4,y1,z,w4,m4) = A4(x4,y1,z,w4,m4)*sqrt(L1(x4))
A4 = computeA_sqrtLx(A4, wave.Lambda{1}, alpha) ;

%* AA4(y1,z,y1',z') = sum{x4,w4,m4}_[A4(x4,y1,z,w4,m4)*A4(x4,y1',z',w4,m4)]
AA4 = contractTensors(A4, 5, [1, 4, 5], A4, 5, [1, 4, 5]) ;

%* A3(x3,y3,z,w2,m3) = A3(x3,y3,z,w2,m3)*sqrt(L1(x3))
A3 = computeA_sqrtLx(wave.A{1}, wave.Lambda{1}, alpha) ;

%* A3(x3,y3,z,w2,m3) = A3(x3,y3,z,w2,m3)*sqrt(L2(y3))
A3 = computeA_sqrtLy(A3, wave.Lambda{2}, alpha) ;

%* AA3(z,w2,z',w2') = sum{x3,y3,m3}_[A3(x3,y3,z,w2,m3)*A3(x3,y3,z',w2',m3)]
AA3 = contractTensors(A3, 5, [1, 2, 5], A3, 5, [1, 2, 5]) ;

%* AA43(y1,y1',w2,w2') = sum{z,z'}_[AA4(y1,z,y1',z')*AA3(z,w2,z',w2')]
AA43 = contractTensors(AA4, 4, [2, 4], AA3, 4, [1, 3]) ;