function [PR, PL] = variation3X4(parameter, wave, T1, T2, PR, PL)

wave.alpha = parameter.project.alpha_variation ;

%* VL(z3,z3',x1,x1',z5,z5')
VL = computeVL(wave, T1) ;

%* VR(z3,z3',x2,x2',z5,z5')
VR = computeVR(wave, T2) ;

%* Me(x1,x1',x2,x2') = sum{z3,z3',z5,z5'}_[VL(z3,z3',x1,x1',z5,z5')*VR(z3,z3',x2,x2',z5,z5')]
Me = contractTensors(VL, 6, [1, 2, 5, 6], VR, 6, [1, 2, 5, 6]) ;

Me = Me ./ max(max(max(max(abs(Me))))) ;

step = 0 ;
converge = 1 ;
while converge > parameter.project.tol_variation && step < parameter.project.maxVariationStep
    step = step + 1 ;
    PR1 = optimizePR(Me, PL) ;

    PL1 = optimizePL(Me, PR1) ;

    converge = max(norm(PR - PR1), norm(PL - PL1)) ;
    
    
    
    PR = PR1 ;
    PL = PL1 ;
end
% disp(['total variation steps = ', num2str(step), ', variation error = ', num2str(converge)]) ;


