function wave = polarizeWave(parameter, wave)

disp('Polarize the wave function by simple update') ;
parameter.project.method = 1 ;

parameter.project.tauFinal = parameter.project.tauFinal_polar ;
parameter.project.tol_projection = parameter.project.tol_polarization ;
parameter.project.field = parameter.project.polarField ;


wave = applyWaveProjection(parameter, wave) ;
