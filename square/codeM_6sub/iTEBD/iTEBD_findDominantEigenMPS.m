function [rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T)
%* T1(a3,a6,a4,a1) T2(a1,a4,a5,a2) T3(a2,a5,a6,a3)

% project from bottom to top
[rightMPS, T] = iTEBD_computeDominantRightEigenMPS(parameter, T) ;

%* transpose to left transferMatrix
%* (T1, T2, T3, T4, T5, T6) -> (T6, T5, T4, T3, T2, T1)
%* T6(a5,a11,a12,a6) -> T1(a12,a6,a5,a11)
%* T5 -> T2
%* T4 -> T3
%* T3 -> T4
%* T2 -> T5
%* T1 -> T6
T = transposeTransferMatrix1D(T) ;

[leftMPS, T] = iTEBD_computeDominantRightEigenMPS(parameter, T) ;

T = transposeTransferMatrix1D(T) ;