function [VRa, VRb, etaR] = applyPowerMethod_doubleLayer...
    (parameter, Lx, Lz, SAb, SAt, SBb, SBt, AAb, BBb, AAt, BBt, AA1, AA2)
%* VRa(c,z,c') VRb(b,x,b')

% D = parameter.bondDimension ;
% Chi = parameter.dim_MPS ;
% sqChi = size(AAb, 1) ;
%* 
% VRa = rand(sqChi, D^2, sqChi) ;
% VRb = rand(sqChi, D^2, sqChi) ;

VRa = initializeVR_doubleLayer(AAb, SAb, AAt, SAt, AA1, Lx) ;
VRb = initializeVR_doubleLayer(BBb, SBb, BBt, SBt, AA2, Lz) ;

converge = 1 ;

etaA0 = 1 ;
etaB0 = 1 ;
etaA = 10 ;
etaB = 10 ;
j = 0 ;
while converge >= parameter.tol_power && j < parameter.maxPowerStep
    j = j + 1 ;
    [VRb, etaB] = updateVector(BBb, AA2, BBt, VRa) ;
    
    [VRa, etaA] = updateVector(AAb, AA1, AAt, VRb) ;
    
    errA = abs(etaA - etaA0) / abs(etaA) ;
    errB = abs(etaB - etaB0) / abs(etaB) ;
    
    converge = max(errA, errB) ;
    
    etaA0 = etaA ;
    etaB0 = etaB ;
end

etaR = etaA * etaB ;