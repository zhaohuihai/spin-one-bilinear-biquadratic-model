function saveExpectationValue(parameter, expectValue, valueName)

M = parameter.siteDimension ;
D = parameter.bondDimension ;
theta = parameter.theta ;

dimSave = parameter.dim_MPS ;
variableName = [valueName] ;
dirName = ['D = ', num2str(D)] ;
pathName = ['result/', dirName] ;
fileName = [pathName, '/', valueName, 'Site', num2str(M), '_dimSave', num2str(dimSave), '.mat'] ;

eval([variableName, '= expectValue ;']) ;

save(fileName, variableName) ;
disp(['theta = ', num2str(theta)]) ;
disp([dirName, ', dimSave = ', num2str(dimSave)]) ;
disp([valueName, ': ']) ;
disp(expectValue) ;
