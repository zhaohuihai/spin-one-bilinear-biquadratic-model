function expectValueSite = dealWithSpecialTensor_1site(parameter, T, rightMPS_1, leftMPS_6, A)

rMPS = cell(1, 6) ;

rMPS{1} = rightMPS_1 ;

for i = 1 : 5
    rMPS{i + 1} = iTEBD_computeSingleGate1D(parameter, T{i}, rMPS{i}) ;
end

%* transpose to left transferMatrix
%* (T1, T2, T3, T4, T5, T6) -> (T6, T5, T4, T3, T2, T1)
%* T6(a5,a11,a12,a6) -> T1(a12,a6,a5,a11)
%* T5 -> T2
%* T4 -> T3
%* T3 -> T4
%* T2 -> T5
%* T1 -> T6
T_left = transposeTransferMatrix1D(T) ;

lMPS = cell(1, 6) ;

lMPS{6} = leftMPS_6 ;

for i = 1 : 5
    lMPS{6 - i} = iTEBD_computeSingleGate1D(parameter, T_left{i}, lMPS{7 - i}) ;
end

for i = 1 : 6
    expectValueSite(i) = contractSpecialTensor_1site(parameter, T{i}, rMPS{i}, lMPS{i}, A{i}) ;
end



