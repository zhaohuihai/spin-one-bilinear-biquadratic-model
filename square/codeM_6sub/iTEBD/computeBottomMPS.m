function MPS = computeBottomMPS(parameter, AA, Ly, Lw, SA, SB)

%* bottomMPS A((a1,a1'),(w,w'),(b1,b1')) B((b1,b1'),(y,y'),(c1,c1'))
%* SA((b1,b1')) SB((c1,c1'))
% MPS = initializeMPS(parameter) ;

MPS = initializeMPS_doubleTensor(AA, Ly, Lw, SA, SB) ;

MPS = canonicalize_double(parameter, MPS) ;

step = 0 ;
[MPS, ~, step] = iterateRightGate1D_doubleLayer(parameter, AA, MPS, step) ;
if parameter.display == 1
    disp(['total steps = ', num2str(step)]) ;
end



