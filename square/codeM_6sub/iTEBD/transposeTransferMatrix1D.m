function T = transposeTransferMatrix1D(T)
%* transpose to left transferMatrix
%* (T1, T2, T3, T4, T5, T6) -> (T6, T5, T4, T3, T2, T1)
%* T6(a5,a11,a12,a6) -> T1(a12,a6,a5,a11)
%* T5 -> T2
%* T4 -> T3
%* T3 -> T4
%* T2 -> T5
%* T1 -> T6

%* T6(a5,a11,a12,a6) -> T1(a12,a6,a5,a11)
T1 = permute(T{6}, [3, 4, 1, 2]) ;
%* T1 -> T6
T6 = permute(T{1}, [3, 4, 1, 2]) ;
T{1} = conj(T1) ;
clear T1 ;
T{6} = conj(T6) ;
clear T6 ;


%* T5 -> T2
T2 = permute(T{5}, [3, 4, 1, 2]) ;
%* T2 -> T5
T5 = permute(T{2}, [3, 4, 1, 2]) ;
T{2} = conj(T2) ;
clear T2 ;
T{5} = conj(T5) ;
clear T5 ;

%* T4 -> T3
T3 = permute(T{4}, [3, 4, 1, 2]) ;
%* T3 -> T4
T4 = permute(T{3}, [3, 4, 1, 2]) ;
T{3} = conj(T3) ;
clear T3 ;
T{4} = conj(T4) ;
clear T4 ;



