function A = contractGate_doubleLayer(parameter, A, AA1)
%* A((a,z),y,(b,x)) = sum{w}_[A(a,w,b)*AA1(w,z,x,y)]

D = size(AA1, 1) ;
Chi = size(A, 1) ;

%* A(a,b,z,x,y) = sum{w}_[A(a,w,b)*AA1(w,z,x,y)]
A = contractTensors(A, 3, 2, AA1, 4, 1) ;

%* A(a,b,z,x,y) -> A(a,z,y,b,x)
A = permute(A, [1, 3, 5, 2, 4]) ;

%* A(a,z,y,b,x) -> A((a,z),y,(b,x))
A = reshape(A, [Chi * D, D, Chi * D]) ;
