function [MPS, T, truncationError] = iTEBD_applyRightGateOperation1D(parameter, T, MPS)
%* T1(a6,a12,a7,a1) T2(a1,a7,a8,a2) T3(a2,a8,a9,a3) T4(a3,a9,a10,a4) T5(a4,a10,a11,a5), T6(a5,a11,a12,a6)

truncErr_single = zeros(1, 6) ;

%* apply T{i} gate; then exchange B1 <--> B2
for i = 1 : 6
    
    [MPS, truncErr_single(i)] = iTEBD_computeSingleGate1D(parameter, T{i}, MPS) ;
end
truncationError = max(truncErr_single) ;
