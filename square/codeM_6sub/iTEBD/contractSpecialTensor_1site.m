function expectValueSite = contractSpecialTensor_1site(parameter, T, rightMPS, leftMPS, A)

%* P((c,c'),(e,e'))
P = createTransferMatrix0D_1site(parameter, T, rightMPS, leftMPS) ;

D = parameter.dim_MPS ;
if parameter.complexWave == 1
    vector = rand(D^2, 1) + 1i * rand(D^2, 1) ;
else 
    vector = rand(D^2, 1) ;
end
eta = norm(vector) ;
vector = vector ./ eta ;

vectorR = applyPowerMethod_1site(parameter, P, vector) ;
% P = P.' ;
P = P' ;
vectorL = applyPowerMethod_1site(parameter, P, vector) ;
%*==================================================================================
computation = parameter.computation ;
%* spontaneous magnetization
if computation.S == 1
    operator = createSx(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Sx = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Sx = Sx ;
    
    operator = createSy(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Sy = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Sy = Sy ;
    
    operator = createSz(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Sz = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Sz = Sz ;
end

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
if computation.Q == 1
    operator = createQxx(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Qxx = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Qxx = Qxx ;
    
    operator = createQyy(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Qyy = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Qyy = Qyy ;
    
    operator = createQzz(parameter) ;
    Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS) ;
    Qzz = findExpectationValue_1site(vectorR, vectorL, P, Q) ;
    expectValueSite.Qzz = Qzz ;
end
