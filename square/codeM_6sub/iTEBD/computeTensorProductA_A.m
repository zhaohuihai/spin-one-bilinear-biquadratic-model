function T = computeTensorProductA_A(A)
%* T{1}((z,z'),(w,w'),(y,y'),(x,x')) = sum{m}_[conj(A{1}(z,w,y,x,m))*A{1}(z',w',y',x',m)]
%* T{2}((x,x'),(y,y'),(w,w'),(z,z')) = sum{m}_[conj(A{2}(x,y,w,z,m))*A{2}(x',y',w',z',m)]

[dz, dw, dy, dx, ~] = size(A) ;


%* T(z,z',w,w',y,y',x,x') = sum{m}_[conj(A{1}(z,w,y,x,m))*A{1}(z',w',y',x',m)]
T = contractTensors(conj(A), 5, 5, A, 5, 5, [1, 5, 2, 6, 3, 7, 4, 8]) ;

%* T((z,z'),(w,w'),(y,y'),(x,x'))
T = reshape(T, [dz^2, dw^2, dy^2, dx^2]) ;
