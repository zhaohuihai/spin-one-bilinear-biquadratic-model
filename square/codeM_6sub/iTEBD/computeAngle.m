function [mag, agl] = computeAngle(s)

a = zeros(1, 6) ;
b = zeros(1, 6) ;

for i = 1 : 6
    if i < 6 
        j = i + 1 ;
    else
        j = 1 ;
    end
    
    n1 = norm(s(i, :)) ;
    n2 = norm(s(j, :)) ;
    a(i) = n1 ;
    phi = sum(s(i, :) .* s(j, :)) / (n1 * n2) ;
    
    b(i) = real(acos(phi)) ;
end
b = b ./ pi 
agl = mean(b) ;
mag = mean(a) ;