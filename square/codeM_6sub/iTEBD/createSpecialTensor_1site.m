function Q = createSpecialTensor_1site(parameter, operator, A, rightMPS, leftMPS)
%* operator(m,m')

da = size(A, 1) ;

%* AH(z,w,y,x,m') = sum{m}_[conj(A(z,w,y,x,m))*operator(m,m')]
AH = contractTensors(conj(A), 5, 5, operator, 2, 1) ;

%* T(z,w,y,x,z',w',y',x') = sum{m'}_[AH(z,w,y,x,m') * A(z',w',y',x',m')]
T = contractTensors(AH, 5, 5, A, 5, 5) ;

%* T(z,w,y,x,z',w',y',x') -> T(z,z',w,w',y,y',x,x')
T = permute(T, [1, 5, 2, 6, 3, 7, 4, 8]) ;

%* T(z,z',w,w',y,y',x,x') -> T((z,z'),(w,w'),(y,y'),(x,x'))
T = reshape(T, [da^2, da^2, da^2, da^2]) ;
%* change notation: T(z,w,y,x)

D = size(rightMPS.B{1}, 1) ;

%* BBR(a,z,w,c) = sum{b}_[rightMPS.B1(a,z,b)*rightMPS.B2(b,w,c)]
BBR = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;

%* BBL(a',y,x,c') = sum{b'}_[leftMPS.B1(a',y,b') * leftMPS.B2(b',x,c')]
BBL = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;

%* Q(a,c,y,x) = sum{z,w}_[BBR(a,z,w,c) * T(z,w,y,x)]
Q = contractTensors(BBR, 4, [2, 3], T, 4, [1, 2]) ;

%* Q(a,c,a',c') = sum{y,x}_[Q(a,c,y,x) * BBL(a',y,x,c')]
% Q = contractTensors(Q, 4, [3, 4], BBL, 4, [2, 3]) ;
Q = contractTensors(Q, 4, [3, 4], conj(BBL), 4, [2, 3]) ;

%* Q(a,c,a',c') -> Q(a,a',c,c')
Q = permute(Q, [1, 3, 2, 4]) ;

Q = reshape(Q, [D^2, D^2]) ;