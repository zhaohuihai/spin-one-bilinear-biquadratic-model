function [expectValueBond, expectValueSite, T] = dealWithSpecialTensor_2site(parameter, T, rightMPS_1, leftMPS, A)
%*  A{1}(z,w,y,x,m) A{2}(x,y,w,z,m)

rMPS = cell(1, 6) ;

rMPS{1} = rightMPS_1 ;
for i = 1 : 5
    rMPS{i + 1} = iTEBD_computeSingleGate1D(parameter, T{i}, rMPS{i}) ;
end

%* transpose to left transferMatrix
%* (T1, T2, T3, T4, T5, T6) -> (T6, T5, T4, T3, T2, T1)
%* T6(a5,a11,a12,a6) -> T1(a12,a6,a5,a11)
%* T5 -> T2
%* T4 -> T3
%* T3 -> T4
%* T2 -> T5
%* T1 -> T6
T = transposeTransferMatrix1D(T) ;

lMPS = cell(1, 6) ;

lMPS{5} = leftMPS ;

for i = 1 : 4
    lMPS{5 - i} = iTEBD_computeSingleGate1D(parameter, T{i}, lMPS{6 - i}) ;
end
lMPS{6} = iTEBD_computeSingleGate1D(parameter, T{5}, lMPS{1}) ;

%* transpose to right transferMatrix
T = transposeTransferMatrix1D(T) ;

for i = 1 : 6
    if i < 6
        j = i + 1 ;
    else
        j = 1 ;
    end
    [expectValueBond(i), expectValueSite(i)] = contractSpecialTensor_x(parameter, T([i, j]), rMPS{i}, lMPS{i}, A([i, j])) ;
    
    k = i + 6 ;
    if parameter.computation.twoSiteOperator == 1
        expectValueBond(k) = contractSpecialTensor_y(parameter, T([i, j]), rMPS{i}, lMPS{i}, A([i, j])) ;
    end
end
