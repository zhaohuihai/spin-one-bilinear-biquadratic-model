function [QR, QL] = createSpecialTensor_x(parameter, operator, ABR, ABL, A)
%* A{1}(z,w,y,x,m1) A{2}(x,y,w,z,m2)

%* H((m1, m2),(m1', m2')) = sum{n}_(U((m1,m1'),n)*S(n)*V((m2,m2'),n))
%* U((m1,m1'),n) = U((m1,m1'),n)*sqrt(S(n))
%* V((m2,m2'),n) = conj(V((m2,m2'),n))*sqrt(S(n))
[U, V] = decomposeOperator(parameter, operator) ;

%* TH1((z,z'),(w,w'),(y,y'),(x,x',n)) = sum{m1,m1'}_[conj(A{1}(z,w,y,x,m1))*A{1}(z',w',y',x',m1')*U((m1,m1'),n)]
%* change notation: TH1(z,w,y,xn)
TH1 = createTH1_x(parameter, A{1}, U) ;

%* QR(a,c,y,xn) = sum{z,w}_[ABR(a,z,w,c)*TH1(z,w,y,xn)]
QR = contractTensors(ABR, 4, [2, 3], TH1, 4, [1, 2]) ;

clear TH1 ;
clear ABR ;

%* TH2((x,x',n),(y,y'),(w,w'),(z,z')) = sum{m2,m2'}_[conj(A{2}(x,y,w,z,m2))*A{2}(x',y',w',z',m2')*V((m2,m2'),n)]
%* change notation: TH2(xn,y,w,z)
TH2 = createTH2_x(parameter, A{2}, V) ;



%* QL(a',c',xn,y) = sum{w,z}_[conj(ABL(a',w,z,c'))*TH2(xn,y,w,z)]
QL = contractTensors(conj(ABL), 4, [2, 3], TH2, 4, [3, 4]) ;

clear TH2 ;
clear ABL ;