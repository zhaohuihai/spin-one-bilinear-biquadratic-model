function [vector, eta] = applyPowerMethod_x_right(parameter, PR, PL, vector)
%* PR(a,c,y',x)
%* PL(a',c',x,y)
%* vector(a,y',a') = sum{c',y,c,x}_[PR(a,c,y',x)*PL(a',c',x,y)*vector(c,y,c')]


convergence = 1 ;
eta = 1 ;
j = 0 ;
while convergence >= parameter.tol_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector = updateVector_x_right(PR, PL, vector) ;
    [vector, eta1] = normalizeVector(vector) ;

    convergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        convergence
    end
    eta = eta1 ;
end
if parameter.display == 1
    disp(['power steps = ', num2str(j)]) ;
end