function MPS = initializeMPS_doubleTensor(AA, Ly, Lw, SA, SB)
%* AA{1}((w,w'),(z,z'),(x,x'),(y,y'))
%* AA{2}((y,y'),(x,x'),(z,z'),(w,w'))

D = length(Ly) ;
%--------------------------------------------------
%* Ly(y,y')
Ly = diag(Ly) ;

%* A((x,x'),(z,z'),(w,w')) = sum{y,y'}_[AA{2}((y,y'),(x,x'),(z,z'),(w,w'))*Ly(y,y')]
A = contractTensors(AA{2}, 4, 1, Ly, 2, [1, 2]) ;


%* A((x,x'),(z,z'),(w,w')) -> A((x,x'),(w,w'),(z,z')) 
MPS.A = permute(A, [1, 3, 2]) ;

%--------------------------
%* Lw(w,w')
Lw = diag(Lw) ;

%* B((z,z'),(x,x'),(y,y')) = sum{w,w'}_[AA{1}((w,w'),(z,z'),(x,x'),(y,y'))*Lw(w,w')]
B = contractTensors(AA{1}, 4, 1, Lw, 2, [1, 2]) ;

%* B((z,z'),(x,x'),(y,y')) -> B((z,z'),(y,y'),(x,x'))
MPS.B = permute(B, [1, 3, 2]) ;
%-------------------------------
MPS.SA = SA ;
MPS.SB = SB ;