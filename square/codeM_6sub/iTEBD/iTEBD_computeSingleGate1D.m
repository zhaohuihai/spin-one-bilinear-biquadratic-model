function [MPS, truncErr] = iTEBD_computeSingleGate1D(parameter, T, MPS)
%* T(z,w,y,x)

%* BB(a,c,z,w) = sum{b}_[B1(a,z,b)*B2(b,w,c)]
BB = contractTensors(MPS.B{1}, 3, 3, MPS.B{2}, 3, 1, [1, 4, 2, 3]) ;

%* R(a, y, x, c)) = sum{z,w}_(BB(a,c,z,w) * T(z,w,y,x))
R = contractTensors(BB, 4, [3, 4], T, 4, [1, 2], [1, 3, 4, 2]) ;

[MPS, truncErr] = canonicalize(parameter, R) ;

MPS = exchangeAandB(MPS) ;

