function iTEBD_findExpectValue(parameter, wave)
%* compute expectation value of local operator

parameter.dim_MPS = parameter.expectValue.dim_MPS ;
parameter.dim_MPS_initial = parameter.expectValue.dim_MPS_initial ;
parameter.dim_MPS_incre = parameter.expectValue.dim_MPS_incre ;

parameter.tol_iTEBD = parameter.expectValue.tol_iTEBD ;
parameter.tol_power = parameter.expectValue.tol_power ;

parameter.maxITEBDstep = parameter.expectValue.maxITEBDstep ;
parameter.maxPowerStep = parameter.expectValue.maxPowerStep ;
%---------------------------------------------------------------------------------------------
A = wave.A ;
%---------------------------------------------------------------------------------------------
T = cell(1, 6) ;
%* T1(a6,a12,a7,a1) T2(a1,a7,a8,a2) T3(a2,a8,a9,a3) T4(a3,a9,a10,a4) T5(a4,a10,a11,a5), T6(a5,a11,a12,a6)
for i = 1 : 6
    %* T{1}((a6,a6'),(a12,a12'),(a7,a7'),(a1,a1')) = sum{m1}_[conj(A{1}(a6,a12,a7,a1,m1))*A{1}(a6',a12',a7',a1',m1)]
    T{i} = computeTensorProductA_A(A{i}) ;
end

[rightMPS, leftMPS, T] = iTEBD_findDominantEigenMPS(parameter, T) ;

%*=========================================================================================
% if parameter.computation.S == 1 || parameter.computation.Q == 1
%     expectValueSite = dealWithSpecialTensor_1site(parameter, T, rightMPS, leftMPS, A) ;
%     averageSite(parameter, expectValueSite) ;
% end
[expectValueBond, expectValueSite, T] = dealWithSpecialTensor_2site(parameter, T, rightMPS, leftMPS, A) ;
clear T ;
%==========================================================================================
% print the 4 bonds values and calculate average value of the 4 bonds
if parameter.computation.OneSiteOperator == 1
    averageSite(parameter, expectValueSite) ;
end
if parameter.computation.twoSiteOperator == 1
    averageBond(parameter, expectValueBond) ;
end
