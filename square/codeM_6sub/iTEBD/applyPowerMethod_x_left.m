function [vector, eta] = applyPowerMethod_x_left(parameter, PR, PL, vector)
%* PR(a,c,y',x)
%* PL(a',c',x,y)
%* vector(c,y,c') = sum{a,y',a',x}_[PL(a',c',x,y)*PR(a,c,y',x)*vector(a,y',a')]


convergence = 1 ;
eta = 1 ;
j = 0 ;
while convergence >= parameter.tol_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector = updateVector_x_left(PR, PL, vector) ;
    [vector, eta1] = normalizeVector(vector) ;

    convergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        convergence
    end
    eta = eta1 ;
end
if parameter.display == 1
    disp(['power steps = ', num2str(j)]) ;
end