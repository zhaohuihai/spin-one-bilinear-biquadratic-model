function Sx = createSx(parameter)
%* Sx(m,n)
M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Sx = zeros(M, M) ;

%* Sx = (1/2)((S+)+(S-))
%* assign values to off-diagonal entries
for q1 = 1 : M
    m1 = S - q1 + 1 ;
    %* x1 = (1/2)*(S+)
    x1 = (1 / 2) * sqrt((S - m1) * (S + m1 + 1)) ;
    %* y1 = (1/2)*(S-)
    y1 = (1 / 2) * sqrt((S + m1) * (S - m1 + 1)) ;
    
    
    if x1 ~= 0
        Sx(q1 - 1, q1) = Sx(q1 - 1, q1) + x1 ;
    end
    if y1 ~= 0
        Sx(q1 + 1, q1) = Sx(q1 + 1, q1) + y1 ;
    end
end
