function SxI = createSxI(parameter)
%* SxI(m1,m2,n1,n2)
M = parameter.siteDimension ;

SxI = zeros(M, M, M, M) ;

%* Sx(m1,n1)
Sx = createSx(parameter) ;

%* SxI(m1,m2,n1,n2) = <m1|Sx|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            SxI(m1, m2, n1, m2) = Sx(m1,n1) ;
        end
    end
end

SxI = reshape(SxI, [M^2, M^2]) ;
