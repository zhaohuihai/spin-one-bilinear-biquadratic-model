function ISz = createISz(parameter)
%* ISz(m1,m2,n1,n2)
M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

ISz = zeros(M, M, M, M) ;

%* ISz(m1,m2,n1,n2) = delta(m1,n1)*<m2|Sz|n2>


%* assign values to diagonal entries
for q2 = 1 : M
    m2 = S - q2 + 1 ;
    for q1 = 1 : M
        ISz(q1, q2, q1, q2) = m2 ;
    end
end

ISz = reshape(ISz, [M^2, M^2]) ;
