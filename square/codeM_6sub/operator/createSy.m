function Sy = createSy(parameter)
%* Sy(m,n)
M = parameter.siteDimension ;
%* total spin
S = (M - 1) / 2 ;

Sy = zeros(M, M) ;

%* Sy = (1/2i)((S+)-(S-))
%* assign values to off-diagonal entries
for q1 = 1 : M
    m1 = S - q1 + 1 ;
    %* x1 = (1/2i)*(S+)
    x1 = (1 / 2i) * sqrt((S - m1) * (S + m1 + 1)) ;
    %* y1 = (1/2i)*(S-)
    y1 = (1 / 2i) * sqrt((S + m1) * (S - m1 + 1)) ;
    
    
    if x1 ~= 0
        Sy(q1 - 1, q1) = Sy(q1 - 1, q1) + x1 ;
    end
    if y1 ~= 0
        Sy(q1 + 1, q1) = Sy(q1 + 1, q1) - y1 ;
    end
end
