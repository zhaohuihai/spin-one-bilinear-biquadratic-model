function hamiltonian = polarize_3sub(parameter, hamiltonian)

M = parameter.siteDimension ;

if parameter.project.AFM3 == 1
    disp('polarize to AFM3') ;
    %* Sx(m,n)
    hx = createSx(parameter) ;
    %* Sy(m,n)
    hy = createSy(parameter) ;
    %* Sz(m,n)
    hz = createSz(parameter) ;
elseif parameter.project.AFQ3 == 1
    disp('polarize to AFQ3') ;
    hx = createQxx(parameter) ;
    hy = createQyy(parameter) ;
    hz = createQzz(parameter) ;
else
    hx = zeros(M, M) ;
    hy = hx ;
    hz = hx ;
end
%* h12(m1,m2,n1,n2) h23 h34 h45 h56 h61

for i = 1 : 2
    %* h12, h45 = Hbb - field * (Sz1 + Sx2) / 2
    hamiltonian{3 * i - 2} = addField(parameter, hamiltonian{3 * i - 2}, hz, hx) ;
    
    %* h23, h56 = Hbb - field * (Sx1 + Sy2) / 2
    hamiltonian{3 * i - 1} = addField(parameter, hamiltonian{3 * i - 1}, hx, hy) ;
    
    %* h34, h61 = Hbb - field * (Sy1 + Sz2) / 2
    hamiltonian{3 * i} = addField(parameter, hamiltonian{3 * i}, hy, hz) ;
end