function hamiltonian = polarize_6sub(parameter, hamiltonian)

M = parameter.siteDimension ;

if parameter.project.AFM6 == 1
    disp('polarize to AFM6') ;
    %* Sx(m,n)
    hx = createSx(parameter) ;
    %* Sy(m,n)
    hy = createSy(parameter) ;
    %* Sz(m,n)
    hz = createSz(parameter) ;
elseif parameter.project.Sz1 == 1
    hz = createSz(parameter) ;
    hx = zeros(M, M) ;
    hy = hx ;
else
    hx = zeros(M, M) ;
    hy = hx ;
    hz = hx ;
end
%* h12(m1,m2,n1,n2) h23 h34 h45 h56 h61

if parameter.project.Sz1 == 1
    hamiltonian{1} = addField(parameter, hamiltonian{1}, hz, hx) ;
    
    hamiltonian{6} = addField(parameter, hamiltonian{6}, - hy, hz) ;
else
    %* h12 = Hbb - field * (Sz1 + Sx2) / 2
    hamiltonian{1} = addField(parameter, hamiltonian{1}, hz, hx) ;
    
    %* h23 = Hbb - field * (Sx1 + Sy2) / 2
    hamiltonian{2} = addField(parameter, hamiltonian{2}, hx, hy) ;
    
    %* h34 = Hbb - field * (Sy1 - Sz2) / 2
    hamiltonian{3} = addField(parameter, hamiltonian{3}, hy, - hz) ;
    
    %* h45 = Hbb - field * (- Sz1 - Sx2) / 2
    hamiltonian{4} = addField(parameter, hamiltonian{4}, - hz, - hx) ;
    
    %* h56 = Hbb - field * (- Sx1 - Sy2) / 2
    hamiltonian{5} = addField(parameter, hamiltonian{5}, - hx, - hy) ;
    
    %* h61 = Hbb - field * (- Sy1 + Sz2) / 2
    hamiltonian{6} = addField(parameter, hamiltonian{6}, - hy, hz) ;
end