function SI = createSI(parameter, singOp)
%* SI(m1,m2,n1,n2)
M = parameter.siteDimension ;

SI = zeros(M, M, M, M) ;

%* SI(m1,m2,n1,n2) = <m1|singOp|n1>*delta(m2,n2)

%* assign values to diagonal entries
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            SI(m1, m2, n1, m2) = singOp(m1,n1) ;
        end
    end
end

SI = reshape(SI, [M^2, M^2]) ;
