function hamiltonian = polarize_2sub(parameter, hamiltonian)

M = parameter.siteDimension ;

if parameter.project.AFM2 == 1
    disp('polarize to AFM2') ;
    %* Sz(m,n)
    hz = createSz(parameter) ;
else
    hz = zeros(M, M) ;
end
%* h12(m1,m2,n1,n2) h23 h34 h45 h56 h61

for i = 1 : 3
    %* h12, h34, h56 = hamiltonian - field * (Sz1 - Sz2) / 2
    hamiltonian{2 * i - 1} = addField(parameter, hamiltonian{2 * i - 1}, hz, - hz) ;
    
    %* h23, h45, h61 = hamiltonian - field * (- Sz1 + Sz2) / 2
    hamiltonian{2 * i} = addField(parameter, hamiltonian{2 * i}, - hz, hz) ;
    
end