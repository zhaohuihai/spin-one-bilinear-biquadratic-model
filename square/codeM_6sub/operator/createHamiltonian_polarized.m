function hamiltonian = createHamiltonian_polarized(parameter)
%* create polarized hamiltonian
%* hamiltonian is 1 X 6 cell array: h12, h23, h34, h45, h56, h61

M = parameter.siteDimension ;

%* Hbb((m1, m2), (n1, n2))
Hbb = createHamiltonian_BilinBiqua(parameter) ;
%* Hbb((m1, m2), (n1, n2)) -> Hbb(m1,m2,n1,n2)
Hbb = reshape(Hbb, [M, M, M, M]) ;


for i = 1 : 6
    hamiltonian{i} = Hbb ;
end

if parameter.project.twoSubPolar == 1
    hamiltonian = polarize_2sub(parameter, hamiltonian) ;
end
if parameter.project.threeSubPolar == 1
    hamiltonian = polarize_3sub(parameter, hamiltonian) ;
end
if parameter.project.sixSubPolar == 1
    hamiltonian = polarize_6sub(parameter, hamiltonian) ;
end

disp(['field = ', num2str(parameter.project.field)]) ;
