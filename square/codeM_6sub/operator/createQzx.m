function Qzx = createQzx(parameter)

Sz = createSz(parameter) ;
Sx = createSx(parameter) ;

Qzx = (Sz * Sx + Sx * Sz) / 2 ;