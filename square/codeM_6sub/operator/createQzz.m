function Qzz = createQzz(parameter)

M = parameter.siteDimension ;

Sz = createSz(parameter) ;

Qzz = Sz * Sz - eye(M) * (2 / 3 ) ;