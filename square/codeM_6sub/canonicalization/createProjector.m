function PR = createProjector(R2, V, S) 
%* PR(x',x) = sum{x"}_[R2(x",x')*V(x",x)/S(x)]

D = size(V, 2) ;

%* V(x",x) = V(x",x)/S(x)
for x = 1 : D
    V(:, x) = V(:, x) ./ S(x) ;
end

%* PR(x',x) = sum{x"}_[R2(x",x')*V(x",x)]
PR = R2.' * V ;

% PR = PR ./ norm(PR) ;