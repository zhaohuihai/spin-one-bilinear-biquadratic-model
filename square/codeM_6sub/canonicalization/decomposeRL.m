function [U, S, V] = decomposeRL(R, L)

%* RL(b1,b2) = sum{b}_[R(b1,b)*L(b,b2)]
RL =  R * L ;

%* S(b)
[U, S, V] = svd(RL) ;
S = diag(S) ;
% S = S ./ sqrt(sum(S.^2)) ;