function canoErr = verifyLeftCano(A, SB, SA)
%* A(a,i,b)

SA = diag(SA) ;
SB = diag(SB) ;

%* M(a1,m,b2) = sum{a2}_[SB(a1,a2)*A(a2,m,b2)]
M = contractTensors(SB, 2, 2, A, 3, 1) ;

%* M(b1,b2) = sum{a1,m}_[conj(A(a1,m,b1)) * M(a1,m,b2)]
M = contractTensors(conj(A), 3, [1, 2], M, 3, [1, 2]) ;

eta = SA(1, 1) / M(1, 1) ; 
canoErr = norm(M * eta - SA) ;

