function parameter = defineModelParameter(parameter)
%* Bilinear biquadratic model parameters
parameter.siteDimension = 2 ;

parameter.theta = (0.0) * pi ;

parameter.thetaInitial = (0.19) *pi ;
parameter.thetaFinal =   (0.24) *pi ;
parameter.thetaIncre =   (0.01) *pi ;

if parameter.theta <= (-pi) || parameter.theta > (pi)
    error('the value of theta is out of parameter range') ;
end