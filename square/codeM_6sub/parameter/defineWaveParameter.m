function parameter = defineWaveParameter(parameter)

parameter.bondDimension = 3 ;
disp(['bond dimension = ', num2str(parameter.bondDimension)]) ;

parameter.bondDimensionInitial = 2 ;
parameter.bondDimensionFinal = 14 ;
parameter.bondDimensionIncre = 1 ;


% *************************************************************
% if initial A and B are same
parameter.sameInitialTensor = 0 ;


parameter.randSeed = 1 ;

parameter.complexWave = 0 ;