function parameter = setParameter

parameter = defineGlobalParameter ;

parameter = defineWaveParameter(parameter) ;
parameter = defineProjectionParameter(parameter) ;
parameter = defineExpectValueParameter(parameter) ;
parameter = defineModelParameter(parameter) ;
parameter = defineCanonicalParameter(parameter) ;
%-------------------------------------------------------------------------------------------
disp(['set maximum number of computational threads = ', num2str(parameter.numCPUthreads)]) ;
maxNumCompThreads(parameter.numCPUthreads) ;






