function parameter = defineProjectionParameter(parameter)

%****************************************************************
%* projection method: 1. simple update; 
%* projection method: 9. variational cluster update
%* projection method: 10. simple update, then variational cluster update
%* projection method: 11. double layer cluster update
%* projection method: 12. simple update, then double layer cluster update
parameter.project.method = 1 ;
parameter.project.alpha = 0.5 ;
%**********************polarization*********************
parameter.project.polarization = 1 ;
parameter.project.polarField = 0.02 ;

parameter.project.field = 0 ;

% parameter.project.fieldInitial = 0.002 ;
% parameter.project.fieldFinal = 1 ;
% parameter.project.fieldIncre = 0.01 ;

parameter.project.tauFinal_polar = 1e-2 ;
parameter.project.tol_polarization = 1e-4 ;
%--------------------------------------------------
parameter.project.twoSubPolar = 1 ;
parameter.project.AFM2 = 1 ;

parameter.project.threeSubPolar = 0 ;
parameter.project.AFM3 = 0 ;

parameter.project.AFQ3 = 0 ;

parameter.project.sixSubPolar = 0 ;
parameter.project.AFM6 = 0 ;
parameter.project.Sz1 = 0 ;

%*****************************************************************
parameter.project.tauInitial = 1e-0 ;
parameter.project.tauFinal = 1 / 1e2 ; %*
parameter.project.tauChangeFactor = 1 ;

parameter.project.eachStep = 1 ;

parameter.project.tol_projection = 1e-8 ;
parameter.project.maxProjectionStep = 2e3 ;
%******************************************************************
parameter.project.dim_MPS = 4 ;
parameter.project.dim_MPS_initial = 2 ;
parameter.project.dim_MPS_incre = 2 ;

parameter.project.tol_iTEBD = 1e-8 ;
parameter.project.tol_power = 1e-8 ;
%* determine the cluster size
parameter.project.maxITEBDstep = 0 ;
parameter.project.maxPowerStep = 1 ;
%**********************************************************************
%* super orthogonality (SO)
parameter.project.SuperOrth = 0 ;
parameter.project.alpha_SO = 0.5 ;
parameter.project.tol_SO = 1e-8 ;
parameter.project.maxSOstep = 1e2 ;
%**********************************************************************
parameter.project.clusterLength = 3 ;
parameter.project.clusterWidth = 2 ;

parameter.project.alpha_variation = 0.5 ;
parameter.project.smallestSingVal = 1e-10 ;

parameter.project.tauInitial_variation = 1e-2 ;

parameter.project.eachStep_variation = 1 ;

parameter.project.tol_projection_variation = 1e-6 ;
parameter.project.maxProjectionStep_variation = 2e2 ;

parameter.project.tol_variation = 1e-4 ;
parameter.project.maxVariationStep = 1e2 ;
%*************************************************************************
%* method = 2: compute energy in all projection methods
parameter.estimate.method = 1 ;
parameter.estimate.dim_MPS = 10 ;
parameter.estimate.dim_MPS_initial = 10 ;
parameter.estimate.dim_MPS_incre = 10 ;

parameter.estimate.tol_iTEBD = 1e-8 ;
parameter.estimate.tol_power = 1e-8 ;

parameter.estimate.maxITEBDstep = 1e2 ;
parameter.estimate.maxPowerStep = 1e2 ;