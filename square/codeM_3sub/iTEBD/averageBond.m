function energy = averageBond(parameter, expectValueBond)

bondNo = length(expectValueBond) ;
computation = parameter.computation ;

% energy computation
if computation.energy == 1
    energyBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyBond(i) = expectValueBond(i).energy ;
    end
    if parameter.display == 1
        disp ('energy of every bond = ') ;
        disp(energyBond) ;
    end
    energy = mean(energyBond) ;
    energy = energy * 2 ;
    saveExpectationValue(parameter, energy, 'energy') ;
end

%* energy derivative
if computation.energyDerivative == 1
    energyDerivativeBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        energyDerivativeBond(i) = expectValueBond(i).energyDerivative ;
    end
    disp ('energy derivative of every bond = ') ;
    disp(energyDerivativeBond) ;
    energyDerivative = mean(energyDerivativeBond) ;
    energyDerivative = energyDerivative * 1.5 ;
    saveExpectationValue(parameter, energyDerivative, 'energyDerivative') ;
end
% nearest neighbor spin correlation
if computation.nearSpinCorrelation == 1
    nearSpinCorrelationBond = zeros(1, bondNo) ;
    for i = 1 : bondNo
        nearSpinCorrelationBond(i) = expectValueBond(i).nearSpinCorrelation ;
    end
    disp ('nearest neighbor spin correlation of every bond = ') ;
    disp(nearSpinCorrelationBond) ;
    P = computePlaquetteOrder(nearSpinCorrelationBond) ;
    disp(['plaquette order = ', num2str(P, 12)])
end