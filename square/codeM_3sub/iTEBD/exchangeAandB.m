function MPS = exchangeAandB(MPS)

MPS.B = MPS.B([2, 1]) ;
if isfield(MPS, 'S')
    MPS.S = MPS.S([2, 1]) ;
end