function [MPS, truncationError] = iTEBD_applyRightGateOperation1D(parameter, T, MPS)
%* T1(a3,a6,a4,a1) T2(a1,a4,a5,a2) T3(a2,a5,a6,a3)

truncErr_single = zeros(1, 6) ;

n = 1 ;
%* apply T{1} gate; then exchange A <--> B
%* apply T{2} gate; then exchange A <--> B
%* apply T{3} gate; then exchange A <--> B
for j = 1 : 2
    for i = 1 : 3
        
        [MPS, truncErr_single(n)] = iTEBD_computeSingleGate1D(parameter, T{i}, MPS) ;
        n = n + 1 ;
    end
end
truncationError = max(truncErr_single) ;
