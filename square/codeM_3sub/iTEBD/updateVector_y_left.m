function vector = updateVector_y_left(PR, PL, vector)
%* vector(c,x,c') = sum{a',x',a,y}_[PR(a,c,y,x)*PL(a',c',x',y)*vector(a,x',a')]


%* vector(c',y,a) = sum{a',x'}_(PL(a',c',x',y)*vector(a,x',a'))
vector = contractTensors(PL, 4, [1, 3], vector, 3, [3, 2]) ;


%* vector(c,x,c') = sum{a,y}_(PR(a,c,y,x)*vector(c',y,a))
vector = contractTensors(PR, 4, [1, 3], vector, 3, [3, 2]) ;
