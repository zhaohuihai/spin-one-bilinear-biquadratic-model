function [vector, eta] = applyPowerMethod_y_right(parameter, PR, PL, vector)
%* PR(a,c,y,x)
%* PL(a',c',x',y)
%* vector(a,x',a') = sum{c,x,c',y}_[PL(a',c',x',y)*PR(a,c,y,x)*vector(c,x,c')]


convergence = 1 ;
eta = 1 ;
j = 0 ;
while convergence >= parameter.tol_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector = updateVector_y_right(PR, PL, vector) ;
    [vector, eta1] = normalizeVector(vector) ;

    convergence = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        convergence
    end
    eta = eta1 ;
end
if parameter.display == 1
    disp(['power steps = ', num2str(j)]) ;
end