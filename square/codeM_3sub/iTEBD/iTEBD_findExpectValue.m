function energy = iTEBD_findExpectValue(parameter, wave)
%* compute expectation value of local operator

parameter.dim_MPS = parameter.expectValue.dim_MPS ;
parameter.dim_MPS_initial = parameter.expectValue.dim_MPS_initial ;
parameter.dim_MPS_incre = parameter.expectValue.dim_MPS_incre ;

parameter.tol_iTEBD = parameter.expectValue.tol_iTEBD ;
parameter.tol_power = parameter.expectValue.tol_power ;

parameter.maxITEBDstep = parameter.expectValue.maxITEBDstep ;
parameter.maxPowerStep = parameter.expectValue.maxPowerStep ;
%---------------------------------------------------------------------------------------------
A = wave.A ;
%---------------------------------------------------------------------------------------------
T = cell(1, 3) ;
%* T1(a3,a6,a4,a1) T2(a1,a4,a5,a2) T3(a2,a5,a6,a3)
for i = 1 : 3
    %* T{1}((a3,a3'),(a6,a6'),(a4,a4'),(a1,a1')) = sum{m1}_[conj(A{1}(a3,a6,a4,a1,m1))*A{1}(a3',a6',a4',a1',m1)]
    T{i} = computeTensorProductA_A(A{i}) ;
end

[rightMPS, leftMPS] = iTEBD_findDominantEigenMPS(parameter, T) ;

%*=========================================================================================
if parameter.computation.S == 1 || parameter.computation.Q == 1
    expectValueSite = dealWithSpecialTensor_1site(parameter, T, rightMPS, leftMPS, A) ;
    averageSite(parameter, expectValueSite) ;
end
expectValueBond = dealWithSpecialTensor_2site(parameter, T, rightMPS, leftMPS, A) ;

%==========================================================================================
% print the 4 bonds values and calculate average value of the 4 bonds
% diary on
energy = averageBond(parameter, expectValueBond) ;
% diary off
