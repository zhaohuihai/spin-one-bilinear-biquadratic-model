function [expectValue, QR, QL] = findExpectationValue_x(PR, PL, vectorR, vectorL, QR, QL)
%* QR(a,c,y',xn)
%* QL(a',c',xn,y)
%* vectorR(c,y,c')
%* vectorL(a,y',a')

%* QvectorR(a',xn,c) = sum{c',y}_[QL(a',c',xn,y)*vectorR(c,y,c')]
%* QvectorR(a,y',a') = sum{c,xn}_(QR(a,c,y',xn)*QvectorR(a',xn,c))
QvectorR = updateVector_x_right(QR, QL, vectorR) ;

%* h = sum{a,y',a'}_[vectorL(a,y',a') * QvectorR(a,y',a')]
h = contractTensors(vectorL, 3, [1, 2, 3], QvectorR, 3, [1, 2, 3]) ;

PvectorR = updateVector_x_right(PR, PL, vectorR) ;

%* g = sum{a,y',a'}_[vectorL1(a,y',a') * PvectorR(a,y',a')]
g =  contractTensors(vectorL, 3, [1, 2, 3], PvectorR, 3, [1, 2, 3]) ;

expectValue = real(h / g) ;
