function averageSite(parameter, expectValueSite)

computation = parameter.computation ;

precision = 12 ;
if computation.S == 1
    
    disp(['Sx1: ', num2str(expectValueSite(1).Sx, precision)]) ;
    disp(['Sy1: ', num2str(expectValueSite(1).Sy, precision)]) ;
    disp(['Sz1: ', num2str(expectValueSite(1).Sz, precision)]) ;
    
    disp(['Sx2: ', num2str(expectValueSite(2).Sx, precision)]) ;
    disp(['Sy2: ', num2str(expectValueSite(2).Sy, precision)]) ;
    disp(['Sz2: ', num2str(expectValueSite(2).Sz, precision)]) ;
    
    disp(['Sx3: ', num2str(expectValueSite(3).Sx, precision)]) ;
    disp(['Sy3: ', num2str(expectValueSite(3).Sy, precision)]) ;
    disp(['Sz3: ', num2str(expectValueSite(3).Sz, precision)]) ;
    
    s = zeros(1, 3) ;
    for i = 1 : 3 
        s(i) = max([expectValueSite(i).Sx, expectValueSite(i).Sy, expectValueSite(i).Sz]) ;
    end
    
%     AFM3 = (expectValueSite(1).Sx + expectValueSite(2).Sy + expectValueSite(3).Sz) / 3 ;
    AFM3 = mean(s) ;
    saveExpectationValue(parameter, AFM3, 'AFM3') ;
end

if computation.Q == 1
    
    disp(['Qxx1: ', num2str(expectValueSite(1).Qxx, precision)]) ;
    disp(['Qyy1: ', num2str(expectValueSite(1).Qyy, precision)]) ;
    disp(['Qzz1: ', num2str(expectValueSite(1).Qzz, precision)]) ;
    
    disp(['Qxx2: ', num2str(expectValueSite(2).Qxx, precision)]) ;
    disp(['Qyy2: ', num2str(expectValueSite(2).Qyy, precision)]) ;
    disp(['Qzz2: ', num2str(expectValueSite(2).Qzz, precision)]) ;
    
    disp(['Qxx3: ', num2str(expectValueSite(3).Qxx, precision)]) ;
    disp(['Qyy3: ', num2str(expectValueSite(3).Qyy, precision)]) ;
    disp(['Qzz3: ', num2str(expectValueSite(3).Qzz, precision)]) ;
    
    q = zeros(1, 3) ;
    for i = 1 : 3
        q(i) = max([expectValueSite(i).Qxx, expectValueSite(i).Qyy, expectValueSite(i).Qzz]) ;
    end
    
%     AFQ3 = (expectValueSite(1).Qxx + expectValueSite(2).Qyy + expectValueSite(3).Qzz) / 3 ;
    AFQ3 = mean(q) ;
    saveExpectationValue(parameter, AFQ3, 'AFQ3') ;
end