function [expectValue, QR, QL] = findExpectationValue_y(PR, PL, vectorR, vectorL, QR, QL)
%* QR(a,c,yn,x)
%* QL(a',c',x',yn)
%* vectorR(c,x,c')
%* vectorL(a,x',a')

%* QvectorR(a,yn,c') = sum{c,x}_[QR(a,c,yn,x)*vectorR(c,x,c')]
%* QvectorR(a',x',a) = sum{c',yn}_(QL(a',c',x',yn)*QvectorR(a,yn,c'))
%* QvectorR(a',x',a) -> QvectorR(a,x',a')
QvectorR = updateVector_y_right(QR, QL, vectorR) ;

%* h = sum{a,x',a'}_[vectorL(a,x',a') * QvectorR(a,x',a')]
h = contractTensors(vectorL, 3, [1, 2, 3], QvectorR, 3, [1, 2, 3]) ;

PvectorR = updateVector_y_right(PR, PL, vectorR) ;

%* g = sum{a,x',a'}_[vectorL(a,x',a') * PvectorR(a,x',a')]
g =  contractTensors(vectorL, 3, [1, 2, 3], PvectorR, 3, [1, 2, 3]) ;

expectValue = real(h / g) ;