function expectValueSite = dealWithSpecialTensor_1site(parameter, T, rightMPS_1, leftMPS_6, A)

rMPS = cell(1, 6) ;

rMPS{1} = rightMPS_1 ;

for i = 1 : 5
    if i > 3
        j = i - 3 ;
    else
        j = i ;
    end
    rMPS{i + 1} = iTEBD_computeSingleGate1D(parameter, T{j}, rMPS{i}) ;
end

%* transpose to left transferMatrix
%* (T1, T2, T3) -> (T3, T2, T1)
%* T3(a2,a5,a6,a3) -> T1(a6,a3,a2,a5)
%* T2 -> T2
%* T1 -> T3
T_left = transposeTransferMatrix1D(T) ;

lMPS = cell(1, 6) ;

lMPS{6} = leftMPS_6 ;

for i = 1 : 5
    if i > 3
        j = i - 3 ;
    else
        j = i ;
    end
    lMPS{6 - i} = iTEBD_computeSingleGate1D(parameter, T_left{j}, lMPS{7 - i}) ;
end

for i = 1 : 3
    j = i + 3 ;
    expectValue1 = contractSpecialTensor_1site(parameter, T{i}, rMPS{i}, lMPS{i}, A{i}) ;
    expectValue2 = contractSpecialTensor_1site(parameter, T{i}, rMPS{j}, lMPS{j}, A{i}) ;
    
    if parameter.computation.S == 1
        expectValueSite(i).Sx = (expectValue1.Sx + expectValue2.Sx) / 2 ;
        expectValueSite(i).Sy = (expectValue1.Sy + expectValue2.Sy) / 2 ;
        expectValueSite(i).Sz = (expectValue1.Sz + expectValue2.Sz) / 2 ;
    end
    if parameter.computation.Q == 1
        expectValueSite(i).Qxx = (expectValue1.Qxx + expectValue2.Qxx) / 2 ;
        expectValueSite(i).Qyy = (expectValue1.Qyy + expectValue2.Qyy) / 2 ;
        expectValueSite(i).Qzz = (expectValue1.Qzz + expectValue2.Qzz) / 2 ;
    end
end



