function vector = updateVector_x_left(PR, PL, vector)
%* vector(c,y,c') = sum{a,y',a',x}_[PL(a',c',x,y)*PR(a,c,y',x)*vector(a,y',a')]

%* vector(c,x,a') = sum{a,y'}_(PR(a,c,y',x)*vector(a,y',a'))
vector = contractTensors(PR, 4, [1, 3], vector, 3, [1, 2]) ;


%* vector(c',y,c) = sum{a',x}_(PL(a',c',x,y)*vector(c,x,a'))
vector = contractTensors(PL, 4, [1, 3], vector, 3, [3, 2]) ;

%* vector(c',y,c) -> vector(c,y,c')
vector = permute(vector, [3, 2, 1]) ;
