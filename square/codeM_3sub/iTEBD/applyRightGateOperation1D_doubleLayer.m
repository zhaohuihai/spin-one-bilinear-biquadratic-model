function [MPS, truncationError] = applyRightGateOperation1D_doubleLayer(parameter, AA, MPS)
%* AA{1}(w,z,x,y); AA{2}(y,x,z,w)

truncErr_single = zeros(1, 2) ;

%* 
[MPS, truncErr_single(1)] = computeSingleGate1D_doubleLayer(parameter, AA, MPS) ;

AA([1, 2]) = AA([2, 1]) ;

[MPS, truncErr_single(2)] = computeSingleGate1D_doubleLayer(parameter, AA, MPS) ;


truncationError = max(truncErr_single) ;

