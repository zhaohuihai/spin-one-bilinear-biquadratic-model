function expectValueBond = dealWithSpecialTensor_2site(parameter, T, rightMPS_1, leftMPS, A)
%*  A{1}(z,w,y,x,m) A{2}(x,y,w,z,m)

rMPS = cell(1, 3) ;

rMPS{1} = rightMPS_1 ;
rMPS{2} = iTEBD_computeSingleGate1D(parameter, T{1}, rMPS{1}) ;
rMPS{3} = iTEBD_computeSingleGate1D(parameter, T{2}, rMPS{2}) ;

T_left = transposeTransferMatrix1D(T) ;

lMPS = cell(1, 3) ;

leftMPS = iTEBD_computeSingleGate1D(parameter, T_left{1}, leftMPS) ;
lMPS{3} = iTEBD_computeSingleGate1D(parameter, T_left{2}, leftMPS) ;
lMPS{2} = iTEBD_computeSingleGate1D(parameter, T_left{3}, lMPS{3}) ;
lMPS{1} = iTEBD_computeSingleGate1D(parameter, T_left{1}, lMPS{2}) ;


for i = 1 : 3
    if i < 3
        j = i + 1 ;
    else
        j = 1 ;
    end
    expectValueBond(i) = contractSpecialTensor_x(parameter, T([i, j]), rMPS{i}, lMPS{i}, A([i, j])) ;
    
    k = i + 3 ;
    expectValueBond(k) = contractSpecialTensor_y(parameter, T([i, j]), rMPS{i}, lMPS{i}, A([i, j])) ;
end
