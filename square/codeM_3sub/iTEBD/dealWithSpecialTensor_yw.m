function expectValueBond = dealWithSpecialTensor_yw(parameter, T, rightMPS_y, leftMPS_y, A)
%*  A{1}(z,w,y,x,m) A{2}(x,y,w,z,m)

%* T{1}(z,w,y,x); T{2}(x,y,w,z)
rightMPS_w = iTEBD_computeSingleGate1D(parameter, T{1}, rightMPS_y) ;


T_left = transposeTransferMatrix1D(T) ;

leftMPS_w = iTEBD_computeSingleGate1D(parameter, T_left{1}, leftMPS_y) ;

parameter.orderSign = 1 ; 
%* y
expectValueBond(1) = contractSpecialTensor_y(parameter, T, rightMPS_y, leftMPS_y, A) ;
%* w
parameter.orderSign = - 1 ; 
expectValueBond(2) = contractSpecialTensor_y(parameter, T([2, 1]), rightMPS_w, leftMPS_w, A([2, 1])) ;
