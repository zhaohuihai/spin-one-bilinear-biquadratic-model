function TH2 = createTH2_y(parameter, A, V)
%* TH2((x,x'),(y,y',n),(w,w'),(z,z')) = sum{m2,m2'}_[conj(A(x,y,w,z,m2))*A(x',y',w',z',m2')*V((m2,m2'),n)]

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = size(V, 2) ;

%* V((m2,m2'),n) -> V(m2,m2',n)
V = reshape(V, [M, M, N]) ;

%* AV(x,y,w,z,m2',n) = sum{m2}_[conj(A(x,y,w,z,m2))*V(m2,m2',n)]
AV = contractTensors(conj(A), 5, 5, V, 3, 1) ;

%* TH2(x,y,w,z,n,x',y',w',z') = sum{m2'}_[AV(x,y,w,z,m2',n) * A(x',y',w',z',m2')]
TH2 = contractTensors(AV, 6, 5, A, 5, 5) ;

%* TH2(x,x',y,y',n,w,w',z,z')
TH2 = permute(TH2, [1, 6, 2, 7, 5, 3, 8, 4, 9]) ;

%* TH2((x,x'),(y,y',n),(w,w'),(z,z')))
TH2 = reshape(TH2, [D^2, D^2 * N, D^2, D^2]) ;