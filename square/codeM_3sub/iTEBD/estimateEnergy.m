function energy = estimateEnergy(parameter, wave)

parameter.dim_MPS = parameter.estimate.dim_MPS ;
parameter.dim_MPS_initial = parameter.estimate.dim_MPS_initial ;
parameter.dim_MPS_incre = parameter.estimate.dim_MPS_incre ;

parameter.tol_iTEBD = parameter.estimate.tol_iTEBD ;
parameter.tol_power = parameter.estimate.tol_power ;

parameter.maxITEBDstep = parameter.estimate.maxITEBDstep ;
parameter.maxPowerStep = parameter.estimate.maxPowerStep ;
%-------------------------------------------------------------------
parameter.display = 0 ;
%* staggered spontaneous magnetization
parameter.computation.stagSz = 0 ;

%* quadrupole (constraint: Qxx + Qyy + Qzz = 0)
parameter.computation.Qzz = 0 ;

%* staggered quadrupole
parameter.computation.stagQzz = 0 ;

% energy computation
parameter.computation.energy = 1 ;
parameter.computation.energyDerivative = 0 ;

%* nearest neighbor spin correlation
parameter.computation.nearSpinCorrelation = 0 ;
%---------------------------------------------------------------
A = wave.A ;

%* A{1}(x,y,z,w,m) -> A{1}(z,w,y,x,m)
A{1} = permute(A{1}, [3, 4, 2, 1, 5]) ;
%* A{2}(x,y,z,w,m) -> A{2}(x,y,w,z,m)
A{2} = permute(A{2}, [1, 2, 4, 3, 5]) ;
%--------------------------------------------------------------------------------------
T = cell(1, 2) ;
%* T{1}(z,w,y,x); T{2}(x,y,w,z)
for i = 1 : 2
    %* T{1}((z,z'),(w,w'),(y,y'),(x,x')) = sum{m}_[A{1}(z,w,y,x,m)*A{1}(z',w',y',x',m)]
    %* T{2}((x,x'),(y,y'),(w,w'),(z,z')) = sum{m}_[A{2}(x,y,w,z,m)*A{2}(x',y',w',z',m)]
    T{i} = computeTensorProductA_A(A{i}) ;
end

%--------------------------------------------------------------------------------------
[rightMPS, leftMPS] = iTEBD_findDominantEigenMPS(parameter, T) ;

%*=====================================================================================
expectValueBond([1, 3]) = dealWithSpecialTensor_xz(parameter, T, rightMPS, leftMPS, A) ;
expectValueBond([2, 4]) = dealWithSpecialTensor_yw(parameter, T, rightMPS, leftMPS, A) ;

%==========================================================================================
% print the 4 bonds values and calculate average value of the 4 bonds

energy = averageBond(parameter, expectValueBond) ;