function MPS = iTEBD_computeDominantRightEigenMPS(parameter, T)

maxDim = parameter.dim_MPS ;
parameter.dim_MPS = parameter.dim_MPS_initial ;

%* MPS: A B
MPS = iTEBD_initializeMPS(parameter) ;

iTEBDstep = 0 ;
[MPS, truncationError, iTEBDstep] = iTEBD_iterateRightGate1D(parameter, T, MPS, iTEBDstep) ;

while parameter.dim_MPS < maxDim
    iTEBDstep = iTEBDstep + 1 ;
    if parameter.display == 1
        disp(['dim MPS = ', num2str(parameter.dim_MPS), ', iTEBD step = ', num2str(iTEBDstep)]) ;
    end
    tic
    MPS = iTEBD_applyRightGateOperation1D(parameter, T, MPS) ;
    cpu_time = toc ;
    if parameter.display == 1
        disp(['single step iTEBD time: ', num2str(cpu_time), ' seconds']) ;
    end
    parameter.dim_MPS = parameter.dim_MPS + parameter.dim_MPS_incre ;
end

parameter.dim_MPS = maxDim ;

[MPS, truncationError, iTEBDstep] = iTEBD_iterateRightGate1D(parameter, T, MPS, iTEBDstep) ;

disp(['total iTEBD steps = ', num2str(iTEBDstep)]) ;