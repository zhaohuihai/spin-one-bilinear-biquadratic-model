function TH1 = createTH1_y(parameter, A, U)
%* TH1((z,z'),(w,w'),(y,y',n),(x,x')) = sum{m1,m1'}_[conj(A(z,w,y,x,m1))*A(z',w',y',x',m1')*U((m1,m1'),n)]

M = parameter.siteDimension ;
D = parameter.bondDimension ;
N = size(U, 2) ;

%* U((m1,m1'),n) -> U(m1,m1',n)
U = reshape(U, [M, M, N]) ;

%* AU(z,w,y,x,m1',n) = sum{m1}_[conj(A(z,w,y,x,m1))*U(m1,m1',n)]
AU = contractTensors(conj(A), 5, 5, U, 3, 1) ;

%* TH1(z,w,y,x,n,z',w',y',x') = sum{m1'}_[AU(z,w,y,x,m1',n) * A(z',w',y',x',m1')]
TH1 = contractTensors(AU, 6, 5, A, 5, 5) ;

%* TH1(z,z',w,w',y,y',n,x,x')
TH1 = permute(TH1, [1, 6, 2, 7, 3, 8, 5, 4, 9]) ;

%* TH1((z,z'),(w,w'),(y,y',n),(x,x'))
TH1 = reshape(TH1, [D^2, D^2, D^2 * N, D^2]) ;