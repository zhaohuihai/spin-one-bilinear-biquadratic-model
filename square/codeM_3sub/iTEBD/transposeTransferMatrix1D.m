function T = transposeTransferMatrix1D(T)
%* transpose to left transferMatrix
%* (T1, T2, T3) -> (T3, T2, T1)
%* T3(a2,a5,a6,a3) -> T1(a6,a3,a2,a5)
%* T2 -> T2
%* T1 -> T3

%* T3(a2,a5,a6,a3) -> T1(a6,a3,a2,a5)
T1 = permute(T{3}, [3, 4, 1, 2]) ;

%* T2 -> T2
T2 = permute(T{2}, [3, 4, 1, 2]) ;

%* T1 -> T3
T3 = permute(T{1}, [3, 4, 1, 2]) ;

T{1} = T1 ;
T{2} = T2 ;
T{3} = T3 ;

