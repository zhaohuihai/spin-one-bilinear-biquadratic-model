function [rightMPS, leftMPS] = iTEBD_findDominantEigenMPS(parameter, T)
%* T1(a3,a6,a4,a1) T2(a1,a4,a5,a2) T3(a2,a5,a6,a3)

% project from bottom to top
rightMPS = iTEBD_computeDominantRightEigenMPS(parameter, T) ;

%* transpose to left transferMatrix
%* (T1, T2, T3) -> (T3, T2, T1)
%* T3(a2,a5,a6,a3) -> T1(a6,a3,a2,a5)
%* T2 -> T2
%* T1 -> T3
T = transposeTransferMatrix1D(T) ;

leftMPS = iTEBD_computeDominantRightEigenMPS(parameter, T) ;