function [MPS, truncationError, step] = iterateRightGate1D_doubleLayer(parameter, AA, MPS, step)

truncationError = 0 ;
converge = 1 ;
Chi = parameter.dim_MPS ;
j = 0 ;
SA0 = 1 ;
SB0 = 1 ;
while converge >= parameter.tol_iTEBD && j < parameter.maxITEBDstep
    step = step + 1 ;
    j = j + 1 ;
    if parameter.display == 1
        disp(['dim MPS = ', num2str(Chi), ', iTEBD step = ', num2str(step)]) ;
    end
    tic
    [MPS, truncationError] = applyRightGateOperation1D_doubleLayer(parameter, AA, MPS) ;
    cpu_time = toc ;
    if parameter.display == 1
        disp(['single step iTEBD time: ', num2str(cpu_time), ' seconds']) ;
    end
    SA = MPS.SA ;
    SB = MPS.SB ;
    errA = norm(SA - SA0) / norm(SA) ;
    errB = norm(SB - SB0) / norm(SB) ;

    converge = max(errA, errB) ;
    
    flag = step / 1 ;
    if flag == floor(flag) && parameter.display == 1
        disp(['iTEBD convergence error = ', num2str(converge)]) ;
        disp(['truncation error = ', num2str(truncationError)]) ;
    end
    SA0 = SA ;
    SB0 = SB ;
end