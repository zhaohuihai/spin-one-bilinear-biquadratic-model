function P = createTransferMatrix0D_1site(parameter, T, rightMPS, leftMPS)
%* P((a,a'),(c,c')) = sum{b,b',z,w,y,x}_
%* [Br1(a,z,b)*Br2(b,w,c)*T(z,w,y,x)*Bl1(a',y,b')*Bl2(b',x,c')]

D = size(rightMPS.B{1}, 1) ;

%* BBR(a,z,w,c) = sum{b}_[rightMPS.B1(a,z,b)*rightMPS.B2(b,w,c)]
BBR = contractTensors(rightMPS.B{1}, 3, 3, rightMPS.B{2}, 3, 1) ;

%* BBL(a',y,x,c') = sum{b'}_[leftMPS.B1(a',y,b') * leftMPS.B2(b',x,c')]
BBL = contractTensors(leftMPS.B{1}, 3, 3, leftMPS.B{2}, 3, 1) ;

%* P(a,c,y,x) = sum{z,w}_[BBR(a,z,w,c) * T(z,w,y,x)]
P = contractTensors(BBR, 4, [2, 3], T, 4, [1, 2]) ;

%* P(a,c,a',c') = sum{y,x}_[P(a,c,y,x) * BBL(a',y,x,c')]
P = contractTensors(P, 4, [3, 4], BBL, 4, [2, 3]) ;

%* P(a,c,a',c') -> P(a,a',c,c')
P = permute(P, [1, 3, 2, 4]) ;

P = reshape(P, [D^2, D^2]) ;