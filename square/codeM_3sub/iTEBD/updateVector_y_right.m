function vector = updateVector_y_right(PR, PL, vector)
%* vector(a,x',a') = sum{c,x,c',y}_[PL(a',c',x',y)*PR(a,c,y,x)*vector(c,x,c')]

%* vector(a,y,c') = sum{c,x}_(PR(a,c,y,x)*vector(c,x,c'))
vector = contractTensors(PR, 4, [2, 4], vector, 3, [1, 2]) ;


%* vector(a',x',a) = sum{c',y}_(PL(a',c',x',y)*vector(a,y,c'))
vector = contractTensors(PL, 4, [2, 4], vector, 3, [3, 2]) ;

%* vector(a',x',a) -> vector(a,x',a')
vector = permute(vector, [3, 2, 1]) ;
