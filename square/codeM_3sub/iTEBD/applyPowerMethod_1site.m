function [vector, eta] = applyPowerMethod_1site(parameter, P, vector)


converge = 1 ;
j = 0 ;
eta = 1 ;
while converge >= parameter.tol_power && j <= parameter.maxPowerStep
    j = j + 1 ;
    vector1 = P * vector ;
    eta1 = norm(vector1) ;
    vector1 = vector1 ./ eta1 ;
    converge = abs(eta1 - eta) / abs(eta) ;
    flag = j / 100 ;
    if flag == floor(flag)
        converge
    end
    vector = vector1 ;
    eta = eta1 ;
end
disp(['power steps = ', num2str(j)]) ;