function h12 = addField(parameter, Hbb, Sx, Sy)
%* h12(m1,m2,n1,n2)

M = parameter.siteDimension ;
field = parameter.project.field ;

h12 = Hbb ;
for m1 = 1 : M
    for m2 = 1 : M
        for n1 = 1 : M
            for n2 = 1 : M
                h12(m1, m2, n1, n2) = h12(m1, m2, n1, n2) - field * (Sx(m1, n1) * delta(m2, n2) + delta(m1, n1) * Sy(m2, n2)) / 2 ;
            end
        end
    end
end

h12 = reshape(h12, [M^2, M^2]) ; %* h12((m1, m2), (n1, n2))