function hamiltonian = createHamiltonian_polarized(parameter)
%* create polarized hamiltonian
%* hamiltonian is 1 X 3 cell array: h12, h23, h31

M = parameter.siteDimension ;

%* Hbb((m1, m2), (n1, n2))
Hbb = createHamiltonian_BilinBiqua(parameter) ;
%* Hbb((m1, m2), (n1, n2)) -> Hbb(m1,m2,n1,n2)
Hbb = reshape(Hbb, [M, M, M, M]) ;

%---------------------------------
% Hbb = zeros(M, M, M, M) ;
%--------------------------------

if parameter.project.AFM3 == 1
    %* Sx(m,n)
    hx = createSx(parameter) ;
    %* Sy(m,n)
    hy = createSy(parameter) ;
    %* Sz(m,n)
    hz = createSz(parameter) ;
elseif parameter.project.AFQ3 == 1
    hx = createQxx(parameter) ;
    hy = createQyy(parameter) ;
    hz = createQzz(parameter) ;
else
    hx = zeros(M, M) ;
    hy = hx ;
    hz = hx ;
end
%* h12(m1,m2,n1,n2) h23 h31
hamiltonian = cell(1, 3) ;

%* h12 = Hbb - field * (Sx1 + Sy2) / 2
hamiltonian{1} = addField(parameter, Hbb, hx, hy) ;

%* h23 = Hbb - field * (Sy1 + Sz2) / 2
hamiltonian{2} = addField(parameter, Hbb, hy, hz) ;

%* h31 = Hbb - field * (Sz1 + Sx2) / 2
hamiltonian{3} = addField(parameter, Hbb, hz, hx) ;

disp(['field = ', num2str(parameter.project.field)]) ;
