function Qyy = createQyy(parameter)

M = parameter.siteDimension ;

Sy = createSy(parameter) ;

Qyy = Sy * Sy - eye(M) * (2 / 3) ;