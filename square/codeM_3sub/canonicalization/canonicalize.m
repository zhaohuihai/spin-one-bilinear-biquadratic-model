function [MPS, truncErr] = canonicalize(parameter, A)
%* A(a,i,j,c)

[da, di, dj, ~] = size(A) ;

%* A(a,(i,j),c)
A = reshape(A, [da, di * dj, da]) ;

[A, S] = canonicalize_single(parameter, A) ;

MPS.S{2} = S ;
%===================================================================
[MPS.B, MPS.S{1}, truncErr] = singleTOdoubleMPS(parameter, A, S, S, di, dj, da) ;


if parameter.verifyCano == 1
    canoErr1_right = verifyRightCano(MPS.B{1}, MPS.S{1}, MPS.S{2})
    canoErr1_left = verifyLeftCano(MPS.B{1}, MPS.S{2}, MPS.S{1})
    canoErr2_right = verifyRightCano(MPS.B{2}, MPS.S{2}, MPS.S{1})
    canoErr2_left = verifyLeftCano(MPS.B{2}, MPS.S{1}, MPS.S{2})
end