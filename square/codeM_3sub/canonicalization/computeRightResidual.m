function R = computeRightResidual(A, varargin)
% function R = computeRightResidual(A)
% function R = computeRightResidual(A, R)
%* R(b1,b)

[da, di, db] = size(A) ;

if nargin == 2
    R = varargin{1} ;
    %* A(a,i,b) = sum{a1}_[R(a,a1)*A(a1,i,b)]
    A = contractTensors(R, 2, 2, A, 3, 1) ;
end

%* A(a,i,b) -> A((a,i), b)
A = reshape(A, [da * di,  db]) ;

%* A((a,i), b) = sum{b1}_[Q((a,i),b1)*R(b1,b)]
% [~, R] = qr(A, 0) ;
R = qr(A, 0) ;
R = triu(R) ;
R = R(1 : db, :) ;

R = R ./ norm(R) ;