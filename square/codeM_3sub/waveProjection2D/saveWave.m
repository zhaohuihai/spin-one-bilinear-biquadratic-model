function saveWave(parameter, wave)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

dirName = ['result/D = ', num2str(D)] ;
[status,message,messageid] = mkdir(dirName) ;

waveFileName = [dirName, '/wavefunctionSite', num2str(M),'.mat'] ;

tau = parameter.project.tau ;

save(waveFileName, 'wave', 'tau') ;