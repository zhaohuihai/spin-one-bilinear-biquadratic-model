function A = computeA_sqrtLy(A, Ly, varargin )

[~, dy, ~, ~, ~] = size(A) ;

if nargin == 3
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A(x,y,z,w,m) = A(x,y,z,w,m)*sqrt(Ly(y))
for y = 1 : dy
    A(:, y, :, :, :) = A(:, y, :, :, :) * Ly(y).^alpha ;
end
