function [wave, parameter] = loadWave(parameter)

M = parameter.siteDimension ;
D = parameter.bondDimension ;

resultFileName = ['result/D = ', num2str(D), '/wavefunctionSite', num2str(M),'.mat'] ;
id = exist(resultFileName, 'file') ;

if id == 2
    load (resultFileName, 'wave', 'tau')
    parameter.project.tau = tau ;
    
else
    disp('No wavefunction file.')
    disp('Create NEW wave instead.') 
    parameter.loadPreviousWave = 0 ;
    wave = initializeWave(parameter) ;
end







