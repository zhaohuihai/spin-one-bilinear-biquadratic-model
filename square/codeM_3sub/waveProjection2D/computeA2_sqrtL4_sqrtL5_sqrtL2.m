function M2 = computeA2_sqrtL4_sqrtL5_sqrtL2( A2, L4, L5, L2, varargin )
%* M2((a4,a5,a2,m2),a1) = A2(a1,a4,a5,a2,m2)*sqrt(L4(a4))*sqrt(L5(a5))*sqrt(L2(a2))

[d1, d4, d5, d2, dm] = size(A2) ;

if nargin == 5
    alpha = varargin{1} ;
else
    alpha = 0.5 ;
end

%* A2(a1,a4,a5,a2,m2)*sqrt(L4(a4))
for a4 = 1 : d4 
    A2(:, a4, :, :, :) = A2(:, a4, :, :, :) * L4(a4).^alpha ;
end

%* A2(a1,a4,a5,a2,m2)*sqrt(L5(a5))
for a5 = 1 : d5
    A2(:, :, a5, :, :) = A2(:, :, a5, :, :) * L5(a5).^alpha ;
end

%* A2(a1,a4,a5,a2,m2)*sqrt(L2(a2))
for a2 = 1 : d2
    A2(:, :, :, a2, :) = A2(:, :, :, a2, :) * L2(a2).^alpha ;
end

%* A2(a1,a4,a5,a2,m2) -> A2(a4,a5,a2,m2,a1)
A2 = permute(A2, [2, 3, 4, 5, 1]) ;

%* A2(a4,a5,a2,m2,a1) -> M2((a4,a5,a2,m2),a1)
M2 = reshape(A2, [d4 * d5 * d2 * dm, d1]) ;