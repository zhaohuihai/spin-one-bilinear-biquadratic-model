function VL = computeVL(wave, T1)
%* VL(z3,z3',x1,x1',z5,z5')

alpha = wave.alpha ;
%--------------------------------------
%* A7(x4,y,z,w,m) = A7(x4,y,z,w,m)*sqrt(L2(y))
A7 = computeA_sqrtLy(wave.A{1}, wave.Lambda{2}, alpha) ;

%* A7(x4,y,z,w,m) = A7(x4,y,z,w,m)*sqrt(L3(z))
A7 = computeA_sqrtLz(A7, wave.Lambda{3}, alpha) ;

%* AA7(x4,w,x4',w') = sum{y,z,m}_[A7(x4,y,z,w,m)*A7(x4',y,z,w',m)]
AA7 = contractTensors(A7, 5, [2, 3, 5], A7, 5, [2, 3, 5]) ;

%--------------------------------------
%* A8(x,y,z1,w,m) = A8(x,y,z1,w,m)*sqrt(L1(x))
A8 = computeA_sqrtLx(wave.A{2}, wave.Lambda{1}, alpha) ;

%* AA8(y,z1,w,y',z1',w') = sum{x,m}_[A8(x,y,z1,w,m)*A8(x,y',z1',w',m)]
AA8 = contractTensors(A8, 5, [1, 5], A8, 5, [1, 5]) ;
%--------------------------------------
%* A9(x6,y,z,w,m) = A9(x6,y,z,w,m)*sqrt(L3(z))
A9 = computeA_sqrtLz(wave.A{1}, wave.Lambda{3}, alpha) ;

%* A9(x6,y,z,w,m) = A9(x6,y,z,w,m)*sqrt(L4(w))
A9 = computeA_sqrtLw(A9, wave.Lambda{4}, alpha) ;

%* AA9(x6,y,x6',y') = sum{z,w,m}_[A9(x6,y,z,w,m)*A9(x6',y',z,w,m)]
AA9 = contractTensors(A9, 5, [3, 4, 5], A9, 5, [3, 4, 5]) ;
%--------------------------------------------------------
%* A4(x4,y1,z3,w,m) = A4(x4,y1,z3,w,m)*sqrt(L4(w))
A4 = computeA_sqrtLw(wave.A{2}, wave.Lambda{4}, alpha) ;

%* AA4(x4,y1,z3,x4',y1',z3') = sum{w,m}_[A4(x4,y1,z3,w,m)*A4(x4',y1',z3',w,m)]
AA4 = contractTensors(A4, 5, [4, 5], A4, 5, [4, 5]) ;
%--------------------------------------------------------
%* TT1(x1,y1,z1,w1,x1',y1',z1',w1') = sum{m}_[T1(x1,y1,z1,w1,m)*T1(x1',y1',z1',w1',m)]
TT1 = contractTensors(T1, 5, 5, T1, 5, 5) ;
%----------------------------------------------------------------------------------------
%* A6(x6,y,z5,w1,m) = A6(x6,y,z5,w1,m)*sqrt(L2(y))
A6 = computeA_sqrtLy(wave.A{2}, wave.Lambda{2}, alpha) ;

%* AA6(x6,z5,w1,x6',z5',w1') = sum{y,m}_[A6(x6,y,z5,w1,m)*A6(x6',y,z5',w1',m)]
AA6 = contractTensors(A6, 5, [2, 5], A6, 5, [2, 5]) ;
%-----------------------------------------------------------------------
%* VL(x4,x4',y,z1,y',z1') = sum{w,w'}_[AA7(x4,w,x4',w')*AA8(y,z1,w,y',z1',w')]
VL = contractTensors(AA7, 4, [2, 4], AA8, 6, [3, 6]) ;

%* VL(x4,x4',z1,z1',x6,x6') = sum{y,y'}_[VL(x4,x4',y,z1,y',z1')*AA9(x6,y,x6',y')]
VL = contractTensors(VL, 6, [3, 5], AA9, 4, [2, 4]) ;

%* VL(y1,z3,y1',z3',z1,z1',x6,x6') = sum{x4,x4'}_[AA4(x4,y1,z3,x4',y1',z3')*VL(x4,x4',z1,z1',x6,x6')]
VL = contractTensors(AA4, 6, [1, 4], VL, 6, [1, 2]) ;

%VL(z3,z3',x6,x6',x1,w1,x1',w1') = sum{y1,y1',z1,z1'}_[VL(y1,z3,y1',z3',z1,z1',x6,x6')*TT1(x1,y1,z1,w1,x1',y1',z1',w1')]
VL = contractTensors(VL, 8, [1, 3, 5, 6], TT1, 8, [2, 6, 3, 7]) ;

%* VL(z3,z3',x1,x1',z5,z5') = sum{x6,x6',w1,w1'}_[VL(z3,z3',x6,x6',x1,w1,x1',w1')*AA6(x6,z5,w1,x6',z5',w1')]
VL = contractTensors(VL, 8, [3, 4, 6, 8], AA6, 6, [1, 4, 3, 6]) ;
