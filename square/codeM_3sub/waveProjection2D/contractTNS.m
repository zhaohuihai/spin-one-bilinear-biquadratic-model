function [bottomMPS, topMPS, VR, VL, eta] = contractTNS(parameter, wave)
%* VR(c1,c1',z2,z2',c2,c2') VL(a1,a1',z1,z1',a2,a2')

A = wave.A ;

%* A{1}(x,y,z1,w,n1) -> A{1}(w,z1,x,y,n1)
A{1} = permute(A{1}, [4, 3, 1, 2, 5]) ;
%* A{2}(x,y,z2,w,n2) -> A{2}(y,x,z2,w,n2)
A{2} = permute(A{2}, [2, 1, 3, 4, 5]) ;
%*
Lx = wave.Lambda{1} ;
Ly = wave.Lambda{2} ;
Lz = wave.Lambda{3} ;
Lw = wave.Lambda{4} ;

%* bottomMPS A(a,m1,b,w) B(b,m2,c,y)
%* topMPS A(a',m1',b',y) B(b',m2',c',w)
[bottomMPS, topMPS] = findDominantEigenMPS(parameter, A, Lx, Ly, Lz, Lw) ;
%-----------------------------------------------------------------------------

%* VR((c1,c1'),(z2,z2'),(c2,c2'))
%* VL((a1,a1'),(z1,z1'),(a2,a2'))
[VR, ~, VL, ~, eta] = findDominantEigen0D(parameter, A, Lx, Lz, bottomMPS, topMPS) ;
%------------------------------------------------------------------------------
Chi = size(bottomMPS.A, 1) ;
D = parameter.bondDimension ;

%* VR(c1,c1',z2,z2',c2,c2')
VR = reshape(VR, [Chi, Chi, D, D, Chi, Chi]) ;
%* VL(a1,a1',z1,z1',a2,a2')
VL = reshape(VL, [Chi, Chi, D, D, Chi, Chi]) ;