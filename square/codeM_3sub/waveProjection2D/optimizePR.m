function PR1 = optimizePR(Me, PL)
%* Me(x1,x1',x2,x2')
%* PR(x1,x) PL(x2,x)

[dxp, dx] = size(PL) ;

N = zeros(dxp, dxp) ;
%* N(x1,x2) = sum{x'}_[Me(x1,x',x2,x')]
for x = 1 : dxp
    
    N = N + squeeze(Me(:, x, :, x)) ;
    
end

%* N(x1,x) = sum{x2}_[N(x1,x2)*PL(x2,x)]
N = contractTensors(N, 2, 2, PL, 2, 1) ;

N = reshape(N, [dxp * dx, 1]) ;
%----------------------------------------------
%* M(x1,x1',x2,x') = sum{x2'}_[Me(x1,x1',x2,x2')*PL(x2',x')]
M = contractTensors(Me, 4, 4, PL, 2, 1) ;

%* M(x1,x1',x',x) = sum{x2}_[M(x1,x1',x2,x')*PL(x2,x)]
M = contractTensors(M, 4, 3, PL, 2, 1) ;

%* M(x1,x1',x',x) -> M(x1,x,x1',x')
M = permute(M, [1, 4, 2, 3]) ;

%* M(x1,x,x1',x') -> M((x1,x),(x1',x'))
M = reshape(M, [dxp * dx, dxp * dx]) ;
% M = (M + M') / 2 ;

%* M((x1,x),(x1',x')) = sum{l}_[U((x1,x),l)*S(l)*V((x1',x'),l)]
[U, S, V] = svd(M) ;
S = diag(S) ;
S = S ./ S(1) ;

S_tol = 1e-12 ;

U = U(:, (S > S_tol)) ;
V = V(:, (S > S_tol)) ;
S = S(S > S_tol) ;

invS = 1 ./ S ;
invS = diag(invS) ;

invM = V * invS ;
invM = invM * U' ;

PR1 = invM * N ;

PR1 = reshape(PR1, [dxp, dx]) ;

PR1 = PR1 ./ norm(PR1) ;
