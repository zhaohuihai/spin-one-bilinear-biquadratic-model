function PL1 = optimizePL(Me, PR)
%* PR(x',x) PL(x",x)

% Me(x',x*',x",x*") -> Me(x",x*",x',x*')
Me = permute(Me, [3, 4, 1, 2]) ;

PL1 = optimizePR(Me, PR) ;