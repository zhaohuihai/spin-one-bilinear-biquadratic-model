function wave = rotateWave231(wave)
% A(1,2,3) -> A(2,3,1)
%* Lambda(1,2,3,4,5,6) -> Lambda(2,3,1,5,6,4)


% A(1,2,3) -> A(2,3,1)
wave.A = wave.A([2, 3, 1]) ;
% 
wave.Lambda = wave.Lambda([2, 3, 1, 5, 6, 4]) ;