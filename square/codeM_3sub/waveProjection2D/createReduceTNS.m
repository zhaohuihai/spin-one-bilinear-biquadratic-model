function AA = createReduceTNS(A)
%* AA((w,w'),(z,z'),(x,x'),(y,y')) = sum{n1}_[A(w,z,x,y,n1) * A(w',z',x',y',n1)]

[D, ~, ~, ~, ~] = size(A) ;

%* AA(w,z,x,y,w',z',x',y') = sum{n1}_[A(w,z,x,y,n1) * A(w',z',x',y',n1)]
AA = contractTensors(A, 5, 5, A, 5, 5) ;

%* AA(w,z,x,y,w',z',x',y') -> AA(w,w',z,z',x,x',y,y')
AA = permute(AA, [1, 5, 2, 6, 3, 7, 4, 8]) ;

%* AA(w,w',z,z',x,x',y,y') -> AA((w,w'),(z,z'),(x,x'),(y,y'))
AA = reshape(AA, [D^2, D^2, D^2, D^2]) ;