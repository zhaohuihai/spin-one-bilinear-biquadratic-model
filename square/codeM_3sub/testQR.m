%* test QR speed

parameter.tol_power = 1e-10 ;
parameter.maxPowerStep = 1e3 ;

dim = 3 ;
P = rand(dim) + 1i * rand(dim) ;
P = P + P' ;

[V, D] = eig(P) ;
diag(D)

vector = rand(dim, 1) + 1i * rand(dim, 1) ;

[vectorR, etaR] = applyPowerMethod_1site(parameter, P, vector) ;

PL = P.' ;

[vectorL, etaL] = applyPowerMethod_1site(parameter, PL, vector) ;



eta = vectorL.' * (P * vectorR) / (vectorL.' * vectorR) ;

real(eta) - etaL