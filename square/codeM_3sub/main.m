%* This is main script.
clear
format long
restoredefaultpath
createPath

parameter = setParameter() ;

if parameter.loadPreviousWave == 0
    wave = initializeWave(parameter) ;
    disp('create new wave')
else
    [wave, parameter] = loadWave(parameter) ;
end

if parameter.projection == 1
    disp('apply first order Trotter decomposition')
    
    wave = findGroundStateWave(parameter, wave) ;
end

if parameter.computation.expectationValue == 1
    iTEBD_findExpectValue(parameter, wave) ;
end
