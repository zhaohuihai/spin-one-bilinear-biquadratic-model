function parameter = defineModelParameter(parameter)
%* Bilinear biquadratic model parameters
parameter.siteDimension = 3 ;

parameter.theta = (0.26) * pi ;

parameter.thetaInitial = (0.0) *pi ;
parameter.thetaFinal = (0.45) *pi ;
parameter.thetaIncre = (0.05) *pi ;

if parameter.theta <= (-pi) || parameter.theta > (pi)
    error('the value of theta is out of parameter range') ;
end