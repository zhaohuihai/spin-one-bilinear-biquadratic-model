function parameter = defineExpectValueParameter(parameter)

%* dimension of MPS
parameter.expectValue.dim_MPS = 40 ;
parameter.expectValue.dim_MPS_initial = 10 ;
parameter.expectValue.dim_MPS_incre = 10 ;
%------------------------------------------------
parameter.expectValue.tol_iTEBD = 1e-8 ;
parameter.expectValue.tol_power = 1e-8 ;

parameter.expectValue.maxITEBDstep = 1e2 ;
parameter.expectValue.maxPowerStep = 1e2 ;
